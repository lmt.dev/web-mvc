﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Data;
using System.Data.SqlClient;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Net;
using System.Net.Mail;
using System.Web;


namespace Coincap
{
    public partial class Form1 : Form
    {
        ChromeDriver chromedri = new ChromeDriver();
        string conn = "Data Source=localhost\\LucasBD;Initial Catalog=Criptosignal;Persist Security Info=True;User ID=sa;Password=ProtocoloZer0";

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            InicializarNavegador();
        }

        public static void SendEmail(string To, string Subject, string Body)
        {
            System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
            mail.From = new MailAddress("innovisstechnology@gmail.com");
            mail.To.Add(To);
            mail.Subject = Subject;
            mail.Body = Body;
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 25; //465; //587
            smtp.Credentials = new NetworkCredential("innovisstechnology@gmail.com", "experiencia");
            smtp.EnableSsl = true;
            try
            {
                smtp.Send(mail);
            }
            catch (Exception ex)
            {
                throw new Exception("No se ha podido enviar el email", 
                    ex.InnerException);
            }
            finally
            {
                smtp.Dispose();
            }

        }

        public void InicializarNavegador()
        {
            chromedri.Url = "https://es.investing.com/crypto/currencies";
            chromedri.Navigate();
            IniciarProceso();
        }


        public void IniciarProceso()
        {
            timer1.Start();
        }

        public void LecturaArchivo()
        {
            try
            {
                chromedri.Url = "https://es.investing.com/crypto/currencies";
                chromedri.Navigate();
                lblMensaje.Text = "";

                Application.DoEvents();
                DateTime vHilo = DateTime.Now;
                StreamReader sr = new StreamReader("investing.text");
                string line = "";
                bool primeraLinea = false;
                List<SqlCommand> listaComandos = new List<SqlCommand>();
                while ((line = sr.ReadLine()) != null)
                {
                    if (primeraLinea == true)
                    {
                        string[] words = line.Split(' ');

                        string vId = words[0];
                        decimal vPrecio = -1;
                        string vSimbolo = "";
                        string vNombre = "";
                        string vCapital = "";
                        string vAcciones = "";
                        string vVolumen = "";

                        int vInicioBusqueda = 3;
                        while (vPrecio == -1)
                        {
                            try
                            {
                                vPrecio = Convert.ToDecimal(words[vInicioBusqueda]);
                                vSimbolo = words[(vInicioBusqueda - 1)];
                                vCapital = words[(vInicioBusqueda + 1)];
                                vAcciones = words[(vInicioBusqueda + 3)];
                                vVolumen = words[(vInicioBusqueda + 5)];

                                int vInicioInverso = vInicioBusqueda - 2;
                                int vInicioInverso1 = 1;
                                while (vInicioInverso1 <= vInicioInverso)
                                {
                                    vNombre = vNombre + " " + words[(vInicioInverso1)];
                                    vInicioInverso1++;
                                }
                            }
                            catch
                            {
                                vInicioBusqueda++;
                            }
                        }
                        //InsertarTransaccion(vNombre.Trim(), vSimbolo, vPrecio, vCapital, vHilo);
                        SqlCommand cmd = new SqlCommand();
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "PRC_ProcesaCriptodivisa";
                        cmd.Parameters.Add(new SqlParameter("@pNombre", SqlDbType.VarChar, 50)); cmd.Parameters["@pNombre"].Value = vNombre.Trim();
                        cmd.Parameters.Add(new SqlParameter("@pSimbolo", SqlDbType.VarChar, 50)); cmd.Parameters["@pSimbolo"].Value = vSimbolo;
                        cmd.Parameters.Add(new SqlParameter("@pPrecio", SqlDbType.Decimal)); cmd.Parameters["@pPrecio"].Value = vPrecio;
                        cmd.Parameters.Add(new SqlParameter("@pCapital", SqlDbType.VarChar, 50)); cmd.Parameters["@pCapital"].Value = vCapital;
                        cmd.Parameters.Add(new SqlParameter("@pAcciones", SqlDbType.VarChar, 50)); cmd.Parameters["@pAcciones"].Value = vAcciones;
                        cmd.Parameters.Add(new SqlParameter("@pVolumen", SqlDbType.VarChar, 50)); cmd.Parameters["@pVolumen"].Value = vVolumen;
                        cmd.Parameters.Add(new SqlParameter("@pHilo", SqlDbType.VarChar, 50)); cmd.Parameters["@pHilo"].Value = vHilo;
                        listaComandos.Add(cmd);
                        lblMensaje.Text = vId.ToString() + " " + vNombre + " configurando comandos de base de datos...";

                        System.GC.Collect();
                        Application.DoEvents();
                    }
                    else
                    {
                        primeraLinea = true;
                    }
                }
                sr.Close();
                sr.Dispose();

                using (SqlConnection connection = new SqlConnection(conn))
                {
                    connection.Open();
                    SqlTransaction transaction;
                    transaction = connection.BeginTransaction("Criptosignal");
                    foreach (SqlCommand comandoEjecutar in listaComandos)
                    {
                        comandoEjecutar.Connection = connection;
                        comandoEjecutar.Transaction = transaction;
                        comandoEjecutar.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }
                lblMensaje.Text = "Lectura finalizada!";
            }
            catch(Exception ex)
            {
                lblMensaje.Text = ex.Message;
                //MessageBox.Show(ex.StackTrace, ex.Message);
            }
        }

        public void GenerarArchivo()
        {
            try
            {
                lblMensaje.Text = "Generando archivo";
                IWebElement investing = chromedri.FindElement(By.XPath("//section[@id='fullColumn']//table[1]"));
                StreamWriter sw = new StreamWriter("investing.text");
                sw.Write(investing.Text);
                sw.Dispose();
                sw.Close();
                lblMensaje.Text = "Archivo generado";
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Error en generación de archivo: " + ex.Message;
            }
        }

        private void btnProcesar_Click(object sender, EventArgs e)
        {
            GenerarArchivo();
        }
        string vHilo = DateTime.Now.ToString();

        public bool esIndice(string valor)
        {
            try
            {
                int valorEntero = Int32.Parse(valor);
                if (valorEntero > 0 && valorEntero < 999)
                {
                    return true;
                }
                else
                {
                    return false;
                }                
            }
            catch
            {
                return false;
            }
        }

        public void InsertarTransaccion(
            string pNombre,
            string pSimbolo,
            decimal pPrecio,
            string pCapital,
            DateTime pHilo)
        {          
            try
            {
                SqlCommand cmd = new SqlCommand();
                //cmd.Connection = sqlConnectionGeneral;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PRC_ProcesaCriptodivisa";

                cmd.Parameters.Add(new SqlParameter("@pNombre", SqlDbType.VarChar, 50)); cmd.Parameters["@pNombre"].Value = pNombre;

                cmd.Parameters.Add(new SqlParameter("@pSimbolo", SqlDbType.VarChar, 50)); cmd.Parameters["@pSimbolo"].Value = pSimbolo    ;

                cmd.Parameters.Add(new SqlParameter("@pPrecio", SqlDbType.Decimal)); cmd.Parameters["@pPrecio"].Value = pPrecio;

                cmd.Parameters.Add(new SqlParameter("@pCapital", SqlDbType.VarChar, 50)); cmd.Parameters["@pCapital"].Value = pCapital;

                cmd.Parameters.Add(new SqlParameter("@pHilo", SqlDbType.VarChar, 50)); cmd.Parameters["@pHilo"].Value = pHilo;

                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
            }
            catch (SqlException ex)
            {
                lblMensaje.Text = "Error insertando transaccion: " + ex.Message;
                Application.DoEvents();
            }
        }

        public void ReprocesarStringCapital()
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                //cmd.Connection = sqlConnectionGeneral;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "proc_sp_ReprocesarStringCapital";

                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
            }
            catch (SqlException ex)
            {
                lblMensaje.Text = "Error insertando transaccion: " + ex.Message;
                Application.DoEvents();
            }
        }

        private void btnLeer_Click(object sender, EventArgs e)
        {
            LecturaArchivo();
        }

        int vAccion = 0;

        public void ProcesarAccion()
        {
            if (vAccion == 0)
            {
                GenerarArchivo();
                vAccion = 1;
            }
            else
            {
                LecturaArchivo();
                vAccion = 0;
            }
            GC.Collect();
            Application.DoEvents();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            ProcesarAccion();
        }        

        private void btnIniciar_Click(object sender, EventArgs e)
        {
            IniciarProceso();
        }
    }
}
