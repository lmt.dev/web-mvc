﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelo;
using Controlador;

namespace Presentacion
{
    public partial class Registro : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        public string CuerpoEmailBienvenida(string clave)
        {
            string vCuerpo = 
                "Bienvenido al proyecto CriptoSignal!"+Environment.NewLine
                +"Deseamos que encuentres una experiencia genial en nuestro sitio. Todas nuestras funcionalidades son de uso 100% gratuito, aunque puedes colaborar en cualquier momento con una donación voluntaria. Por cualquier consulta no dudes en contactarte a través de éste email, estamos atentos a tus sugerencias."
                + Environment.NewLine
                + Environment.NewLine
                +"Recibirás una notificación cuando renovemos nuestro dominio web, y podrás seguir utilizando las mismas credenciales. Estamos agregando funcionalidades a medida que recibimos colaboraciones de parte de nuestra comunidad."
                + Environment.NewLine
                + Environment.NewLine
                + "Por cierto, necesitarás la siguiente clave de activación para comenzar a utilizar tu cuenta: '"+clave+"'"
                + Environment.NewLine
                + Environment.NewLine
                +"Exitos!"+Environment.NewLine
                +"Equipo CriptoSignal"
                ;
            return vCuerpo;
        }

        protected void btnRegistro_Click(object sender, EventArgs e)
        {
            try
            {
                lblMensaje.Text = "";
                if (txtContraseña.Text != txtConfirmarContraseña.Text)
                {
                    lblMensaje.Text = "Los campos 'Contraseña' y 'Confirmar contraseña' deben ser iguales";
                }
                else
                {
                    Usuario userNew = new Usuario();
                    userNew.NombreUsuario = txtUsuarioUnico.Text;
                    userNew.EmailUsuario = txtEmail.Text;
                    userNew.PasswordUsuario = txtContraseña.Text;
                    UsuarioController userController = new UsuarioController();
                    int vIdUsuario = userController.RegistrarUsuario(userNew, GetIP());
                    if (vIdUsuario > 0)
                    {
                        Email emailRegistro = new Email();
                        EmailController emailC = new EmailController();
                        emailRegistro.asuntoEmail = "Bienvenido a CriptoSignal";
                        emailRegistro.cuerpoEmail = CuerpoEmailBienvenida("NoSeAdmitenHomeros");
                        emailRegistro.destinatarioEmail = userNew.EmailUsuario;
                        emailC.Enviar(emailRegistro);

                        //Inhabilitamos controles de registro
                        txtUsuarioUnico.Visible = false;
                        txtEmail.Visible = false;
                        txtContraseña.Visible = false;
                        txtConfirmarContraseña.Visible = false;
                        btnRegistro.Visible = false;
                        btnLogin.Visible = false;
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "text", "alert('Genial! Solo falta un paso, ingrese la clave de activación que le hemos enviado a su email')", true);
                        //Habilitamos controles de activación
                        txtClaveActivacion.Visible = true;
                        btnActivacion.Visible = true;
                        //
                    }
                    else
                    {
                        lblMensaje.Text = "Las credenciales ya corresponden a otro usuario, por favor intente nuevamente";
                    }
                }
            }
            catch
            {
                lblMensaje.Text = "Vaya, ocurrió algo inesperado... por favor intente nuevamente";
            }
        }
        protected String GetIP()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }
            return context.Request.ServerVariables["REMOTE_ADDR"];
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            Response.Redirect("Login.aspx");
        }
        protected void btnActivacion_Click(object sender, EventArgs e)
        {
            if (txtClaveActivacion.Text == "NoSeAdmitenHomeros")
            {
                Usuario userNew = new Usuario();
                userNew.NombreUsuario = txtUsuarioUnico.Text;
                userNew.EmailUsuario = txtEmail.Text;
                userNew.PasswordUsuario = txtContraseña.Text;
                UsuarioController userController = new UsuarioController();
                int vIdUsuario = userController.ActivarUsuario(userNew, GetIP());
                Response.Redirect("Login.aspx");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "text", "alert('reintente nuevamente')", true);
            }
            
        }
    }
}