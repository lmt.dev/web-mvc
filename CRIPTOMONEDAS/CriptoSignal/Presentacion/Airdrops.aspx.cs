﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelo;
using Controlador;

namespace Presentacion
{
    public partial class Airdrops : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AirdropController airdropController = new AirdropController();
            airdropController.ListarAirdrop(getIdUsuarioNormalizado(),GetIP());
        }

        private string getIdUsuarioNormalizado()
        {
            if (Session["CodigoUsuario"] == null)
            {
                return "0";
            }
            else
            {
                return Session["CodigoUsuario"].ToString();
            }
        }

        protected String GetIP()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }
            return context.Request.ServerVariables["REMOTE_ADDR"];
        }
    }
}