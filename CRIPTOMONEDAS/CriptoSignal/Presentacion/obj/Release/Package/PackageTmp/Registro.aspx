﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Principal.Master" AutoEventWireup="true" CodeBehind="Registro.aspx.cs" Inherits="Presentacion.Registro" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script language="javascript">
        
        function openModalActivarCuenta()
        {
            $("#myModalActivarCuenta").modal()
        }

    </script>

    <form id="form1" runat="server">
        <div class="row">
            <section class="wrapper">
                  <div class="login-wrap">

                    <div class="input-group">
                      <span class="input-group-addon"><i class="icon_profile"></i></span>
                      <asp:TextBox ID="txtUsuarioUnico" runat="server" MaxLength="50" class="form-control" placeholder="Nombre de usuario único" autofocus></asp:TextBox>
                    </div>

                    <div class="input-group">
                      <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtUsuarioUnico" ErrorMessage="RequiredFieldValidator" ForeColor="Red" ValidationGroup="ValidacionesForm">El campo 'Nombre de usuario único' no puede ser vacío</asp:RequiredFieldValidator>
                    </div>

                    <div class="input-group">
                      <span class="input-group-addon"><i class="icon_profile"></i></span>
                      <asp:TextBox ID="txtEmail" runat="server" MaxLength="50" class="form-control" placeholder="Email"></asp:TextBox>
                    </div>

                    <div class="input-group">
                      <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEmail" ErrorMessage="RequiredFieldValidator" ForeColor="Red" ValidationGroup="ValidacionesForm">El campo 'Nombre de usuario único' no puede ser vacío</asp:RequiredFieldValidator>
                    </div>

                    <div class="input-group">
                      <span class="input-group-addon"><i class="icon_key_alt"></i></span>
                      <asp:TextBox ID="txtContraseña" type="password" runat="server" MaxLength="50" class="form-control" placeholder="Contraseña"></asp:TextBox>
                    </div>

                    <div class="input-group">
                      <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtContraseña" ErrorMessage="RequiredFieldValidator" ForeColor="Red" ValidationGroup="ValidacionesForm">El campo 'Contraseña' no puede ser vacío</asp:RequiredFieldValidator>
                    </div>

                    <div class="input-group">
                      <span class="input-group-addon"><i class="icon_key_alt"></i></span>
                      <asp:TextBox ID="txtConfirmarContraseña" type="password" runat="server" MaxLength="50" class="form-control" placeholder="Confirmar contraseña"></asp:TextBox>
                    </div>

                    <div class="input-group">
                      <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtConfirmarContraseña" ErrorMessage="RequiredFieldValidator" ForeColor="Red" ValidationGroup="ValidacionesForm">El campo 'Confirmar contraseña' no puede ser vacío</asp:RequiredFieldValidator>
                    </div>

                    <p></p>

                    <asp:Button ID="btnRegistro" 
                        runat="server"
                        Text="Registrarme" 
                        class="btn btn-primary btn-lg btn-block"
                        ValidationGroup="ValidacionesForm" 
                        OnClick="btnRegistro_Click" />

                    <asp:Button ID="btnLogin" 
                        runat="server"
                        Text="¿Ya tiene una cuenta? Iniciar sesión" 
                        class="btn btn-info btn-lg btn-block"
                        OnClick="btnLogin_Click" />
                    
                    <div class="input-group">
                      <span class="input-group-addon"><i class="icon_key_alt"></i></span>
                      <asp:TextBox ID="txtClaveActivacion"
                          runat="server" 
                          MaxLength="50" 
                          class="form-control" 
                          Visible ="false"
                          placeholder="Ingrese su clave de activación recibida via email"></asp:TextBox>
                    </div>

                    <asp:Button ID="btnActivacion" 
                        runat="server"
                        Text="Activar mi cuenta!" 
                        class="btn btn-primary btn-lg btn-block"
                        ValidationGroup="ValidacionesForm"
                        Visible="false"
                        OnClick="btnActivacion_Click" />

                    <div class="input-group">
                        <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                    </div>
                  </div>

            </section>
        </div>
    </form>
</asp:Content>
