﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Principal.Master" AutoEventWireup="true" CodeBehind="Airdrops.aspx.cs" Inherits="Presentacion.Airdrops" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <section class="wrapper">
            <div class="col-lg-9 col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                    <h2><i class="fa fa-flag-o red"></i>
                        <strong>AIRDROPS ACTIVAS. 100% Verificadas</strong></h2>
                    </div>
                    <div class="panel-body">
                        <p></p>
                        <p>Airdrops de suscripcion con requerimiento de Email:</p>

                        <a href="https://www.mannabase.com/?ref=d06811f3fc" target="_blank">MANNA (LA MÁS RECOMENDADA DEL MOMENTO)</a>
                        <p></p>

                        <a href="https://leonardian.com/ref/709f0d11ebd9f88fd40177a984c1900e" target="_blank">LEONARDIAN</a>
                        <p></p>

                        <a href="http://zipansion.com/1heYm" target="_blank">ALLSPORTER COIN</a>
                        <p></p>
                        <p></p>
                        <p>Airdrops de suscripcion con requerimiento de Telegram:</p>
                        <a href="http://zipansion.com/1hdlL" target="_blank">MIBOODLE</a>
                        <p></p>
                            
                        <a href="http://zipansion.com/17562753/monopoly-airdrop" target="_blank">MONOPOLY</a>
                        <p></p>

                        <a href="http://zipansion.com/1heBI" target="_blank">BARREL</a>
                        <p></p>

                        <a href="http://zipansion.com/1hejO" target="_blank">TERAWATT</a>
                        <p></p>

                        <a href="http://zipansion.com/1hepS" target="_blank">0XCERT</a>
                        <p></p>

                        <a href="http://zipansion.com/1herp" target="_blank">COGNITO</a>
                        <p></p>
                
                        <a href="http://zipansion.com/1hfGu" target="_blank">BITNEY</a>
                        <p></p>

                        <a href="http://zipansion.com/1hfwg" target="_blank">AMO</a>
                        <p></p>

                        <a href="http://zipansion.com/1hg8n" target="_blank">NAVIADRESS</a>
                        <p></p>

                        <p></p>
                        <p>Airdrops de suscripcion con requerimiento de Telegram, Twitter, Facebook y Reddit:</p>
                        <a href="http://zipansion.com/1hg4b" target="_blank">SWACHHCOIN</a>
                        <p></p>
                        <p></p>
                        <p>Todas nuestras funcionalidades son gratuitas, aunque puedes realizar una donación para ayudarnos a ofrecer un mejor servicio.</p>
                        <p>Más info sobre nuestros proyectos en CriptoSignalWeb@gmail.com</p>
                        <button type="button" onclick="openModalDonacionesTransferencias()" data-dismiss="modal" class="btn btn-default">Realizar donaciones</button>
                    </div>
                </div>
            </div>
        </section>
    </div>
</asp:Content>
