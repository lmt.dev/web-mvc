﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelo;
using Controlador;

namespace Presentacion
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btnRegistro_Click(object sender, EventArgs e)
        {
            Response.Redirect("Registro.aspx");
        }
        protected String GetIP()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }
            return context.Request.ServerVariables["REMOTE_ADDR"];
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            Usuario usuarioRegistro = new Usuario();
            usuarioRegistro.EmailUsuario = txtEmail.Text;
            usuarioRegistro.PasswordUsuario = txtContraseña.Text;
            UsuarioController usuarioNegocio = new UsuarioController();
            int vIdResultado = usuarioNegocio.LoginUsuario(usuarioRegistro, GetIP());
            if (vIdResultado > 0)
            {
                Session.Clear();
                Session["CodigoUsuario"] = vIdResultado.ToString();
                Session["accesoPng"] = "registrado";
                Session["usuarioNombre"] = usuarioRegistro.NombreUsuario;
                Response.Redirect("Inicio.aspx");
            }
            else
            {
                lblMensaje.Text = "Email y/o Password no válido.";
            }
        }
    }
}