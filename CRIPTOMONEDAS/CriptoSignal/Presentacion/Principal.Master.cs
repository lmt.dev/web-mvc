﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentacion
{
    public partial class Principal : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GetUser();
        }
        Modelo.Usuario userSession = new Modelo.Usuario();
        Controlador.UsuarioController userController = new Controlador.UsuarioController(); 
        protected void GetUser()
        {
            if (Session["CodigoUsuario"] == null)
            {
                Session["accesoPng"] = "anonimous";
                Session["usuarioNombre"] = "Usuario anónimo";
            }
            else
            {
                userSession = userController.ConsultarUsuario(Session["CodigoUsuario"].ToString());
                Session["accesoPng"] = "registrado";
                Session["usuarioNombre"] = userSession.NombreUsuario;
            }
        }
    }
}