﻿using System;
using System.Data;
using Modelo;
using System.Data.SqlClient;
namespace Datos
{
    public class UsuarioDatos
    {
        SqlConnection cnx;
        Usuario UsuarioEntidad = new Usuario();
        Conexion MiConexi = new Conexion();
        SqlCommand cmd = new SqlCommand();
        bool vexito;
        public UsuarioDatos()
        {
            cnx = new SqlConnection(MiConexi.GetConex());
        }

        public bool CargarUsuario(Usuario UsuarioEntidad)
        {
            cmd.Connection = cnx;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Proc_CargarUsuario";
            try
            {
                cmd.Parameters.Add(new SqlParameter("@p_IdUsuario", SqlDbType.Int));
                cmd.Parameters["@p_IdUsuario"].Value = UsuarioEntidad.IdUsuario;

                cmd.Parameters.Add(new SqlParameter("@p_Nombre", SqlDbType.VarChar));
                cmd.Parameters["@p_Nombre"].Value = UsuarioEntidad.NombreUsuario;


                cnx.Open();
                cmd.ExecuteNonQuery();
                vexito = true;
            }
            catch (SqlException)
            {
                vexito = false;
            }
            finally
            {
                if (cnx.State == ConnectionState.Open)
                {
                    cnx.Close();
                }
                cmd.Parameters.Clear();
            }
            return vexito;
        }

        public bool EliminarUsuario(Usuario UsuarioEntidad)
        {
            cmd.Connection = cnx;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "proc_UsuarioEliminar";
            try
            {
                cmd.Parameters.Add(new SqlParameter("@codigo", SqlDbType.VarChar, 5));
                cmd.Parameters["@codigo"].Value = UsuarioEntidad.IdUsuario;
                cnx.Open();
                cmd.ExecuteNonQuery();
                vexito = true;
            }
            catch (SqlException)
            {
                vexito = false;
            }
            finally
            {
                if (cnx.State == ConnectionState.Open)
                {
                    cnx.Close();
                }
                cmd.Parameters.Clear();
            }
            return vexito;
        }

        public DataTable ListarUsuarios(string p_NombreUsuario)
        {
            DataSet dts = new DataSet();
            try
            {
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "Proc_ListarUsuarios";
                cmd.Parameters.Add(new SqlParameter("@p_Nombre", SqlDbType.VarChar));
                cmd.Parameters["@p_Nombre"].Value = p_NombreUsuario;
                SqlDataAdapter miada;
                miada = new SqlDataAdapter(cmd);
                miada.Fill(dts, "Usuarios");
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmd.Parameters.Clear();
            }
            return (dts.Tables["Usuarios"]);
        }

        public int RegistrarUsuario(Usuario usuarioRegistro, string pIp)
        {
            try
            {
                int vRetorno = 0;
                SqlDataReader dtr;
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "Proc_RegistroUsuario";

                cmd.Parameters.Add(new SqlParameter("@p_Nombre", SqlDbType.VarChar));
                cmd.Parameters["@p_Nombre"].Value = usuarioRegistro.NombreUsuario;

                cmd.Parameters.Add(new SqlParameter("@p_Email", SqlDbType.VarChar));
                cmd.Parameters["@p_Email"].Value = usuarioRegistro.EmailUsuario;

                cmd.Parameters.Add(new SqlParameter("@p_Password", SqlDbType.VarChar));
                cmd.Parameters["@p_Password"].Value = usuarioRegistro.PasswordUsuario;

                cmd.Parameters.Add(new SqlParameter("@pIp", SqlDbType.VarChar));
                cmd.Parameters["@pIp"].Value = pIp;

                if (cnx.State == ConnectionState.Closed)
                {
                    cnx.Open();
                }
                dtr = cmd.ExecuteReader();
                if (dtr.HasRows == true)
                {
                    dtr.Read();
                    vRetorno = Convert.ToInt32(dtr[0]);
                }
                cnx.Close();
                cmd.Parameters.Clear();
                return vRetorno;
            }
            catch (SqlException)
            {
                throw new Exception();
            }
            finally
            {
                if (cnx.State == ConnectionState.Open)
                {
                    cnx.Close();
                }
                cmd.Parameters.Clear();
            }
        }

        public string ActualizarUsuario(Usuario usuarioRegistro)
        {
            try
            {
                string vRetorno = "";
                SqlDataReader dtr;
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "Proc_ActualizarUsuario";

                cmd.Parameters.Add(new SqlParameter("@p_idUsuario", SqlDbType.Int));
                cmd.Parameters["@p_idUsuario"].Value = usuarioRegistro.IdUsuario;

                cmd.Parameters.Add(new SqlParameter("@p_Nombre", SqlDbType.VarChar));
                cmd.Parameters["@p_Nombre"].Value = usuarioRegistro.NombreUsuario;

                cmd.Parameters.Add(new SqlParameter("@p_Email", SqlDbType.VarChar));
                cmd.Parameters["@p_Email"].Value = usuarioRegistro.EmailUsuario;

                cmd.Parameters.Add(new SqlParameter("@p_Password", SqlDbType.VarChar));
                cmd.Parameters["@p_Password"].Value = usuarioRegistro.PasswordUsuario;

                if (cnx.State == ConnectionState.Closed)
                {
                    cnx.Open();
                }
                dtr = cmd.ExecuteReader();
                if (dtr.HasRows == true)
                {
                    dtr.Read();
                    vRetorno = Convert.ToString(dtr[0]);
                }
                cnx.Close();
                cmd.Parameters.Clear();
                return vRetorno;
            }
            catch (SqlException)
            {
                throw new Exception();
            }
            finally
            {
                if (cnx.State == ConnectionState.Open)
                {
                    cnx.Close();
                }
                cmd.Parameters.Clear();
            }
        }

        public int LoginUsuario(Usuario usuarioRegistro, string pIp)
        {
            try
            {
                int vRetorno = 0;
                SqlDataReader dtr;
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "Proc_LoginUsuario";

                cmd.Parameters.Add(new SqlParameter("@p_Email", SqlDbType.VarChar));
                cmd.Parameters["@p_Email"].Value = usuarioRegistro.EmailUsuario;

                cmd.Parameters.Add(new SqlParameter("@p_Password", SqlDbType.VarChar));
                cmd.Parameters["@p_Password"].Value = usuarioRegistro.PasswordUsuario;

                cmd.Parameters.Add(new SqlParameter("@pIp", SqlDbType.VarChar));
                cmd.Parameters["@pIp"].Value = pIp;

                if (cnx.State == ConnectionState.Closed)
                {
                    cnx.Open();
                }
                dtr = cmd.ExecuteReader();
                if (dtr.HasRows == true)
                {
                    dtr.Read();
                    vRetorno = Convert.ToInt32(dtr[0]);
                }
                cnx.Close();
                cmd.Parameters.Clear();
                return vRetorno;
            }
            catch (SqlException)
            {
                throw new Exception();
            }
            finally
            {
                if (cnx.State == ConnectionState.Open)
                {
                    cnx.Close();
                }
                cmd.Parameters.Clear();
            }
        }

        public int ActivarUsuario(Usuario usuarioRegistro, string pIp)
        {
            try
            {
                int vRetorno = 0;
                SqlDataReader dtr;
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PRC_ActivarUsuario";

                cmd.Parameters.Add(new SqlParameter("@p_Email", SqlDbType.VarChar));
                cmd.Parameters["@p_Email"].Value = usuarioRegistro.EmailUsuario;

                cmd.Parameters.Add(new SqlParameter("@pIp", SqlDbType.VarChar));
                cmd.Parameters["@pIp"].Value = pIp;

                if (cnx.State == ConnectionState.Closed)
                {
                    cnx.Open();
                }
                dtr = cmd.ExecuteReader();
                if (dtr.HasRows == true)
                {
                    dtr.Read();
                    vRetorno = Convert.ToInt32(dtr[0]);
                }
                cnx.Close();
                cmd.Parameters.Clear();
                return vRetorno;
            }
            catch (SqlException)
            {
                throw new Exception();
            }
            finally
            {
                if (cnx.State == ConnectionState.Open)
                {
                    cnx.Close();
                }
                cmd.Parameters.Clear();
            }
        }

        public Usuario ConsultarUsuario(string codigo)
        {
            try
            {
                SqlDataReader dtr;
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "Prc_ConsultarUsuario";

                cmd.Parameters.Add(new SqlParameter("@p_idUsuario", SqlDbType.Int));
                cmd.Parameters["@p_idUsuario"].Value = Convert.ToInt32(codigo);

                if (cnx.State == ConnectionState.Closed)
                {
                    cnx.Open();
                }
                dtr = cmd.ExecuteReader();
                if (dtr.HasRows == true)
                {
                    dtr.Read();
                    UsuarioEntidad.IdUsuario = Convert.ToInt32(dtr[0]);
                    UsuarioEntidad.NombreUsuario = Convert.ToString(dtr[1]);
                    UsuarioEntidad.EmailUsuario = Convert.ToString(dtr[2]);
                    UsuarioEntidad.PasswordUsuario = Convert.ToString(dtr[3]);
                }
                cnx.Close();
                cmd.Parameters.Clear();
                return UsuarioEntidad;
            }
            catch (SqlException)
            {
                throw new Exception();
            }
            finally
            {
                if (cnx.State == ConnectionState.Open)
                {
                    cnx.Close();
                }
                cmd.Parameters.Clear();
            }
        }
    }
}