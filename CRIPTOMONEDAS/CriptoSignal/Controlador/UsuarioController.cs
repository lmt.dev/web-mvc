﻿using System.Data;
using Modelo;
using Datos;
namespace Controlador
{
    public class UsuarioController
    {
        UsuarioDatos _UsuarioDatos = new UsuarioDatos();

        public bool CargarUsuario(Usuario UsuarioEntidad)
        {
            return _UsuarioDatos.CargarUsuario(UsuarioEntidad);
        }

        public bool EliminarUsuario(Usuario UsuarioEntidad)
        {
            return _UsuarioDatos.EliminarUsuario(UsuarioEntidad);
        }

        public DataTable ListarUsuarios(string p_nombre)
        {
            return _UsuarioDatos.ListarUsuarios(p_nombre);
        }

        public Usuario ConsultarUsuario(string codigo)
        {
            return _UsuarioDatos.ConsultarUsuario(codigo);
        }

        public int RegistrarUsuario(Usuario usuarioRegistro, string pIp)
        {
            return _UsuarioDatos.RegistrarUsuario(usuarioRegistro, pIp);
        }

        public string ActualizarUsuario(Usuario usuarioRegistro)
        {
            return _UsuarioDatos.ActualizarUsuario(usuarioRegistro);
        }

        public int LoginUsuario(Usuario usuarioRegistro, string pIp)
        {
            return _UsuarioDatos.LoginUsuario(usuarioRegistro, pIp);
        }

        public int ActivarUsuario(Usuario usuarioRegistro, string pIp)
        {
            return _UsuarioDatos.ActivarUsuario(usuarioRegistro, pIp);
        }
    }
}