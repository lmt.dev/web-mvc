﻿using System;
using System.Data;
using CapaEntidad;
using System.Data.SqlClient;
namespace CapaDatos
{
    public class EmpleadoDatos
    {
        SqlConnection cnx;
        Empleado empEntidad = new Empleado();
        Conexion MiConexi = new Conexion();
        SqlCommand cmd = new SqlCommand();
        bool vexito;
        public EmpleadoDatos()
        {
            cnx = new SqlConnection(MiConexi.GetConex());
        }
        public bool InsertarEmpleado(Empleado empEntidad)
        {
            cmd.Connection = cnx;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "proc_empleadoInsertar";
            try
            {
                cmd.Parameters.Add(new SqlParameter("@codigo", SqlDbType.Int));
                cmd.Parameters["@codigo"].Value = empEntidad.codigoEmpleado;
                cmd.Parameters.Add(new SqlParameter("@nombre", SqlDbType.VarChar, 50));
                cmd.Parameters["@nombre"].Value = empEntidad.nombreEmpleado;
                cmd.Parameters.Add(new SqlParameter("@apellido", SqlDbType.VarChar, 100));
                cmd.Parameters["@apellido"].Value = empEntidad.apellidoEmpleado;
                cmd.Parameters.Add(new SqlParameter("@correo", SqlDbType.VarChar, 100));
                cmd.Parameters["@correo"].Value = empEntidad.correoEmpleado;
                cmd.Parameters.Add(new SqlParameter("@estado", SqlDbType.Int));
                cmd.Parameters["@estado"].Value = empEntidad.estadoEmpleado;
                cnx.Open();
                cmd.ExecuteNonQuery();
                vexito = true;
            }
            catch (SqlException)
            {
                vexito = false;
            }
            finally
            {
                if (cnx.State == ConnectionState.Open)
                {
                    cnx.Close();
                }
                cmd.Parameters.Clear();
            }
            return vexito;
        }
        public bool ActualizarEmpleado(Empleado empEntidad)
        {
            cmd.Connection = cnx;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "proc_empleadoActualizar";
            try
            {
                cmd.Parameters.Add(new SqlParameter("@codigo", SqlDbType.Int));
                cmd.Parameters["@codigo"].Value = empEntidad.codigoEmpleado;
                cmd.Parameters.Add(new SqlParameter("@nombre", SqlDbType.VarChar, 50));
                cmd.Parameters["@nombre"].Value = empEntidad.nombreEmpleado;
                cmd.Parameters.Add(new SqlParameter("@apellido", SqlDbType.VarChar, 100));
                cmd.Parameters["@apellido"].Value = empEntidad.apellidoEmpleado;
                cmd.Parameters.Add(new SqlParameter("@correo", SqlDbType.VarChar, 100));
                cmd.Parameters["@correo"].Value = empEntidad.correoEmpleado;
                cnx.Open();
                cmd.ExecuteNonQuery();
                vexito = true;
            }
            catch (SqlException)
            {
                vexito = false;
            }
            finally
            {
                if (cnx.State == ConnectionState.Open)
                {
                    cnx.Close();
                }
                cmd.Parameters.Clear();
            }
            return vexito;
        }
        public bool EliminarEmpleado(Empleado empEntidad)
        {
            cmd.Connection = cnx;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "proc_empleadoEliminar";
            try
            {
                cmd.Parameters.Add(new SqlParameter("@codigo", SqlDbType.VarChar, 5));
                cmd.Parameters["@codigo"].Value = empEntidad.codigoEmpleado;
                cmd.Parameters.Add(new SqlParameter("@estado", SqlDbType.Int));
                cmd.Parameters["@estado"].Value = empEntidad.estadoEmpleado;
                cnx.Open();
                cmd.ExecuteNonQuery();
                vexito = true;
            }
            catch (SqlException)
            {
                vexito = false;
            }
            finally
            {
                if (cnx.State == ConnectionState.Open)
                {
                    cnx.Close();
                }
                cmd.Parameters.Clear();
            }
            return vexito;
        }
        public DataTable ListarEmpleado(string parametro)
        {
            DataSet dts = new DataSet();
            try
            {
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "proc_empleadoListarPorApellido";
                cmd.Parameters.Add(new SqlParameter("@apellido", parametro));
                SqlDataAdapter miada;
                miada = new SqlDataAdapter(cmd);
                miada.Fill(dts, "empleado");
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmd.Parameters.Clear();
            }
            return (dts.Tables["empleado"]);
        }
        public Empleado ConsultarEmpleado(string codigo)
        {
            try
            {
                SqlDataReader dtr;
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "proc_empleadoConsultarPorCodigo";
                cmd.Parameters.Add(new SqlParameter("@codigo", SqlDbType.Int));
                cmd.Parameters["@codigo"].Value = codigo;
                if (cnx.State == ConnectionState.Closed)
                {
                    cnx.Open();
                }
                dtr = cmd.ExecuteReader();
                if (dtr.HasRows == true)
                {
                    dtr.Read();
                    empEntidad.codigoEmpleado = Convert.ToString(dtr[0]);
                    empEntidad.nombreEmpleado = Convert.ToString(dtr[1]);
                    empEntidad.apellidoEmpleado = Convert.ToString(dtr[2]);
                    empEntidad.correoEmpleado = Convert.ToString(dtr[3]);
                }
                cnx.Close();
                cmd.Parameters.Clear();
                return empEntidad;
            }
            catch (SqlException)
            {
                throw new Exception();
            }
            finally
            {
                if (cnx.State == ConnectionState.Open)
                {
                    cnx.Close();
                }
                cmd.Parameters.Clear();
            }
        }
    }
}