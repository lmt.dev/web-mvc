﻿using System.Data;
using CapaEntidad;
using CapaDatos;
namespace CapaNegocio
{
    public class EmpleadoNegocio
    {
        EmpleadoDatos _EmpleadoDatos = new EmpleadoDatos();

        public bool InsertarEmpleado(Empleado EmpNegocio)
        {
            return _EmpleadoDatos.InsertarEmpleado(EmpNegocio);
        }

        public bool ActualizarEmpleado(Empleado EmpNegocio)
        {
            return _EmpleadoDatos.ActualizarEmpleado(EmpNegocio);
        }

        public bool EliminarEmpleado(Empleado EmpNegocio)
        {
            return _EmpleadoDatos.EliminarEmpleado(EmpNegocio);
        }

        public DataTable ListarEmpleados(string parametro)
        {
            return _EmpleadoDatos.ListarEmpleado(parametro);
        }
        public Empleado ConsultarEmpleado(string codigo)
        {
            return _EmpleadoDatos.ConsultarEmpleado(codigo);
        }
    }
}