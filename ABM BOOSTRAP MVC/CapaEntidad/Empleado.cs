﻿namespace CapaEntidad
{
    public class Empleado
    {
        private string codigo, nombre, apellido, correo;
        private int estado;
        public string codigoEmpleado
        {
            get { return codigo; }
            set { codigo = value; }
        }
        public string nombreEmpleado
        {
            get { return nombre; }
            set { nombre = value; }
        }
        public string apellidoEmpleado
        {
            get { return apellido; }
            set { apellido = value; }
        }
        public string correoEmpleado
        {
            get { return correo; }
            set { correo = value; }
        }
        public int estadoEmpleado
        {
            get { return estado; }
            set { estado = value; }
        }
    }
}