﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Reflection;

public static class Extensions
{
    public static void DarDeBaja<ISUD>(this ISUD modelo, string fhBaja = "FHBaja")
    {
        PropertyInfo propertyInfo = modelo.GetType().GetProperty(fhBaja);
        propertyInfo.SetValue(modelo, Convert.ChangeType(DateTime.Now, propertyInfo.PropertyType));

        Type type = modelo.GetType();
        object classInstance = Activator.CreateInstance(type, null);
        MethodInfo methodInfo = type.GetMethod("UPDATE");

        methodInfo.Invoke(classInstance, null);
    }

    public static DataTable ToDataTableFromListWithChild<T>(this List<T> list)
    {
        PropertyDescriptorCollection propertyDescriptorCollection = TypeDescriptor.GetProperties(typeof(T));
        DataTable dataTable = new DataTable();
        for (int i = 0; i < propertyDescriptorCollection.Count; i++)
        {
            PropertyDescriptor propertyDescriptor = propertyDescriptorCollection[i];
            if (!propertyDescriptor.Name.StartsWith("_list"))
            {
                Type type;
                if (!propertyDescriptor.PropertyType.ToString().Contains("System.Nullable"))
                {
                    type = propertyDescriptor.PropertyType;
                }
                else
                {
                    type = typeof(string);
                }
                dataTable.Columns.Add(propertyDescriptor.Name, type);
            }
        }
        object[] values = new object[propertyDescriptorCollection.Count];
        foreach (T item in list)
        {
            for (int i = 0; i < values.Length; i++)
            {
                values[i] = propertyDescriptorCollection[i].GetValue(item);
            }
            dataTable.Rows.Add(values);
        }
        return dataTable;
    }
}