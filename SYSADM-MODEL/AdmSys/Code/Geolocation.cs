﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Code
{
    public class Geolocation
    {
        public string IP { get; set; }
        public string CodigoContinente { get; set; }
        public string NombreContinente { get; set; }
        public string CodigoPais { get; set; }
        public string NombrePais { get; set; }
        public string CodigoRegion { get; set; }
        public string NombreRegion { get; set; }
        public string Ciudad { get; set; }
        public string CP { get; set; }
        public float Latitud { get; set; }
        public float Longitud { get; set; }
    }
}