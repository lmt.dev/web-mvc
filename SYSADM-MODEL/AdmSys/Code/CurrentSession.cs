﻿using Code;
using Newtonsoft.Json;
using System.Web;

public class CurrentSession
{
    public const string currentVersion = "v1";

    public string Version { get; set; }
    public string Username { get; set; }
    public string Password { get; set; }
    public short IdAplicacion { get; set; }
    public int IdPerfil { get; set; }
    public long IdCuenta { get; set; }
    public long IdSesionTrabajo { get; set; }
    
}