﻿using Code;
using System;
using System.Drawing;
using System.Globalization;
using System.Reflection;
using System.Threading;
using static Code.Utils;

public partial class Default : System.Web.UI.Page
{
    // Ids varios
    private short idAplicacionAdministrador = System.Configuration.ConfigurationManager.AppSettings["IdAplicacionAdministrador"].ToShort();

    private short idAplicacionDistribuidor = System.Configuration.ConfigurationManager.AppSettings["IdAplicacionDistribuidor"].ToShort();

    public string Version { get; set; }

    private bool logout = false;

    protected override void InitializeCulture()
    {
        base.InitializeCulture();
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("es");
        Thread.CurrentThread.CurrentUICulture = new CultureInfo("es");
    }

    protected void Page_Preload(object sender, EventArgs e)
    {
        base.InitializeCulture();

        //// Version de la compilación.
        //Version = Assembly.GetExecutingAssembly().GetName().Version.ToString();

        //if (logout)
        //{
        //    Utils.NavigateTo("~/Content/html/Logout.html");
        //    this.Dispose();
        //}
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            try
            {

            }
            catch
            {
                Utils.DeleteCookie("SessionSysAdm");
                NavigateTo("Apps.aspx");
            }
        }
    }

    private void Page_PreRender(object sender, EventArgs e)
    {
        //if (logout)
        //{
        //    NavigateTo("Apps.aspx");
        //}
        //else
        //{
        //    Permission permission = new Permission(Page.Controls);
        //}
    }
}