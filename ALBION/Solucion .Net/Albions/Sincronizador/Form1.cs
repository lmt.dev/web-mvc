﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MagicSQL;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace Sincronizador
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            sincronizarOrdenes();
            timer1.Start();
        }

        MySqlCommand Query = new MySqlCommand();
        MySqlConnection Conexion;
        string sqlConn = @"server=192.168.100.51;uid=ltorres;password=lucas1234;database=albion;port=3306;charset=utf8";

        protected DataTable market_orders()
        {
            DataTable dtOrdenes = new DataTable();
            dtOrdenes.Columns.AddRange(new DataColumn[14]
                    {
                    new DataColumn("id", typeof(long)),
                    new DataColumn("item_id", typeof(string)),
                    new DataColumn("location",typeof(int)),
                    new DataColumn("quality_level",typeof(int)),
                    new DataColumn("enchantment_level",typeof(int)),
                    new DataColumn("price", typeof(long)),
                    new DataColumn("amount", typeof(int)),
                    new DataColumn("auction_type", typeof(string)),
                    new DataColumn("expires", typeof(DateTime)),
                    new DataColumn("albion_id", typeof(long)),
                    new DataColumn("initial_amount", typeof(int)),
                    new DataColumn("created_at", typeof(DateTime)),
                    new DataColumn("updated_at", typeof(DateTime)),
                    new DataColumn("deleted_at", typeof(DateTime))
                    });
            string consultaSP = "call PROC_ConsultaNexo();";
            Conexion = new MySqlConnection();
            Conexion.ConnectionString = sqlConn;
            Conexion.Open();
            MySqlCommand cmd = new MySqlCommand(consultaSP, Conexion);
            MySqlDataAdapter returnVal = new MySqlDataAdapter(consultaSP, Conexion);
            returnVal.Fill(dtOrdenes);
            sqlConn.Clone();
            return dtOrdenes;
        }

        protected void sincronizarOrdenes()
        {
            try
            {
                dataGridView1.Rows.Clear();
                DataTable dtOrdenes = market_orders();

                foreach (DataRow dtRow in dtOrdenes.Rows)
                {
                    Albions.Orden orden = new Albions.Orden();
                    orden.Identificador = Convert.ToInt64(dtRow["id"].ToString());
                    orden.item_id = dtRow["item_id"].ToString();
                    orden.location = Convert.ToInt32(dtRow["location"].ToString());
                    orden.quality_level = Convert.ToInt32(dtRow["quality_level"].ToString());
                    orden.enchantment_level = Convert.ToInt32(dtRow["enchantment_level"].ToString());
                    orden.price = Convert.ToInt64(dtRow["price"].ToString());
                    orden.amount = Convert.ToInt32(dtRow["amount"].ToString());
                    orden.auction_type = dtRow["auction_type"].ToString();
                    orden.expires = Convert.ToDateTime(dtRow["expires"].ToString()).AddHours(-3);
                    orden.albion_id = Convert.ToInt64(dtRow["albion_id"].ToString());
                    orden.initial_amount = Convert.ToInt32(dtRow["initial_amount"].ToString());
                    orden.created_at = Convert.ToDateTime(dtRow["created_at"].ToString()).AddHours(-3);
                    orden.updated_at = Convert.ToDateTime(dtRow["updated_at"].ToString()).AddHours(-3);
                    orden.deleted_at = null;
                    orden.FHAlta = DateTime.Now;
                    orden.Insert();
                    dataGridView1.Rows.Add("Registro insertado ", orden.Identificador.ToString(), DateTime.Now.ToString());
                    Application.DoEvents();
                    GC.Collect();
                }

                dataGridView1.Rows.Add("Cantidad insertados " + dtOrdenes.Rows.Count, "", DateTime.Now.ToString());
                Application.DoEvents();
                GC.Collect();

            }
            catch (Exception ex)
            {
                dataGridView1.Rows.Add("Error",ex.Message,ex.StackTrace);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            sincronizarOrdenes();
            dataGridView1.Rows.Add("Sincronizacion exitosa. ", "", DateTime.Now.ToString());
        }
    }
}
