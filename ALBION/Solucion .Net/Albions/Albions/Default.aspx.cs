﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MagicSQL;
using System.Data;

namespace Albions
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Timer1.Enabled = true;
            ListarDatos();
        }

        protected void gvCompraVenta_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvCompraVenta.PageIndex = e.NewPageIndex;
                gvCompraVenta.DataBind();
                ListarDatos();
            }
            catch (Exception)
            {
            }
        }

        protected void ListarDatos()
        {
            DataTable dtCompraVenta = new DataTable();

            using (Tn tn = new Tn("Albions"))
            {
                using (SP sp = new SP(tn))
                {
                    dtCompraVenta = sp.Execute("PRC_OBTENERITEMS");
                    tn.Dispose();
                }
            }
            List<Promocion> listaPromociones = new List<Promocion>();
            DataTable dtCompraVentaFiltrado = new DataTable();
            dtCompraVentaFiltrado.Columns.AddRange(new DataColumn[11]
                {
                new DataColumn("id_oferta", typeof(int)),
                new DataColumn("id_demanda", typeof(int)),
                new DataColumn("item_id",typeof(string)),
                new DataColumn("Tier",typeof(string)),
                new DataColumn("Traduccion",typeof(string)),
                new DataColumn("Calidad", typeof(string)),
                new DataColumn("Oferta", typeof(int)),
                new DataColumn("Demanda", typeof(int)),
                new DataColumn("Ganancia", typeof(int)),
                new DataColumn("qo", typeof(int)),
                new DataColumn("qr", typeof(int)),
                });
            foreach (DataRow dtRow in dtCompraVenta.Rows)
            {
                Promocion promo = new Promocion();
                promo.albion_id_oferta = Convert.ToInt64(dtRow["id_oferta"].ToString());
                promo.albion_id_demanda = Convert.ToInt64(dtRow["id_demanda"].ToString());
                promo.item_id = dtRow["item_id"].ToString();
                promo.Tier = dtRow["Tier"].ToString();
                promo.Traduccion = dtRow["Traduccion"].ToString();
                promo.Calidad = dtRow["Calidad"].ToString();
                promo.Oferta = Convert.ToInt64(dtRow["Oferta"].ToString());
                promo.Demanda = Convert.ToInt64(dtRow["Demanda"].ToString());
                promo.Ganancia = Convert.ToInt64(dtRow["Ganancia"].ToString());
                promo.qo = Convert.ToInt32(dtRow["qo"].ToString());
                promo.qr = Convert.ToInt32(dtRow["qr"].ToString());
                listaPromociones.Add(promo);
            }

            List<Promocion> listaPromocionOrden1 = listaPromociones.OrderByDescending(o => o.item_id).ThenByDescending(o=>o.Ganancia).ToList();
            List<Promocion> listaPromocionOrden2 = new List<Promocion>();
            List<Promocion> listaPromocionOrden3 = new List<Promocion>();

            Promocion promoAnterior = new Promocion(); promoAnterior.albion_id_demanda = 0;
            foreach (Promocion promo in listaPromocionOrden1)
            {
                if (promo.albion_id_demanda != promoAnterior.albion_id_demanda)
                {
                    listaPromocionOrden2.Add(promo);
                }
                promoAnterior = promo;
            }

            Promocion promoAnterior2 = new Promocion(); promoAnterior2.albion_id_oferta = 0;
            foreach (Promocion promo in listaPromocionOrden2)
            {
                if (promo.albion_id_oferta != promoAnterior.albion_id_oferta)
                {
                    listaPromocionOrden3.Add(promo);
                }
                promoAnterior = promo;
            }

            gvCompraVenta.DataSource = listaPromocionOrden3.OrderByDescending(o=>o.Ganancia);
            gvCompraVenta.DataBind();
        }

        protected void gvCompraVenta_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

            long vIdOferta_albion = Convert.ToInt64(gvCompraVenta.Rows[e.RowIndex].Cells[0].Text);
            long vIdDemanda_albion = Convert.ToInt64(gvCompraVenta.Rows[e.RowIndex].Cells[1].Text);
            long vPrecioOferta = Convert.ToInt64(gvCompraVenta.Rows[e.RowIndex].Cells[6].Text);
            long vPrecioDemanda = Convert.ToInt64(gvCompraVenta.Rows[e.RowIndex].Cells[7].Text);

            int vCantidadOferta = Convert.ToInt32(gvCompraVenta.Rows[e.RowIndex].Cells[9].Text);
            int vCantidadDemanda = Convert.ToInt32(gvCompraVenta.Rows[e.RowIndex].Cells[10].Text);

            Orden ofertaQuitar = new Orden().Select().Where(o=>
                o.albion_id == vIdOferta_albion &&
                o.price == vPrecioOferta &&
                o.amount == vCantidadOferta
                ).FirstOrDefault();

            Orden demandaQuitar = new Orden().Select().Where(o => 
                o.albion_id == vIdDemanda_albion &&
                o.price == vPrecioDemanda &&
                o.amount == vCantidadDemanda
                ).FirstOrDefault();

            using (Tn tn = new Tn("Albions"))
            {
                if (ofertaQuitar != null)
                {
                    ofertaQuitar.deleted_at = DateTime.Now;
                    ofertaQuitar.Update(tn);
                }

                if (demandaQuitar != null)
                {
                    demandaQuitar.deleted_at = DateTime.Now;
                    demandaQuitar.Update(tn);
                }
                tn.Commit();
            }
            upErrorHandler.Update();
        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            ListarDatos();
        }
    }
}