﻿// Created for MagicSQL using MagicMaker [v.3.75.101.7049]

using System;
using MagicSQL;

namespace GestionContable
{
    public partial class ContadorCliente : ISUD<ContadorCliente>
    {
        public ContadorCliente() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdContadorCliente { get; set; }

        public DateTime? FHAlta { get; set; }

        public string NombreApellido { get; set; }

        public string DNI { get; set; }

        public string CUIT { get; set; }

        public int? IdContador { get; set; }
    }
}