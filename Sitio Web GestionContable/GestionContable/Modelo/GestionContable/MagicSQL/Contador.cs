﻿// Created for MagicSQL using MagicMaker [v.3.75.101.7049]

using System;
using MagicSQL;

namespace GestionContable
{
    public partial class Contador : ISUD<Contador>
    {
        public Contador() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdContador { get; set; }

        public DateTime? FHAlta { get; set; }

        public string NombreApellido { get; set; }

        public string Matricula { get; set; }

        public string CUIT { get; set; }

        public string Provincia { get; set; }

        public string Domicilio_Estudio { get; set; }

        public string Telefono { get; set; }

        public string Especialidad { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public DateTime? FHBaja { get; set; }
    }
}