﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Principal.Master" AutoEventWireup="true" CodeBehind="ConceptoCarga.aspx.cs" Inherits="Presentacion.ConceptoCarga" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<div class="card card-warning">
    <div class="card-header">
    <h3 class="card-title">Carga/Edición de Concepto</h3>
    </div>

    <script runat=server>
        
        string obtenerClientesDeContador()
        {
            System.Data.DataTable tabla = new System.Data.DataTable();
            tabla.Columns.Add("IdConcepto", typeof(string));
            tabla.Columns.Add("Nombre", typeof(string));
            tabla.Columns.Add("Descripcion", typeof(string));
            tabla.Columns.Add("Tipo", typeof(string));

            List<GestionContable.Concepto> listaConceptos = new GestionContable.Concepto().Select();

            string[] table = new string[listaConceptos.Count];
            int counter = 0;
            foreach (GestionContable.Concepto item in listaConceptos)
            {
                table[counter] =
                @"<tr>"

                //Columna 1           
                + @"<td><a href=""ConceptoCarga.aspx?idConcepto="+item.IdConcepto.ToString()+@""">"+item.Nombre+"</a></td>"

                //Columna 3
                + "<td>"+item.Descripcion+"</td>"

                //Columna 4
                + "<td>"+item.Tipo+"</td>"
                            
                +"</tr>"
                ;
                counter += 1;
            }
            return String.Join("", table);
        }

    </script>

    <!-- /.card-header -->
    <div class="card-body">

        <div class="row">
            <div class="col-sm-6">
                <!-- text input -->
                <div class="form-group">
                <label>NOMBRE</label>
                <asp:TextBox 
                ID="txtNombre" 
                CssClass="form-control" 
                runat="server"
                Enabled="true"
                placeholder="Nombre del Concepto"
                data-rule="minlen:4" 
                type="text"
                />
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                <label>TIPO</label>
                <asp:TextBox 
                ID="txtTipo" 
                CssClass="form-control" 
                runat="server"
                Enabled="true"
                placeholder="Tipo de Concepto"
                type="text"
                />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <!-- textarea -->
                <div class="form-group">
                <label>DESCRIPCIÓN</label>
                <asp:TextBox 
                ID="txtDescripcion" 
                CssClass="form-control" 
                runat="server"
                Enabled="true"
                placeholder="Describa la funcionalidad del Concepto cargado..."
                data-rule="minlen:4" 
                type="text"
                />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <asp:UpdatePanel ID="upBotonAlta" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                        <ContentTemplate>
                            <asp:Button ID="btnAltaConcepto"
                                Text="Ingresar nuevo Concepto"
                                CssClass="btn btn-success col-lg-12 col-md-12 col-sm-12 col-xs-12"
                                runat="server"
                                Style="margin-bottom: 5px;"
                                OnClick="btnAltaConcepto_Click"
                                />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>

        <table id="tableTemplate" class="table table-bordered table-hover table-striped">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Descripción</th>
                    <th>Tipo</th>
                </tr>
            </thead>

            <tbody>
                <% = obtenerClientesDeContador()%>
            </tbody>
        </table>
    </div>
</div>
</asp:Content>
