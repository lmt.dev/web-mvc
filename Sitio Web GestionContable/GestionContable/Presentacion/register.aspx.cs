﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MagicSQL;
using GestionContable;

namespace Presentacion
{
    public partial class register : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnAltaContador_Click(object sender, EventArgs e)
        {
            lblMensajeError.Text = string.Empty;
            try
            {
                if (txtPassword.Text != txtPasswordConfirmacion.Text)
                {
                    lblMensajeError.Text = "Las passwords deben coincidir";
                    upMensajeError.Update();
                }

                if (txtNombreApellido.Text == string.Empty
                    ||
                    txtDomicilioEstudio.Text == string.Empty
                    ||
                    txtMatriculaCuit.Text == string.Empty
                    ||
                    txtProvincia.Text == string.Empty
                    ||
                    txtTelefono.Text == string.Empty
                    ||
                    txtEspecialidad.Text == string.Empty
                    ||
                    txtEmail.Text == string.Empty
                    ||
                    txtPassword.Text == string.Empty
                    ||
                    txtPasswordConfirmacion.Text == string.Empty
                    )
                {
                    lblMensajeError.Text = "Debe completar los campos requeridos";
                    upMensajeError.Update();
                }

                if (lblMensajeError.Text == string.Empty)
                {
                    Contador contadorAlta = new Contador();
                    contadorAlta.FHAlta = DateTime.Now;
                    contadorAlta.NombreApellido = txtNombreApellido.Text;
                    contadorAlta.Matricula = txtMatriculaCuit.Text;
                    contadorAlta.Provincia = txtProvincia.Text;
                    contadorAlta.Domicilio_Estudio = txtDomicilioEstudio.Text;
                    contadorAlta.Telefono = txtTelefono.Text;
                    contadorAlta.Especialidad = txtEspecialidad.Text;
                    contadorAlta.Email = txtEmail.Text;
                    contadorAlta.Password = txtPassword.Text;
                    contadorAlta.Insert();
                    lblMensajeError.Text = "Registro exitoso! Inicie sesión con sus credenciales";
                    upMensajeError.Update();

                    btnAltaContador.Enabled = false;
                    upBotonAlta.Update();
                }
            }
            catch
            {
                lblMensajeError.Text = "Error, intente nuevamente";
                upMensajeError.Update();
            }            
        }
    }
}