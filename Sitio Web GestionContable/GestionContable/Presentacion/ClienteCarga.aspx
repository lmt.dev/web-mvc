﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Principal.Master" AutoEventWireup="true" CodeBehind="ClienteCarga.aspx.cs" Inherits="Presentacion.ClienteCarga" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <link href="Content/vendor/bootstrap.min.css" rel="stylesheet">
    <%-- Fonts --%>
    <link href="Content/fonts/font-awesome.min.css" rel="stylesheet" />

<div class="card card-warning">
    <div class="card-header">
    <h3 class="card-title">Carga/Edición de cliente</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">

        <div class="row">
            <asp:UpdatePanel ID="upMensajeError" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                <ContentTemplate>
                    <asp:Label ID="lblMensajeError" Text="" ForeColor="Red" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <!-- text input -->
                <div class="form-group">
                <label>NOMBRE</label>
                <asp:TextBox 
                ID="txtNombre" 
                CssClass="form-control" 
                runat="server"
                Enabled="true"
                placeholder="Nombre"
                data-rule="minlen:4" 
                type="text"
                />
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                <label>CUIT REPRESENTANTE</label>
                <asp:TextBox 
                ID="txtCuit" 
                CssClass="form-control" 
                runat="server"
                Enabled="true"
                placeholder="CUIT"
                data-rule="minlen:4" 
                type="text"
                />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <!-- textarea -->
                <div class="form-group">
                <label>CLAVE FISCAL</label>
                <asp:TextBox 
                ID="txtClaveFiscal" 
                CssClass="form-control" 
                runat="server"
                Enabled="true"
                placeholder="Clave Fiscal"
                data-rule="minlen:4" 
                type="text"
                />
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                <label>NUMERO DE TELEFONO</label>
                <asp:TextBox 
                ID="txtNumeroTelefono" 
                CssClass="form-control" 
                runat="server"
                Enabled="true"
                placeholder="Teléfono"
                data-rule="minlen:4" 
                type="text"
                />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <asp:UpdatePanel ID="upBotonAlta" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                        <ContentTemplate>
                            <asp:Button ID="btnAltaCliente"
                                Text="Ingresar nuevo Cliente"
                                CssClass="btn btn-success col-lg-12 col-md-12 col-sm-12 col-xs-12"
                                runat="server"
                                Style="margin-bottom: 5px;" 
                                OnClick="btnAltaCliente_Click"
                                />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>

        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
	            <asp:UpdatePanel ID="upConceptoSinAsignar" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
		            <ContentTemplate>
                        <div class="row">
                            <h4 class="modal-title">
                                <asp:Label Text="Conceptos disponibles:" runat="server" />
                            </h4>
                        </div>

			            <div class="row" style="margin-top: 5px;">
                            <asp:GridView ID="gvConceptoSinAsignar" 
                                CssClass="table table-striped table-bordered table-hover grid-view crypto-id" 
                                data-model="SolicitudDeTaller.aspx"
                                Width="100%" 
                                AutoGenerateColumns="False" 
                                runat="server" 
                                OnRowDataBound="gvConceptoSinAsignar_RowDataBound"
                                EmptyDataText="Sin datos para mostrar."
                                OnSelectedIndexChanging="gvConceptoSinAsignar_SelectedIndexChanging"
                                AutoPostBack="true"
                                >
                                <Columns>
                                    <asp:BoundField HeaderText="#" DataField="IdConcepto" HeaderStyle-CssClass="crypto-id-colConcepton" ItemStyle-CssClass="crypto-id-colConcepton" />
                                    <asp:BoundField HeaderText="Nombre" DataField="Nombre" />
                                    <asp:BoundField HeaderText="Tipo" DataField="Tipo" />

                                    <asp:TemplateField HeaderText="Agregar" ItemStyle-Width="1">
                                        <ItemTemplate>
                                            <center>
                                                <asp:LinkButton ID="btnAsignarConcepto"
                                                    runat="server"
                                                    CssClass="btn btn-success"
                                                    Style="margin-left: 5px;"
                                                    title="Asignar concepto"
                                                    CommandName="Select"
                                                    >
                                                    <span aria-hidden="true" class="glyphicon glyphicon-plus"></span>
                                                </asp:LinkButton>
                                            </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
			            </div>
		            </ContentTemplate>
	            </asp:UpdatePanel>
            </div>

            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
	            <asp:UpdatePanel ID="upConceptoAsignadas" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
		            <ContentTemplate>
                        <div class="row">
                            <h4 class="modal-title">
                                <asp:Label Text="Conceptos asignados:" runat="server" />
                            </h4>
                        </div>

			            <div class="row" style="margin-top: 5px;">
                            <asp:GridView ID="gvConceptoAsignadas" 
                                CssClass="table table-striped table-bordered table-hover grid-view crypto-id" 
                                data-model="VendingConceptoxReseller.aspx"
                                Width="100%" 
                                AutoGenerateColumns="False" 
                                runat="server" 
                                OnRowDataBound="gvConceptoAsignadas_RowDataBound"
                                EmptyDataText="No hay conceptos asignados a éste cliente"
                                OnSelectedIndexChanging="gvConceptoAsignadas_SelectedIndexChanging"
                                >
                                <Columns>
                                    <asp:BoundField HeaderText="#" DataField="IdContadorClienteConcepto" HeaderStyle-CssClass="crypto-id-colConcepton" ItemStyle-CssClass="crypto-id-colConcepton" />
                                    <asp:BoundField HeaderText="Concepto" DataField="ConceptoNombre" />
                                    <asp:TemplateField HeaderText="Valor" ItemStyle-Width="100">
                                        <ItemTemplate>
                                            <center>
                                                <asp:TextBox ID="txtValor"
                                                CssClass="form-control"
                                                PlaceHolder="Valor"
                                                runat="server"
                                                TextMode="SingleLine" />
                                            </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Orden" DataField="Orden" />
                                    <asp:TemplateField HeaderText="Quitar" ItemStyle-Width="1">
                                        <ItemTemplate>
                                            <center>
                                                <asp:LinkButton 
                                                    ID="btnQuitarConcepto"
                                                    runat="server"
                                                    CssClass="btn btn-danger"
                                                    Style="margin-le ft: 5px;"
                                                    title="Desasignar Concepto"
                                                    CommandName="Select"
                                                    Visible="true"
                                                    >
                                                    <span aria-hidden="true" class="glyphicon glyphicon-off"></span>
                                                </asp:LinkButton>
                                            </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
			            </div>
		            </ContentTemplate>
	            </asp:UpdatePanel>
                <asp:UpdatePanel ID="upBotonModificacion" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                    <ContentTemplate>
                        <asp:Button ID="btnModificarCliente"
                            Text="Modificar datos de Cliente"
                            CssClass="btn btn-primary col-lg-12 col-md-12 col-sm-12 col-xs-12"
                            runat="server"
                            Style="margin-bottom: 5px;" 
                            OnClick="btnModificarCliente_Click"
                            />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>   
        </div>

    </div>
</div>

</asp:Content>
