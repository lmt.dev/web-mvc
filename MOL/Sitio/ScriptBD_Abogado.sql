-- Creacion de tabla
CREATE TABLE [dbo].[Abogado]([IdAbogado] [int] IDENTITY(1,1) NOT NULL,[Nombre] [varchar](50) NOT NULL,[Descripcion] [varchar](max) NOT NULL,[FHAlta] [datetime] NOT NULL,[FHBaja] [datetime] NULL,[Estado] [int] NOT NULL, CONSTRAINT [PK_Abogado] PRIMARY KEY CLUSTERED ([IdAbogado] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];


-- Sp Insercion
CREATE PROCEDURE [PRC_AbogadoInsert]
@Nombre varchar(50),@Descripcion varchar(max) 
AS DECLARE @SQLString varchar(MAX); set @SQLString =
 'INSERT INTO '+DB_NAME()+'.dbo.Abogado([Nombre], [Descripcion], [FHAlta], [FHBaja], [Estado]) 
 VALUES ('''+@Nombre+''', '''+@Descripcion+''', GETDATE(), null, 1);'
 exec sp_sqlexec @SQLString ;


-- Sp Consultar por Id
CREATE PROCEDURE [PRC_AbogadoSelect] @IdAbogado int AS SET NOCOUNT ON SET XACT_ABORT ON BEGIN TRAN SELECT [IdAbogado], [Nombre], [Descripcion], [FHAlta], [FHBaja], [Estado] FROM   [dbo].[Abogado] WHERE  ([IdAbogado] = @IdAbogado OR @IdAbogado IS NULL AND FHBaja is NULL);


-- Sp Listar por nombre
CREATE PROCEDURE [PRC_Abogado_ListByName] @nombre varchar(50) as begin select * from Abogado where FHBaja IS NULL and Nombre like @nombre + '%' end


-- Sp Actualizacion
CREATE PROCEDURE [PRC_AbogadoUpdate] @IdAbogado int, @Nombre varchar(50), @Descripcion varchar(max), @Estado int AS
DECLARE @SQLString varchar(MAX);
set @SQLString = 'UPDATE '+DB_NAME()+'.dbo.Abogado SET Nombre ='''+@Nombre+''', Descripcion ='''+@Descripcion+''', Estado = 1 WHERE  [IdAbogado] = '+convert(varchar(50),@IdAbogado)+';' 
exec sp_sqlexec @SQLString


-- Sp Eliminacion
CREATE PROCEDURE [PRC_AbogadoDelete] @IdAbogado int AS 
DECLARE @SQLString varchar(MAX);
set @SQLString = 'DELETE FROM   [dbo].[Abogado] WHERE  [IdAbogado] = '+convert(varchar(50),@IdAbogado)+';'
exec sp_sqlexec @SQLString


