﻿using System;
using Modelo;
using Negocio;

namespace mejoropcionlegal.Controls
{
    public partial class UCTraza : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void InsertarTraza(string pDescripcion)
        {
            try
            {
                //INICIO Datos de Traza
                Traza trazaInsertar = new Traza();
                trazaInsertar.nombreTraza = ObtenerIp();
                trazaInsertar.descripcionTraza += pDescripcion;
                TrazaNegocio trazaNegocio = new TrazaNegocio();
                trazaNegocio.InsertarTraza(trazaInsertar);
                //FIN Datos de Traza
            } catch{}
        }

        public string ObtenerIp()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }
            return context.Request.ServerVariables["REMOTE_ADDR"];
        }
    }
}