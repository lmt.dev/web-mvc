﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCMenu.ascx.cs" Inherits="mejoropcionlegal.Controls.WebUserControl1" %>
  <header>
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="navigation">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse.collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
            <div class="navbar-brand">
              <a href="Default.aspx"><h1><span>Mejor Opcion Legal</span></h1></a>
            </div>
          </div>

          <div class="navbar-collapse collapse">
            <div class="menu">
              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation"><a href="Servicios.aspx">CÓMO FUNCIONA</a></li>
                <li role="presentation"><a href="Default.aspx">HACÉ TU CONSULTA</a></li>
                <li role="presentation"><a href="Abogado.aspx">SOY ABOGADO</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </nav>
  </header>