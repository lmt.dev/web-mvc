﻿using System;
using System.Net;
using System.Net.Mail;
using Modelo;
using Negocio;
using System.Data;

namespace mejoropcionlegal.Controls
{
    public partial class UCEnvioMail : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void enviarMail
            (
                string pDestinatario,
                string pAsunto,
                string pCuerpo
            )
        {
            string emailOrigen = "mejoropcionlegal@gmail.com";
            string emailPassword = "ProtocoloZer0";
            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(emailOrigen);
            mail.To.Add(pDestinatario);
            mail.Subject = pAsunto;
            mail.Body = pCuerpo;
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 25; //465; //587
            smtp.Credentials = new NetworkCredential(emailOrigen, emailPassword);
            smtp.EnableSsl = true;
            try
            {
                smtp.Send(mail);
                string pDescripcionTraza = "Envío de email exitoso;";
                pDescripcionTraza += "Destinatario: " + pDestinatario + ";";
                pDescripcionTraza += "Asunto: " + pAsunto + ";";
                pDescripcionTraza += "Cuerpo: " + pCuerpo + ";";
                UCTraza1.InsertarTraza(pDescripcionTraza);
            }
            catch(Exception ex)
            {
                string pDescripcionTraza = "Problemas en el envío de email;";
                pDescripcionTraza += "Destinatario: "+pDestinatario+";";
                pDescripcionTraza += "Asunto: "+pAsunto+";";
                pDescripcionTraza += "Cuerpo: "+pCuerpo+";";
                pDescripcionTraza += "Ex Message: " + ex.Message + ";";
                pDescripcionTraza += "Ex StackTrace: " + ex.StackTrace + ";";
                UCTraza1.InsertarTraza(pDescripcionTraza);
            }
            finally
            {
                smtp.Dispose();
            }
        }

        public ErrorHandler validacionesSeguridad(string pEmail, string pIp)
        {
            ErrorHandler eh = new ErrorHandler();

            try
            {
                TrazaNegocio trazaNegocio = new TrazaNegocio();

                DataTable dtValidaciones = trazaNegocio.ObtenerValidacionesEmail(pEmail, pIp);
                foreach (DataRow dtRow in dtValidaciones.Rows)
                {
                    eh.Mensaje = dtRow[0].ToString();
                    return eh;
                }
                eh.Mensaje = "EXITO";
                return eh;
            }
            catch
            {
                eh.Mensaje = "Error en validaciones de seguridad.";
                return eh;
            }
        }
    }
}