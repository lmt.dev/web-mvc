﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelo;
using Negocio;
using System.Data;

namespace mejoropcionlegal.Controls
{
    public partial class UCConsulta : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnEnviarConsulta_Click(object sender, EventArgs e)
        {
            enviarConsulta();
        }

        protected void enviarConsulta()
        {
            string pDescripcionTraza = "";
            try
            {
                Consulta consultaInsertar = new Consulta();
                ConsultaNegocio consultaNegocio = new ConsultaNegocio();
                if (txtNombre.Text != "" && txtConsulta.Text != "" && txtEmail.Text != "")
                {
                    ErrorHandler eh = UCEnvioMail1.validacionesSeguridad(txtEmail.Text, UCTraza1.ObtenerIp());

                    if (eh.Mensaje == "EXITO")
                    {
                        consultaInsertar.nombreConsulta = txtNombre.Text;
                        consultaInsertar.descripcionConsulta = txtConsulta.Text;
                        consultaInsertar.telefonoConsulta = txtTelefono.Text;
                        consultaInsertar.emailConsulta = txtEmail.Text;
                        consultaNegocio.InsertarConsulta(consultaInsertar);
                        txtConsulta.Visible = false;
                        txtEmail.Visible = false;
                        txtNombre.Visible = false;
                        txtTelefono.Visible = false;
                        btnEnviarConsulta.Visible = false;
                        btnEnviarOtraConsulta.Visible = true;

                        lblMensaje.ForeColor = System.Drawing.Color.Green;
                        lblMensaje.Text = "CONSULTA ENVIADA EXITOSAMENTE!";
                        lblMensaje.BackColor = System.Drawing.Color.Aqua;

                        //INICIO Datos de Traza
                        pDescripcionTraza += "Ingreso de consulta. CONSULTA ENVIADA EXITOSAMENTE;";
                        pDescripcionTraza += "Nombre: " + txtNombre.Text + ";";
                        pDescripcionTraza += "Telefono: " + txtTelefono.Text + ";";
                        pDescripcionTraza += "Email: " + txtEmail.Text + ";";
                        pDescripcionTraza += "Consulta: " + txtConsulta.Text + ";";
                        UCTraza1.InsertarTraza(pDescripcionTraza);

                        pDescripcionTraza += Environment.NewLine;
                        pDescripcionTraza += "Su consulta será procesada a la brevedad.";

                        UCEnvioMail1.enviarMail
                        (
                            consultaInsertar.emailConsulta, //Destinatario
                            "Comprobante de Consulta. Mejor Opcion Legal", //Asunto
                            pDescripcionTraza.Replace(";", Environment.NewLine) //Cuerpo
                        );
                        //FIN Datos de Traza
                    }
                    else
                    {
                        lblMensaje.Text = eh.Mensaje;
                        lblMensaje.BackColor = System.Drawing.Color.Aqua;
                        //INICIO Datos de Traza
                        pDescripcionTraza += "Ingreso de Consulta. Validacion de seguridad: "+ eh.Mensaje + ";";
                        pDescripcionTraza += "Nombre: " + txtNombre.Text + ";";
                        pDescripcionTraza += "Telefono: " + txtTelefono.Text + ";";
                        pDescripcionTraza += "Email: " + txtEmail.Text + ";";
                        pDescripcionTraza += "Consulta: " + txtConsulta.Text + ";";
                        UCTraza1.InsertarTraza(pDescripcionTraza);
                        //FIN Datos de Traza
                    }
                }
                else
                {
                    lblMensaje.ForeColor = System.Drawing.Color.Red;
                    lblMensaje.Text = "NECESITAS COMPLETAR LOS CAMPOS PARA ENVIAR LA CONSULTA!";
                    //INICIO Datos de Traza
                    pDescripcionTraza += "Ingreso de Consulta. DATOS INCOMPLETOS;";
                    pDescripcionTraza += "Nombre: " + txtNombre.Text + ";";
                    pDescripcionTraza += "Telefono: " + txtTelefono.Text + ";";
                    pDescripcionTraza += "Email: " + txtEmail.Text + ";";
                    pDescripcionTraza += "Consulta: " + txtConsulta.Text + ";";
                    UCTraza1.InsertarTraza(pDescripcionTraza);
                    //FIN Datos de Traza
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Error al procesar la consulta";
                //INICIO Datos de Traza
                pDescripcionTraza += "Error al procesar la consulta";
                pDescripcionTraza += "Nombre: " + txtNombre.Text + ";";
                pDescripcionTraza += "Telefono: " + txtTelefono.Text + ";";
                pDescripcionTraza += "Email: " + txtEmail.Text + ";";
                pDescripcionTraza += "Consulta: " + txtConsulta.Text + ";";
                pDescripcionTraza += "Ex Message; " + ex.Message;
                pDescripcionTraza += "Ex StackTrace; " + ex.StackTrace;
                UCTraza1.InsertarTraza(pDescripcionTraza);
                //FIN Datos de Traza
            }
        }

        protected void btnEnviarOtraConsulta_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }
    }
}