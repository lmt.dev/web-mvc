﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCServicios.ascx.cs" Inherits="mejoropcionlegal.Controls.UCServicios" %>
    <div class="services">
        <div class="container">
            <h3>Nuestros servicios</h3>
            <hr>
            <div class="col-md-6">
            <img src="images/abogado.jpg" class="img-responsive">
                <h3>

          
            <p>MOL es una plataforma que permite unir a personas, pymes o empresas con el profesional legal que mejor se adapte a su necesidad o consulta.

    Cada profesional que se suma a MOL totalmente GRATIS completa un formulario que ayuda a nuestro algoritmo a definir si es tu mejor opción o no.</p>
                </h3>
            </div>

            <div class="col-md-6">
            <div class="media">
                <ul>
                <li>
                    <div class="media-left">
                    <i class="fa fa-pencil"></i>
                    </div>
                    <div class="media-body">
                    <h4 class="media-heading">Carga de consulta</h4>
                    <h3><p>Necesitamos esta información para poder asesorarte de una mejor manera.</p></h3>
                    </div>
                </li>
                <li>
                    <div class="media-left">
                    <i class="fa fa-book"></i>
                    </div>
                    <div class="media-body">
                    <h4 class="media-heading">Contacto telefónico</h4>
                    <h3><p>El teléfono es muy importante ya que nos permitirá contactarte si necesitamos mas información, la dirección no es requerida.</p></h3>
                    </div>
                </li>
                <li>
                    <div class="media-left">
                    <i class="fa fa-rocket"></i>
                    </div>
                    <div class="media-body">
                    <h4 class="media-heading">Un profesional a medida</h4>
                    <h3><p>Es esencial que nos cuentes todos los detalles de tu consulta, por más pequeños que sean, esto nos permitirá asignarte el mejor profesional</p></h3>
                    </div>
                </li>
                </ul>
            </div>
            </div>
        </div>
        </div>