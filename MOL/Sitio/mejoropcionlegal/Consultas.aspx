﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Consultas.aspx.cs" Inherits="mejoropcionlegal.Consultas" %>
<%@ Register Src="~/Controls/UCMenu.ascx" TagPrefix="uc" TagName="UCMenu" %>
<%@ Register Src="~/Controls/UCServicios.ascx" TagPrefix="uc" TagName="UCServicios" %>
<%@ Register Src="~/Controls/UCFooter.ascx" TagPrefix="uc" TagName="UCFooter" %>
<%@ Register Src="~/Controls/UCConsulta.ascx" TagPrefix="uc" TagName="UCConsulta" %>
<%@ Register Src="~/Controls/UCTraza.ascx" TagPrefix="uc" TagName="UCTraza" %>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Mejor Opcion Legal</title>

  <!-- Bootstrap -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="css/font-awesome.min.css">
  <link rel="stylesheet" href="css/animate.css">
  <link href="css/prettyPhoto.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet" />
  <!-- =======================================================
    Theme Name: Company
    Theme URL: https://bootstrapmade.com/company-free-html-bootstrap-template/
    Author: BootstrapMade
    Author URL: https://bootstrapmade.com
  ======================================================= -->
</head>

<body>
   <uc:UCMenu runat="server" ID="UCMenu1" />

  <section id="main-slider" class="no-margin">
    <div class="carousel slide">
      <div class="carousel-inner">
        <div class="item active" style="background-image: url(images/slider/green.jpg)">
          <div class="container">
              <p></p>
              <br />
              <p></p>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <label>
                        <asp:Label ID="lblNombreAbogado" Text="" runat="server" />
                    </label>
                    <form id="Form1" runat="server">
                        <div class="panel-body">

                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:GridView ID="gvConsultas" 
                                    CssClass="table table-bordered" 
                                    data-model="Consultas.aspx"
                                    Width="100%" AutoGenerateColumns="False" runat="server" 
                                    EmptyDataText="No hay consultas disponibles"
                                    OnPageIndexChanging="gvConsultas_PageIndexChanging"
                                    PageSize="5"
                                    >
                                    <Columns>
                                        <%-- Requeridas para las GridViews clickeables - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                                        <%-- Responsive + - --%>
                                        <asp:BoundField HeaderText="#" DataField="IdConsulta" />
                                        <asp:BoundField HeaderText="Nombre y Apellido" DataField="Nombre" />
<%--                                        <asp:BoundField HeaderText="Teléfono" DataField="Telefono" />
                                        <asp:BoundField HeaderText="Email" DataField="Email" />--%>
                                        <asp:BoundField HeaderText="Consulta" DataField="Descripcion" />
                                        <asp:TemplateField HeaderText="Desestimar" ItemStyle-Width="1">
                                            <ItemTemplate>
                                                <center>
                                                    <asp:LinkButton 
                                                        ID="btnReservarConsulta"
                                                        runat="server"
                                                        CssClass="btn btn-danger"
                                                        Style="margin-left: 5px;"
                                                        title="Desestimar consulta"
                                                        >
                                                        <span aria-hidden="true" class="glyphicon glyphicon-trash"></span>
                                                    </asp:LinkButton>
                                                </center>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Reservar" ItemStyle-Width="1">
                                            <ItemTemplate>
                                                <center>
                                                    <asp:LinkButton 
                                                        ID="btnReservarConsulta"
                                                        runat="server"
                                                        CssClass="btn btn-success"
                                                        Style="margin-left: 5px;"
                                                        title="Reservar consulta"
                                                        >
                                                        <span aria-hidden="true" class="glyphicon glyphicon-ok"></span>
                                                    </asp:LinkButton>
                                                </center>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                            <EditRowStyle BackColor="#7C6F57" />
                            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                            <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                            <SortedAscendingCellStyle BackColor="#F8FAFA" />
                            <SortedAscendingHeaderStyle BackColor="#246B61" />
                            <SortedDescendingCellStyle BackColor="#D4DFE1" />
                            <SortedDescendingHeaderStyle BackColor="#15524A" />
                                </asp:GridView>
                            </div>
                        </div>
                    </form>
              </div>
          </div>
        </div>
        <!--/.item-->
      </div>
      <!--/.carousel-inner-->
    </div>
    <!--/.carousel-->
  </section>

  <uc:UCFooter runat="server" ID="UCFooter1" />
  <uc:UCTraza runat="server" ID="UCTraza1" />
  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="js/jquery-2.1.1.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.prettyPhoto.js"></script>
  <script src="js/jquery.isotope.min.js"></script>
  <script src="js/wow.min.js"></script>
  <script src="js/functions.js"></script>

</body>

</html>