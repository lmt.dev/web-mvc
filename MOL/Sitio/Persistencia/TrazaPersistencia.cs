using System;
using System.Data;
using Modelo;
using System.Data.SqlClient;
namespace Persistencia
{
    public class TrazaPersistencia
    {
        SqlConnection cnx;
        Traza entidadTraza = new Traza();
        Conexion MiConexi = new Conexion();
        SqlCommand cmd = new SqlCommand();
        public TrazaPersistencia()
        {
            cnx = new SqlConnection(MiConexi.GetConex());
        }


        public bool InsertarTraza(Traza entidadTraza)
        {
            cmd.Connection = cnx;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "PRC_TrazaInsert";
            try
            {
                cmd.Parameters.Add(new SqlParameter("@nombre", SqlDbType.VarChar, 50));
                cmd.Parameters["@nombre"].Value = entidadTraza.nombreTraza;
                cmd.Parameters.Add(new SqlParameter("@descripcion", SqlDbType.VarChar,500));
                cmd.Parameters["@descripcion"].Value = entidadTraza.descripcionTraza;
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
                return true;
            }
            catch (SqlException)
            {
                return false;
            }
        }


        public bool ActualizarTraza(Traza entidadTraza)
        {
            cmd.Connection = cnx;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "PRC_TrazaUpdate";
            try
            {
                cmd.Parameters.Add(new SqlParameter("@IdTraza", SqlDbType.Int));
                cmd.Parameters["@IdTraza"].Value = entidadTraza.idTraza;
                cmd.Parameters.Add(new SqlParameter("@nombre", SqlDbType.VarChar, 50));
                cmd.Parameters["@nombre"].Value = entidadTraza.nombreTraza;
                cmd.Parameters.Add(new SqlParameter("@descripcion", SqlDbType.VarChar, 500));
                cmd.Parameters["@descripcion"].Value = entidadTraza.descripcionTraza;
                cmd.Parameters.Add(new SqlParameter("@estado", SqlDbType.Int));
                cmd.Parameters["@estado"].Value = entidadTraza.estadoTraza;
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
                return true;
            }
            catch (SqlException)
            {
                return false;
            }
        }


        public bool EliminarTraza(Traza entidadTraza)
        {
            cmd.Connection = cnx;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "PRC_TrazaDelete";
            try
            {
                cmd.Parameters.Add(new SqlParameter("@IdTraza", SqlDbType.Int));
                cmd.Parameters["@IdTraza"].Value = entidadTraza.idTraza;
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
                return true;
            }
            catch (SqlException)
            {
                return false;
            }
        }


        public DataTable ListarTraza(string pFiltro)
        {
            DataSet dts = new DataSet();
            try
            {
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PRC_Traza_ListByName";
                cmd.Parameters.Add(new SqlParameter("@nombre", pFiltro));
                SqlDataAdapter miada;
                miada = new SqlDataAdapter(cmd);
                miada.Fill(dts, "Traza");
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmd.Parameters.Clear();
            }
            return (dts.Tables["Traza"]);
        }
        public Traza ConsultarTraza(int IdTraza)
        {
            try


            {
                SqlDataReader dtr;
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PRC_TrazaSelect";
                cmd.Parameters.Add(new SqlParameter("@IdTraza", SqlDbType.Int));
                cmd.Parameters["@IdTraza"].Value = IdTraza;
                if (cmd.Connection.State == ConnectionState.Closed)
                {
                    cmd.Connection.Open();
                }
                dtr = cmd.ExecuteReader();
                if (dtr.HasRows == true)
                {
                    dtr.Read();
                    entidadTraza.idTraza = Convert.ToInt32(dtr[0]);
                    entidadTraza.nombreTraza = Convert.ToString(dtr[1]);
                    entidadTraza.descripcionTraza = Convert.ToString(dtr[2]);
                    entidadTraza.fhAltaTraza = Convert.ToDateTime(dtr[3]);
                    entidadTraza.fhBajaTraza = Convert.ToDateTime(dtr[4]);
                    entidadTraza.estadoTraza = Convert.ToInt32(dtr[5]);
                }
                cnx.Close();
                cmd.Parameters.Clear();
                return entidadTraza;
            }


            catch (SqlException)
            {
                throw new Exception();
            }
            finally
            {
                if (cmd.Connection.State == ConnectionState.Open)
                {
                    cmd.Connection.Close();
                }
                cmd.Parameters.Clear();
            }
        }

        public DataTable ObtenerValidacionesEmail(string pEmail, string pIp)
        {
            DataSet dts = new DataSet();
            try
            {
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PRC_ObtenerValidaciones_Email";

                cmd.Parameters.Add(new SqlParameter("@pEmail", SqlDbType.VarChar,100));
                cmd.Parameters["@pEmail"].Value = pEmail;

                cmd.Parameters.Add(new SqlParameter("@pIp", SqlDbType.VarChar, 100));
                cmd.Parameters["@pIp"].Value = pIp;

                SqlDataAdapter miada;
                miada = new SqlDataAdapter(cmd);
                miada.Fill(dts, "Traza");
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmd.Parameters.Clear();
            }
            return (dts.Tables["Traza"]);
        }
    }
} 


