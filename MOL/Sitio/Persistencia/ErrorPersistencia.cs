using System;
using System.Data;
using Modelo;
using System.Data.SqlClient;
namespace Persistencia
{
    public class ErrorPersistencia
    {
        SqlConnection cnx;
        Error entidadError = new Error();
        Conexion MiConexi = new Conexion();
        SqlCommand cmd = new SqlCommand();
        public ErrorPersistencia()
        {
            cnx = new SqlConnection(MiConexi.GetConex());
        }


        public bool InsertarError(Error entidadError)
        {
            cmd.Connection = cnx;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "PRC_ErrorInsert";
            try
            {
                cmd.Parameters.Add(new SqlParameter("@nombre", SqlDbType.VarChar, 50));
                cmd.Parameters["@nombre"].Value = entidadError.nombreError;
                cmd.Parameters.Add(new SqlParameter("@descripcion", SqlDbType.VarChar,500));
                cmd.Parameters["@descripcion"].Value = entidadError.descripcionError;
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
                return true;
            }
            catch (SqlException)
            {
                return false;
            }
        }


        public bool ActualizarError(Error entidadError)
        {
            cmd.Connection = cnx;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "PRC_ErrorUpdate";
            try
            {
                cmd.Parameters.Add(new SqlParameter("@IdError", SqlDbType.Int));
                cmd.Parameters["@IdError"].Value = entidadError.idError;
                cmd.Parameters.Add(new SqlParameter("@nombre", SqlDbType.VarChar, 50));
                cmd.Parameters["@nombre"].Value = entidadError.nombreError;
                cmd.Parameters.Add(new SqlParameter("@descripcion", SqlDbType.VarChar, 500));
                cmd.Parameters["@descripcion"].Value = entidadError.descripcionError;
                cmd.Parameters.Add(new SqlParameter("@estado", SqlDbType.Int));
                cmd.Parameters["@estado"].Value = entidadError.estadoError;
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
                return true;
            }
            catch (SqlException)
            {
                return false;
            }
        }


        public bool EliminarError(Error entidadError)
        {
            cmd.Connection = cnx;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "PRC_ErrorDelete";
            try
            {
                cmd.Parameters.Add(new SqlParameter("@IdError", SqlDbType.Int));
                cmd.Parameters["@IdError"].Value = entidadError.idError;
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
                return true;
            }
            catch (SqlException)
            {
                return false;
            }
        }


        public DataTable ListarError(string pFiltro)
        {
            DataSet dts = new DataSet();
            try
            {
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PRC_Error_ListByName";
                cmd.Parameters.Add(new SqlParameter("@nombre", pFiltro));
                SqlDataAdapter miada;
                miada = new SqlDataAdapter(cmd);
                miada.Fill(dts, "Error");
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmd.Parameters.Clear();
            }
            return (dts.Tables["Error"]);
        }
        public Error ConsultarError(int IdError)
        {
            try


            {
                SqlDataReader dtr;
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PRC_ErrorSelect";
                cmd.Parameters.Add(new SqlParameter("@IdError", SqlDbType.Int));
                cmd.Parameters["@IdError"].Value = IdError;
                if (cmd.Connection.State == ConnectionState.Closed)
                {
                    cmd.Connection.Open();
                }
                dtr = cmd.ExecuteReader();
                if (dtr.HasRows == true)
                {
                    dtr.Read();
                    entidadError.idError = Convert.ToInt32(dtr[0]);
                    entidadError.nombreError = Convert.ToString(dtr[1]);
                    entidadError.descripcionError = Convert.ToString(dtr[2]);
                    entidadError.fhAltaError = Convert.ToDateTime(dtr[3]);
                    entidadError.fhBajaError = Convert.ToDateTime(dtr[4]);
                    entidadError.estadoError = Convert.ToInt32(dtr[5]);
                }
                cmd.Connection.Close();
                cmd.Parameters.Clear();
                return entidadError;
            }


            catch (SqlException)
            {
                throw new Exception();
            }
            finally
            {
                if (cnx.State == ConnectionState.Open)
                {
                    cmd.Connection.Close();
                }
                cmd.Parameters.Clear();
            }
        }
    }
} 


