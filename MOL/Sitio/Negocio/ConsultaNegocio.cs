using System.Data;
using Modelo;
using Persistencia;
namespace Negocio
{
    public class ConsultaNegocio
    {
        ConsultaPersistencia _ConsultaPersistencia = new ConsultaPersistencia();
        public bool InsertarConsulta(Consulta ConsultaNegocio)
        {
            return _ConsultaPersistencia.InsertarConsulta(ConsultaNegocio);
        }
        public bool ActualizarConsulta(Consulta ConsultaNegocio)
        {
            return _ConsultaPersistencia.ActualizarConsulta(ConsultaNegocio);
        }
        public bool EliminarConsulta(Consulta ConsultaNegocio)
        {
            return _ConsultaPersistencia.EliminarConsulta(ConsultaNegocio);
        }
        public DataTable ListarConsultas(string pFiltro)
        {
            return _ConsultaPersistencia.ListarConsulta(pFiltro);
        }
        public Consulta ConsultarConsulta(int IdConsulta)
        {
            return _ConsultaPersistencia.ConsultarConsulta(IdConsulta);
        }
    }
} 
