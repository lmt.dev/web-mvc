using System;


namespace Modelo
{
    public class Abogado
    {
        private int id;
        private string nombre, descripcion, telefono, email, direccion, localidad, provincia, otrosDatos;
        private DateTime fhAlta, fhBaja;
        private int estado;

        public int idAbogado
        {
            get { return id; }
            set { id = value; }
        }        
        public string nombreAbogado
        {
            get { return nombre; }
            set { nombre = value; }
        }        
        public string descripcionAbogado
        {
            get { return descripcion; }
            set { descripcion = value; }
        }        
        public DateTime fhAltaAbogado
        {
            get { return fhAlta; }
            set { fhAlta = value; }
        }        
        public DateTime fhBajaAbogado
        {
            get { return fhBaja; }
            set { fhBaja = value; }
        }        
        public int estadoAbogado
        {
            get { return estado; }
            set { estado = value; }
        }
        public string telefonoAbogado
        {
            get { return telefono; }
            set { telefono = value; }
        }
        public string emailAbogado
        {
            get { return email; }
            set { email = value; }
        }
        public string direccionAbogado
        {
            get { return direccion; }
            set { direccion = value; }
        }
        public string localidadAbogado
        {
            get { return localidad; }
            set { localidad = value; }
        }
        public string provinciaAbogado
        {
            get { return provincia; }
            set { provincia = value; }
        }
        public string otrosDatosAbogado
        {
            get { return otrosDatos; }
            set { otrosDatos = value; }
        }
    }
}
