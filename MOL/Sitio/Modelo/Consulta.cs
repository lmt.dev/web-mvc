using System;


namespace Modelo
{
    public class Consulta
    {
        private int id;
        private string nombre, descripcion, telefono, email;
        private DateTime fhAlta, fhBaja;
        private int estado;


        public int idConsulta
        {
            get { return id; }
            set { id = value; }
        }


        public string nombreConsulta
        {
            get { return nombre; }
            set { nombre = value; }
        }


        public string descripcionConsulta
        {
            get { return descripcion; }
            set { descripcion = value; }
        }


        public DateTime fhAltaConsulta
        {
            get { return fhAlta; }
            set { fhAlta = value; }
        }


        public DateTime fhBajaConsulta
        {
            get { return fhBaja; }
            set { fhBaja = value; }
        }


        public int estadoConsulta
        {
            get { return estado; }
            set { estado = value; }
        }


        public string telefonoConsulta
        {
            get { return telefono; }
            set { telefono = value; }
        }

        public string emailConsulta
        {
            get { return email; }
            set { email = value; }
        }
    }
}
