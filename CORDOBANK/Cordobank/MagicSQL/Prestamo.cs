﻿// Created for MagicSQL using MagicMaker [v.3.51.119.1109]

using System;
using MagicSQL;

namespace Cordobank
{
    public partial class Prestamo : ISUD<Prestamo>
    {
        public Prestamo() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdPrestamo { get; set; }

        public int IdCliente { get; set; }

        public decimal Monto { get; set; }

        public DateTime FHAlta { get; set; }

        public DateTime? FHBaja { get; set; }

        public int IdPrestamoEstado { get; set; }

        public string Motivo { get; set; }

        public string DevolucionTipo { get; set; }
    }
}