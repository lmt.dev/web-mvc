﻿// Created for MagicSQL using MagicMaker [v.3.51.119.1109]

using System;
using MagicSQL;

namespace Cordobank
{
  public partial class Patrocinador : ISUD<Patrocinador>
  {
    public Patrocinador() : base(1) { } // base(SPs_Version)

    // Properties
   
    public int IdPatrocinador { get; set; }

    public string Nombre { get; set; }

    public string Descripcion { get; set; }

    public DateTime? FHAlta { get; set; }
  }
}