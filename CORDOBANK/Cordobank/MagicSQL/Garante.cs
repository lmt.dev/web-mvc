﻿// Created for MagicSQL using MagicMaker [v.3.51.119.1109]

using System;
using MagicSQL;

namespace Cordobank
{
    public partial class Garante : ISUD<Garante>
    {
        public Garante() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdGarante { get; set; }

        public int IdCliente { get; set; }

        public DateTime FHAlta { get; set; }

        public string Nombre { get; set; }

        public string Apellido { get; set; }

        public DateTime FHNacimiento { get; set; }

        public string DNI { get; set; }

        public string CUIL { get; set; }

        public string Domicilio { get; set; }

        public string Barrio { get; set; }

        public string Localidad { get; set; }

        public string Provincia { get; set; }

        public int IdGaranteEstado { get; set; }

        public decimal Sueldo { get; set; }
    }
}