﻿<%@ Page Title="" Language="C#" MasterPageFile="~/principal.Master" AutoEventWireup="true" CodeBehind="ListarPartidos.aspx.cs" Inherits="Resultadistas.ListarPartidos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            text-align: left;
        }
        .auto-style2 {
            text-align: left;
            width: 74px;
        }
        .auto-style3 {
            text-align: left;
            width: 129px;
        }
        .auto-style4 {
            text-align: left;
            width: 136px;
        }
        .auto-style5 {
            text-align: left;
        }
        .auto-style12 {
            width: 296px;
            text-align: right;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="panel panel-default">
    <div class="panel-heading">
        Listar Partidos
        <asp:Label ID="lblIdUsuario" runat="server" Visible="False"></asp:Label>
    </div>
    <div class="panel-body">

<form id="form1" runat="server" style="">
        <div style="text-align: center;">
            <table border="0" style="margin: 0 auto;">
                <tr>
                    <td class="auto-style4">
                        &nbsp;</td>
                    <td class="auto-style1">
                        <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                    <td class="auto-style1">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style4">
                        <asp:Label ID="Label1" runat="server" Text="Fecha"></asp:Label>
                    </td>
                    <td class="auto-style1">
                        <asp:DropDownList ID="ddlFecha" runat="server" Height="35px" Width="189px" AutoPostBack="True" OnSelectedIndexChanged="ddlFecha_SelectedIndexChanged">
                        </asp:DropDownList>
                        <asp:Button ID="btnNuevoPartido" runat="server" OnClick="btnNuevoPartido_Click" Text="Nuevo Partido" CssClass="btn btn-info" Visible="False" />
                        <asp:Button ID="btnMostrarRespondido"  Text="Ver cargados"  runat="server" CssClass="btn btn-info" Visible="true" OnClick="btnMostrarRespondido_Click" />
                    </td>
                    <td class="auto-style1">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style4">
                        &nbsp;</td>
                    <td class="auto-style5">
                        <asp:Panel ID="pnlPregunta" runat="server" BackColor="#CCFFFF" BorderStyle="Double" ForeColor="Black" Width="666px">
                            <table style="width:100%;">
                                <tr>
                                    <td class="auto-style12">
                                        <asp:Label ID="lblPartidoNombre" runat="server">Ganador del partido</asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlPartidoEquipos" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlPartidoEquipos_SelectedIndexChanged" Width="181px">
                                        </asp:DropDownList>
                                        <asp:Label ID="lblIdPartido" runat="server" Visible="False"></asp:Label>
                                    </td>
                                    <td style="text-align: right">
                                        <asp:Button ID="btnCancelar" runat="server" CssClass="btn btn-info" Height="33px" OnClick="btnCancelar_Click" Text="x" ValidationGroup="ValidacionesForm" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style12">
                                        <asp:Label ID="lblGolesEquipo1" runat="server">Goles de Equipo 1</asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlGoles1" runat="server" OnSelectedIndexChanged="ddlGoles1_SelectedIndexChanged" AutoPostBack="True">
                                            <asp:ListItem>0</asp:ListItem>
                                            <asp:ListItem>1</asp:ListItem>
                                            <asp:ListItem>2</asp:ListItem>
                                            <asp:ListItem>3</asp:ListItem>
                                            <asp:ListItem>4</asp:ListItem>
                                            <asp:ListItem>5</asp:ListItem>
                                            <asp:ListItem>6</asp:ListItem>
                                            <asp:ListItem>7</asp:ListItem>
                                            <asp:ListItem>8</asp:ListItem>
                                            <asp:ListItem>9</asp:ListItem>
                                            <asp:ListItem>10</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style12">
                                        <asp:Label ID="lblGolesEquipo2" runat="server">Goles de Equipo 2</asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlGoles2" runat="server" OnSelectedIndexChanged="ddlGoles2_SelectedIndexChanged" AutoPostBack="True">
                                            <asp:ListItem>0</asp:ListItem>
                                            <asp:ListItem>1</asp:ListItem>
                                            <asp:ListItem>2</asp:ListItem>
                                            <asp:ListItem>3</asp:ListItem>
                                            <asp:ListItem>4</asp:ListItem>
                                            <asp:ListItem>5</asp:ListItem>
                                            <asp:ListItem>6</asp:ListItem>
                                            <asp:ListItem>7</asp:ListItem>
                                            <asp:ListItem>8</asp:ListItem>
                                            <asp:ListItem>9</asp:ListItem>
                                            <asp:ListItem>10</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                    <td class="auto-style1">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2">
                        &nbsp;</td>
                    <td colspan="2" class="auto-style2">
                        <asp:GridView ID="GridViewDatos"
                            runat="server" 
                            AllowSorting="true" 
                            AutoGenerateColumns="False" 
                            CellPadding="4"
                            ForeColor="#333333" 
                            GridLines="None"
                            OnRowDeleting="GridViewDatos_RowDeleting"
                            OnRowCommand="GridViewDatos_RowCommand" 
                            AllowPaging="True"
                            OnPageIndexChanging="GridViewDatos_PageIndexChanging" 
                            PageSize="50" Height="69px" Width="1056px" 
                            CssClass="auto-style3" OnSorting="GridViewDatos_Sorting">
                            
                            <HeaderStyle BackColor="#337ab7" Font-Bold="True" ForeColor="White" />
                                <EditRowStyle BackColor="#ffffcc" />
                                <EmptyDataRowStyle forecolor="Red" CssClass="table table-bordered" />
                                <emptydatatemplate>
                                    ¡No hay Partidos con los criterios seleccionados!  
                                </emptydatatemplate> 

                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                            <Columns>
                                <%--<asp:TemplateField HeaderText="Eliminar" ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="False"
                                            CommandName="Delete" ImageUrl="~/Imagenes/delete.gif"
                                            OnClientClick="return confirm('Esta seguro que desea eliminar el registro?');"
                                            Text="Eliminar" />
                                    </ItemTemplate>
                                </asp:TemplateField>--%>

                                <asp:ButtonField ButtonType="Image" CommandName="Actualizar"
                                    HeaderText="Editar" ImageUrl="~/Imagenes/lapiz.png" Text="Botón" />                                

                                <asp:BoundField DataField="id" HeaderText="Id" SortExpression="id"/>
                                <asp:BoundField DataField="nombreEquipo1" HeaderText="Equipo 1" SortExpression="nombreEquipo1"/>
                                <asp:BoundField DataField="nombreEquipo2" HeaderText="Equipo 2" SortExpression="nombreEquipo2"/>
                                
                            </Columns>
                            <EditRowStyle BackColor="#999999" />
                            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                            <SortedDescendingCellStyle BackColor="#FFFDF8" />
                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                                
                        </asp:GridView>
                    </td>
                </tr>              
                                 
                </table>
        </div>
    </form>

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
</asp:Content>
