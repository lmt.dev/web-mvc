﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CapaNegocio;
using CapaEntidad;

namespace Resultadistas
{
    public partial class InsertarActualizarFecha : System.Web.UI.Page
    {
        FechaNegocio fecNego = new FechaNegocio();
        Fecha fecEntidad = new Fecha();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ValidarLogin();
                MostrarDatos();
            }
        }
        private void ValidarLogin()
        {
            string strUser = Session["CodigoUsuario"].ToString();
            if (Convert.ToInt32(strUser) == 0)
            {
                Response.Redirect("~/Login.aspx");
            }
            else
            {
                lblIdUsuario.Text = strUser;
            }
        }
        private void MostrarDatos()
        {
            try
            {
                string strCD = Session["CodigoFecha"].ToString();
                if (strCD != "0")
                {
                    fecEntidad = fecNego.ConsultarFecha(strCD);
                    {
                        txtCodigo.Text = fecEntidad.IdFecha.ToString();
                        txtNombre.Text = fecEntidad.NombreFecha;
                        txtFechaCreacion.Text = fecEntidad.FechaInicioFecha;
                        txtFechaFin.Text = fecEntidad.FechaFinFecha;

                        txtFechaCreacion.Visible = true;
                        btnFechaCreacion.Visible = true;
                        calFechaCreacion.Visible = false;

                        txtFechaFin.Visible = true;
                        btnFechaFin.Visible = true;
                        calFechaFin.Visible = false;

                        btnGrabar.Visible = false;
                        btnActualizar.Visible = true;
                        btnCancelar.Visible = false;
                        
                        lblMensaje.Text = "Edición habilitada";
                    }
                }
                else
                {
                    lblMensaje.Text = "Alta habilitada";
                    btnGrabar.Visible = true;
                    btnActualizar.Visible = false;
                    btnCancelar.Visible = false;
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Error en MostrarDatos(): " + ex.Message;
            }
        }

        protected void btnFechaCreacion_Click(object sender, EventArgs e)
        {
            calFechaCreacion.Visible = true;
            txtFechaCreacion.Visible = false;
            btnFechaCreacion.Visible = false;
        }

        protected void calFechaCreacion_SelectionChanged(object sender, EventArgs e)
        {
            lblMensaje.Text = "";
            string strYear = calFechaCreacion.SelectedDate.Year.ToString();
            string strMonth = calFechaCreacion.SelectedDate.Month.ToString();
            string strDay = calFechaCreacion.SelectedDate.Day.ToString();
            if (strMonth.Length < 2)
            {
                strMonth = "0" + strMonth;
            }
            if (strDay.Length < 2)
            {
                strDay = "0" + strDay;
            }
            txtFechaCreacion.Text = strYear + "-" + strMonth + "-" + strDay + " 00:00:00";
            calFechaCreacion.Visible = false;
            txtFechaCreacion.Visible = true;
            btnFechaCreacion.Visible = true;

            DateTime dtIngresada, dtAyer = new DateTime();
            dtIngresada = calFechaCreacion.SelectedDate;
            dtAyer = DateTime.Now;
            dtAyer = dtAyer.AddDays(-1);

            if (dtIngresada < dtAyer)
            {
                txtFechaCreacion.Text = "";
                calFechaCreacion.Visible = true;
                txtFechaCreacion.Visible = false;
                btnFechaCreacion.Visible = false;
                lblMensaje.Text = "La fecha seleccionada debe ser superior a la fecha actual";
            }
        }

        protected void btnFechaFin_Click(object sender, EventArgs e)
        {
            calFechaFin.Visible = true;
            txtFechaFin.Visible = false;
            btnFechaFin.Visible = false;
        }

        protected void calFechaFin_SelectionChanged(object sender, EventArgs e)
        {
            lblMensaje.Text = "";
            string strYear = calFechaFin.SelectedDate.Year.ToString();
            string strMonth = calFechaFin.SelectedDate.Month.ToString();
            string strDay = calFechaFin.SelectedDate.Day.ToString();
            if (strMonth.Length < 2)
            {
                strMonth = "0" + strMonth;
            }
            if (strDay.Length < 2)
            {
                strDay = "0" + strDay;
            }
            txtFechaFin.Text = strYear + "-" + strMonth + "-" + strDay + " 00:00:00";
            calFechaFin.Visible = false;
            txtFechaFin.Visible = true;
            btnFechaFin.Visible = true;

            DateTime dtIngresada, dtAyer = new DateTime();
            dtIngresada = calFechaFin.SelectedDate;
            dtAyer = DateTime.Now;
            dtAyer = dtAyer.AddDays(-1);

            if (dtIngresada < dtAyer)
            {
                txtFechaFin.Text = "";
                calFechaFin.Visible = true;
                txtFechaFin.Visible = false;
                btnFechaFin.Visible = false;
                lblMensaje.Text = "La fecha seleccionada debe ser superior a la fecha actual";
            }
        }

        protected void btnGrabar_Click(object sender, EventArgs e)
        {
            CargarFecha();
        }

        protected void btnActualizar_Click(object sender, EventArgs e)
        {
            CargarFecha();
        }
        
        public void CargarFecha()
        {
            if (Page.IsValid)
            {
                try
                {
                    if (lblMensaje.Text == "")
                    {
                        if (txtCodigo.Text == "")
                        {
                            txtCodigo.Text = "0";
                        }
                        fecEntidad.IdFecha = Convert.ToInt32(txtCodigo.Text);
                        fecEntidad.NombreFecha = txtNombre.Text;
                        fecEntidad.FechaInicioFecha = txtFechaCreacion.Text;
                        fecEntidad.FechaFinFecha = txtFechaFin.Text;

                        if (fecNego.CargarFecha(fecEntidad) == true)
                        {
                            lblMensaje.Text = "Registro cargado Correctamente";
                            Response.Redirect("~/ListarFechas.aspx");
                        }
                        else
                        {
                            lblMensaje.Text = "Error de grabación de datos";
                        }
                    }
                }
                catch (Exception exc)
                {
                    lblMensaje.Text = exc.Message.ToString();
                }
            }
            else
            {
                lblMensaje.Text = "Por favor complete los datos requeridos";
            }
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            MostrarDatos();
        }

        protected void btnSalir_Click(object sender, EventArgs e)
        {
            Session.RemoveAll();
            Response.Redirect("~/ListarFechas.aspx");
        }
    }
}