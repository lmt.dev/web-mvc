﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CapaNegocio;
using CapaEntidad;
using System.Data;


namespace Resultadistas
{
    public partial class Perfil : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ValidarLogin();
                ListarEquipos();
            }
        }
        private void ValidarLogin()
        {
            string strUser = Session["CodigoUsuario"].ToString();
            if (Convert.ToInt32(strUser) == 0)
            {
                Response.Redirect("~/Login.aspx");
            }
            else
            {
                lblIdUsuario.Text = strUser;
                Usuario usuarioEntidad = new Usuario();
                UsuarioNegocio usuarioNeg = new UsuarioNegocio();
                usuarioEntidad = usuarioNeg.ConsultarUsuario(lblIdUsuario.Text);
                txtNombre.Text = usuarioEntidad.NombreUsuario;
                txtIdFacebook.Text = usuarioEntidad.IdFacebookUsuario.ToString();
                txtEmail.Text = usuarioEntidad.EmailUsuario;
                txtPassword.Text = usuarioEntidad.PasswordUsuario;
                txtConfirmePassword.Text = usuarioEntidad.PasswordUsuario;
                ddlEquipo.Text = usuarioEntidad.IdEquipoUsuario.ToString();
                if (Convert.ToInt32(strUser) == 3)
                {
                    //algo admin
                }
            }
        }

        private void ListarEquipos()
        {
            try
            {
                EquipoNegocio equipoNegocio = new EquipoNegocio();
                ddlEquipo.Items.Clear();
                ddlEquipo.DataSource = equipoNegocio.ListarEquipos("");
                ddlEquipo.DataTextField = "nombre";
                ddlEquipo.DataValueField = "id";
                ddlEquipo.DataBind();
            }
            catch (Exception ex)
            {
                //lblMensaje.Text = "Error en ListarFechas: " + ex.Message;
            }
        }

        protected void btnRegistro_Click(object sender, EventArgs e)
        {
            if (txtPassword.Text == txtConfirmePassword.Text)
            {
                Usuario usuarioRegistro = new Usuario();
                usuarioRegistro.IdUsuario = Convert.ToInt32(lblIdUsuario.Text);
                usuarioRegistro.NombreUsuario = txtNombre.Text;
                usuarioRegistro.IdFacebookUsuario = txtIdFacebook.Text;
                usuarioRegistro.EmailUsuario = txtEmail.Text;
                usuarioRegistro.PasswordUsuario = txtPassword.Text;
                usuarioRegistro.IdEquipoUsuario = Convert.ToInt32(ddlEquipo.Text);
                UsuarioNegocio usuarioNegocio = new UsuarioNegocio();
                lblError.Text = usuarioNegocio.ActualizarUsuario(usuarioRegistro);
                //int vIdUsuario = usuarioNegocio.RegistrarUsuario(usuarioRegistro);
                //if (vIdUsuario > 0)
                //{
                //    Session["CodigoUsuario"] = vIdUsuario.ToString();
                //    Response.Redirect("~/ListarPartidos.aspx");
                //}
                //else
                //{
                //    lblError.Text = "Las credenciales ya corresponden a otro usuario";
                //}
            }
            else
            {
                lblError.Text = "Password y Confirmación de password deben coincidir";
            }
        }
    }
}