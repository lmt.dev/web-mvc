﻿<%@ Page Title="" Language="C#" MasterPageFile="~/principal.Master" AutoEventWireup="true" CodeBehind="InsertarActualizarPartido.aspx.cs" Inherits="Resultadistas.InsertarActualizarPartido" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="panel panel-default">
    <div class="panel-heading">
    Insertar/Actualizar Partido
        <asp:Label ID="lblIdUsuario" runat="server" Visible="False"></asp:Label>
    </div>
    <div class="panel-body">

    <form id="form1" runat="server" style="">
        <div style="text-align: center;">
            <table border="0" style="margin: 0 auto;">
                <tr>
                    <td colspan="2">
                        <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style3">
                        <asp:Label ID="Label1" runat="server" Text="Codigo   "></asp:Label>
                    </td>
                    <td style="text-align: left" class="auto-style1">
                        <asp:TextBox ID="txtCodigo" runat="server" MaxLength="10" CssClass="form-control" Width="300px" placeholder="Codigo" style="margin-right: 0px" Enabled="False"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">
                        &nbsp;</td>
                    <td style="text-align: left" class="auto-style1">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style3">
                        <asp:Label ID="Label2" runat="server" Text="Fecha"></asp:Label>
                    </td>
                    <td style="text-align: left" class="auto-style1">
                        <asp:DropDownList ID="ddlFecha" runat="server" Enabled="False" Height="30px" Width="300px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">
                        &nbsp;</td>
                    <td style="text-align: left" class="auto-style1">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style3">
                        <asp:Label ID="Label3" runat="server" Text="Equipo 1"></asp:Label>
                    </td>
                    <td style="text-align: left" class="auto-style1">
                        <asp:DropDownList ID="ddlEquipo1" runat="server" Height="30px" Width="300px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">
                        &nbsp;</td>
                    <td style="text-align: left" class="auto-style1">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlEquipo1" ErrorMessage="RequiredFieldValidator" ForeColor="Red" ValidationGroup="ValidacionesForm">El campo &#39;Equipo 1&#39; no puede ser vacío</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">
                        <asp:Label ID="Label4" runat="server" Text="Equipo 2"></asp:Label>
                    </td>
                    <td style="text-align: left" class="auto-style1">
                        <asp:DropDownList ID="ddlEquipo2" runat="server" Height="30px" Width="300px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">
                        &nbsp;</td>
                    <td style="text-align: left" class="auto-style1">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlEquipo2" ErrorMessage="RequiredFieldValidator" ForeColor="Red" ValidationGroup="ValidacionesForm">El campo &#39;Equipo 2&#39; no puede ser vacío</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">
                        &nbsp;</td>
                    <td style="text-align: left" class="auto-style1">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Button ID="btnGrabar" runat="server" OnClick="btnGrabar_Click" Text="Grabar" CssClass="btn btn-info" ValidationGroup="ValidacionesForm" />
                        <asp:Button ID="btnActualizar" runat="server" OnClick="btnActualizar_Click" Text="Actualizar" CssClass="btn btn-info" ValidationGroup="ValidacionesForm" />
                        <asp:Button ID="btnCancelar" runat="server" OnClick="btnCancelar_Click" Text="Cancelar" CssClass="btn btn-info" />
                        <asp:Button ID="btnSalir" runat="server" OnClick="btnSalir_Click" Text="Salir" CssClass="btn btn-info" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="auto-style2">
                        &nbsp;</td>
                </tr>
                </table>
        </div>
 
    </form>

</asp:Content>
