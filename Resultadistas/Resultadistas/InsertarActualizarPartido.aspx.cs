﻿using System;
using System.Web.UI;
using CapaNegocio;
using CapaEntidad;
namespace Resultadistas
{
    public partial class InsertarActualizarPartido : System.Web.UI.Page
    {
        PartidoNegocio partidoNegocio = new PartidoNegocio();
        Partido partidoEntidad = new Partido();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ValidarLogin();
                MostrarDatos();
            }
        }
        private void ValidarLogin()
        {
            string strUser = Session["CodigoUsuario"].ToString();
            if (Convert.ToInt32(strUser) == 0)
            {
                Response.Redirect("~/Login.aspx");
            }
            else
            {
                lblIdUsuario.Text = strUser;
            }
        }
        private void MostrarDatos()
        {
            try
            {
                ListarFechas();
                ListarEquipos();
                string strCD = Session["CodigoPartido"].ToString();
                string strCDFecha = Session["CodigoFecha"].ToString();
                if (strCD != "0")
                {
                    partidoEntidad = partidoNegocio.ConsultarPartido(strCD);
                    {
                        txtCodigo.Text = partidoEntidad.IdPartido.ToString();
                        btnGrabar.Visible = false;
                        btnActualizar.Visible = true;
                        btnCancelar.Visible = false;
                    }
                }
                else
                {
                    txtCodigo.Text = "0";
                    ddlFecha.Text = strCDFecha;
                    btnGrabar.Visible = true;
                    btnActualizar.Visible = false;
                    btnCancelar.Visible = false;
                    lblMensaje.Text = "Alta habilitada";
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Error en MostrarDatos(): " + ex.Message;
            }
        }

        protected void btnGrabar_Click(object sender, EventArgs e)
        {
            CargarPartido();
        }

        protected void btnActualizar_Click(object sender, EventArgs e)
        {
            CargarPartido();
        }
        public void Validaciones()
        {
            if (ddlEquipo1.Text == ddlEquipo2.Text)
            {
                lblMensaje.Text = "Equipo 1 y Equipo 2 no pueden ser iguales";
            }
        }
        private void CargarPartido()
        {
            lblMensaje.Text = "";
            if (Page.IsValid)
            {
                try
                {
                    Validaciones();
                    if (lblMensaje.Text == "")
                    {
                        FechaNegocio fechaNegocio = new FechaNegocio();
                        Fecha fechaEntidad = new Fecha();
                        fechaEntidad = fechaNegocio.ConsultarFecha(ddlFecha.Text);
                        partidoEntidad.FechaPartido = fechaEntidad;
                        partidoEntidad.IdEquipo1Partido = Convert.ToInt32(ddlEquipo1.Text);
                        partidoEntidad.IdEquipo2Partido = Convert.ToInt32(ddlEquipo2.Text);

                        if (partidoNegocio.CargarPartido(partidoEntidad) == true)
                        {
                            lblMensaje.Text = "Registro cargado correctamente";
                            Session["CodigoFecha"] = ddlFecha.Text;
                            Response.Redirect("~/ListarPartidos.aspx");
                        }
                        else
                        {
                            lblMensaje.Text = "Error de grabación de datos";
                        }
                    }
                }
                catch (Exception exc)
                {
                    lblMensaje.Text = exc.Message.ToString();
                }
            }
            else
            {
                lblMensaje.Text = "Por favor complete los datos requeridos";
            }
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            MostrarDatos();
        }

        protected void btnSalir_Click(object sender, EventArgs e)
        {
            Session.RemoveAll();
            Session["CodigoFecha"] = ddlFecha.Text;
            Response.Redirect("~/ListarPartidos.aspx");
        }


        private void ListarFechas()
        {
            try
            {
                FechaNegocio fechaNegocio = new FechaNegocio();
                ddlFecha.Items.Clear();
                ddlFecha.DataSource = fechaNegocio.ListarFechas();
                ddlFecha.DataTextField = "nombre";
                ddlFecha.DataValueField = "id";
                ddlFecha.DataBind();
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Error en ListarFechas: " + ex.Message;
            }
        }

        private void ListarEquipos()
        {
            try
            {
                EquipoNegocio equipoNegocio = new EquipoNegocio();
                ddlEquipo1.Items.Clear();
                ddlEquipo2.Items.Clear();

                ddlEquipo1.DataSource = equipoNegocio.ListarEquipos("");
                ddlEquipo1.DataTextField = "nombre";
                ddlEquipo1.DataValueField = "id";
                ddlEquipo1.DataBind();

                ddlEquipo2.DataSource = equipoNegocio.ListarEquipos("");
                ddlEquipo2.DataTextField = "nombre";
                ddlEquipo2.DataValueField = "id";
                ddlEquipo2.DataBind();
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Error en ListarFechas: " + ex.Message;
            }
        }
    }
}