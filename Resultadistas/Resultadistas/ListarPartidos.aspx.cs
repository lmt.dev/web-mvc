﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using CapaNegocio;
using CapaEntidad;
using System.Data;

namespace Resultadistas
{
    public partial class ListarPartidos : System.Web.UI.Page
    {
        PartidoNegocio partidoNegocio = new PartidoNegocio();
        Partido partidoEntidad = new Partido();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                MostrarPanel(false);
                if (!Page.IsPostBack)
                {
                    ValidarLogin();
                    ListarFechas();
                    ListarDatos();
                }
            }
            catch (Exception ex)
            {
                lblError.Text = "Error en carga de formulario: " + ex.Message;
            }
        }

        private void ValidarLogin()
         {
            string strUser = Session["CodigoUsuario"].ToString();
            if (Convert.ToInt32(strUser) == 0)
            {
                Response.Redirect("~/Login.aspx");
            }
            else
            {
                lblIdUsuario.Text = strUser ;
                if (Convert.ToInt32(strUser) == 3)
                {
                    btnNuevoPartido.Visible = true;
                }
            }
         }
                        

        private void ListarDatos()
        {
            try
            {
                lblError.Text = "";
                int vModo = 1;
                if (btnMostrarRespondido.Text == "Ver cargados")
                {
                    vModo = 2;
                }
                else
                {
                    vModo = 1;
                }
                GridViewDatos.DataSource = partidoNegocio.ListarPartidos(Convert.ToInt32(ddlFecha.Text), Convert.ToInt32(lblIdUsuario.Text), vModo);
                GridViewDatos.DataBind();
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message + " " + ex.StackTrace;
            }
        }

        protected void btnNuevoPartido_Click(object sender, EventArgs e)
        {
            Session["CodigoPartido"] = "0";
            Session["CodigoFecha"] = ddlFecha.Text;
            Response.Redirect("~/InsertarActualizarPartido.aspx");
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            ListarDatos();
        }

        protected void GridViewDatos_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                GridViewRow row = GridViewDatos.Rows[e.RowIndex];
                string strcod = Convert.ToString(row.Cells[2].Text);

                {
                    partidoEntidad.IdPartido = Convert.ToInt32(strcod);
                }
                if (partidoNegocio.EliminarPartido(partidoEntidad) == true)
                {
                    ListarDatos();
                }
                else
                {
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        protected void GridViewDatos_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                short indicefila;
                indicefila = Convert.ToInt16(e.CommandArgument);
                string strIdPartido;
                string strEquipo1;
                string strEquipo2;
                if (indicefila >= 0 & indicefila < GridViewDatos.Rows.Count)
                {
                    //Warning: cambiar GridViewDatos.Rows[indicefila].Cells[3].Text por GridViewDatos.Rows[indicefila].Cells[2].Text al corregir look&feel
                    //WArning 2: 05/10/2017: Se pide cambio para que no muestre botón eliminar, bajar a 1 del array
                    strIdPartido = GridViewDatos.Rows[indicefila].Cells[1].Text;
                    strEquipo1 = GridViewDatos.Rows[indicefila].Cells[2].Text;
                    strEquipo2 = GridViewDatos.Rows[indicefila].Cells[3].Text;
                    
                    if (e.CommandName == "Actualizar")
                    {
                        MostrarPanel(true);
                        ddlPartidoEquipos.Items.Clear();
                        ddlPartidoEquipos.Items.Add("Empate");
                        ddlPartidoEquipos.Items.Add("Gana "+strEquipo1);
                        ddlPartidoEquipos.Items.Add("Gana " + strEquipo2);
                        lblIdPartido.Text = strIdPartido;
                        lblGolesEquipo1.Text = "Goles de " + strEquipo1;
                        lblGolesEquipo2.Text = "Goles de " + strEquipo2;
                        lblPartidoNombre.Text = "Partido: "+strEquipo1+" - "+strEquipo2;
                        ddlGoles1.SelectedIndex = 0;
                        ddlGoles2.SelectedIndex = 0;
                        if (btnMostrarRespondido.Text != "Ver cargados")
                        {
                            ConsultarRespuestaPartido();
                        }
                    }

                    
                }
            }
            catch (Exception)
            {
            }
        }

        protected void GridViewDatos_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                GridViewDatos.PageIndex = e.NewPageIndex;
                GridViewDatos.DataBind();
                ListarDatos();
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
            }
        }
        protected void GridViewDatos_Sorting(object sender, GridViewSortEventArgs e)
        {
            int vModo = 1;
            if (btnMostrarRespondido.Text == "Ver cargados")
            {
                vModo = 2;
            }
            else
            {
                vModo = 1;
            }
            DataTable dtbl = new DataTable();
            dtbl = partidoNegocio.ListarPartidos(Convert.ToInt32(ddlFecha.Text), Convert.ToInt32(lblIdUsuario.Text), vModo);//here get the datatable from db
            if (ViewState["Sort Order"] == null)
            {
                dtbl.DefaultView.Sort = e.SortExpression + " DESC";
                GridViewDatos.DataSource = dtbl;
                GridViewDatos.DataBind();
                ViewState["Sort Order"] = "DESC";
            }
            else
            {
                dtbl.DefaultView.Sort = e.SortExpression + "" + " ASC";
                GridViewDatos.DataSource = dtbl;
                GridViewDatos.DataBind();
                ViewState["Sort Order"] = null;
            }
        }

        private void ListarFechas()
        {
            try
            {
                FechaNegocio fechaNegocio = new FechaNegocio();
                ddlFecha.Items.Clear();
                ddlFecha.DataSource = fechaNegocio.ListarFechas();
                ddlFecha.DataTextField = "nombre";
                ddlFecha.DataValueField = "id";
                ddlFecha.DataBind();
            }
            catch (Exception ex)
            {
                lblError.Text = "Error en ListarFechas: " + ex.Message;
            }
        }

        protected void ddlFecha_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListarDatos();
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            MostrarPanel(false);
        }

        private void MostrarPanel(bool vValor)
        {
            pnlPregunta.Visible = vValor;
        }

        protected void ddlGoles1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ValidarRespuestaPartido();
        }

        protected void ddlGoles2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ValidarRespuestaPartido();
        }
        protected void ddlPartidoEquipos_SelectedIndexChanged(object sender, EventArgs e)
        {
            ValidarRespuestaPartido();
        }

        public void ValidarRespuestaPartido()
        {
            int v_golesEquipo1 = ddlGoles1.SelectedIndex;
            int v_golesEquipo2 = ddlGoles2.SelectedIndex;
            int v_EquipoGanador = ddlPartidoEquipos.SelectedIndex;
            lblError.Text = "";
            if (v_EquipoGanador == 0)
            {
                if (v_golesEquipo1==v_golesEquipo2)
                {
                    CargarRespuestaPartido();
                }
                else
                {
                    lblError.Text = "La selección de goles debe ser acorde al equipo ganador seleccionado";
                }
            }
            else
                {
                    if (v_golesEquipo1==v_golesEquipo2)
                    {
                        lblError.Text = "La selección de goles debe ser acorde al equipo ganador seleccionado";
                    }
                    else
                    {
                        if (v_golesEquipo1 < v_golesEquipo2)
                        {
                            if (ddlPartidoEquipos.SelectedIndex == 2)
                            {
                                CargarRespuestaPartido();
                            }
                            else
                            {
                                lblError.Text = "La selección de goles debe ser acorde al equipo ganador seleccionado";
                            }
                        }
                        else
                        {
                            if (ddlPartidoEquipos.SelectedIndex == 1)
                            {
                                CargarRespuestaPartido();
                            }
                            else
                            {
                                lblError.Text = "La selección de goles debe ser acorde al equipo ganador seleccionado";
                            }
                        }
                    }
                    
                }
            MostrarPanel(true);
        }
        private void CargarRespuestaPartido()
        {
            RespuestaPartido rp = new RespuestaPartido();
            rp.IdusuarioRespuestaPartido = Convert.ToInt32(lblIdUsuario.Text);
            rp.IdPartidoRespuestaPartido = Convert.ToInt32(lblIdPartido.Text);
            rp.GolesEquipo1RespuestaPartido = Convert.ToInt32(ddlGoles1.SelectedIndex);
            rp.GolesEquipo2RespuestaPartido = Convert.ToInt32(ddlGoles2.SelectedIndex);
            rp.EquipoGanadorRespuestaPartido = Convert.ToInt32(ddlPartidoEquipos.SelectedIndex);
            RespuestaPartidoNegocio rpNeg = new RespuestaPartidoNegocio();
            rpNeg.CargarRespuestaPartido(rp);
            ListarDatos();
        }

        private void ConsultarRespuestaPartido()
        {
            RespuestaPartido rp = new RespuestaPartido();
            RespuestaPartidoNegocio rpNeg = new RespuestaPartidoNegocio();
            rp = rpNeg.ConsultarRespuestaPartido(Convert.ToInt32(lblIdPartido.Text), Convert.ToInt32(lblIdUsuario.Text));
            ddlGoles1.SelectedIndex = rp.GolesEquipo1RespuestaPartido;
            ddlGoles2.SelectedIndex = rp.GolesEquipo2RespuestaPartido;
            ddlPartidoEquipos.SelectedIndex = rp.EquipoGanadorRespuestaPartido;
        }

        protected void btnMostrarRespondido_Click(object sender, EventArgs e)
        {
            if (btnMostrarRespondido.Text == "Ver cargados")
            {
                btnMostrarRespondido.Text = "Ver pendientes";
            }
            else
            {
                btnMostrarRespondido.Text = "Ver cargados";
            }
            ListarDatos();
        }

        

        
    }
}