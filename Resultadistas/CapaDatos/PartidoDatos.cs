﻿using System;
using System.Data;
using CapaEntidad;
using System.Data.SqlClient;
namespace CapaDatos
{
    public class PartidoDatos
    {
        SqlConnection cnx;
        Partido PartidoEntidad = new Partido();
        Conexion MiConexi = new Conexion();
        SqlCommand cmd = new SqlCommand();
        bool vexito;
        public PartidoDatos()
        {
            cnx = new SqlConnection(MiConexi.GetConex());
        }

        public bool CargarPartido(Partido PartidoEntidad)
        {
            cmd.Connection = cnx;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Proc_CargarPartido";
            try
            {
                cmd.Parameters.Add(new SqlParameter("@p_idFecha", SqlDbType.Int));
                cmd.Parameters["@p_idFecha"].Value = PartidoEntidad.FechaPartido.IdFecha;

                cmd.Parameters.Add(new SqlParameter("@p_idEquipo1", SqlDbType.Int));
                cmd.Parameters["@p_idEquipo1"].Value = PartidoEntidad.IdEquipo1Partido;

                cmd.Parameters.Add(new SqlParameter("@p_idEquipo2", SqlDbType.Int));
                cmd.Parameters["@p_idEquipo2"].Value = PartidoEntidad.IdEquipo2Partido;
                
                cnx.Open();
                cmd.ExecuteNonQuery();
                vexito = true;
            }
            catch (SqlException)
            {
                vexito = false;
            }
            finally
            {
                if (cnx.State == ConnectionState.Open)
                {
                    cnx.Close();
                }
                cmd.Parameters.Clear();
            }
            return vexito;
        }


        public bool EliminarPartido(Partido PartidoEntidad)
        {
            cmd.Connection = cnx;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "proc_PartidoEliminar";
            try
            {
                cmd.Parameters.Add(new SqlParameter("@codigo", SqlDbType.VarChar, 5));
                cmd.Parameters["@codigo"].Value = PartidoEntidad.IdPartido;
                cnx.Open();
                cmd.ExecuteNonQuery();
                vexito = true;
            }
            catch (SqlException)
            {
                vexito = false;
            }
            finally
            {
                if (cnx.State == ConnectionState.Open)
                {
                    cnx.Close();
                }
                cmd.Parameters.Clear();
            }
            return vexito;
        }

        public DataTable ListarPartidos(int idFecha, int idUsuario, int modo)
        {
            DataSet dts = new DataSet();
            try
            {
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "Proc_ListarPartidos";

                cmd.Parameters.Add(new SqlParameter("@p_idFecha", SqlDbType.Int));
                cmd.Parameters["@p_idFecha"].Value = idFecha;

                cmd.Parameters.Add(new SqlParameter("@p_idUsuario", SqlDbType.Int));
                cmd.Parameters["@p_idUsuario"].Value = idUsuario;

                cmd.Parameters.Add(new SqlParameter("@p_modo", SqlDbType.Int));
                cmd.Parameters["@p_modo"].Value = modo;

                SqlDataAdapter miada;
                miada = new SqlDataAdapter(cmd);
                miada.Fill(dts, "Partidos");
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmd.Parameters.Clear();
            }
            return (dts.Tables["Partidos"]);
        }

        public Partido ConsultarPartido(string codigo)
        {
            try
            {
                SqlDataReader dtr;
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "Proc_ConsultarPartidoPorId";
                cmd.Parameters.Add(new SqlParameter("@p_idPartido", SqlDbType.Int));
                cmd.Parameters["@p_idPartido"].Value = Convert.ToInt32(codigo);
                if (cnx.State == ConnectionState.Closed)
                {
                    cnx.Open();
                }
                dtr = cmd.ExecuteReader();
                if (dtr.HasRows == true)
                {
                    dtr.Read();
                }
                cnx.Close();
                cmd.Parameters.Clear();
                return PartidoEntidad;
            }
            catch (SqlException)
            {
                throw new Exception();
            }
            finally
            {
                if (cnx.State == ConnectionState.Open)
                {
                    cnx.Close();
                }
                cmd.Parameters.Clear();
            }
        }
    }
}