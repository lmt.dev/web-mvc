﻿using System;
using System.Data;
using CapaEntidad;
using System.Data.SqlClient;
namespace CapaDatos
{
    public class RespuestaPartidoDatos
    {
        SqlConnection cnx;
        RespuestaPartido RespuestaPartidoEntidad = new RespuestaPartido();
        Conexion MiConexi = new Conexion();
        SqlCommand cmd = new SqlCommand();
        bool vexito;
        public RespuestaPartidoDatos()
        {
            cnx = new SqlConnection(MiConexi.GetConex());
        }

        public bool CargarRespuestaPartido(RespuestaPartido RespuestaPartidoEntidad)
        {
            cmd.Connection = cnx;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Proc_CargarRespuestaPartido";
            try
            {

                cmd.Parameters.Add(new SqlParameter("@p_idUsuario", SqlDbType.Int));
                cmd.Parameters["@p_idUsuario"].Value = RespuestaPartidoEntidad.IdusuarioRespuestaPartido;

                cmd.Parameters.Add(new SqlParameter("@p_idPartido", SqlDbType.Int));
                cmd.Parameters["@p_idPartido"].Value = RespuestaPartidoEntidad.IdPartidoRespuestaPartido;

                cmd.Parameters.Add(new SqlParameter("@p_golesEquipo1", SqlDbType.Int));
                cmd.Parameters["@p_golesEquipo1"].Value = RespuestaPartidoEntidad.GolesEquipo1RespuestaPartido;

                cmd.Parameters.Add(new SqlParameter("@p_golesEquipo2", SqlDbType.Int));
                cmd.Parameters["@p_golesEquipo2"].Value = RespuestaPartidoEntidad.GolesEquipo2RespuestaPartido;

                cmd.Parameters.Add(new SqlParameter("@p_EquipoGanador", SqlDbType.Int));
                cmd.Parameters["@p_EquipoGanador"].Value = RespuestaPartidoEntidad.EquipoGanadorRespuestaPartido;
                cnx.Open();
                cmd.ExecuteNonQuery();
                vexito = true;
            }
            catch (SqlException)
            {
                vexito = false;
            }
            finally
            {
                if (cnx.State == ConnectionState.Open)
                {
                    cnx.Close();
                }
                cmd.Parameters.Clear();
            }
            return vexito;
        }


        public bool EliminarRespuestaPartido(RespuestaPartido RespuestaPartidoEntidad)
        {
            cmd.Connection = cnx;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "proc_RespuestaPartidoEliminar";
            try
            {
                cmd.Parameters.Add(new SqlParameter("@codigo", SqlDbType.VarChar, 5));
                cmd.Parameters["@codigo"].Value = RespuestaPartidoEntidad.IdRespuestaPartido;
                cnx.Open();
                cmd.ExecuteNonQuery();
                vexito = true;
            }
            catch (SqlException)
            {
                vexito = false;
            }
            finally
            {
                if (cnx.State == ConnectionState.Open)
                {
                    cnx.Close();
                }
                cmd.Parameters.Clear();
            }
            return vexito;
        }

        public DataTable ListarRespuestaPartidos(int idFecha)
        {
            DataSet dts = new DataSet();
            try
            {
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "Proc_ListarRespuestaPartidos";
                cmd.Parameters.Add(new SqlParameter("@p_idFecha", SqlDbType.Int));
                cmd.Parameters["@p_idFecha"].Value = idFecha;
                SqlDataAdapter miada;
                miada = new SqlDataAdapter(cmd);
                miada.Fill(dts, "RespuestaPartidos");
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmd.Parameters.Clear();
            }
            return (dts.Tables["RespuestaPartidos"]);
        }

        public RespuestaPartido ConsultarRespuestaPartido(int idPartido, int idUsuario)
        {
            try
            {
                SqlDataReader dtr;
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "Proc_ConsultarRespuestaPartido";

                cmd.Parameters.Add(new SqlParameter("@p_idPartido", SqlDbType.Int));
                cmd.Parameters["@p_idPartido"].Value = idPartido;

                cmd.Parameters.Add(new SqlParameter("@p_idUsuario", SqlDbType.Int));
                cmd.Parameters["@p_idUsuario"].Value = idUsuario;

                if (cnx.State == ConnectionState.Closed)
                {
                    cnx.Open();
                }
                dtr = cmd.ExecuteReader();
                if (dtr.HasRows == true)
                {
                    dtr.Read();
                    RespuestaPartidoEntidad.IdRespuestaPartido = Convert.ToInt32(dtr[0]);
                    RespuestaPartidoEntidad.IdPartidoRespuestaPartido = Convert.ToInt32(dtr[1]);
                    RespuestaPartidoEntidad.IdusuarioRespuestaPartido = Convert.ToInt32(dtr[2]);
                    RespuestaPartidoEntidad.GolesEquipo1RespuestaPartido = Convert.ToInt32(dtr[3]);
                    RespuestaPartidoEntidad.GolesEquipo2RespuestaPartido = Convert.ToInt32(dtr[4]);
                    RespuestaPartidoEntidad.EquipoGanadorRespuestaPartido = Convert.ToInt32(dtr[5]);
                }
                cnx.Close();
                cmd.Parameters.Clear();
                return RespuestaPartidoEntidad;
            }
            catch (SqlException)
            {
                throw new Exception();
            }
            finally
            {
                if (cnx.State == ConnectionState.Open)
                {
                    cnx.Close();
                }
                cmd.Parameters.Clear();
            }
        }
    }
}