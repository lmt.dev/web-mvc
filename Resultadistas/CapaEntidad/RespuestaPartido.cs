﻿namespace CapaEntidad
{
    public class RespuestaPartido
    {
        private int id, idusuario, idPartido, golesEquipo1, golesEquipo2, equipoGanador;

        public int EquipoGanadorRespuestaPartido
        {
            get { return equipoGanador; }
            set { equipoGanador = value; }
        }


        public int GolesEquipo2RespuestaPartido
        {
            get { return golesEquipo2; }
            set { golesEquipo2 = value; }
        }

        public int GolesEquipo1RespuestaPartido
        {
            get { return golesEquipo1; }
            set { golesEquipo1 = value; }
        }

        public int IdPartidoRespuestaPartido
        {
            get { return idPartido; }
            set { idPartido = value; }
        }

        public int IdusuarioRespuestaPartido
        {
            get { return idusuario; }
            set { idusuario = value; }
        }

        public int IdRespuestaPartido
        {
            get { return id; }
            set { id = value; }
        } 
    }
}
