﻿namespace CapaEntidad
{
    public class Fecha
    {
        private int id, activo;
        private string nombre, fechaInicio, fechaFin;

        public string NombreFecha
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public string FechaFinFecha
        {
            get { return fechaFin; }
            set { fechaFin = value; }
        }

        public string FechaInicioFecha
        {
            get { return fechaInicio; }
            set { fechaInicio = value; }
        }
        public int IdFecha
        {
            get { return id; }
            set { id = value; }
        }

        public int ActivoFecha
        {
            get { return activo; }
            set { activo = value; }
        }
        
    }
}
