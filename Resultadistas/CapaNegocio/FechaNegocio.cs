﻿using System.Data;
using CapaEntidad;
using CapaDatos;
namespace CapaNegocio
{
    public class FechaNegocio
    {
        FechaDatos _FechaDatos = new FechaDatos();

        public bool CargarFecha(Fecha FechaEntidad)
        {
            return _FechaDatos.CargarFecha(FechaEntidad);
        }

        public bool EliminarFecha(Fecha FechaEntidad)
        {
            return _FechaDatos.EliminarFecha(FechaEntidad);
        }

        public DataTable ListarFechas()
        {
            return _FechaDatos.ListarFechas();
        }

        public Fecha ConsultarFecha(string codigo)
        {
            return _FechaDatos.ConsultarFecha(codigo);
        }
    }
}