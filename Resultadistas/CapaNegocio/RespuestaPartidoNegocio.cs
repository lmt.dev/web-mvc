﻿using System.Data;
using CapaEntidad;
using CapaDatos;
namespace CapaNegocio
{
    public class RespuestaPartidoNegocio
    {
        RespuestaPartidoDatos _RespuestaPartidoDatos = new RespuestaPartidoDatos();

        public bool CargarRespuestaPartido(RespuestaPartido RespuestaPartidoEntidad)
        {
            return _RespuestaPartidoDatos.CargarRespuestaPartido(RespuestaPartidoEntidad);
        }

        public bool EliminarRespuestaPartido(RespuestaPartido RespuestaPartidoEntidad)
        {
            return _RespuestaPartidoDatos.EliminarRespuestaPartido(RespuestaPartidoEntidad);
        }

        public DataTable ListarRespuestaPartidos(int idFecha)
        {
            return _RespuestaPartidoDatos.ListarRespuestaPartidos(idFecha);
        }

        public RespuestaPartido ConsultarRespuestaPartido(int idPartido, int idUsuario)
        {
            return _RespuestaPartidoDatos.ConsultarRespuestaPartido(idPartido, idUsuario);
        }
    }
}