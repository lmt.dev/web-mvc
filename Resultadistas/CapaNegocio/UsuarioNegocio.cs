﻿using System.Data;
using CapaEntidad;
using CapaDatos;
namespace CapaNegocio
{
    public class UsuarioNegocio
    {
        UsuarioDatos _UsuarioDatos = new UsuarioDatos();

        public bool CargarUsuario(Usuario UsuarioEntidad)
        {
            return _UsuarioDatos.CargarUsuario(UsuarioEntidad);
        }

        public bool EliminarUsuario(Usuario UsuarioEntidad)
        {
            return _UsuarioDatos.EliminarUsuario(UsuarioEntidad);
        }

        public DataTable ListarUsuarios(string p_nombre)
        {
            return _UsuarioDatos.ListarUsuarios(p_nombre);
        }

        public Usuario ConsultarUsuario(string codigo)
        {
            return _UsuarioDatos.ConsultarUsuario(codigo);
        }

        public int RegistrarUsuario(Usuario usuarioRegistro)
        {
            return _UsuarioDatos.RegistrarUsuario(usuarioRegistro);
        }

        public string ActualizarUsuario(Usuario usuarioRegistro)
        {
            return _UsuarioDatos.ActualizarUsuario(usuarioRegistro);
        }

        public int LoginUsuario(Usuario usuarioRegistro)
        {
            return _UsuarioDatos.LoginUsuario(usuarioRegistro);
        }
    }
}