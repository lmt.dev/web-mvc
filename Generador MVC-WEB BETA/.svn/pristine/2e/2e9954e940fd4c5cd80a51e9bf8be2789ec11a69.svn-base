﻿using CGUsuarios.Common;
using Code;
using MagicSQL;

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;
using static Code.Utils;

namespace Pages
{
    public partial class PendienteFacturar : BasePage
    {
        // Modelo que se utilizará en la página
        private CGADM.PendienteFacturar model = new CGADM.PendienteFacturar();

        // Id del Modelo
        private int idModel = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            // Id del Modelo recuperado del Request
            if (Request["cryptoID"] != null)
            {
                try
                {
                    idModel = Request["cryptoID"].ToIntID();
                }
                catch (Exception)
                {
                    NavigateTo("~/Content/html/Alert.html");
                    return;
                }
            }

            if (!IsPostBack)
            {
                lblTitulo.Text = Title;
                txtImporte.Text = "0";
                txtEstado.Disabled = true;

                if (SecurityManager.IdAplicacion == idAplicacionDistribuidor)
                {
                    var distribuidor = new CGADM.Distribuidor().Select((int)SecurityManager.IdPropietario);
                    ddlDistribuidor.Items.Add(new ListItem(distribuidor.RazonSocial, distribuidor.IdDistribuidor.ToCryptoID()));
                    ddlDistribuidor.Enabled = false;
                }
                else
                {
                    foreach (CGADM.Distribuidor m in new CGADM.Distribuidor().Select().Where(m => m.FHBaja == null))
                    {
                        ddlDistribuidor.Items.Add(new ListItem(m.RazonSocial, m.IdDistribuidor.ToCryptoID()));
                    }
                }

                //  Cargar la lista de productos
                ddlArticulo.Items.Add(new ListItem("Seleccione un Artículo/Servicio", ""));
                foreach (CGADM.ListaPrecio m in new CGADM.ListaPrecio().Select().Where(m => m.FHBaja == null))
                {
                    //ddlArticulo.Items.Add(new ListItem(m.Descripcion, m.IdListaPrecio.ToCryptoID()));
                }

                ddlDistribuidor_OnSelectedIndexChanged(null, null);
                if (idModel == 0)
                {
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnSave.Enabled = true;
                    txtEstado.Value = "Pendiente";
                }
                else
                { // Ver Modelo
                    // Recupera el Modelo a partir del Id encriptado del Modelo
                    model.Select(idModel);
                    if (model.IdPendienteEstado == 1)
                    {
                        txtEstado.Value = "Pendiente";
                    }
                    else if (model.IdPendienteEstado == 2)
                    {
                        txtEstado.Value = "Liquidado";
                    }
                    else if (model.IdPendienteEstado == 3)
                    {
                        txtEstado.Value = "Cancelado";
                    }

                    // Genera el Hash del modelo y lo guarda en el control hdnHash
                    string jsonModel = model.ToJson();
                    hdnHash.Value = ModelHash(jsonModel);

                    // Establece los valores de los controles desde las propiedades del Modelo
                    SetControls();
                }

                EnableControls();
            }
        }

        protected void ddlDistribuidor_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            int idDistribuidor = ddlDistribuidor.SelectedValue.ToIntID();
            List<CGADM.DistribuidorUnidadNegocio> listDistribuidorUnidadNegocio = new CGADM.DistribuidorUnidadNegocio().Select().Where(dun => dun.IdDistribuidor == idDistribuidor).ToList();
            List<CGADM.UnidadNegocio> listUnidadNegocio = new CGADM.UnidadNegocio().Select().Where(dun => listDistribuidorUnidadNegocio.Any(un => dun.IdUnidadNegocio == un.IdUnidadNegocio)).ToList();
            ddlUnidadNegocio.Items.Clear();

            ddlVendingReseller.Items.Clear();
            foreach (CGADM.UnidadNegocio unidadNegocio in listUnidadNegocio) { ddlUnidadNegocio.Items.Add(new ListItem(unidadNegocio.Descripcion, unidadNegocio.IdUnidadNegocio.ToCryptoID())); }
            if (ddlUnidadNegocio.Items.Count > 0) { ddlUnidadNegocio_OnSelectedIndexChanged(null, null); }
        }

        protected void ddlUnidadNegocio_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            int idDistribuidor = ddlDistribuidor.SelectedValue.ToIntID();
            int idUnidadNegocio = ddlUnidadNegocio.SelectedValue.ToIntID();
            List<CGADM.DistribuidorUnidadNegocio> listaDistribuidorUnidadNegocio = new CGADM.DistribuidorUnidadNegocio().Select().Where(dun => dun.IdDistribuidor == idDistribuidor && dun.IdUnidadNegocio == idUnidadNegocio).ToList();
            int idDistribuidorUnidadNegocio = listaDistribuidorUnidadNegocio[0].IdDistribuidorUnidadNegocio;

            List<CGADM.PlanComercial> listaPlanesComerciales = new CGADM.PlanComercial().Select().Where(p => p.IdDistribuidorUnidadNegocio == idDistribuidorUnidadNegocio).ToList();

            ddlVendingReseller.Items.Clear();
            ddlVendingReseller.Items.Add(new ListItem("Seleccione un Reseller", ""));

            List<CGADM.DistribuidorReseller> distribuidorResellerAux = new CGADM.DistribuidorReseller().Select();
            switch (idUnidadNegocio)
            {
                case 1:
                    CGVending.VendingReseller vr = new CGVending.VendingReseller();
                    foreach (CGVending.VendingReseller m in vr.Select().Where(m => m.FechaBaja == null))
                    {
                        if (distribuidorResellerAux.Where(dr => dr.IdDistribuidorUnidadNegocio == idDistribuidorUnidadNegocio && dr.IdReseller == m.IdVendingReseller && dr.FHBaja == null).Count() > 0)
                        {
                            ddlVendingReseller.Items.Add(new ListItem(m.RazonSocial, m.IdVendingReseller.ToCryptoID()));
                        }
                    }
                    break;

                case 2:
                    CGAccess.Reseller ar = new CGAccess.Reseller();
                    foreach (CGAccess.Reseller m in ar.Select().Where(m => m.FHBaja == null))
                    {
                        if (distribuidorResellerAux.Where(dr => dr.IdDistribuidorUnidadNegocio == idDistribuidorUnidadNegocio && dr.IdReseller == m.IdReseller && dr.FHBaja == null && dr.IdPlanComercial == null).Count() > 0)
                        {
                            ddlVendingReseller.Items.Add(new ListItem(m.RazonSocial, m.IdReseller.ToCryptoID()));
                        }
                    }
                    break;
            }
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            model.Select(idModel);
            if (model.IdPendienteEstado != 1)
            {
                Message("Pendiente ", "Solo se pueden alterar ítems en estado Pendiente.", MessagesTypes.warning, "PendientesFacturar.aspx");
                return;
            }

            // Deshabilita el botón Editar
            btnEdit.Enabled = false;

            // Deshabilita el botón Eliminar
            btnDelete.Enabled = false;

            // Habilita el botón Guardar
            btnSave.Enabled = true;

            // Habilita o Deshabilita los controles según el estado del btnSave
            EnableControls();
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            // Recupera el Modelo a partir del Id encriptado
            model.Select(idModel);
            if (model.IdPendienteEstado != 1)
            {
                Message("Pendiente ", "Solo se pueden alterar ítems en estado Pendiente.", MessagesTypes.warning, "PendientesFacturar.aspx");
                return;
            }

            // Genera el Hash del Modelo de la DB y lo compara con el guardado en el control hdnHash
            string jsonModel = model.ToJson();
            if (hdnHash.Value.Equals(ModelHash(jsonModel)))
            {// El Modelo está actualizado
                // Actualiza la Fecha Hora Baja
                model.IdPendienteEstado = 3;

                // Inserta el Modelo
                using (Tn tn = new Tn("CGAdm"))
                {
                    model.Update(tn);
                    model.Observacion += " - Baja";
                    tn.Commit();
                }

                // Confirma al usuario
                Message("Pendiente Cancelado", "El Pendiente de Facturar ha sido cancelado.", MessagesTypes.success, "PendientesFacturar.aspx");
            }
            else
            {// El Modelo está desactualizado
             // Notifica que el Modelo está desactualizado en la DB
                Message("Pendiente Desactualizada", "El Pendiente cambió en la DB desde la última vez que se lo recuperó.", MessagesTypes.warning, Request.Url.ToString());
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            #region Validar

            if (ddlVendingReseller.SelectedValue.Equals(""))
            {
                Message("Atención", "Debe seleccionar un Reseller.", MessagesTypes.warning);
                return;
            }

            if (ddlArticulo.SelectedValue.Equals(""))
            {
                Message("Atención", "Debe seleccionar un Artículo/Servicio.", MessagesTypes.warning);
                return;
            }

            if (txtImporte.Text.ToDecimal() <= 0)
            {
                Message("Atención", "Debe ingresar los Artículos a Facturar.", MessagesTypes.warning);
                return;
            }
            if (DateTime.Parse(txtFechaParaImputar.Text).Date <= DateTime.Now.Date)
            {
                Message("Atención", "Debe ingresar una fecha posterior.", MessagesTypes.warning);
                return;
            }

            #endregion Validar

            int idDistribuidor = ddlDistribuidor.SelectedValue.ToIntID();
            int idunidadNegocio = ddlUnidadNegocio.SelectedValue.ToIntID();
            int idReseller = ddlVendingReseller.SelectedValue.ToIntID();

            var distribuidor = new CGADM.Distribuidor().Select().Where(d => d.IdDistribuidor == idDistribuidor && d.FHBaja == null).First();

            if (idModel == 0)
            {
                SetProperties();
                using (Tn tn = new Tn("CGAdm"))
                {
                    model.IdDistribuidor = idDistribuidor;
                    model.IdUnidadNegocio = idunidadNegocio;
                    model.IdReseller = idReseller;
                    model.IdMoneda = 1;
                    model.IdConcepto = 2;
                    model.IdPendienteEstado = 1;
                    model.Insert(tn);

                    tn.Commit();
                }
                Message("Pendiente Creado", "El Pendiente de Facturar ha sido creado exitosamente.", MessagesTypes.success, "PendientesFacturar.aspx");
            }
            else
            {
                // Recupera el Modelo a partir del Id encriptado
                model.Select(idModel);

                // Genera el Hash del Modelo de la DB y lo compara con el guardado en el control hdnHash
                string jsonModel = model.ToJson();
                if (hdnHash.Value.Equals(ModelHash(jsonModel)))
                {// El Modelo está actualizado
                    // Establece los valores de las propiedades desde los controles
                    SetProperties();

                    // Actualiza el Modelo
                    using (Tn tn = new Tn("CGAdm"))
                    {
                        model.Update(tn);
                        tn.Commit();
                    }

                    // Confirma al usuario
                    Message("Pendiente Editado", "El pendiente ha sido modificado exitosamente.", MessagesTypes.success, "PendientesFacturar.aspx");
                }
                else
                {// El Modelo está desactualizado
                    // Notifica que el Modelo está desactualizado en la DB
                    Message("Pendiente Desactualizado", "El Pendiente cambió en la DB desde la última vez que se lo recuperó.", MessagesTypes.warning, Request.Url.ToString());
                }
            }
        }

        protected void EnableControls()
        {
            if (idModel == 0)
            {
                ddlDistribuidor.Enabled =
                ddlUnidadNegocio.Enabled =
                ddlVendingReseller.Enabled = true;
            }
            else
            {
                ddlDistribuidor.Enabled =
                ddlUnidadNegocio.Enabled =
                ddlVendingReseller.Enabled = false;
            }
            txtFechaParaImputar.Enabled =
                ddlArticulo.Enabled =
            txtImporte.Enabled = btnSave.Enabled;

            txtObservacion.Disabled = !btnSave.Enabled;
        }

        protected void SetProperties()
        {
            model.IdListaPrecio = ddlArticulo.SelectedValue.ToIntID();

            model.Observacion = txtObservacion.Value;
            model.FHAlta = DateTime.Now;
            model.FechaParaImputar = DateTime.Parse(txtFechaParaImputar.Text);
            model.Importe = decimal.Parse(txtImporte.Text);
        }

        protected void SetControls()
        {
            ddlDistribuidor.SelectedValue = model.IdDistribuidor.ToCryptoID();
            ddlDistribuidor_OnSelectedIndexChanged(null, null);

            ddlUnidadNegocio.SelectedValue = model.IdUnidadNegocio.ToCryptoID();
            ddlUnidadNegocio_OnSelectedIndexChanged(null, null);

            ddlVendingReseller.SelectedValue = model.IdReseller.ToCryptoID();
            ddlArticulo.SelectedValue = model.IdListaPrecio.ToCryptoID();

            txtFechaParaImputar.Text = model.FechaParaImputar.ToString();
            txtImporte.Text = model.Importe.ToString();
            txtObservacion.Value = model.Observacion;
        }
    }
}