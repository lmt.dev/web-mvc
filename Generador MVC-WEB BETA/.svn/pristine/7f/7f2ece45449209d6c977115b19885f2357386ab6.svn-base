﻿using CGUsuarios.Common;
using Code;
using MagicSQL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;
using static Code.Utils;

namespace Pages
{
    public partial class UMDesasignarDistribuidor : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Establece Título del Formulario = Título de la Página
                lblTitulo.Text = Title;

                foreach (CGADM.Distribuidor distribuidor in new CGADM.Distribuidor().Select().Where(m => m.FHBaja == null))
                {
                    ddlDistribuidor.Items.Add(new ListItem(distribuidor.RazonSocial, distribuidor.IdDistribuidor.ToCryptoID()));
                }
                ddlDistribuidor_OnSelectedIndexChanged(null, null);

                gvUM.DataSource = new SP("CGAdm").Execute("usp_GetUMSinResellerConDistribuidor", P.Add("idUnidadNegocio", ddlUnidadNegocio.SelectedValue.ToIntID()));
                gvUM.DataBind();
            }
        }

        protected void ddlDistribuidor_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlDistribuidor.Items.Count == 0) return;

            // Id del Distribuidor desencriptado
            int idDistribuidor = ddlDistribuidor.SelectedValue.ToIntID();

            // Lista de los registros de la tabla de enlace DistribuidorUnidadNegocio con IdDistribuidor igual al seleccionado en el ddlDistribuidor
            List<CGADM.DistribuidorUnidadNegocio> listDistribuidorUnidadNegocio =
                new CGADM.DistribuidorUnidadNegocio().Select().Where(dun => dun.IdDistribuidor == idDistribuidor).ToList();

            // Lista de los registros de la tabla UnidadNegocio con IdUnidadNegocio contenido en la lista listDistribuidorUnidadNegocio (filtrada en el paso anterior)
            List<CGADM.UnidadNegocio> listUnidadNegocio =
                new CGADM.UnidadNegocio().Select().Where(dun => listDistribuidorUnidadNegocio.Any(un => dun.IdUnidadNegocio == un.IdUnidadNegocio)).ToList();

            // Limpia el ddlUnidadNegocio
            ddlUnidadNegocio.Items.Clear();
            foreach (CGADM.UnidadNegocio unidadNegocio in listUnidadNegocio)
            {
                // Agrega el item UnidadNegocio
                ddlUnidadNegocio.Items.Add(new ListItem(unidadNegocio.Descripcion, unidadNegocio.IdUnidadNegocio.ToCryptoID()));
            }

            ddlUnidadNegocio_OnSelectedIndexChanged(null, null);
        }

        protected void ddlUnidadNegocio_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(ddlUnidadNegocio.SelectedValue)) return;
            gvUM.DataSource = new SP("CGAdm").Execute("usp_GetUMSinResellerConDistribuidor", P.Add("idUnidadNegocio", ddlUnidadNegocio.SelectedValue.ToIntID()));
            gvUM.DataBind();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            var idUnidadNegocio = ddlUnidadNegocio.SelectedValue.ToInt();

            using (Tn tx = new Tn("CGAdm"))
            {
                foreach (GridViewRow rUM in gvUM.Rows)
                {
                    CheckBox chkRow = (rUM.Cells[0].FindControl("chkRow") as CheckBox);
                    if (chkRow.Checked)
                    {
                        var unidadNegocio = new CGADM.DistribuidorUnidadNegocio().Select().FirstOrDefault(u => u.IdDistribuidor == ddlDistribuidor.SelectedValue.ToIntID() && u.IdUnidadNegocio == ddlUnidadNegocio.SelectedValue.ToIntID());

                        var asignacion = new CGADM.DistribuidorUM().Select().FirstOrDefault(d => d.FHBaja == null && d.IdDistribuidorUnidadNegocio == unidadNegocio.IdDistribuidorUnidadNegocio && d.UMId == (int)gvUM.DataKeys[rUM.RowIndex]["UMId"]);
                        if (asignacion != null)
                        {
                            asignacion.FHBaja = DateTime.Now;
                            asignacion.IdServidorOutput = null;
                            asignacion.AccionABMServidorOutput = "B";
                            asignacion.IdSesionTrabajoDetalle = SecurityManager.IdSesionTrabajo;
                            asignacion.Update(tx);
                        }
                    }
                }

                tx.Commit();
            }

            Message("Desasignación de Distribuidor", "Las desasignaciones al distribuidor se realizaron exitosamente.", MessagesTypes.success);
            ddlUnidadNegocio_OnSelectedIndexChanged(null, null);
        }
    }
}