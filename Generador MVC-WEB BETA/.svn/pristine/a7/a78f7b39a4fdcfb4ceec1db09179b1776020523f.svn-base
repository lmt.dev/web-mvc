﻿using CGUsuarios.Common;
using Code;
using MagicSQL;

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;
using static Code.Utils;

namespace Pages
{
    public partial class SaldoInicial : BasePage
    {
        // Modelo que se utilizará en la página
        private CGADM.Comprobante model = new CGADM.Comprobante();

        // Id del Modelo
        private int idModel = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            // Id del Modelo recuperado del Request
            if (Request["cryptoID"] != null)
            {
                try
                {
                    idModel = Request["cryptoID"].ToIntID();
                }
                catch (Exception)
                {
                    NavigateTo("~/Content/html/Alert.html");
                    return;
                }
            }

            if (!IsPostBack)
            {
                lblTitulo.Text = Title;
                txtImporteAjuste.Text = "0";

                if (SecurityManager.IdAplicacion == idAplicacionDistribuidor)
                {
                    var distribuidor = new CGADM.Distribuidor().Select((int)SecurityManager.IdPropietario);
                    ddlDistribuidor.Items.Add(new ListItem(distribuidor.RazonSocial, distribuidor.IdDistribuidor.ToCryptoID()));
                    ddlDistribuidor.Enabled = false;
                }
                else
                {
                    foreach (CGADM.Distribuidor m in new CGADM.Distribuidor().Select().Where(m => m.FHBaja == null))
                    {
                        ddlDistribuidor.Items.Add(new ListItem(m.RazonSocial, m.IdDistribuidor.ToCryptoID()));
                    }
                }
                ddlDistribuidor_OnSelectedIndexChanged(null, null);
                if (idModel == 0)
                {
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnSave.Enabled = true;
                }

                EnableControls();
            }
        }

        protected void ddlDistribuidor_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            int idDistribuidor = ddlDistribuidor.SelectedValue.ToIntID();
            List<CGADM.DistribuidorUnidadNegocio> listDistribuidorUnidadNegocio = new CGADM.DistribuidorUnidadNegocio().Select().Where(dun => dun.IdDistribuidor == idDistribuidor).ToList();
            List<CGADM.UnidadNegocio> listUnidadNegocio = new CGADM.UnidadNegocio().Select().Where(dun => listDistribuidorUnidadNegocio.Any(un => dun.IdUnidadNegocio == un.IdUnidadNegocio)).ToList();
            ddlUnidadNegocio.Items.Clear();

            ddlVendingReseller.Items.Clear();
            foreach (CGADM.UnidadNegocio unidadNegocio in listUnidadNegocio) { ddlUnidadNegocio.Items.Add(new ListItem(unidadNegocio.Descripcion, unidadNegocio.IdUnidadNegocio.ToCryptoID())); }
            if (ddlUnidadNegocio.Items.Count > 0) { ddlUnidadNegocio_OnSelectedIndexChanged(null, null); }
        }

        protected void ddlUnidadNegocio_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            int idDistribuidor = ddlDistribuidor.SelectedValue.ToIntID();
            int idUnidadNegocio = ddlUnidadNegocio.SelectedValue.ToIntID();
            List<CGADM.DistribuidorUnidadNegocio> listaDistribuidorUnidadNegocio = new CGADM.DistribuidorUnidadNegocio().Select().Where(dun => dun.IdDistribuidor == idDistribuidor && dun.IdUnidadNegocio == idUnidadNegocio).ToList();
            int idDistribuidorUnidadNegocio = listaDistribuidorUnidadNegocio[0].IdDistribuidorUnidadNegocio;

            List<CGADM.PlanComercial> listaPlanesComerciales = new CGADM.PlanComercial().Select().Where(p => p.IdDistribuidorUnidadNegocio == idDistribuidorUnidadNegocio).ToList();

            ddlVendingReseller.Items.Clear();
            List<CGADM.DistribuidorReseller> distribuidorResellerAux = new CGADM.DistribuidorReseller().Select();
            switch (idUnidadNegocio)
            {
                case 1:
                    CGVending.VendingReseller vr = new CGVending.VendingReseller();
                    foreach (CGVending.VendingReseller m in vr.Select().Where(m => m.FechaBaja == null))
                    {
                        if (distribuidorResellerAux.Where(dr => dr.IdDistribuidorUnidadNegocio == idDistribuidorUnidadNegocio && dr.IdReseller == m.IdVendingReseller && dr.FHBaja == null).Count() > 0)
                        {
                            ddlVendingReseller.Items.Add(new ListItem(m.RazonSocial, m.IdVendingReseller.ToCryptoID()));
                        }
                    }
                    break;

                case 2:
                    CGAccess.Reseller ar = new CGAccess.Reseller();
                    foreach (CGAccess.Reseller m in ar.Select().Where(m => m.FHBaja == null))
                    {
                        if (distribuidorResellerAux.Where(dr => dr.IdDistribuidorUnidadNegocio == idDistribuidorUnidadNegocio && dr.IdReseller == m.IdReseller && dr.FHBaja == null && dr.IdPlanComercial == null).Count() > 0)
                        {
                            ddlVendingReseller.Items.Add(new ListItem(m.RazonSocial, m.IdReseller.ToCryptoID()));
                        }
                    }
                    break;
            }
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            #region Validar

            if (txtImporteAjuste.Text.ToDecimal() <= 0)
            {
                Message("Atención", "Debe ingresar los Artículos a Facturar.", MessagesTypes.warning);
                return;
            }

            #endregion Validar

            int idDistribuidor = ddlDistribuidor.SelectedValue.ToIntID();
            int idunidadNegocio = ddlUnidadNegocio.SelectedValue.ToIntID();
            int idReseller = ddlVendingReseller.SelectedValue.ToIntID();

            var distribuidor = new CGADM.Distribuidor().Select().Where(d => d.IdDistribuidor == idDistribuidor && d.FHBaja == null).First();

            if (idModel == 0)
            {
                SetProperties();
                using (Tn tn = new Tn("CGAdm"))
                {
                    model.IdMoneda = 1; // TODO: definir en que moneda esta la lista comercial. No esta claro si moneda va en plan, en articulo, etc.

                    model.NroComprobanteInterno = 9999;

                    model.Generacion = "Manual";
                    model.Insert(tn); // Cabecera

                    // Actualizar Cuenta Corriente Reseller
                    CGADM.CtaCte cuentaCorrienteReseller = null;
                    var cuentaCorrienteAnterior = new CGADM.CtaCte().Select().Where(c => c.IdReseller == idReseller && c.IdUnidadNegocio == idunidadNegocio).OrderByDescending(c => c.IdCtaCte);
                    decimal saldoAnterior = 0;
                    if (cuentaCorrienteAnterior.Count() == 0)
                    {
                        cuentaCorrienteReseller = new CGADM.CtaCte();
                    }
                    else
                    {
                        cuentaCorrienteReseller = cuentaCorrienteAnterior.First();
                        saldoAnterior = cuentaCorrienteReseller.Saldo;
                    }

                    cuentaCorrienteReseller.IdUnidadNegocio = idunidadNegocio;
                    cuentaCorrienteReseller.IdReseller = idReseller;
                    cuentaCorrienteReseller.FHAlta = DateTime.Now;

                    cuentaCorrienteReseller.TipoMov = "Debito";
                    cuentaCorrienteReseller.Saldo = cuentaCorrienteReseller.Saldo - model.ImporteTotal;

                    cuentaCorrienteReseller.IdComprobante = model.IdComprobante;
                    cuentaCorrienteReseller.IdRecibo = null;
                    cuentaCorrienteReseller.IdDistribuidor = distribuidor.IdDistribuidor;
                    cuentaCorrienteReseller.Insert(tn);

                    // Actualizar Saldo Cuenta Corriente
                    CGADM.SaldoCuentaCorriente saldoCuentaCorrienteReseller = null;
                    var saldoCuentaCorriente = new CGADM.SaldoCuentaCorriente().Select().Where(c => c.IdReseller == idReseller && c.IdUnidadNegocio == idunidadNegocio);
                    if (cuentaCorrienteAnterior.Count() == 0)
                    {
                        saldoCuentaCorrienteReseller = new CGADM.SaldoCuentaCorriente();
                        saldoCuentaCorrienteReseller.IdUnidadNegocio = idunidadNegocio;
                        saldoCuentaCorrienteReseller.IdReseller = idReseller;
                    }
                    else
                    {
                        saldoCuentaCorrienteReseller = saldoCuentaCorriente.First();
                    }
                    saldoCuentaCorrienteReseller.Saldo = saldoCuentaCorrienteReseller.Saldo - model.ImporteTotal;
                    saldoCuentaCorrienteReseller.FHActualizacion = DateTime.Now;
                    if (saldoCuentaCorrienteReseller.IdSaldoCuentaCorriente == 0)
                    {
                        saldoCuentaCorrienteReseller.IdDistribuidor = idDistribuidor;
                        saldoCuentaCorrienteReseller.Insert(tn);
                    }
                    else { saldoCuentaCorrienteReseller.Update(tn); }

                    tn.Commit();
                }
                Message("Comprobante creado", "El Saldo Inicial ha sido creado exitosamente.", MessagesTypes.success, "ListadoCuentasCorrientes.aspx");
            }
        }

        protected void EnableControls()
        {
            ddlUnidadNegocio.Enabled =
            txtFechaVencimiento.Enabled =
            ddlVendingReseller.Enabled = btnSave.Enabled;
            txtObservacion.Disabled = !btnSave.Enabled;
        }

        protected void SetProperties()
        {
            model.Descripcion = "Saldo Inicial";
            model.Observacion = txtObservacion.Value;
            model.FHAlta = DateTime.Now;
            model.ImporteTotal = txtImporteAjuste.Text.ToDecimal();

            model.TipoComprobanteInterno = 4; // 4 - Saldo Inicial

            model.FHEmisionComprobante = DateTime.Now;
            model.ImporteSaldado = 0;
            model.FHVencimiento = txtFechaVencimiento.Text.ToDateTime();
        }

        protected void SetControls()
        {
        }
    }
}