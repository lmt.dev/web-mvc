﻿using CGUsuarios.Common;
using Code;
using System;
using System.Drawing;
using System.Globalization;
using System.Reflection;
using System.Threading;
using static Code.Utils;

public partial class Default : System.Web.UI.Page
{
    // Ids varios
    private short idAplicacionAdministrador = System.Configuration.ConfigurationManager.AppSettings["IdAplicacionAdministrador"].ToShort();

    private short idAplicacionDistribuidor = System.Configuration.ConfigurationManager.AppSettings["IdAplicacionDistribuidor"].ToShort();

    public string Version { get; set; }

    private bool logout = false;

    protected override void InitializeCulture()
    {
        logout = !CurrentSession.ValidateSession();
        if (!logout)
        {
            string culture = "";
            switch (CGUsuarios.Common.SecurityManager.Usuario.IdIdioma)
            {
                case 1:
                    culture = "es";
                    break;

                case 4:
                    culture = "en";
                    break;
            }

            base.InitializeCulture();
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
        }
    }

    protected void Page_Preload(object sender, EventArgs e)
    {
        base.InitializeCulture();

        // Version de la compilación.
        Version = Assembly.GetExecutingAssembly().GetName().Version.ToString();

        if (logout)
        {
            Utils.NavigateTo("~/Content/html/Logout.html");
            this.Dispose();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            try
            {
                var cookie = Utils.ReadCookie("SessionSysAdm").ToDynamic();
                int idApp = cookie.IdAplicacion;
                var application = CGUsuarios.Common.Managers.GenericManager.Get<CGUsuarios.Common.Aplicacion>("aplicacion/" + idApp);
                lblApp.Text = application.Descripcion.Replace("Control Global - Administración - ", "");
                switch (lblApp.Text)
                {
                    case "Administradores":
                        lblApp.ForeColor = Color.FromArgb(33, 150, 243);
                        break;

                    case "Distribuidores":
                        lblApp.ForeColor = Color.FromArgb(156, 39, 176);
                        break;
                }

                if (SecurityManager.Usuario != null)
                {
                    lblNombre.Text = string.Format("{0}, {1}", SecurityManager.Usuario.Apellido, SecurityManager.Usuario.Nombre);
                    lnkPerfilUsuario.HRef = string.Format("Pages/Usuario.aspxcryptoID={0}", SecurityManager.Usuario.IdUsuario);
                }
            }
            catch
            {
                Utils.DeleteCookie("SessionSysAdm");
                NavigateTo("Apps.aspx");
            }
        }
    }

    private void Page_PreRender(object sender, EventArgs e)
    {
        if (logout)
        {
            NavigateTo("Apps.aspx");
        }
        else
        {
            Permission permission = new Permission(Page.Controls);
        }
    }
}