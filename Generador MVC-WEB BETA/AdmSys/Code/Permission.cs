﻿using CGUsuarios.Common;
using Code;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

internal class Permission
{
    // Constructor
    public Permission(ControlCollection controlCollection)
    {
        // Nombre del WebForm que creó el objeto Permission
        string queryString = HttpContext.Current.Request.ServerVariables["query_string"]; // Solo querystring
        string pageName = HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath + (queryString == null ? "" : "?" + queryString); // Url sin el directorio virtual más la query string

        if (!pageName.ToLower().StartsWith("~/default.aspx") && !pageName.Equals("~/"))
        {
            var permiso = (from p in SecurityManager.Permisos where p.FechaHoraBaja == null && p.Componente.FechaHoraBaja == null && pageName.ToLower().StartsWith(p.Componente.Identificador.Trim().ToLower()) select p).FirstOrDefault();
            if (permiso == null)
            {
                // No tiene permiso para acceder
                Utils.NavigateTo("~/Content/html/403.html");
            }
            else
            {
                ApplyPermissions(pageName, controlCollection, queryString);
            }
        }
        else
        {
            ApplyPermissions(pageName, controlCollection, queryString);
        }
    }

    private void ApplyPermissions(string parentPage, ControlCollection controlCollection, string permissionSufix = null)
    {
        // Recorre la lista de controles del WebForm
        foreach (Control control in controlCollection)
        {
            // Id del control
            string controlId = control.ID;
            if (!string.IsNullOrEmpty(controlId))
            {
                // Controles ASP.NET
                if (control is WebControl)
                {
                    if (((WebControl)control).Attributes["data-securable"] != null)
                    {
                        var permissionIdentifier = ((WebControl)control).Attributes["data-securable"];

                        if (permissionSufix != null)
                        {
                            permissionIdentifier = permissionIdentifier + "#" + permissionSufix;
                        }

                        var permiso = (from p in SecurityManager.Permisos where p.FechaHoraBaja == null && p.Componente.FechaHoraBaja == null && p.Componente.Identificador.StartsWith(permissionIdentifier) select p).FirstOrDefault();
                        control.Visible = permiso != null;
                    }
                }

                // Controles HTML puro
                if (control is HtmlControl)
                {
                    if (((HtmlControl)control).Attributes["data-securable"] != null)
                    {
                        var permissionIdentifier = ((WebControl)control).Attributes["data-securable"];

                        if (permissionSufix != null)
                        {
                            permissionIdentifier = permissionIdentifier + "#" + permissionSufix;
                        }

                        var permiso = (from p in SecurityManager.Permisos where p.FechaHoraBaja == null && p.Componente.FechaHoraBaja == null && p.Componente.Identificador.StartsWith(permissionIdentifier) select p).FirstOrDefault();
                        control.Visible = permiso != null;
                    }

                    // Solo para control del menú principal
                    if (((HtmlControl)control).Attributes["data-menu-securable"] != null)
                    {
                        if (control is HtmlAnchor)
                        {
                            var permissionIdentifier = ((HtmlAnchor)control).HRef;
                            if (!permissionIdentifier.StartsWith("~/")) permissionIdentifier = "~/" + permissionIdentifier;
                            var permiso = (from p in SecurityManager.Permisos where p.FechaHoraBaja == null && p.Componente.FechaHoraBaja == null && permissionIdentifier.StartsWith(p.Componente.Identificador) select p).FirstOrDefault();
                            control.Visible = permiso != null;
                        }

                        if (((HtmlControl)control).TagName == "li")
                        {
                            var permissionIdentifier = ((HtmlControl)control).Attributes["data-menu-securable"];
                            var permiso = (from p in SecurityManager.Permisos where p.FechaHoraBaja == null && p.Componente.FechaHoraBaja == null && permissionIdentifier.StartsWith(p.Componente.Identificador) select p).FirstOrDefault();
                            control.Visible = permiso != null;
                        }
                    }
                }
            }

            // Si el control tiene controles hijos se aplican los permisos
            if (control.HasControls())
            {
                ApplyPermissions(parentPage, control.Controls, permissionSufix);
            }
        }
    }
}