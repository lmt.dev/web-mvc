﻿using CGUsuarios.Common;
using Code;
using Newtonsoft.Json;
using System.Web;

public class CurrentSession
{
    public const string currentVersion = "v1";

    public string Version { get; set; }
    public string Username { get; set; }
    public string Password { get; set; }
    public short IdAplicacion { get; set; }
    public int IdPerfil { get; set; }
    public long IdCuenta { get; set; }
    public long IdSesionTrabajo { get; set; }

    public static bool ValidateSession()
    {
        // Recupera la cookie Session
        string json = Utils.ReadCookie("SessionSysAdm");

        // Existe la cookie?
        if (json == null)
        { // Si no existe se redirecciona a la página de login
            Utils.DeleteCookie("SessionSysAdm");
            Utils.NavigateTo("~/Content/html/Logout.html");
            return false;
        }
        else
        { // Si existe se valida el usuario y la contraseña
            // Objeto Session creado a partir de la información contenida en la cookie
            CurrentSession session = new CurrentSession();

            try
            {
                session = JsonConvert.DeserializeObject<CurrentSession>(json);
                if (session.Version != currentVersion) throw new System.ApplicationException();
            }
            catch (System.Exception)
            {
                // no es válida
                Utils.DeleteCookie("SessionSysAdm");
                Utils.NavigateTo("~/Content/html/Logout.html");
                return false;
            }

            try
            {
                // Validar sesión de trabajo en el sistema de Usuarios
                SecurityManager.IdAplicacion = session.IdAplicacion;
                SecurityManager.ValidateSession(HttpContext.Current, session.Username, session.IdPerfil, session.IdSesionTrabajo);
                if (SecurityManager.Usuario == null)
                {
                    var currentIdApp = session.IdAplicacion;

                    // Elimina la cookie Session
                    Utils.DeleteCookie("SessionSysAdm");

                    var application = CGUsuarios.Common.Managers.GenericManager.Get<CGUsuarios.Common.Aplicacion>("aplicacion/" + currentIdApp);
                    Utils.NavigateTo("~/Logout.aspx?app=" + application.Url);
                    return false;
                }
            }
            catch
            {
                // Redirecciona a la página de login
                Utils.DeleteCookie("SessionSysAdm");
                Utils.NavigateTo("~/Content/html/Logout.html");
                return false;
            }
        }
        return true;
    }
}