﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CGADM;
using MagicSQL;
using static Code.Utils;
using Code;

namespace AdmSys.Controls
{
    public partial class ucGridViewSolicitudesDeTallerRemitar : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        public GridView gridviewSolicitudesDeTaller
        {
            get { return gvSolicitudesDeTaller; }
        }
        
        public void ActualizarGrillaSolicitudes(List<SDT> listaSolicitudesT)
        {
            if (listaSolicitudesT != null)
            {
                gvSolicitudesDeTaller.DataSource = listaSolicitudesT.OrderByDescending(s => s.SDTNro);
                gvSolicitudesDeTaller.DataBind();
                if (gvSolicitudesDeTaller.Rows.Count > 0)
                {
                    //btnObtenerProductosRemitar.Visible = true;
                }
                foreach (GridViewRow rowListaSDT in gvSolicitudesDeTaller.Rows)
                {
                    if (rowListaSDT.RowType == DataControlRowType.DataRow)
                    {
                        try
                        {
                            ((CheckBox)gvSolicitudesDeTaller.Rows[rowListaSDT.RowIndex].Cells[6].FindControl("cbxRemitar")).Checked = true;
                        }
                        catch (Exception)
                        {
                            Message("Errores en actualización", "No se ha actualizado correctamente, por favor reintente nuevamente", MessagesTypes.error);
                        }
                    }
                }
            }
            else
            {
                gvSolicitudesDeTaller.DataSource = null;
                gvSolicitudesDeTaller.DataBind();
            }
            upSolicitudesTaller.Update();
        }

        protected void gvSolicitudesDeTaller_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            string vIdSDTDetalleCripto = Convert.ToInt32(gvSolicitudesDeTaller.Rows[e.NewSelectedIndex].Cells[0].Text).ToCryptoID();
            Utils.NavigateTo("SolicitudDeTaller.aspx?cryptoID="+ vIdSDTDetalleCripto);
        }
    }
}