﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucProductosRemitar.ascx.cs" Inherits="AdmSys.Controls.ucProductosRemitar" %>
<%@ Register Src="~/Controls/ucSDTDetalleFajaIngreso.ascx" TagPrefix="uc" TagName="ucSDTDetalleFajaIngreso" %>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <asp:UpdatePanel ID="upDetalleCarrito" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <ContentTemplate>

            <asp:GridView ID="gvListaProductosCargados"
                CssClass="table table-bordered table-hover col-lg-8 col-md-8 col-sm-8 col-xs-8"
                Width="100%"
                AutoGenerateColumns="False"
                runat="server"
                EmptyDataText="Aún no ha cargado productos para remitar."
                AutoPostBack="true">
                <Columns>
                    <asp:BoundField HeaderText="#" DataField="IdListaPrecio" HeaderStyle-CssClass="crypto-id-column" ItemStyle-CssClass="crypto-id-column" />
                    <asp:BoundField HeaderText="Descripción" DataField="DescripcionProducto" />
                    <asp:BoundField HeaderText="Precio unitario" DataField="ImporteProducto" ItemStyle-CssClass="numeric-money">
                        <ItemStyle CssClass="numeric-money" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Subtotal s/IVA" DataField="Subtotal" ItemStyle-CssClass="numeric-money show-total">
                        <ItemStyle CssClass="numeric-money show-total" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Tipo" DataField="TipoSolicitud" />
                    <asp:BoundField HeaderText="Solicitud Nro" DataField="SolNro" />
                    <asp:BoundField HeaderText="Cantidad Total" DataField="CantidadProducto" />
                    <asp:BoundField HeaderText="Cantidad Remitados" DataField="CantidadRemitados" />
                    <asp:BoundField HeaderText="Serial" DataField="Serial" />
                    <asp:BoundField HeaderText="Tipo de Hardware" DataField="TipoHardWare" />

                    <asp:TemplateField HeaderText="Cantidad a remitar" ItemStyle-Width="50">
                        <ItemTemplate>
                            <div class="input-group col-lg-12 col-md-12 col-sm-12 col-xs-12" 
                                style="margin-top: 5px;">
                                <asp:TextBox ID="txtCantidadRemitar" 
                                    runat="server" 
                                    CssClass="form-control numeric-integer-positive" 
                                    Width="50" />
                            </div>
                        </ItemTemplate>
                        <ItemStyle Width="50px" />
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Faja de ingreso" ItemStyle-Width="250">
                        <ItemTemplate>
                            <uc:ucSDTDetalleFajaIngreso runat="server" ID="ucSDTDetalleFajaIngresoRow" />
                        </ItemTemplate>
                        <ItemStyle/>
                    </asp:TemplateField>

                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>