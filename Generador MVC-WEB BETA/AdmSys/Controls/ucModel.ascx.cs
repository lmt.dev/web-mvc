﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CGADM;
using MagicSQL;
using static Code.Utils;

namespace AdmSys.Controls
{
    public partial class ucModel : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        public ErrorHandler errorHandler
        {
            get { return errorHandler; }
            set
            {
                errorHandler = value;
                lblErrorHandler.Text = errorHandler.Mensaje+". "+errorHandler.Descripcion;
                lblErrorHandler.ForeColor = System.Drawing.Color.Red;
                upUCModel.Update();
            }
        }
    }
}