﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CGADM;
using MagicSQL;
using static Code.Utils;
using System.Data;

namespace AdmSys.Controls
{
    public partial class ucProductoServicioEscala : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        public void modoLectura()
        {
            btnAgregarEscala.Visible = false;
            gvProductoServicioEscala.Enabled = false;
        }

        public int cantidadEscalasValidas
        {
            get
            {
                List<Escala> listaEscalasValidar = new List<Escala>();

                foreach (GridViewRow gvRowEscalas in gvProductoServicioEscala.Rows)
                {
                    Escala escalaAgregar = new Escala();

                    escalaAgregar.IdEscala = gvProductoServicioEscala.Rows[gvRowEscalas.RowIndex].Cells[0].Text.ToInt();

                    TextBox txtCantidadMinima = ((TextBox)gvProductoServicioEscala.Rows[gvRowEscalas.RowIndex].Cells[1].FindControl("txtCantidadMinima"));
                    escalaAgregar.CantInferior = txtCantidadMinima.Text.ToInt();

                    TextBox txtCantidadMaxima = ((TextBox)gvProductoServicioEscala.Rows[gvRowEscalas.RowIndex].Cells[2].FindControl("txtCantidadMaxima"));
                    escalaAgregar.CantSuperior = txtCantidadMaxima.Text.ToInt();

                    TextBox txtPrecioFijoLista = ((TextBox)gvProductoServicioEscala.Rows[gvRowEscalas.RowIndex].Cells[3].FindControl("txtPrecioFijoLista"));
                    escalaAgregar.PrecioLista = txtPrecioFijoLista.Text.ToDecimal();

                    TextBox txtPrecioFijoContado = ((TextBox)gvProductoServicioEscala.Rows[gvRowEscalas.RowIndex].Cells[4].FindControl("txtPrecioFijoContado"));
                    escalaAgregar.PrecioContado = txtPrecioFijoContado.Text.ToDecimal();

                    TextBox txtPrecioPorcentualPorcentaje = ((TextBox)gvProductoServicioEscala.Rows[gvRowEscalas.RowIndex].Cells[5].FindControl("txtPrecioPorcentualPorcentaje"));
                    escalaAgregar.Porcentaje = txtPrecioPorcentualPorcentaje.Text.ToInt();

                    TextBox txtPrecioPorcentualCobroMinimo = ((TextBox)gvProductoServicioEscala.Rows[gvRowEscalas.RowIndex].Cells[6].FindControl("txtPrecioPorcentualCobroMinimo"));
                    escalaAgregar.CobroMinimoEnPorcentual = txtPrecioPorcentualCobroMinimo.Text.ToDecimal();

                    if (escalaAgregar.IdEscala != 0)
                    {
                        listaEscalasValidar.Add(escalaAgregar);
                    }
                }

                int vCantidad = listaEscalasValidar.Count;
                return vCantidad;
            }
            set { }
        }

        public void mostrarFijas()
        {
            gvProductoServicioEscala.Visible = false;
            gvProductoServicioEscalasFijasMuestra.Visible = true;
            gvProductoServicioEscalasPorcentualMuestra.Visible = false;
            upProductoServicioEscala.Update();
        }

        public void mostrarPorcentuales()
        {
            gvProductoServicioEscala.Visible = false;
            gvProductoServicioEscalasFijasMuestra.Visible = false;
            gvProductoServicioEscalasPorcentualMuestra.Visible = true;
            upProductoServicioEscala.Update();
        }

        public string idListaPrecio
        {
            get
            {
                string idListaPrecioReturn = lblIdListaPrecio.Text;
                return idListaPrecioReturn;
            }

            set
            {
                lblIdListaPrecio.Text = value;
                int vIdListaPrecio = lblIdListaPrecio.Text.ToInt();

                if (vIdListaPrecio > 0)
                {
                    List<Escala> listaEscalas = 
                        new Escala().
                            Select().
                                Where(esc => 
                                esc.IdListaPrecio == vIdListaPrecio && 
                                esc.FHBaja == null
                    ).ToList();

                    if (listaEscalas.Count > 0)
                    {
                        gvProductoServicioEscala.DataSource = listaEscalas.OrderBy(ec => ec.CantInferior);
                        gvProductoServicioEscala.DataBind();

                        gvProductoServicioEscalasFijasMuestra.DataSource = listaEscalas.OrderBy(ec => ec.CantInferior);
                        gvProductoServicioEscalasFijasMuestra.DataBind();
                        
                        gvProductoServicioEscalasPorcentualMuestra.DataSource = listaEscalas.OrderBy(ec => ec.CantInferior);
                        gvProductoServicioEscalasPorcentualMuestra.DataBind();

                        foreach (GridViewRow gvRowEscala in gvProductoServicioEscala.Rows)
                        {
                            int idEscalaGrilla = gvProductoServicioEscala.Rows[gvRowEscala.RowIndex].Cells[0].Text.ToInt();
                            foreach (Escala escalaMostrar in listaEscalas)
                            {
                                if (escalaMostrar.IdEscala == idEscalaGrilla)
                                {
                                    //Seatear grilla genérica de escalas
                                    ((TextBox)gvProductoServicioEscala.Rows[gvRowEscala.RowIndex].Cells[1].FindControl("txtCantidadMinima")).Text = escalaMostrar.CantInferior.ToString();
                                    ((TextBox)gvProductoServicioEscala.Rows[gvRowEscala.RowIndex].Cells[2].FindControl("txtCantidadMaxima")).Text = escalaMostrar.CantSuperior.ToString();
                                    ((TextBox)gvProductoServicioEscala.Rows[gvRowEscala.RowIndex].Cells[3].FindControl("txtPrecioFijoLista")).Text = escalaMostrar.PrecioLista.ToString();
                                    ((TextBox)gvProductoServicioEscala.Rows[gvRowEscala.RowIndex].Cells[4].FindControl("txtPrecioFijoContado")).Text = escalaMostrar.PrecioContado.ToString();
                                    ((TextBox)gvProductoServicioEscala.Rows[gvRowEscala.RowIndex].Cells[5].FindControl("txtPrecioPorcentualPorcentaje")).Text = escalaMostrar.Porcentaje.ToString();
                                    ((TextBox)gvProductoServicioEscala.Rows[gvRowEscala.RowIndex].Cells[6].FindControl("txtPrecioPorcentualCobroMinimo")).Text = escalaMostrar.CobroMinimoEnPorcentual.ToString();

                                    //Setear grilla de escalas fijas
                                    ((Label)gvProductoServicioEscalasFijasMuestra.Rows[gvRowEscala.RowIndex].Cells[0].FindControl("lblCantidadMinima")).Text = escalaMostrar.CantInferior.ToString();
                                    ((Label)gvProductoServicioEscalasFijasMuestra.Rows[gvRowEscala.RowIndex].Cells[1].FindControl("lblCantidadMaxima")).Text = escalaMostrar.CantSuperior.ToString();
                                    ((Label)gvProductoServicioEscalasFijasMuestra.Rows[gvRowEscala.RowIndex].Cells[2].FindControl("lblPrecioFijoLista")).Text = "$ "+escalaMostrar.PrecioLista.ToString();
                                    ((Label)gvProductoServicioEscalasFijasMuestra.Rows[gvRowEscala.RowIndex].Cells[3].FindControl("lblPrecioFijoContado")).Text = "$ "+ escalaMostrar.PrecioContado.ToString();

                                    //Setear grilla de escalas porcentuales
                                    ((TextBox)gvProductoServicioEscalasPorcentualMuestra.Rows[gvRowEscala.RowIndex].Cells[0].FindControl("txtCantidadMinima")).Text = escalaMostrar.CantInferior.ToString();
                                    ((TextBox)gvProductoServicioEscalasPorcentualMuestra.Rows[gvRowEscala.RowIndex].Cells[1].FindControl("txtCantidadMaxima")).Text = escalaMostrar.CantSuperior.ToString();
                                    ((TextBox)gvProductoServicioEscalasPorcentualMuestra.Rows[gvRowEscala.RowIndex].Cells[2].FindControl("txtPrecioPorcentualPorcentaje")).Text = escalaMostrar.Porcentaje.ToString();
                                    ((TextBox)gvProductoServicioEscalasPorcentualMuestra.Rows[gvRowEscala.RowIndex].Cells[3].FindControl("txtPrecioPorcentualCobroMinimo")).Text = escalaMostrar.CobroMinimoEnPorcentual.ToString();
                                }
                            }
                        }
                    }
                    else
                    {
                        configurarEscalaDefault();
                    }
                }
                else
                {
                    configurarEscalaDefault();
                }
                upProductoServicioEscala.Update();
            }
        }

        public void configurarEscalaDefault()
        {
            DataTable dtEscalas = new DataTable();
            dtEscalas.Columns.AddRange(new DataColumn[1]
            {
                new DataColumn("IdEscala",typeof(int)),
            });
            dtEscalas.Rows.Add(0);

            gvProductoServicioEscala.DataSource = dtEscalas;
            gvProductoServicioEscala.DataBind();

            upProductoServicioEscala.Update();
        }

        public void seleccionTipoPrecio(string tipoPrecio)
        {
            foreach (GridViewRow gvRowEscalas in gvProductoServicioEscala.Rows)
            {
                if (tipoPrecio == "FIJO")
                {
                    ((TextBox)gvProductoServicioEscala.Rows[gvRowEscalas.RowIndex].Cells[3].FindControl("txtPrecioFijoLista")).Enabled = true;
                    ((TextBox)gvProductoServicioEscala.Rows[gvRowEscalas.RowIndex].Cells[4].FindControl("txtPrecioFijoContado")).Enabled = true;
                    ((TextBox)gvProductoServicioEscala.Rows[gvRowEscalas.RowIndex].Cells[5].FindControl("txtPrecioPorcentualPorcentaje")).Enabled = false;
                    ((TextBox)gvProductoServicioEscala.Rows[gvRowEscalas.RowIndex].Cells[6].FindControl("txtPrecioPorcentualCobroMinimo")).Enabled = false;
                }

                if (tipoPrecio == "PORCENTUAL")
                {
                    ((TextBox)gvProductoServicioEscala.Rows[gvRowEscalas.RowIndex].Cells[3].FindControl("txtPrecioFijoLista")).Enabled = false;
                    ((TextBox)gvProductoServicioEscala.Rows[gvRowEscalas.RowIndex].Cells[4].FindControl("txtPrecioFijoContado")).Enabled = false;
                    ((TextBox)gvProductoServicioEscala.Rows[gvRowEscalas.RowIndex].Cells[5].FindControl("txtPrecioPorcentualPorcentaje")).Enabled = true;
                    ((TextBox)gvProductoServicioEscala.Rows[gvRowEscalas.RowIndex].Cells[6].FindControl("txtPrecioPorcentualCobroMinimo")).Enabled = true;
                }
            }
            upProductoServicioEscala.Update();
        }

        public ErrorHandler productoServicioEscalasValidar( int idListaPrecio)
        {
            ErrorHandler eh = new ErrorHandler();
            if (Session["combinacionPanelesPrecioEscala"] != null)
            {
                string combinacionPanelesPrecioEscala = Session["combinacionPanelesPrecioEscala"].ToString();
                bool vAplicaEscala = false;
                bool vPrecioFijo = false;

                switch (combinacionPanelesPrecioEscala)
                {
                    case "escalasSIprecioFIJO":
                        vAplicaEscala = true;
                        vPrecioFijo = true;
                        break;

                    case "escalasSIprecioPORCENTUAL":
                        vAplicaEscala = true;
                        break;
                }

                if (vAplicaEscala == false)
                {
                    //Si todo anda bien nunca debería pasar por aquí, pero por si las moscas
                    eh.Mensaje = "La configuración de precio detectada no corresponde con la funcionalidad de escala";
                    return eh;
                }

                //INICIO LISTADO DE ESCALAS
                listaEscalasCargar.Clear();

                foreach (GridViewRow gvEscalaRow in gvProductoServicioEscala.Rows)
                {
                    try
                    {
                        Escala escalaRow = new Escala();

                        string idEscala = gvProductoServicioEscala.Rows[gvEscalaRow.RowIndex].Cells[0].Text;
                        escalaRow.IdEscala = idEscala.ToInt();

                        if (!vPrecioFijo)
                        {
                            TextBox tbxEscalaPorcentaje = ((TextBox)gvProductoServicioEscala.Rows[gvEscalaRow.RowIndex].Cells[5].FindControl("txtPrecioPorcentualPorcentaje"));
                            TextBox tbxEscalaCobroMinimo = ((TextBox)gvProductoServicioEscala.Rows[gvEscalaRow.RowIndex].Cells[6].FindControl("txtPrecioPorcentualCobroMinimo"));

                            if (tbxEscalaPorcentaje.Text == "")
                            {
                                eh.Mensaje = "Para las escalas con precio tipo porcentual se debe indicar un porcentaje de manera obligatoria";
                                return eh;
                            }

                            escalaRow.Porcentaje = tbxEscalaPorcentaje.Text.ToInt();
                            escalaRow.CobroMinimoEnPorcentual = tbxEscalaCobroMinimo.Text.ToDecimal();
                        }
                        else
                        {
                            TextBox tbxEscalaPrecioLista = ((TextBox)gvProductoServicioEscala.Rows[gvEscalaRow.RowIndex].Cells[3].FindControl("txtPrecioFijoLista"));
                            TextBox tbxEscalaPrecioContado = ((TextBox)gvProductoServicioEscala.Rows[gvEscalaRow.RowIndex].Cells[4].FindControl("txtPrecioFijoContado"));

                            if (tbxEscalaPrecioLista.Text == "")
                            {
                                eh.Mensaje = "Para las escalas con precio tipo fijo se debe indicar un precio de lista de manera obligatoria";
                                return eh;
                            }

                            if (tbxEscalaPrecioContado.Text == "")
                            {
                                eh.Mensaje = "Para las escalas con precio tipo fijo se debe indicar un precio de contado de manera obligatoria";
                                return eh;
                            }

                            escalaRow.PrecioLista = tbxEscalaPrecioLista.Text.ToDecimal();
                            escalaRow.PrecioContado = tbxEscalaPrecioContado.Text.ToDecimal();

                            if (escalaRow.PrecioLista < escalaRow.PrecioContado)
                            {
                                eh.Mensaje = "El precio de contado de cada escala debe ser inferior al precio de lista correspondiente a la misma";
                                return eh;
                            }
                        }

                        TextBox tbxEscalaCantidadMinima = ((TextBox)gvProductoServicioEscala.Rows[gvEscalaRow.RowIndex].Cells[1].FindControl("txtCantidadMinima"));
                        TextBox tbxEscalaCantidadMaxima = ((TextBox)gvProductoServicioEscala.Rows[gvEscalaRow.RowIndex].Cells[2].FindControl("txtCantidadMaxima"));

                        if (tbxEscalaCantidadMinima.Text == "" || tbxEscalaCantidadMaxima.Text == "")
                        {
                            eh.Mensaje = "Es obligatorio asignar cantidades mínimas y máximas en cada escala";
                            return eh;
                        }
                        else
                        {
                            escalaRow.CantInferior = tbxEscalaCantidadMinima.Text.ToInt();
                            escalaRow.CantSuperior = tbxEscalaCantidadMaxima.Text.ToInt();
                        }

                        if (tbxEscalaCantidadMinima.Text.ToInt() > tbxEscalaCantidadMaxima.Text.ToInt())
                        {
                            eh.Mensaje = "La cantidad máxima de cada escala debe ser superior a la cantidad mínima";
                            return eh;
                        }                        

                        //INICIO VALIDAR LOGICA ESCALABLE
                        if (listaEscalasCargar.Count == 0)
                        {
                            //Si aún no se han agregado escalas
                            if (escalaRow.CantInferior != 1)
                            {
                                eh.Mensaje = "La cantidad inferior de la primera escala siempre debe ser 1";
                                return eh;
                            }
                        }
                        else
                        {
                            //Si ya existen escalas para validar
                            Escala ultimaEscala = listaEscalasCargar[listaEscalasCargar.Count - 1];
                            int vConsecutivo = ultimaEscala.CantSuperior+1;
                            if (escalaRow.CantInferior != vConsecutivo)
                            {
                                eh.Mensaje = "La cantidad mínima de cada escala debe corresponder con el valor consecutivo a la cantidad máxima de la escala anterior, para evitar errores por rangos de cantidades no contempladas para el Producto/Servicio";
                                return eh;
                            }
                        }
                        //FIN VALIDAR LOGICA ESCALABLE
                        listaEscalasCargar.Add(escalaRow);
                    }
                    catch (Exception ex)
                    {
                        eh.Mensaje = "Error en la lectura de escalas";
                        eh.Descripcion = ex.Message;
                        return eh;
                    }
                }
                //FIN LISTADO DE ESCALAS

                if (listaEscalasCargar.Count == 0)
                {
                    eh.Mensaje = "No se han cargado escalas";
                }
            }
            else
            {
                eh.Mensaje = "No se ha detectado la configuración de paneles para administrar precios";
            }

            if (eh.Mensaje == null)
            {
                eh.Mensaje = "EXITO";
                return eh;
            }
            else
            {
                return eh;
            }
        }

        
        List<Escala> listaEscalasCargar = new List<Escala>();
        List<Escala> listaEscalasPreexistentes = new List<Escala>();

        List<Escala> listaEscalasInsertar = new List<Escala>();
        List<Escala> listaEscalasActualizar = new List<Escala>();
        List<Escala> listaEscalasEliminar = new List<Escala>();

        public ErrorHandler escalasCargar(Tn transaccionEscalasCargar, int idListaPrecio, int idMoneda)
        {
            ErrorHandler eh = new ErrorHandler();
            List<EscalaHistorial> listaEscalaHistorialGuardar = new List<EscalaHistorial>();
            ErrorHandler proSerEscalasValidar = productoServicioEscalasValidar(idListaPrecio);
            if (proSerEscalasValidar.Mensaje != "EXITO")
            {
                eh = proSerEscalasValidar;
                return eh;
            }
            else
            {
                /*Si no hubo ningún problema en la parametrización de las escalas, intenta transaccionar*/

                //Verificamos escalas preexistentes asociadas a ListaPrecio
                listaEscalasPreexistentes = new Escala().Select().Where(esc => esc.IdListaPrecio == idListaPrecio && esc.FHBaja == null).ToList();
                if (listaEscalasPreexistentes.Count > 0)
                {
                    //Si hay escalas preexistentes, comparamos cada una con las escalas a cargar
                    foreach (Escala escalaPreexistenteRevisar in listaEscalasPreexistentes)
                    {
                        bool aunExiste = false;
                        foreach (Escala escalaCargaRevisar in listaEscalasCargar)
                        {
                            if (escalaCargaRevisar.IdEscala == escalaPreexistenteRevisar.IdEscala)
                            {
                                //Si la escala aún existe, verificar diferencias

                                //Si hay diferencias, actualizar el registro en la base
                                if (
                                    escalaCargaRevisar.CantInferior != escalaPreexistenteRevisar.CantInferior ||
                                    escalaCargaRevisar.CantSuperior != escalaPreexistenteRevisar.CantSuperior ||
                                    escalaCargaRevisar.CobroMinimoEnPorcentual != escalaPreexistenteRevisar.CobroMinimoEnPorcentual ||
                                    escalaCargaRevisar.Porcentaje != escalaPreexistenteRevisar.Porcentaje ||
                                    escalaCargaRevisar.PrecioContado != escalaPreexistenteRevisar.PrecioContado ||
                                    escalaCargaRevisar.PrecioLista != escalaPreexistenteRevisar.PrecioLista
                                    )
                                {
                                    Escala escalaActualizar = escalaPreexistenteRevisar;

                                    escalaActualizar.CantInferior = escalaCargaRevisar.CantInferior;
                                    escalaActualizar.CantSuperior = escalaCargaRevisar.CantSuperior;
                                    escalaActualizar.CobroMinimoEnPorcentual = escalaCargaRevisar.CobroMinimoEnPorcentual;
                                    escalaActualizar.Porcentaje = escalaCargaRevisar.Porcentaje;
                                    escalaActualizar.PrecioContado = escalaCargaRevisar.PrecioContado;
                                    escalaActualizar.PrecioLista = escalaCargaRevisar.PrecioLista;

                                    escalaActualizar.IdMoneda = idMoneda;

                                    listaEscalasActualizar.Add(escalaActualizar);
                                }                            
                                aunExiste = true;
                            }
                        }

                        if (!aunExiste)
                        {
                            //Si la escala ya no existe, eliminar el registro de la base de datos
                            listaEscalasEliminar.Add(escalaPreexistenteRevisar);
                        }
                    }
                }

                //Luego ingresamos en la lista de inserts todas las escalas nuevas
                foreach (Escala escalaCargaRevisar in listaEscalasCargar)
                {
                    if (escalaCargaRevisar.IdEscala == 0)
                    {
                        Escala escalaInsertar = new Escala();
                        escalaInsertar.IdListaPrecio = idListaPrecio;
                        escalaInsertar.FHAlta = DateTime.Now;
                        escalaInsertar.FHVigencia = DateTime.Now;
                        escalaInsertar.CantInferior = escalaCargaRevisar.CantInferior;
                        escalaInsertar.CantSuperior = escalaCargaRevisar.CantSuperior;
                        escalaInsertar.IdMoneda = escalaCargaRevisar.IdMoneda;
                        escalaInsertar.PrecioLista = escalaCargaRevisar.PrecioLista;
                        escalaInsertar.PrecioContado = escalaCargaRevisar.PrecioContado;
                        escalaInsertar.Porcentaje = escalaCargaRevisar.Porcentaje;
                        escalaInsertar.CobroMinimoEnPorcentual = escalaCargaRevisar.CobroMinimoEnPorcentual;

                        escalaInsertar.IdMoneda = idMoneda;

                        listaEscalasInsertar.Add(escalaInsertar);
                    }
                }

                //Primero se eliminan las escalas que ya no forman parte
                foreach (Escala escalaEliminar in listaEscalasEliminar)
                {
                    escalaEliminar.FHBaja = DateTime.Now;
                    cargarEscalaHistorial(escalaEliminar, transaccionEscalasCargar);
                }

                //Luego insertamos las escalas nuevas
                foreach (Escala escalaInsertar in listaEscalasInsertar)
                {
                    escalaInsertar.Insert(transaccionEscalasCargar);
                    cargarEscalaHistorial(escalaInsertar,transaccionEscalasCargar);
                }

                //Luego se actualizan las escalas que se modificaron
                foreach (Escala escalaActualizar in listaEscalasActualizar)
                {
                    escalaActualizar.FHVigencia = DateTime.Now;
                    escalaActualizar.Update(transaccionEscalasCargar);
                    cargarEscalaHistorial(escalaActualizar,transaccionEscalasCargar);
                }

                eh.Mensaje = "EXITO";
                return eh;
            }
        }

        protected void cargarEscalaHistorial(Escala escala, Tn transaccionEscalaHistorial)
        {
            EscalaHistorial escalaHistorial = new EscalaHistorial();
            escalaHistorial.IdEscala = escala.IdEscala;
            escalaHistorial.IdListaPrecio = escala.IdListaPrecio;
            escalaHistorial.FHAlta = escala.FHAlta;
            escalaHistorial.FHVigencia = escala.FHVigencia;
            escalaHistorial.CantInferior = escala.CantInferior;
            escalaHistorial.CantSuperior = escala.CantSuperior;
            escalaHistorial.IdMoneda = escala.IdMoneda;
            escalaHistorial.PrecioLista = escala.PrecioLista;
            escalaHistorial.PrecioContado = escala.PrecioContado;
            escalaHistorial.Porcentaje = escala.Porcentaje;
            escalaHistorial.CobroMinimoEnPorcentual = escala.CobroMinimoEnPorcentual;
            escalaHistorial.FHBaja = escala.FHBaja;
            escalaHistorial.Insert(transaccionEscalaHistorial);
        }

        protected void gvProductoServicioEscala_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            eliminarEscala(e.RowIndex);
            this.escalasCambio(this, e);
        }

        protected void btnAgregarEscala_Click(object sender, EventArgs e)
        {
            ErrorHandler ehAgregarEscala = productoServicioEscalasValidar(1);
            if (ehAgregarEscala.Mensaje == "EXITO" || ehAgregarEscala.Mensaje == "No se han cargado escalas")
            {
                agregarEscala();
                this.escalasCambio(this, e);
            }
            else
            {
                Message("Aún no puede agregar otra escala","Verifique la siguiente validación antes de agregar una nueva escala: "+ehAgregarEscala.Mensaje,MessagesTypes.warning);
            }
        }

        protected void eliminarEscala(int rowEscalaEliminar)
        {
            List<Escala> listaEscalasBotonEliminar = new List<Escala>();

            foreach (GridViewRow gvRowEscalas in gvProductoServicioEscala.Rows)
            {
                if (gvRowEscalas.RowIndex != rowEscalaEliminar)
                {
                    Escala escalaAgregar = new Escala();
                    escalaAgregar.IdEscala = gvProductoServicioEscala.Rows[gvRowEscalas.RowIndex].Cells[0].Text.ToInt();
                    TextBox txtCantidadMinima = ((TextBox)gvProductoServicioEscala.Rows[gvRowEscalas.RowIndex].Cells[1].FindControl("txtCantidadMinima"));
                    escalaAgregar.CantInferior = txtCantidadMinima.Text.ToInt();
                    TextBox txtCantidadMaxima = ((TextBox)gvProductoServicioEscala.Rows[gvRowEscalas.RowIndex].Cells[2].FindControl("txtCantidadMaxima"));
                    escalaAgregar.CantSuperior = txtCantidadMaxima.Text.ToInt();
                    TextBox txtPrecioFijoLista = ((TextBox)gvProductoServicioEscala.Rows[gvRowEscalas.RowIndex].Cells[3].FindControl("txtPrecioFijoLista"));
                    escalaAgregar.PrecioLista = txtPrecioFijoLista.Text.ToDecimal();
                    TextBox txtPrecioFijoContado = ((TextBox)gvProductoServicioEscala.Rows[gvRowEscalas.RowIndex].Cells[4].FindControl("txtPrecioFijoContado"));
                    escalaAgregar.PrecioContado = txtPrecioFijoContado.Text.ToDecimal();
                    TextBox txtPrecioPorcentualPorcentaje = ((TextBox)gvProductoServicioEscala.Rows[gvRowEscalas.RowIndex].Cells[5].FindControl("txtPrecioPorcentualPorcentaje"));
                    escalaAgregar.Porcentaje = txtPrecioPorcentualPorcentaje.Text.ToInt();
                    TextBox txtPrecioPorcentualCobroMinimo = ((TextBox)gvProductoServicioEscala.Rows[gvRowEscalas.RowIndex].Cells[6].FindControl("txtPrecioPorcentualCobroMinimo"));
                    escalaAgregar.CobroMinimoEnPorcentual = txtPrecioPorcentualCobroMinimo.Text.ToDecimal();
                    listaEscalasBotonEliminar.Add(escalaAgregar);
                }
            }

            List<Escala> listaEscalasOrdenada = listaEscalasBotonEliminar.OrderBy(ec => ec.CantInferior).ToList();

            gvProductoServicioEscala.DataSource = listaEscalasOrdenada.ToDataTable();
            gvProductoServicioEscala.DataBind();
            upProductoServicioEscala.Update();

            string combinacionPanelesPrecioEscala = Session["combinacionPanelesPrecioEscala"].ToString();

            if (combinacionPanelesPrecioEscala.Contains("FIJO"))
            {
                seleccionTipoPrecio("FIJO");
            }
            else
            {
                seleccionTipoPrecio("PORCENTUAL");
            }

            int indiceEscalaRecuperar = 0;
            foreach (Escala escalaRecuperar in listaEscalasOrdenada)
            {
                if (escalaRecuperar.CantInferior != 0 && escalaRecuperar.CantSuperior != 0)
                {
                    ((TextBox)gvProductoServicioEscala.Rows[indiceEscalaRecuperar].Cells[1].FindControl("txtCantidadMinima")).Text = escalaRecuperar.CantInferior.ToString();
                    ((TextBox)gvProductoServicioEscala.Rows[indiceEscalaRecuperar].Cells[2].FindControl("txtCantidadMaxima")).Text = escalaRecuperar.CantSuperior.ToString();
                    ((TextBox)gvProductoServicioEscala.Rows[indiceEscalaRecuperar].Cells[3].FindControl("txtPrecioFijoLista")).Text = escalaRecuperar.PrecioLista.ToString();
                    ((TextBox)gvProductoServicioEscala.Rows[indiceEscalaRecuperar].Cells[4].FindControl("txtPrecioFijoContado")).Text = escalaRecuperar.PrecioContado.ToString();
                    ((TextBox)gvProductoServicioEscala.Rows[indiceEscalaRecuperar].Cells[5].FindControl("txtPrecioPorcentualPorcentaje")).Text = escalaRecuperar.Porcentaje.ToString();
                    ((TextBox)gvProductoServicioEscala.Rows[indiceEscalaRecuperar].Cells[6].FindControl("txtPrecioPorcentualCobroMinimo")).Text = escalaRecuperar.CobroMinimoEnPorcentual.ToString();
                }
                indiceEscalaRecuperar++;
            }
            upProductoServicioEscala.Update();
            Message("Se ha quitado la escala exitosamente","Los cambios impactarán al guardar la configuración de precios, para deshacer presione CANCELAR",MessagesTypes.info);
        }

        protected void agregarEscala()
        {
            List<Escala> listaEscalasBotonAgregar = new List<Escala>();
            foreach (GridViewRow gvRowEscalas in gvProductoServicioEscala.Rows)
            {
                Escala escalaAgregar = new Escala();
                escalaAgregar.IdEscala = gvProductoServicioEscala.Rows[gvRowEscalas.RowIndex].Cells[0].Text.ToInt();
                TextBox txtCantidadMinima = ((TextBox)gvProductoServicioEscala.Rows[gvRowEscalas.RowIndex].Cells[1].FindControl("txtCantidadMinima"));
                escalaAgregar.CantInferior = txtCantidadMinima.Text.ToInt();
                TextBox txtCantidadMaxima = ((TextBox)gvProductoServicioEscala.Rows[gvRowEscalas.RowIndex].Cells[2].FindControl("txtCantidadMaxima"));
                escalaAgregar.CantSuperior = txtCantidadMaxima.Text.ToInt();
                TextBox txtPrecioFijoLista = ((TextBox)gvProductoServicioEscala.Rows[gvRowEscalas.RowIndex].Cells[3].FindControl("txtPrecioFijoLista"));
                escalaAgregar.PrecioLista = txtPrecioFijoLista.Text.ToInt();
                TextBox txtPrecioFijoContado = ((TextBox)gvProductoServicioEscala.Rows[gvRowEscalas.RowIndex].Cells[4].FindControl("txtPrecioFijoContado"));
                escalaAgregar.PrecioContado = txtPrecioFijoContado.Text.ToInt();
                TextBox txtPrecioPorcentualPorcentaje = ((TextBox)gvProductoServicioEscala.Rows[gvRowEscalas.RowIndex].Cells[5].FindControl("txtPrecioPorcentualPorcentaje"));
                escalaAgregar.Porcentaje = txtPrecioPorcentualPorcentaje.Text.ToInt();
                TextBox txtPrecioPorcentualCobroMinimo = ((TextBox)gvProductoServicioEscala.Rows[gvRowEscalas.RowIndex].Cells[6].FindControl("txtPrecioPorcentualCobroMinimo"));
                escalaAgregar.CobroMinimoEnPorcentual = txtPrecioPorcentualCobroMinimo.Text.ToInt();
                listaEscalasBotonAgregar.Add(escalaAgregar);
            }

            Escala escalaNueva = new Escala(); escalaNueva.IdEscala = 0;
            List<Escala> listaEscalasOrdenada = listaEscalasBotonAgregar.OrderBy(ec => ec.CantInferior).ToList();
            listaEscalasOrdenada.Add(escalaNueva);

            gvProductoServicioEscala.DataSource = listaEscalasOrdenada.ToDataTable();
            gvProductoServicioEscala.DataBind();
            upProductoServicioEscala.Update();

            string combinacionPanelesPrecioEscala = Session["combinacionPanelesPrecioEscala"].ToString();

            if (combinacionPanelesPrecioEscala.Contains("FIJO"))
            {
                seleccionTipoPrecio("FIJO");
            }
            else
            {
                seleccionTipoPrecio("PORCENTUAL");
            }

            int indiceEscalaRecuperar = 0;
            foreach (Escala escalaRecuperar in listaEscalasOrdenada)
            {
                if (escalaRecuperar.CantInferior != 0 && escalaRecuperar.CantSuperior != 0)
                {
                    ((TextBox)gvProductoServicioEscala.Rows[indiceEscalaRecuperar].Cells[1].FindControl("txtCantidadMinima")).Text = escalaRecuperar.CantInferior.ToString();
                    ((TextBox)gvProductoServicioEscala.Rows[indiceEscalaRecuperar].Cells[2].FindControl("txtCantidadMaxima")).Text = escalaRecuperar.CantSuperior.ToString();
                    ((TextBox)gvProductoServicioEscala.Rows[indiceEscalaRecuperar].Cells[3].FindControl("txtPrecioFijoLista")).Text = escalaRecuperar.PrecioLista.ToString();
                    ((TextBox)gvProductoServicioEscala.Rows[indiceEscalaRecuperar].Cells[4].FindControl("txtPrecioFijoContado")).Text = escalaRecuperar.PrecioContado.ToString();
                    ((TextBox)gvProductoServicioEscala.Rows[indiceEscalaRecuperar].Cells[5].FindControl("txtPrecioPorcentualPorcentaje")).Text = escalaRecuperar.Porcentaje.ToString();
                    ((TextBox)gvProductoServicioEscala.Rows[indiceEscalaRecuperar].Cells[6].FindControl("txtPrecioPorcentualCobroMinimo")).Text = escalaRecuperar.CobroMinimoEnPorcentual.ToString();
                }
                indiceEscalaRecuperar++;
            }
            upProductoServicioEscala.Update();
        }

        public event EventHandler escalasCambio;
    }
}