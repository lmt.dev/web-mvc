﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using CGADM;
using MagicSQL;

namespace AdmSys.Controls
{
    public partial class ucFiltro_DistribuidorUnidadNegocioReseller : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

            }
        }

        public void upActualizar()
        {
            upCombosDistribuidorUnidadNegocioReseller.Update();
        }

        public int idDistribuidor
        {
            get { return ddlDistribuidor.SelectedValue.ToIntID(); }
            set
            {
                ddlDistribuidor.SelectedValue = value.ToCryptoID();
                colectarIdentificadores();
                upCombosDistribuidorUnidadNegocioReseller.Update();
            }
        }

        public int idUnidadNegocio
        {
            get { return ddlUnidadNegocio.SelectedValue.ToIntID(); }
            set
            {
                ddlUnidadNegocio.SelectedValue = value.ToCryptoID();
                colectarIdentificadores();
                upCombosDistribuidorUnidadNegocioReseller.Update();
            }
        }

        public int idReseller
        {
            get { return ddlReseller.SelectedValue.ToIntID(); }
            set
            {
                ddlReseller.SelectedValue = value.ToCryptoID();
                colectarIdentificadores();
                upCombosDistribuidorUnidadNegocioReseller.Update();
            }
        }

        private void colectarIdentificadores()
        {
            Session.Add("idDistribuidor", ddlDistribuidor.SelectedValue.ToIntID().ToString());
            Session.Add("idUnidadNegocio", ddlUnidadNegocio.SelectedValue.ToIntID().ToString());
            Session.Add("idReseller", ddlDistribuidor.SelectedValue.ToIntID().ToString());
        }

        public DistribuidorUnidadNegocio distribuidorUnidadNegocio
        {
            get
            {
                DistribuidorUnidadNegocio distribuidorUnidadNegocio =
                    new DistribuidorUnidadNegocio().
                        Select().
                            Where(dun =>
                                dun.IdDistribuidor == idDistribuidor &&
                                dun.IdUnidadNegocio == idUnidadNegocio
                                ).FirstOrDefault();
                return distribuidorUnidadNegocio;
            }
            set
            {
                distribuidorUnidadNegocio = value;
            }
        }

        public PlanComercial planComercial
        {
            get
            {
                PlanComercial planComercial = 
                    new PlanComercial().
                        Select().Where(pc => 
                        pc.IdDistribuidorUnidadNegocio == distribuidorUnidadNegocio.IdDistribuidorUnidadNegocio
                        ).FirstOrDefault();
                return planComercial;
            }

            set
            {
                planComercial = value;
            }
        }

        public void ddlDistribuidorInhabilitar()
        {
            ddlDistribuidor.Enabled = false;
            upCombosDistribuidorUnidadNegocioReseller.Update();
        }

        public void ddlUnidadNegocioInhabilitar()
        {
            ddlUnidadNegocio.Enabled = false;
            upCombosDistribuidorUnidadNegocioReseller.Update();
        }

        public void ddlResellerInhabilitar()
        {
            ddlReseller.Enabled = false;
            upCombosDistribuidorUnidadNegocioReseller.Update();
        }

        public void ddlDistribuidorHabilitar()
        {
            ddlDistribuidor.Enabled = true;
            upCombosDistribuidorUnidadNegocioReseller.Update();
        }

        public void ddlUnidadNegocioHabilitar()
        {
            ddlUnidadNegocio.Enabled = true;
            upCombosDistribuidorUnidadNegocioReseller.Update();
        }

        public void ddlResellerHabilitar()
        {
            ddlReseller.Enabled = true;
            upCombosDistribuidorUnidadNegocioReseller.Update();
        }

        public void ddlDistribuidorCargar()
        {
            foreach (Distribuidor m in new Distribuidor().Select().Where(m => m.FHBaja == null))
            {
                ddlDistribuidor.Items.Add(new ListItem(m.RazonSocial, m.IdDistribuidor.ToCryptoID()));
            }
            ddlDistribuidor_OnSelectedIndexChanged(null, null);
        }

        public void ddlDistribuidor_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            // Id del Distribuidor desencriptado
            int idDistribuidor = ddlDistribuidor.SelectedValue.ToIntID();

            // Lista de los registros de la tabla de enlace DistribuidorUnidadNegocio con IdDistribuidor igual al seleccionado en el ddlDistribuidor
            List<DistribuidorUnidadNegocio> listDistribuidorUnidadNegocio =
            new DistribuidorUnidadNegocio().Select().Where(dun => dun.IdDistribuidor == idDistribuidor).ToList();

            // Lista de los registros de la tabla UnidadNegocio con IdUnidadNegocio contenido en la lista listDistribuidorUnidadNegocio (filtrada en el paso anterior)
            List<UnidadNegocio> listUnidadNegocio = new UnidadNegocio()
                .Select().Where(dun =>
                listDistribuidorUnidadNegocio.Any(un => dun.IdUnidadNegocio == un.IdUnidadNegocio)).ToList();

            ddlUnidadNegocio.Items.Clear();
            ddlReseller.Items.Clear();

            foreach (UnidadNegocio unidadNegocio in listUnidadNegocio)
            {
                ddlUnidadNegocio.Items.Add(
                    new
                    ListItem(unidadNegocio.Descripcion, unidadNegocio.IdUnidadNegocio.ToCryptoID()));
            }
            if (ddlUnidadNegocio.Items.Count > 0) { ddlUnidadNegocio_OnSelectedIndexChanged(null, null); }
            this.filtrosCambio(this, e);
            upCombosDistribuidorUnidadNegocioReseller.Update();
        }

        public void ddlUnidadNegocio_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            int idDistribuidor = ddlDistribuidor.SelectedValue.ToIntID();
            int idUnidadNegocio = ddlUnidadNegocio.SelectedValue.ToIntID();

            ddlReseller.Items.Clear();

            System.Data.DataTable dt = 
                new SP("CGADM").Execute("usp_GetResellersByDistDistUnidadNegocio",
                    P.Add("idDistribuidor", idDistribuidor),
                    P.Add("idUnidadNegocio", idUnidadNegocio));

            ddlReseller.Items.Add(new ListItem("Todos", 0.ToCryptoID()));
            foreach (System.Data.DataRow r in dt.Rows)
            {
                ddlReseller.Items.Add(new ListItem(r[1].ToString(), r[0].ToString().ToInt().ToCryptoID()));
            }
            this.filtrosCambio(this, e);
            upCombosDistribuidorUnidadNegocioReseller.Update();
        }

        public void ddlReseller_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            upCombosDistribuidorUnidadNegocioReseller.Update();
        }

        public void ddlReseller_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.filtrosCambio(this, e);
            upCombosDistribuidorUnidadNegocioReseller.Update();
        }

        public event EventHandler filtrosCambio;
    }
}