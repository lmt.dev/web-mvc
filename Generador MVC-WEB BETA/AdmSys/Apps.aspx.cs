﻿using System;
using System.Reflection;

namespace AdmSys
{
    public partial class Apps : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Version de la compilación.
            lblVersion.Text = "Versión: " + Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }
    }
}