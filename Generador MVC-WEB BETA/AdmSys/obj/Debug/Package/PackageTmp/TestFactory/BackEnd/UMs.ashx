﻿<%@ WebHandler Language="C#" Class="UMs" %>

using System;
using System.Collections.Generic;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

public class UMs : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        string j = "";
        string connectionString = ConfigurationManager.ConnectionStrings["TestFactory"].ConnectionString;
        using (SqlConnection connection = new SqlConnection(connectionString))
        {
            string tsql = @"SELECT IdDispositivo ID
                                  ,VersionHardware Hard
                                  ,IdFirmware Soft
                                  ,GsmImei IMEI
                                  ,WifiMac MAC
                                  ,CONVERT(VARCHAR, FechaHoraAlta,120) Reportado
                                  ,FechaHoraProduccion Produccion
                            FROM [TestFactory].[dbo].[UMs]";
            using (SqlCommand command = new SqlCommand(tsql, connection))
            {
                command.CommandType = CommandType.Text;
                connection.Open();
                SqlDataReader dataReader = command.ExecuteReader();
                DataTable dataTable = new DataTable();
                dataTable.Load(dataReader);
                j = AdmSys.TestFactory.BackEnd.json.DataTable2JSON(dataTable);
            }
        }
        context.Response.ContentType = "application/json";
        context.Response.Write(j);
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
    
}