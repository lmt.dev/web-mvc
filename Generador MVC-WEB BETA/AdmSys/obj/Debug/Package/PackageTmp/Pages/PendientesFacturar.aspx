﻿<%@ Page Title="Pendientes Facturar" Language="C#" MasterPageFile="~/Pages/Page.Master" AutoEventWireup="true" CodeBehind="PendientesFacturar.aspx.cs" Inherits="Pages.PendientesFacturar" %>

<%-- Head --%>
<asp:Content ID="cHead" ContentPlaceHolderID="cphHead" runat="server">

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
        #modalNuevoPago .modal-dialog {
            width: 95%;
        }
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>
    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {
        });

    </script>
</asp:Content>

<%-- Body --%>
<asp:Content ID="cBody" ContentPlaceHolderID="cphBody" runat="server">

    <%-- Encabezado --%>
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">

                <%-- Atras --%>
                <a class="btn btn-default pull-left" onclick="history.back();">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                </a>

                <%-- Separador horizontal --%>
                <span class="pull-left">&nbsp;</span>

                <%-- Actualizar --%>
                <a class="btn btn-default pull-left" onclick="window.location.reload()">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                </a>

                <%-- Titulo --%>
                <asp:Label ID="lblTitulo" Text="" runat="server" />

                <%-- Nuevo --%>
                <asp:Button ID="btnNuevo" Text="Nuevo" CssClass="btn btn-primary pull-right" runat="server" OnClick="btnNuevo_Click" />

            </h3>

        </div>
    </div>

    <%-- Contenido --%>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel-body">
                <div class="row">

                    <%-- Filtros --%>
                    <div class="panel panel-default">

                        <div class="panel-body">

                            <%-- Distribuidor --%>
                            <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Distribuidor" runat="server" />
                                </label>
                                <asp:DropDownList ID="ddlDistribuidor" CssClass="form-control select-single" AutoPostBack="true" OnSelectedIndexChanged="ddlDistribuidor_OnSelectedIndexChanged" runat="server">
                                </asp:DropDownList>
                            </div>

                            <%-- Unidad Negocio --%>
                            <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Unidad Negocio" runat="server" />
                                </label>
                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="ddlUnidadNegocio" CssClass="form-control select-single" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlUnidadNegocio_OnSelectedIndexChanged">
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlDistribuidor" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>

                            <%-- Reseller --%>
                            <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Reseller" runat="server" />
                                </label>
                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="ddlVendingReseller" CssClass="form-control select-single" runat="server">
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlUnidadNegocio" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 text-center">
                        <asp:Button ID="btnFiltrar" Text="Filtrar" CssClass="btn btn-info" runat="server" OnClick="btnFiltrar_Click" />
                        <button type="reset" class="btn btn-default">
                            <asp:Label Text="Restablecer" runat="server" />
                        </button>
                    </div>

                </div>
            </div>
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <asp:GridView ID="gvPendientesFacturar" CssClass="table table-striped table-bordered table-hover grid-view clickable crypto-id" data-model="PendienteFacturar.aspx"
                        Width="100%" AutoGenerateColumns="False" runat="server" OnRowDataBound="gvPendientesFacturar_RowDataBound" EmptyDataText="Sin datos para mostrar.">
                        <Columns>
                            <asp:BoundField HeaderText="" />

                            <asp:BoundField HeaderText="#" DataField="IdPendienteFacturar" HeaderStyle-CssClass="crypto-id-column" ItemStyle-CssClass="crypto-id-column" />
                            <asp:BoundField HeaderText="Fecha Alta" DataField="FHAlta" ItemStyle-CssClass="date-time" />
                            <asp:BoundField HeaderText="Fecha Imputacion" DataField="FechaParaImputar" ItemStyle-CssClass="date-time" />
                            <asp:BoundField HeaderText="Descripcion" DataField="Descripcion" />
                            <asp:BoundField HeaderText="Importe" DataField="Importe" ItemStyle-CssClass="numeric-money-positive" />
                            <asp:BoundField HeaderText="Observación" DataField="Observacion" />
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnFiltrar" />
                </Triggers>
            </asp:UpdatePanel>
            <br />
            <div class="well">
                <h4>
                    <asp:Label Text="Información adicional o ayuda" runat="server" />
                </h4>
                <p>
                    <asp:Label Text="Tincidunt integer eu augue augue nunc elit dolor, luctus placerat scelerisque euismod, iaculis eu lacus nunc mi elit, vehicula ut laoreet ac, aliquam sit amet justo nunc tempor, metus vel." runat="server" />
                </p>
            </div>
        </div>
    </div>
</asp:Content>