﻿<%@ Page Title="Usuario" Language="C#" MasterPageFile="~/Pages/Page.Master" AutoEventWireup="true" CodeBehind="Usuario.aspx.cs" Inherits="Pages.Usuario" %>

<%-- Head --%>
<asp:Content ID="cHead" ContentPlaceHolderID="cphHead" runat="server">

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
        .btn-usuarios {
            float: left;
        }

        .btns-reset-save {
            float: right;
        }
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>

    <%-- Script - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {
        });

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        // Solicita confirmación antes de eliminar
        function ConfirmDelete() {
            swal({
                title: "¿Eliminar el Usuario?",
                text: "Confirma la eliminación",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ac2925",
                confirmButtonText: "Si",
                closeOnConfirm: false
            },
            function () {
                __doPostBack('<%= btnDelete.UniqueID %>', "");
            });
        }
    </script>
</asp:Content>

<%-- Body --%>
<asp:Content ID="cBody" ContentPlaceHolderID="cphBody" runat="server">

    <%-- Hash del Usuario --%>
    <asp:HiddenField ID="hdnHash" runat="server"></asp:HiddenField>

    <%-- Perfil del Usuario --%>
    <asp:HiddenField ID="hdnIdPerfil" runat="server"></asp:HiddenField>

    <%-- Cuenta del Usuario --%>
    <asp:HiddenField ID="hdnIdCuenta" runat="server"></asp:HiddenField>

    <%-- Encabezado --%>
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">

                <%-- Atras --%>
                <a class="btn btn-default pull-left" onclick="history.back();">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                </a>

                <%-- Separador horizontal --%>
                <span class="pull-left">&nbsp;</span>

                <%-- Actualizar --%>
                <a class="btn btn-default pull-left" onclick="window.location.reload()">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                </a>

                <%-- Titulo --%>
                <asp:Label ID="lblTitulo" Text="" runat="server" />

                <%-- Eliminar --%>
                <asp:Button ID="btnDelete" Text="Eliminar" CssClass="btn btn-danger pull-right" runat="server"
                    OnClientClick="ConfirmDelete(); return false;" OnClick="btnDelete_Click" data-securable="ABMUsuario" CausesValidation="false" />

                <%-- Separador horizontal --%>
                <span class="pull-right">&nbsp;</span>

                <%-- Editar --%>
                <asp:Button ID="btnEdit" Text="Editar" CssClass="btn btn-warning pull-right" runat="server" CausesValidation="false"
                    OnClick="btnEdit_Click" data-securable="ABMUsuario" />

            </h3>

        </div>
    </div>

    <%-- Contenido --%>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel-body">
                <div class="row">

                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12" id="fgrDistribuidor" runat="server">
                                <label>
                                    <asp:Label Text="Distribuidor" runat="server" />
                                </label>
                                <asp:DropDownList ID="ddlDistribuidor" CssClass="form-control select-single" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlPropietario_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12" runat="server">
                                <label>
                                    <asp:Label Text="Perfil" runat="server" />
                                </label>
                                <asp:DropDownList ID="ddlPerfil" CssClass="form-control select-single" runat="server">
                                </asp:DropDownList>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlDistribuidor" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <hr />
                <div class="row">
                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Nombre" runat="server" />
                        </label>
                        <asp:TextBox ID="txtNombre" CssClass="form-control text-capital" Text="" runat="server" />
                        <asp:RequiredFieldValidator CssClass="required-field-validator" ControlToValidate="txtNombre" runat="server" />
                    </div>

                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Apellido" runat="server" />
                        </label>
                        <asp:TextBox ID="txtApellido" CssClass="form-control text-capital" Text="" runat="server" />
                        <asp:RequiredFieldValidator CssClass="required-field-validator" ControlToValidate="txtApellido" runat="server" />
                    </div>

                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Email" runat="server" />
                        </label>
                        <asp:TextBox ID="txtEmail" CssClass="form-control  text-lower email" Text="" runat="server" OnTextChanged="txtEmail_TextChanged" AutoPostBack="true" />
                        <asp:RequiredFieldValidator CssClass="required-field-validator" ControlToValidate="txtEmail" runat="server" />
                    </div>

                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Telefono" runat="server" />
                        </label>
                        <asp:TextBox ID="txtTelefono" CssClass="form-control phone" Text="" runat="server" />
                        <asp:RequiredFieldValidator CssClass="required-field-validator" ControlToValidate="txtTelefono" runat="server" />
                    </div>

                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Login Web" runat="server" />
                        </label>
                        <asp:TextBox ID="txtNombreUsuario" CssClass="form-control text-lower" Text="" runat="server" OnTextChanged="txtNombreUsuario_TextChanged" AutoPostBack="true" />
                        <asp:RequiredFieldValidator CssClass="required-field-validator" ControlToValidate="txtNombreUsuario" runat="server" />
                    </div>
                </div>
                <hr />
                <div class="row">
                    <div class="form-group col-xs-12">
                        <label>
                            <asp:Label Text="Notas" runat="server" />
                        </label>
                        <textarea class="form-control" rows="3" id="txtNotas" runat="server"></textarea>
                    </div>
                </div>

                <!-- Nav tabs -->
                <ul id="tabs" class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#seguridad" aria-controls="misc" role="tab" data-toggle="tab">Seguridad</a></li>
                    <li role="presentation"><a href="#idioma" aria-controls="home" role="tab" data-toggle="tab">Idioma y Sistema Métrico</a></li>
                    <li role="presentation"><a href="#numeros" aria-controls="numeric" role="tab" data-toggle="tab">Números</a></li>
                    <li role="presentation"><a href="#monedas" aria-controls="select" role="tab" data-toggle="tab">Monedas</a></li>
                    <li role="presentation"><a href="#horafecha" aria-controls="check" role="tab" data-toggle="tab">Hora y Fecha</a></li>
                    <li role="presentation"><a href="#baja" aria-controls="radio" role="tab" data-toggle="tab">Estado de Baja</a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane active" id="seguridad">
                        <br />

                        <div style="padding-left: 10px; padding-right: 10px; overflow-x: hidden;">

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Pregunta Secreta 1" runat="server" />
                                </label>
                                <asp:TextBox ID="txtPreguntaSecreta1" CssClass="form-control" Text="" runat="server" />
                                <asp:RequiredFieldValidator CssClass="required-field-validator" ControlToValidate="txtPreguntaSecreta1" runat="server" />
                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Respuesta 1" runat="server" />
                                </label>
                                <asp:TextBox ID="txtRespuesta1" CssClass="form-control" Text="" runat="server" />
                                <asp:RequiredFieldValidator CssClass="required-field-validator" ControlToValidate="txtRespuesta1" runat="server" />
                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Pregunta Secreta 2" runat="server" />
                                </label>
                                <asp:TextBox ID="txtPreguntaSecreta2" CssClass="form-control" Text="" runat="server" />
                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Respuesta 2" runat="server" />
                                </label>
                                <asp:TextBox ID="txtRespuesta2" CssClass="form-control" Text="" runat="server" />
                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Pregunta Secreta 3" runat="server" />
                                </label>
                                <asp:TextBox ID="txtPreguntaSecreta3" CssClass="form-control" Text="" runat="server" />
                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Respuesta 3" runat="server" />
                                </label>
                                <asp:TextBox ID="txtRespuesta3" CssClass="form-control" Text="" runat="server" />
                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Pregunta Secreta 4" runat="server" />
                                </label>
                                <asp:TextBox ID="txtPreguntaSecreta4" CssClass="form-control" Text="" runat="server" />
                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Respuesta 4" runat="server" />
                                </label>
                                <asp:TextBox ID="txtRespuesta4" CssClass="form-control" Text="" runat="server" />
                            </div>

                        </div>

                    </div>

                    <div role="tabpanel" class="tab-pane" id="idioma">
                        <br />
                        <div style="padding-left: 10px; padding-right: 10px; overflow-x: hidden;">

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Idioma" runat="server" />
                                </label>
                                <asp:DropDownList ID="ddlIdioma" CssClass="form-control select-single" runat="server">
                                </asp:DropDownList>
                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Sistema de Medida" runat="server" />
                                </label>
                                <asp:DropDownList ID="ddlSistemaMedida" CssClass="form-control select-single" runat="server">
                                    <asp:ListItem Selected="True" Value="1">Métrico</asp:ListItem>
                                    <asp:ListItem Value="2">U. S.</asp:ListItem>
                                </asp:DropDownList>
                            </div>

                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="numeros">
                        <br />
                        <div style="padding-left: 10px; padding-right: 10px; overflow-x: hidden;">

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Símbolo Decimal en Números" runat="server" />
                                </label>
                                <asp:DropDownList ID="ddlNumeroSimboloDecimal" CssClass="form-control select-single" runat="server">
                                    <asp:ListItem Selected="True" Value=",">Coma</asp:ListItem>
                                    <asp:ListItem Value=".">Punto</asp:ListItem>
                                </asp:DropDownList>
                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Dígitos Decimales en Números" runat="server" />
                                </label>
                                <asp:TextBox ID="txtNumeroDigitosDecimales" CssClass="form-control numeric-integer" Text="" runat="server" />
                                <asp:RequiredFieldValidator CssClass="required-field-validator" ControlToValidate="txtNumeroDigitosDecimales" runat="server" />
                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Separador de Miles en Números" runat="server" />
                                </label>
                                <asp:DropDownList ID="ddlNumeroSeparadorMiles" CssClass="form-control select-single" runat="server">
                                    <asp:ListItem Selected="True" Value=".">Punto</asp:ListItem>
                                    <asp:ListItem Value=",">Coma</asp:ListItem>
                                </asp:DropDownList>
                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Ceros a la Izquierda en Números" runat="server" />
                                </label>
                                <asp:DropDownList ID="ddlNumeroCerosIzquierda" class="form-control select-single" runat="server">
                                    <asp:ListItem Selected="True" Value="false">No</asp:ListItem>
                                    <asp:ListItem Value="true">Si</asp:ListItem>
                                </asp:DropDownList>
                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Completar Decimales con Ceros en Números" runat="server" />
                                </label>
                                <asp:DropDownList ID="ddlNumeroCompletarDecimalesCeros" CssClass="form-control select-single" runat="server">
                                    <asp:ListItem Selected="True" Value="false">No</asp:ListItem>
                                    <asp:ListItem Value="true">Si</asp:ListItem>
                                </asp:DropDownList>
                            </div>

                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="monedas">
                        <br />
                        <div style="padding-left: 10px; padding-right: 10px; overflow-x: hidden;">

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Símbolo de Moneda" runat="server" />
                                </label>
                                <asp:TextBox ID="txtMonedaSimbolo" CssClass="form-control" Text="" runat="server" />
                                <asp:RequiredFieldValidator CssClass="required-field-validator" ControlToValidate="txtMonedaSimbolo" runat="server" />
                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Formato de Moneda" runat="server" />
                                </label>
                                <asp:DropDownList ID="ddlMonedaFormato" CssClass="form-control select-single" runat="server">
                                    <asp:ListItem Selected="True" Value="1">$1,1</asp:ListItem>
                                    <asp:ListItem Value="2">1,1$</asp:ListItem>
                                    <asp:ListItem Value="3">$ 1,1</asp:ListItem>
                                    <asp:ListItem Value="4">1,1 $</asp:ListItem>
                                </asp:DropDownList>
                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Símbolo Decimal en Monedas" runat="server" />
                                </label>
                                <asp:DropDownList ID="ddlMonedaSimboloDecimal" CssClass="form-control select-single" runat="server">
                                    <asp:ListItem Selected="True" Value=",">Coma</asp:ListItem>
                                    <asp:ListItem Value=".">Punto</asp:ListItem>
                                </asp:DropDownList>
                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Dígitos Decimales en Monedas" runat="server" />
                                </label>
                                <asp:TextBox ID="txtMonedaDigitosDecimales" CssClass="form-control numeric-integer" Text="" runat="server" />
                                <asp:RequiredFieldValidator CssClass="required-field-validator" ControlToValidate="txtMonedaDigitosDecimales" runat="server" />
                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Separador de Miles en Monedas" runat="server" />
                                </label>
                                <asp:DropDownList ID="ddlMonedaSeparadorMiles" CssClass="form-control select-single" runat="server">
                                    <asp:ListItem Selected="True" Value=".">Punto</asp:ListItem>
                                    <asp:ListItem Value=",">Coma</asp:ListItem>
                                </asp:DropDownList>
                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Ceros a la Izquierda en Monedas" runat="server" />
                                </label>
                                <asp:DropDownList ID="ddlMonedaCerosIzquierda" CssClass="form-control select-single" runat="server">
                                    <asp:ListItem Selected="True" Value="false">No</asp:ListItem>
                                    <asp:ListItem Value="true">Si</asp:ListItem>
                                </asp:DropDownList>
                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Completar Decimales con Ceros en Monedas" runat="server" />
                                </label>
                                <asp:DropDownList ID="ddlMonedaCompletarDecimalesCeros" CssClass="form-control select-single" runat="server">
                                    <asp:ListItem Selected="True" Value="false">No</asp:ListItem>
                                    <asp:ListItem Value="true">Si</asp:ListItem>
                                </asp:DropDownList>
                            </div>

                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="horafecha">
                        <br />
                        <div style="padding-left: 10px; padding-right: 10px; overflow-x: hidden;">

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Zona Horaria" runat="server" />
                                </label>
                                <asp:DropDownList ID="ddlZonaHoraria" CssClass="form-control select-single" runat="server">
                                </asp:DropDownList>
                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Formato de Hora" runat="server" />
                                </label>
                                <asp:DropDownList ID="ddlFormatoHora" CssClass="form-control select-single" runat="server">
                                    <asp:ListItem Selected="True" Value="1">hh:mm tt</asp:ListItem>
                                    <asp:ListItem Value="2">h:mm tt</asp:ListItem>
                                    <asp:ListItem Value="3">HH:mm</asp:ListItem>
                                    <asp:ListItem Value="4">H:mm</asp:ListItem>
                                </asp:DropDownList>
                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Formato de Fecha" runat="server" />
                                </label>
                                <asp:TextBox ID="txtFormatoFecha" CssClass="form-control" Text="" runat="server" />
                                <asp:RequiredFieldValidator CssClass="required-field-validator" ControlToValidate="txtFormatoFecha" runat="server" />
                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Primer Día de la Semana" runat="server" />
                                </label>
                                <asp:DropDownList ID="ddlPrimerDiaSemana" CssClass="form-control select-single" runat="server">
                                </asp:DropDownList>
                            </div>

                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="baja">
                        <br />
                        <div style="padding-left: 10px; padding-right: 10px; overflow-x: hidden;">

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Fecha de Baja" runat="server" />
                                </label>
                                <asp:TextBox ID="txtFechaBaja" CssClass="form-control" Text="" runat="server" Enabled="false" />
                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Fecha y Hora de Bloqueo" runat="server" />
                                </label>
                                <asp:TextBox ID="txtFechaHoraBloqueo" CssClass="form-control" Text="" runat="server" Enabled="false" />
                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Intentos de Ingreso Fallidos" runat="server" />
                                </label>
                                <asp:TextBox ID="txtIntentosIngresoFallidos" CssClass="form-control" Text="" runat="server" Enabled="false" />
                            </div>

                        </div>
                    </div>

                </div>
            </div>

            <%-- Separador vertical --%>
            <hr />

            <%-- Botón Usuarios, Guardar y Restablecer --%>
            <div class="btn-usuarios">
                <asp:Button ID="btnUsuarios" Text="Usuarios" CssClass="btn btn-info" runat="server" OnClick="btnUsuarios_Click" CausesValidation="false" />
            </div>
            <div class="btns-reset-save">
                <button type="reset" class="btn btn-default">
                    <asp:Label Text="Restablecer" runat="server" />
                </button>
                <asp:Button ID="btnSave" Text="Guardar" CssClass="btn btn-success" Enabled="False" runat="server" OnClick="btnSave_Click" data-securable="ABMUsuario" />
            </div>
        </div>
    </div>
</asp:Content>