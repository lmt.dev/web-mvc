﻿<%@ Page Title="Perfil" Language="C#" MasterPageFile="~/Pages/Page.Master" AutoEventWireup="true" CodeBehind="Perfil.aspx.cs" Inherits="Pages.Perfil" %>

<%-- Head --%>
<asp:Content ID="cHead" ContentPlaceHolderID="cphHead" runat="server">

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- jsTree --%>
    <link href="../Content/vendor/jstree/themes/default/style.min.css" rel="stylesheet" />

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
        .btn-perfiles {
            float: left;
        }

        .btns-reset-save {
            float: right;
        }
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>

    <%-- jsTree --%>
    <script src="../Scripts/vendor/jstree.min.js"></script>

    <%-- Script - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {
            $('#trPermisos').jstree({ // aplica el plugin jstree
                plugins: ['checkbox'], // aplica el plugin checkbox al jstree
                checkbox: { // opciones de configuracion del plugin checkbox
                    three_state: false, // evita que al checkear un nodo padre se checkeen los nodos hijos
                    whole_node: false, // evita que se checkee un nodo al seleccionarlo
                    tie_selection: false // permite checkear o descheckear un nodo que no esta seleccionado
                },
                'core': {
                    data: <%= Componentes %>
                    }
            }).on("check_node.jstree", function (e, data) { // evento node uncheck
                var parentNodeName = data.node.parent; // nodo padre del nodo checkeado
                if (parentNodeName != "#") { // si el nodo padre existe lo checkeo (si fuese el nodo raiz no tendria padre)
                    $("#trPermisos").jstree("check_node", $('#' + parentNodeName)); // checkeo el nodo padre del nodo hijo checkeado
                }
            }).on("uncheck_node.jstree", function (e, data) { // evento node check
                for (var n = 0; n < data.node.children.length; n++) { // recorro los nodos hijos
                    var childNodeName = data.node.children[n]; // nombre del nodo hijo
                    $("#trPermisos").jstree("uncheck_node", $('#' + childNodeName)); // descheckeo los nodos hijos
                };
            });
        });

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        // Solicita confirmación antes de eliminar
        function ConfirmDelete() {
            swal({
                title: "¿Eliminar el Perfil?",
                text: "Confirma la eliminación",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ac2925",
                confirmButtonText: "Si",
                closeOnConfirm: false
            },
            function () {
                __doPostBack('<%= btnDelete.UniqueID %>', "");
            });
        }

        function GetSelectedPermissions(){
            var selectedElmsIds = [];
            var selectedElms = $('#trPermisos').jstree("get_checked", true);
            $.each(selectedElms, function () {
                selectedElmsIds.push(this.id);
            });
            $('#<%= hidSelectedComponents.ClientID %>').val(JSON.stringify(selectedElmsIds));
            return $('#<%= hidSelectedComponents.ClientID %>').val();
        }
    </script>
</asp:Content>

<%-- Body --%>
<asp:Content ID="cBody" ContentPlaceHolderID="cphBody" runat="server">

    <%-- Hash del Perfil --%>
    <asp:HiddenField ID="hdnHash" runat="server"></asp:HiddenField>

    <%-- Encabezado --%>
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">

                <%-- Atras --%>
                <a class="btn btn-default pull-left" onclick="history.back();">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                </a>

                <%-- Separador horizontal --%>
                <span class="pull-left">&nbsp;</span>

                <%-- Actualizar --%>
                <a class="btn btn-default pull-left" onclick="window.location.reload()">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                </a>

                <%-- Titulo --%>
                <asp:Label ID="lblTitulo" Text="" runat="server" />

                <%-- Eliminar --%>
                <asp:Button ID="btnDelete" Text="Eliminar" CssClass="btn btn-danger pull-right" runat="server" CausesValidation="false"
                    OnClientClick="ConfirmDelete(); return false;" OnClick="btnDelete_Click" />

                <%-- Separador horizontal --%>
                <span class="pull-right">&nbsp;</span>

                <%-- Editar --%>
                <asp:Button ID="btnEdit" Text="Editar" CssClass="btn btn-warning pull-right" runat="server" CausesValidation="false"
                    OnClick="btnEdit_Click" />

            </h3>

        </div>
    </div>

    <%-- Contenido --%>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel-body">
                <div class="row">
                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12" id="fgrReseller" runat="server">
                        <label>
                            <asp:Label Text="Distribuidor" runat="server" />
                        </label>
                        <asp:DropDownList ID="ddlDistribuidor" CssClass="form-control select-single" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
                <hr />
                <div class="row">

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

                        <div class="form-group">
                            <label>
                                <asp:Label Text="Nombre" runat="server" />
                            </label>
                            <asp:TextBox ID="txtNombre" CssClass="form-control" Text="" runat="server" />
                            <asp:RequiredFieldValidator CssClass="required-field-validator" ControlToValidate="txtNombre" runat="server" />
                        </div>

                        <div class="form-group">
                            <label>
                                <asp:Label Text="Descripción" runat="server" />
                            </label>
                            <asp:TextBox ID="txtDesc" CssClass="form-control" Text="" runat="server" TextMode="MultiLine" />
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

                        <div class="form-group">
                            <label>
                                <asp:Label Text="Permisos" runat="server" />
                            </label>
                            <div id="trPermisos" class="panel panel-default">
                            </div>
                            <asp:HiddenField ID="hidSelectedComponents" runat="server" />
                        </div>
                    </div>
                </div>
            </div>

            <%-- Botón Perfiles, Guardar y Restablecer --%>
            <div class="btn-models">
                <asp:Button ID="btnPerfiles" Text="Perfiles" CssClass="btn btn-info" runat="server" OnClick="btnPerfiles_Click" CausesValidation="false" />
            </div>
            <div class="btns-reset-save">
                <button type="reset" class="btn btn-default">
                    <asp:Label Text="Restablecer" runat="server" />
                </button>
                <asp:Button ID="btnSave" Text="Guardar" CssClass="btn btn-success" Enabled="False" runat="server" OnClick="btnSave_Click" OnClientClick="var ids = GetSelectedPermissions();" />
            </div>

            <br />
            <br />
            <br />
        </div>
    </div>
</asp:Content>