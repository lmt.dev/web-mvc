﻿<%@ Page Title="Componentes de Aplicaciones" Language="C#" MasterPageFile="~/Pages/Page.Master" AutoEventWireup="true" CodeBehind="Componentes.aspx.cs" Inherits="Pages.Componentes" EnableEventValidation="false" %>

<%-- Head --%>
<asp:Content ID="cHead" ContentPlaceHolderID="cphHead" runat="server">

    <%-- CSSs ----------------------------------------------------------------------------------------%>
    <%-- jsTree --%>
    <link href="../Content/vendor/jstree/themes/default/style.min.css" rel="stylesheet" />

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>
    <%-- jsTree --%>
    <script src="../Scripts/vendor/jstree.min.js"></script>

    <%-- Script - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {

            $('#trComponentes').jstree({
                'plugins': ['contextmenu'],
                'contextmenu': {
                    'items': customMenuComponentes
                }
            });

        });

        function LoadComponentes(nodes) {
            console.log(nodes);

            $('#trComponentes').jstree(true).settings.core.data = nodes;
            $('#trComponentes').jstree(true).refresh();
        }

        function customMenuComponentes(node) {
            var items = {
                'AgregarSubcomponente': {
                    'label': 'Agregar Subcomponente',
                    'action': function () { newSubComponente(node); return false; }
                },
                'Editar': {
                    'label': 'Editar',
                    'action': function () { editSubComponente(node); return false; }
                },
                'Eliminar': {
                    'label': 'Eliminar',
                    'action': function () { deleteComponente(node); }
                }
            }

            return items;
        }

        function newRootComponente() {
            $('#<%= hidSelectedNode.ClientID %>').val('');
            $('#<%= hidAction.ClientID %>').val('NEW_ROOT');
            doPostBackAsync('<%= btnNewSubComponente.UniqueID %>', "");
        }

        function newSubComponente(node) {
            $('#<%= hidSelectedNode.ClientID %>').val(node.id);
            $('#<%= hidAction.ClientID %>').val('NEW');
            doPostBackAsync('<%= btnNewSubComponente.UniqueID %>', "");
        }

        function editSubComponente(node) {
            $('#<%= hidSelectedNode.ClientID %>').val(node.id);
            $('#<%= hidAction.ClientID %>').val('EDIT');
            doPostBackAsync('<%= btnEditSubComponente.UniqueID %>', "");
        }

        function deleteComponente(node) {
            $('#<%= hidSelectedNode.ClientID %>').val(node.id);
            $('#<%= hidAction.ClientID %>').val('DELETE');
            doPostBackAsync('<%= btnDeleteComponente.UniqueID %>', "");
        }

        function doPostBackAsync(eventName, eventArgs) {
            var prm = Sys.WebForms.PageRequestManager.getInstance();

            if (!Array.contains(prm._asyncPostBackControlIDs, eventName)) {
                prm._asyncPostBackControlIDs.push(eventName);
            }

            if (!Array.contains(prm._asyncPostBackControlClientIDs, eventName)) {
                prm._asyncPostBackControlClientIDs.push(eventName);
            }

            __doPostBack(eventName, eventArgs);
        }

    </script>
</asp:Content>

<%-- Body --%>
<asp:Content ID="cBody" ContentPlaceHolderID="cphBody" runat="server">

    <%-- Hash del Componentes --%>
    <asp:HiddenField ID="hdnHash" runat="server"></asp:HiddenField>

    <%-- Encabezado --%>
    <div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">

                <%-- Actualizar --%>
                <a href="" class="btn btn-default pull-left">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                </a>

                <%-- Titulo --%>
                <asp:Label ID="lblTitulo" Text="" runat="server" />

                <%-- Nuevo --%>
                <asp:Button ID="btnNuevo" Text="Nuevo Componente Raíz" CssClass="btn btn-primary pull-right" runat="server" OnClientClick="newRootComponente(); return false;" OnClick="btnNuevo_Click" />

            </h2>

        </div>
    </div>

    <%-- Contenido --%>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel-body">
                <div class="row">

                    <asp:UpdatePanel runat="server" ID="udpComponentes">
                        <ContentTemplate>
                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12" runat="server">
                                <label>
                                    <asp:Label Text="Sistema" runat="server" />
                                </label>
                                <asp:DropDownList ID="ddlSistema" CssClass="form-control select-single" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSistema_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12" runat="server">
                                <label>
                                    <asp:Label Text="Aplicación" runat="server" />
                                </label>
                                <asp:DropDownList ID="ddlAplicacion" CssClass="form-control select-single" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAplicacion_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlSistema" />
                            <asp:AsyncPostBackTrigger ControlID="ddlAplicacion" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <hr />
                <div class="row">

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

                        <div class="form-group">
                            <label>
                                <asp:Label Text="Componentes (click derecho sobre el componente para gestionar)" runat="server" />
                            </label>
                            <div id="trComponentes" class="panel panel-default">
                            </div>
                            <asp:HiddenField ID="hidSelectedComponents" runat="server" />
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <asp:Button ID="btnDeleteComponente" Text="NEW" CssClass="btn btn-danger pull-right" runat="server" OnClick="btnDeleteComponente_Click" CausesValidation="false" Visible="false" />

    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <asp:HiddenField ID="hidSelectedNode" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hidAction" runat="server"></asp:HiddenField>
            <asp:Button ID="btnNewSubComponente" Text="NEW" CssClass="btn btn-danger pull-right" runat="server" OnClick="btnNewSubComponente_Click" CausesValidation="false" Visible="false" />
            <asp:Button ID="btnEditSubComponente" Text="EDIT" CssClass="btn btn-danger pull-right" runat="server" OnClick="btnEditSubComponente_Click" CausesValidation="false" Visible="false" />

            <%-- Modal Editor Componente--%>
            <div id="componente_modal" class="modal fade" tabindex="-1" role="dialog" runat="server" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">
                                <asp:Label Text="Componente" runat="server" />
                            </h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group col-xs-12">
                                    <label>
                                        <asp:Label Text="Nombre" runat="server" />
                                    </label>
                                    <asp:TextBox ID="txtNombre" CssClass="form-control" Text="" runat="server" />
                                    <asp:RequiredFieldValidator CssClass="required-field-validator" ControlToValidate="txtNombre" runat="server" />
                                </div>

                                <div class="form-group col-xs-12">
                                    <label>
                                        <asp:Label Text="Identificador" runat="server" />
                                    </label>
                                    <asp:TextBox ID="txtIdentificador" CssClass="form-control" Text="" runat="server" />
                                    <asp:RequiredFieldValidator CssClass="required-field-validator" ControlToValidate="txtIdentificador" runat="server" />
                                </div>

                                <div class="form-group col-xs-12">
                                    <label>
                                        <asp:Label Text="Tipo Componente" runat="server" />
                                    </label>
                                    <asp:DropDownList ID="ddlTipoComponente" CssClass="form-control select-single" runat="server">
                                        <asp:ListItem Text="Página" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Funcionalidad" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="Sección de Menú" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="Link" Value="5"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:HiddenField ID="hidIndexComponente" runat="server" />
                            <asp:Button ID="btnComponenteClose" Text="Cancelar" CssClass="btn btn-default" runat="server" CausesValidation="false" OnClientClick="$('#cphBody_componente_modal').modal('hide'); return true;" OnClick="btnComponenteClose_Click" />
                            <asp:Button ID="btnComponenteSave" Text="Guardar" CssClass="btn btn-primary" runat="server" OnClientClick="$('#cphBody_componente_modal').modal('hide'); return true;" OnClick="btnComponenteSave_Click" />
                        </div>
                    </div>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>