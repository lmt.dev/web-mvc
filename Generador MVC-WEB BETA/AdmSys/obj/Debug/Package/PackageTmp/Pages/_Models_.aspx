﻿<%@ Page Title="_Models_" Language="C#" MasterPageFile="~/Pages/Page.Master" AutoEventWireup="true" CodeBehind="_Models_.aspx.cs" Inherits="Pages._Models_" %>

<%-- Head --%>
<asp:Content ID="cHead" ContentPlaceHolderID="cphHead" runat="server">

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>

    <%-- Script - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {
        });

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    </script>
</asp:Content>

<%-- Body --%>
<asp:Content ID="cBody" ContentPlaceHolderID="cphBody" runat="server">

    <%-- Encabezado --%>
    <div class="row">
        <div class="col-lg-12">

            <h3 class="page-header">

                <%-- Atras --%>
                <a class="btn btn-default pull-left" onclick="history.back();">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                </a>

                <%-- Separador horizontal --%>
                <span class="pull-left">&nbsp;</span>

                <%-- Actualizar --%>
                <a class="btn btn-default pull-left" onclick="window.location.reload()">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                </a>

                <%-- Titulo --%>
                <asp:Label ID="lblTitulo" Text="" runat="server" />

                <%-- Botón Nuevo --%>
                <asp:Button ID="btnNuevo" Text="Nuevo" CssClass="btn btn-primary pull-right" runat="server" OnClick="btnNuevo_Click" />

            </h3>

        </div>
    </div>

    <%-- Contenido --%>
    <div class="row">
        <div class="col-lg-12">

            <%-- Panel de filtros --%>
            <div class="panel-body">
                <div class="row">

                    <%-- Filtros --%>
                    <div class="panel panel-default">

                        <div class="panel-body">

                            <%-- Filtro 1 --%>
                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Filtro 1 (Nombre)" runat="server" />
                                </label>
                                <asp:TextBox ID="txtNombre" CssClass="form-control" autocomplete="off" runat="server" />
                            </div>

                            <%-- Filtro 2 --%>
                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Filtro 2" runat="server" />
                                </label>
                                <asp:DropDownList ID="ddlFiltro2" CssClass="form-control select-single" AutoPostBack="true" OnSelectedIndexChanged="ddlFiltro2_OnSelectedIndexChanged" runat="server">
                                </asp:DropDownList>
                            </div>

                            <%-- Filtro 3 --%>
                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Filtro 3" runat="server" />
                                </label>
                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>

                                        <asp:DropDownList ID="ddlFiltro3" CssClass="form-control select-single" runat="server">
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlFiltro2" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>

                    <%-- Botones Filtrar y Restablecer --%>
                    <div class="col-lg-12 text-center">
                        <asp:Button ID="btnFiltrar" Text="Filtrar" CssClass="btn btn-info" runat="server" OnClick="btnFiltrar_Click" />
                        <button type="reset" class="btn btn-default">
                            <asp:Label Text="Restablecer" runat="server" />
                        </button>
                    </div>
                </div>
            </div>

            <%-- Tabla de resultados de búsqueda de _Models_ --%>
            <asp:UpdatePanel runat="server">
                <ContentTemplate>

                    <asp:GridView ID="gv_Models_" CssClass="table table-striped table-bordered table-hover grid-view show-buttons crypto-id clickable" data-model="_Model_.aspx"
                        Width="100%" AutoGenerateColumns="False" runat="server" OnRowDataBound="gv_Models__RowDataBound" EmptyDataText="Sin datos para mostrar.">
                        <Columns>
                            <%-- Requeridas para las GridViews clickeables - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                            <%-- Responsive + - --%>
                            <asp:BoundField HeaderText="" />
                            <%-- cryptoID --%>
                            <asp:BoundField HeaderText="#" DataField="IdModel" HeaderStyle-CssClass="crypto-id-column" ItemStyle-CssClass="crypto-id-column" />
                            <%-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                            <asp:BoundField HeaderText="Nombre del Modelo" DataField="Nombre" />
                            <asp:BoundField HeaderText="Descripcion del Modelo" DataField="Descripcion" ItemStyle-CssClass="show-total" />
                            <asp:BoundField HeaderText="Entero" DataField="Entero" ItemStyle-CssClass="numeric-integer" />
                            <asp:BoundField HeaderText="Decimal" DataField="Decimal" ItemStyle-CssClass="numeric-decimal show-total" />
                            <asp:BoundField HeaderText="Moneda" DataField="Moneda" ItemStyle-CssClass="numeric-money show-total" />
                            <asp:BoundField HeaderText="Alta" DataField="FHAlta" ItemStyle-CssClass="date-time" />
                            <asp:BoundField HeaderText="Baja" DataField="FHBaja" ItemStyle-CssClass="date-time" />
                        </Columns>
                    </asp:GridView>

                </ContentTemplate>
                <Triggers>
                    <%-- Triggers que actualizan el UpdatePanel que contiene la GridView con los resultados --%>
                    <asp:AsyncPostBackTrigger ControlID="btnFiltrar" />
                </Triggers>
            </asp:UpdatePanel>

            <%-- Información adicional o ayuda --%>
            <div class="well">
                <h4>
                    <asp:Label Text="Información adicional o ayuda" runat="server" />
                </h4>
                <p>
                    <asp:Label Text="Tincidunt integer eu augue augue nunc elit dolor, luctus placerat scelerisque euismod, iaculis eu lacus nunc mi elit, vehicula ut laoreet ac, aliquam sit amet justo nunc tempor, metus vel." runat="server" />
                </p>
            </div>
        </div>
    </div>
</asp:Content>