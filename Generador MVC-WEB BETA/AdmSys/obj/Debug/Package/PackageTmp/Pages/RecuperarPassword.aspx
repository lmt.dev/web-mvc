﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RecuperarPassword.aspx.cs" Inherits="Pages.RecuperarPassword" %>

<!DOCTYPE html>

<html lang="es">

<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="description" content="Sistema Administrativo">
    <meta name="author" content="MF">

    <title>Access Control</title>

    <%-- Favicon --%>
    <link rel="shortcut icon" type="image/png" href="~/Content/images/favicon.png" />

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- Bootstrap --%>
    <link href="~/Content/vendor/bootstrap.min.css" rel="stylesheet">
    <%-- Fonts --%>
    <link href="~/Content/fonts/font-awesome.min.css" rel="stylesheet" />
    <%-- Custom --%>
    <link href="~/Content/site.css?v.1.0" rel="stylesheet" />

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
        #imgLogo {
            padding: 20px;
        }

        #login-app-name {
            text-align: center;
            font-variant: small-caps;
            background-color: #00496d;
            padding: 10px;
            border-radius: 3px;
            color: white;
        }

        #bottom {
            position: absolute;
            bottom: 0;
            left: 0;
            font-size: xx-small;
            width: 100%;
            color: #337ab7;
            text-align: center;
        }

        #foot {
            text-align: center;
        }
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>

    <%-- jQuery --%>
    <script src="Scripts/vendor/jquery.min.js"></script>
    <%-- Bootstrap --%>
    <script src="Scripts/vendor/bootstrap.min.js"></script>

    <%-- Script - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {
        });

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    </script>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <img id="imgLogo" class="img-responsive center-block" src="../Content/images/logo-login.png" />

                    </div>
                    <div class="panel-body">

                        <form id="form1" runat="server">
                            <div>
                                <asp:Label Text="" runat="server" ID="lblResult" />
                                <br />
                                <br />
                                <asp:HyperLink NavigateUrl="~/" runat="server" ID="lnkLogin" Visible="false" Text="Iniciar Sesión" />
                            </div>
                            <fieldset id="fsUser" runat="server">

                                <div class="form-group">
                                    <label>
                                        <asp:Label Text="Email" runat="server" />
                                    </label>
                                    <asp:TextBox ID="txtEmail" CssClass="form-control email" Text="" runat="server" />
                                    <asp:RequiredFieldValidator CssClass="required-field-validator" ControlToValidate="txtEmail" runat="server" />
                                </div>
                                <br />
                                <br />
                                <asp:Button ID="btnValidate" Text="Enviar Email de recuperación" CssClass="btn btn-lg btn-info btn-block" runat="server" OnClick="btnValidate_Click" />
                            </fieldset>
                        </form>

                    </div>
                </div>
            </div>
        </div>
        <%-- Pie de página --%>
        <div class="centered">
            <div id="bottom" class="text-center">
                <span><strong>Version 1.0</strong></span>
                <br />
                <span>©</span>
                <span>2017</span>
                <span>-</span>
                <span>Sistema Administrativo</span>
            </div>
        </div>
    </div>
</body>
</html>