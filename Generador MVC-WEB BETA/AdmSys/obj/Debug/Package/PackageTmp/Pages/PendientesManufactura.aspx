﻿<%@ Page Title="Manufactura Pendientes" Language="C#" MasterPageFile="~/Pages/Page.Master" AutoEventWireup="true" CodeBehind="PendientesManufactura.aspx.cs" Inherits="Pages.PendientesManufactura" %>

<%@ Register Src="~/Controls/ucFiltro_DistribuidorUnidadNegocioReseller.ascx" TagPrefix="uc" TagName="ucFiltro_DistribuidorUnidadNegocioReseller" %>
<%@ Register Src="~/Controls/ucPendienteManufacturaSDT.ascx" TagPrefix="uc" TagName="ucPendienteManufacturaSDT" %>

<%-- Head --%>
<asp:Content ID="cHead" ContentPlaceHolderID="cphHead" runat="server">

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
</style>

    <%-- JSs -----------------------------------------------------------------------------------------%>

    <%-- Script - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {

            // Evento click sobre una pestaña
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // Pestaña seleccionada
                var clickedTab = $(e.target).attr("href")
                // Guarda la pestaña seleccionada
                document.getElementById('<%= hdnTab.ClientID %>').value = clickedTab;
});

    // Enfoca la pestaña seleccionada
    var storedTab = document.getElementById('<%= hdnTab.ClientID %>').value;
    $('#tabs a[href="' + storedTab + '"]').tab('show');

});

// Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

    </script>
</asp:Content>

<%-- Body --%>
<asp:Content ID="cBody" ContentPlaceHolderID="cphBody" runat="server">

    <%-- Recuerda la pestaña seleccionada --%>
    <asp:HiddenField ID="hdnTab" runat="server" Value="" />

    <%-- Encabezado --%>
    <div class="row">
        <div class="col-lg-12">

            <%-- Título del formulario --%>
            <h3 class="page-header">

                <%-- Botón Actualizar --%>
                <a href="" class="btn btn-default pull-left">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                </a>

                <%-- Titulo --%>
                <asp:Label ID="lblTitulo" Text="" runat="server" />

            </h3>

        </div>
    </div>

    <%-- Contenido --%>
    <div class="row">
        <div class="col-lg-12">

            <%-- Panel de controles del formulario --%>
            <div class="panel-body">
                <div class="row">

                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <uc:ucFiltro_DistribuidorUnidadNegocioReseller runat="server" ID="ucFiltro_DistribuidorUnidadNegocioReseller1" />

                        <div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;">
                            <ul class="nav nav-tabs">

                                <li class="active"><a data-toggle="tab" href="#menuSDT">
                                    <asp:UpdatePanel ID="upCantidadSDT" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
	                                    <ContentTemplate>
                                            <h5>
                                                SDT <span class="label label-primary"><asp:Label ID="lblCantidadSDT" Text="" runat="server" Visible="true" /></span>
                                            </h5>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </a></li>


                                <li><a data-toggle="tab" href="#menuSDS">
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
	                                    <ContentTemplate>
                                            <h5>
                                                SDS <span class="label label-danger"><asp:Label ID="Label2" Text="15" runat="server" Visible="true" /></span>
                                            </h5>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </a></li>
                            </ul>


                            <div class="tab-content">
                                <div id="menuSDT" class="tab-pane fade in active">
                                    <uc:ucPendienteManufacturaSDT runat="server" ID="ucPendienteManufacturaSDT1" />
                                </div>

                                <div id="menuSDS" class="tab-pane fade">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 5px;">
                                        <label>
                                            <asp:Label ID="Label1" Text="Aquí el UC de SDS" Font-Size="Medium" runat="server" />
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
</asp:Content>
