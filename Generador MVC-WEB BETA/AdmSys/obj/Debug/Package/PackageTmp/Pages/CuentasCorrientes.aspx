﻿<%@ Page Title="Cuenta Corriente Resellers" Language="C#" MasterPageFile="~/Pages/Page.Master" AutoEventWireup="true" CodeBehind="CuentasCorrientes.aspx.cs" Inherits="Pages.CuentasCorrientes" %>

<%-- Head --%>
<asp:Content ID="cHead" ContentPlaceHolderID="cphHead" runat="server">

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
        #modalNuevoPago .modal-dialog {
            width: 95%;
        }
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>
    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {
        });

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        function mostrarModalPago() {
            $('#ifNuevoPago').attr('src', 'CobroRecibido.aspx?idDistribuidor=' + $('#<%= ddlDistribuidor.ClientID %>').val() + '&&idUnidadNegocio=' + $('#<%= ddlUnidadNegocio.ClientID %>').val() + '&&idReseller=' + $('#<%= ddlVendingReseller.ClientID %>').val());

            $('#modalNuevoPago').modal('show');
        }

        function actualizarCuentaCorriente() {
            __doPostBack('<%= btnFiltrar.UniqueID %>', "");
            $('#modalNuevoPago').modal('hide');
        }
    </script>
</asp:Content>

<%-- Body --%>
<asp:Content ID="cBody" ContentPlaceHolderID="cphBody" runat="server">

    <%-- Encabezado --%>
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">

                <%-- Atras --%>
                <a class="btn btn-default pull-left" onclick="history.back();">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                </a>

                <%-- Separador horizontal --%>
                <span class="pull-left">&nbsp;</span>

                <%-- Actualizar --%>
                <a class="btn btn-default pull-left" onclick="window.location.reload()">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                </a>

                <%-- Titulo --%>
                <asp:Label ID="lblTitulo" Text="" runat="server" />

                <%-- Nuevo --%>
                <asp:Button ID="btnNuevo" Text="Nuevo" CssClass="btn btn-primary pull-right" runat="server" OnClick="btnNuevo_Click" />

            </h3>

        </div>
    </div>

    <%-- Contenido --%>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel-body">
                <div class="row">

                    <%-- Filtros --%>
                    <div class="panel panel-default">

                        <div class="panel-body">

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Distribuidor" runat="server" />
                                </label>
                                <asp:DropDownList ID="ddlDistribuidor" CssClass="form-control select-single" AutoPostBack="true" OnSelectedIndexChanged="ddlDistribuidor_OnSelectedIndexChanged" runat="server">
                                </asp:DropDownList>
                            </div>
                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Unidad Negocio" runat="server" />
                                </label>
                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="ddlUnidadNegocio" CssClass="form-control select-single" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlUnidadNegocio_OnSelectedIndexChanged">
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlDistribuidor" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Reseller" runat="server" />
                                </label>
                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="ddlVendingReseller" CssClass="form-control select-single" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlVendingReseller_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlUnidadNegocio" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>

                            <%-- Fecha Desde --%>
                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Desde" runat="server" />
                                </label>
                                <asp:TextBox ID="txtFechaDesde" CssClass="form-control date-picker" runat="server" />
                            </div>

                            <%-- Fecha Hasta --%>
                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Hasta" runat="server" />
                                </label>
                                <asp:TextBox ID="txtFechaHasta" CssClass="form-control date-picker" runat="server" />
                            </div>
                        </div>
                    </div>

                    <%-- Resumen --%>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <asp:Label Text="Resumen Cuenta Corriente" runat="server" />
                        </div>
                        <div class="panel-body">
                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Saldo Cta. Cte." runat="server" />
                                </label>
                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox Text="" runat="server" CssClass="form-control numeric-money" ID="txtSaldoCuentaCorriente" />
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlVendingReseller" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Facturas-ND s/ Imputar" runat="server" />
                                </label>
                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox Text="" runat="server" CssClass="form-control numeric-money" ID="txtComprobantesNoImputados" />
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlVendingReseller" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Pagos-NC s/ Imputar" runat="server" />
                                </label>
                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox Text="" runat="server" CssClass="form-control numeric-money" ID="txtPagosNoImputados" />
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlVendingReseller" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Imputaciones Futuras" runat="server" />
                                </label>
                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox Text="" runat="server" CssClass="form-control numeric-money" ID="txtImputacionesFuturas" />
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlVendingReseller" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">

                                <asp:Button ID="btnFiltrar" Text="Detalle Cuenta Corriente" CssClass="btn btn-info" runat="server" OnClick="btnFiltrar_Click" />
                                <asp:Button ID="btnRegistrarPago" Text="Registrar Pago" CssClass="btn btn-success" runat="server" OnClientClick="mostrarModalPago(); return false;" OnClick="btnRegistrarPago_Click" CausesValidation="false" />
                                <asp:Button ID="btnRegistrarComprobante" Text="Facturar" CssClass="btn btn-warning" runat="server" OnClick="btnRegistrarComprobante_Click" />

                                <div style="display: none;">
                                    <button type="reset" class="btn btn-default">
                                        <asp:Label Text="Restablecer" runat="server" />
                                    </button>
                                </div>

                            </div>
                        </div>
                    </div>

                    <%-- Botones Filtrar y Restablecer --%>
                </div>
            </div>

            <ul id="tabs" class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#cuentaCorriente" aria-controls="misc" role="tab" data-toggle="tab">Cuenta Corriente</a></li>
                <li role="presentation"><a href="#comprobante" aria-controls="home" role="tab" data-toggle="tab">Fact./ND</a></li>
                <li role="presentation"><a href="#recibo" aria-controls="numeric" role="tab" data-toggle="tab">Pagos</a></li>
                <li role="presentation"><a href="#notaCredito" aria-controls="numeric" role="tab" data-toggle="tab">Notas Cred.</a></li>
                <li role="presentation"><a href="#imputacionesFuturas" aria-controls="numeric" role="tab" data-toggle="tab">Imputaciones Futuras</a></li>
            </ul>

            <div class="tab-content">

                <%-- Cuenta Corriente --%>
                <div role="tabpanel" class="tab-pane active" id="cuentaCorriente">
                    <br />

                    <div style="padding-left: 10px; padding-right: 10px; overflow-x: hidden;">

                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>

                                <asp:GridView ID="gvCuentasCorrientes" CssClass="table table-striped table-bordered table-hover grid-view crypto-id clickable show-buttons" data-model="CuentaCorrienteRedireccion.aspx"
                                    Width="100%" AutoGenerateColumns="False" runat="server" OnRowDataBound="gvCuentasCorrientes_RowDataBound" EmptyDataText="Sin datos para mostrar.">
                                    <Columns>
                                        <%-- Requeridas para las GridViews clickeables - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                                        <%-- Responsive + - --%>
                                        <asp:BoundField HeaderText="" />
                                        <%-- cryptoID --%>
                                        <asp:BoundField HeaderText="#" DataField="IdCtaCte" HeaderStyle-CssClass="crypto-id-column" ItemStyle-CssClass="crypto-id-column" />
                                        <%-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                                        <asp:BoundField HeaderText="Fecha Movimiento" DataField="FHAlta" ItemStyle-CssClass="date-time" />
                                        <asp:BoundField HeaderText="Nro. Interno" DataField="NumeroInterno" ItemStyle-CssClass="numeric-positive" />
                                        <asp:BoundField HeaderText="Tipo" DataField="TipoMov" />
                                        <asp:BoundField HeaderText="Concepto" DataField="Concepto" />
                                        <asp:BoundField HeaderText="Descripcion" DataField="Descripcion" />
                                        <asp:BoundField HeaderText="Importe Comprobante" DataField="ImporteTotal" ItemStyle-CssClass="numeric-money-positive" />
                                        <asp:BoundField HeaderText="Cta. Cte Resultante" DataField="Saldo" ItemStyle-CssClass="numeric-money-positive" />
                                        <asp:BoundField HeaderText="Observacion" DataField="ComprobanteObservacion" />
                                    </Columns>
                                </asp:GridView>

                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnFiltrar" />
                            </Triggers>
                        </asp:UpdatePanel>

                    </div>

                </div>

                <%-- Facturas --%>
                <div role="tabpanel" class="tab-pane" id="comprobante">
                    <br />

                    <div style="padding-left: 10px; padding-right: 10px; overflow-x: hidden;">

                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>

                                <asp:GridView ID="gvComprobantes" CssClass="table table-striped table-bordered table-hover grid-view crypto-id" data-model="CuentaCorrienteRedireccion.aspx"
                                    Width="100%" AutoGenerateColumns="False" runat="server" OnRowDataBound="gvComprobantes_RowDataBound" EmptyDataText="Sin datos para mostrar.">
                                    <Columns>
                                        <%-- Requeridas para las GridViews clickeables - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                                        <%-- Responsive + - --%>
                                        <asp:BoundField HeaderText="" />
                                        <%-- cryptoID --%>
                                        <asp:BoundField HeaderText="#" HeaderStyle-CssClass="crypto-id-column" ItemStyle-CssClass="crypto-id-column" />
                                        <%-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                                        <asp:BoundField HeaderText="Fecha Movimiento" DataField="FHAlta" ItemStyle-CssClass="date-time" />
                                        <asp:BoundField HeaderText="Numero Interno" DataField="NroComprobanteInterno" />
                                        <asp:BoundField HeaderText="Descripcion" DataField="Descripcion" />
                                        <asp:BoundField HeaderText="Generacion" DataField="Generacion" ItemStyle-CssClass="" />
                                        <asp:BoundField HeaderText="Importe Total" DataField="ImporteTotal" ItemStyle-CssClass="numeric-money-positive" />
                                        <asp:BoundField HeaderText="Importe Saldado" DataField="ImporteSaldado" ItemStyle-CssClass="numeric-money-positive" />

                                        <asp:TemplateField HeaderText="Importe s/ Imputar" ItemStyle-CssClass="numeric-money-positive">
                                            <ItemTemplate>
                                                <asp:Label ID="ImporteImputar" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Impresión">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="HyperLink1" runat="server"
                                                    NavigateUrl='<%# Eval("IdComprobante", @"printables/factura.aspx?IdFactura={0}") %>'
                                                    Text='Imprimir' Target="_blank">
                                                </asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="NC">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="HyperLink2" runat="server" ToolTip="Nota de Crédito"
                                                    NavigateUrl='<%# Eval("IdComprobante", @"ComprobanteManual.aspx?IdComprobante={0}&&TipoNota=2") %>'
                                                    Text='<%# Eval("TipoComprobanteInterno").ToString() == "1" ? "NC" : ""  %>'>
                                                </asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ND">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="HyperLink3" runat="server" ToolTip="Nota de Débito"
                                                    NavigateUrl='<%# Eval("IdComprobante", @"ComprobanteManual.aspx?IdComprobante={0}&&TipoNota=3") %>'
                                                    Text='<%# Eval("TipoComprobanteInterno").ToString() == "1" ? "ND" : ""  %>'>
                                                </asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="AFIP">
                                            <ItemTemplate>
                                                <asp:UpdatePanel runat="server">
                                                    <ContentTemplate>
                                                        <asp:LinkButton ID="lnkDownload" Text='<%# Eval("IdImagen") != null ? "Descargar" : "" %> ' CommandArgument='<%# Eval("IdComprobante") %>' runat="server" OnClick="DownloadFile"></asp:LinkButton>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="lnkDownload" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>

                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnFiltrar" />
                            </Triggers>
                        </asp:UpdatePanel>

                    </div>
                </div>

                <%-- Pagos --%>
                <div role="tabpanel" class="tab-pane" id="recibo">
                    <br />

                    <div style="padding-left: 10px; padding-right: 10px; overflow-x: hidden;">

                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>

                                <asp:GridView ID="gvRecibos" CssClass="table table-striped table-bordered table-hover grid-view crypto-id" data-model="CuentaCorrienteRedireccion.aspx"
                                    Width="100%" AutoGenerateColumns="False" runat="server">
                                    <Columns>
                                        <%-- Requeridas para las GridViews clickeables - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                                        <%-- Responsive + - --%>
                                        <asp:BoundField HeaderText="" />
                                        <%-- cryptoID --%>
                                        <asp:BoundField HeaderText="#" HeaderStyle-CssClass="crypto-id-column" ItemStyle-CssClass="crypto-id-column" />
                                        <%-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                                        <asp:BoundField HeaderText="Fecha Movimiento" DataField="FHAlta" ItemStyle-CssClass="date-time" />
                                        <asp:BoundField HeaderText="Numero Interno" DataField="NumeroRecibo" />
                                        <asp:BoundField HeaderText="Importe Pago" DataField="Importe" ItemStyle-CssClass="numeric-money-positive" />
                                        <asp:BoundField HeaderText="Pago s/ Imputar" DataField="Saldo" ItemStyle-CssClass="numeric-money-positive" />

                                        <asp:TemplateField HeaderText="Impresión">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="HyperLink1" runat="server"
                                                    NavigateUrl='<%# Eval("IdRecibo", @"printables/recibo.aspx?IdRecibo={0}") %>'
                                                    Text='Imprimir' Target="_blank">
                                                </asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>

                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnFiltrar" />
                            </Triggers>
                        </asp:UpdatePanel>

                    </div>

                </div>

                <%-- Notas Credito --%>
                <div role="tabpanel" class="tab-pane" id="notaCredito">
                    <br />

                    <div style="padding-left: 10px; padding-right: 10px; overflow-x: hidden;">

                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>

                                <asp:GridView ID="gvNotaCredito" CssClass="table table-striped table-bordered table-hover grid-view crypto-id" data-model="CuentaCorrienteRedireccion.aspx"
                                    Width="100%" AutoGenerateColumns="False" runat="server" OnRowDataBound="gvNotaCredito_RowDataBound" EmptyDataText="Sin datos para mostrar.">
                                    <Columns>
                                        <%-- Requeridas para las GridViews clickeables - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                                        <%-- Responsive + - --%>
                                        <asp:BoundField HeaderText="" />
                                        <%-- cryptoID --%>
                                        <asp:BoundField HeaderText="#" HeaderStyle-CssClass="crypto-id-column" ItemStyle-CssClass="crypto-id-column" />
                                        <%-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                                        <asp:BoundField HeaderText="Fecha Movimiento" DataField="FHAlta" ItemStyle-CssClass="date-time" />
                                        <asp:BoundField HeaderText="Numero Interno" DataField="NroComprobanteInterno" />
                                        <asp:BoundField HeaderText="Descripcion" DataField="Descripcion" />
                                        <asp:BoundField HeaderText="Generacion" DataField="Generacion" ItemStyle-CssClass="" />
                                        <asp:BoundField HeaderText="Importe Nota" DataField="ImporteTotal" ItemStyle-CssClass="numeric-money-positive" />
                                        <asp:BoundField HeaderText="Importe s/ Imputar" DataField="SaldoFavor" ItemStyle-CssClass="numeric-money-positive" />
                                        <asp:BoundField HeaderText="Observación" DataField="Observacion" />
                                        <asp:TemplateField HeaderText="Impresión">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="HyperLink1" runat="server"
                                                    NavigateUrl='<%# Eval("IdComprobante", @"printables/ajuste.aspx?IdAjuste={0}") %>'
                                                    Text='Imprimir' Target="_blank">
                                                </asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>

                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnFiltrar" />
                            </Triggers>
                        </asp:UpdatePanel>

                    </div>

                </div>

                <%-- Imputaciones Futuras --%>
                <div role="tabpanel" class="tab-pane" id="imputacionesFuturas">
                    <br />

                    <div style="padding-left: 10px; padding-right: 10px; overflow-x: hidden;">

                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>

                                <asp:GridView ID="gvImputacionesPendientes" CssClass="table table-striped table-bordered table-hover grid-view crypto-id" data-model="CuentaCorrienteRedireccion.aspx"
                                    Width="100%" AutoGenerateColumns="False" runat="server">
                                    <Columns>
                                        <%-- Requeridas para las GridViews clickeables - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                                        <%-- Responsive + - --%>
                                        <asp:BoundField HeaderText="" />
                                        <%-- cryptoID --%>
                                        <asp:BoundField HeaderText="#" HeaderStyle-CssClass="crypto-id-column" ItemStyle-CssClass="crypto-id-column" />
                                        <%-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                                        <asp:BoundField HeaderText="Fecha Imputación" DataField="FechaParaImputar" ItemStyle-CssClass="date-time" />
                                        <asp:BoundField HeaderText="Descripción" DataField="Descripcion" />
                                        <asp:BoundField HeaderText="Importe" DataField="Importe" ItemStyle-CssClass="numeric-money-positive" />
                                        <asp:BoundField HeaderText="Observación" DataField="Observacion" />

                                    </Columns>
                                </asp:GridView>

                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnFiltrar" />
                            </Triggers>
                        </asp:UpdatePanel>

                    </div>

                </div>
            </div>

            <br />

            <div class="well">
                <h4>
                    <asp:Label Text="Información adicional o ayuda" runat="server" />
                </h4>
                <p>
                    <asp:Label Text="Tincidunt integer eu augue augue nunc elit dolor, luctus placerat scelerisque euismod, iaculis eu lacus nunc mi elit, vehicula ut laoreet ac, aliquam sit amet justo nunc tempor, metus vel." runat="server" />
                </p>
            </div>
        </div>
    </div>

    <%-- Modal Recibo Pago --%>
    <div class="modal fade" id="modalNuevoPago" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-header" style="padding-bottom: 0px; padding-top: 5px;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding-top: 10px;">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4>
                        <asp:Label ID="lblDialogPago" Text="Registrar Pago" runat="server" />
                    </h4>
                </div>

                <div class="modal-body" style="text-align: center; height: calc(100vh - 110px);">
                    <iframe id="ifNuevoPago" width="100%" height="100%" frameborder="0"></iframe>
                </div>

            </div>
        </div>
    </div>

</asp:Content>