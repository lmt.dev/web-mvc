﻿<%@ Page Title="Cobro Recibido" Language="C#" MasterPageFile="~/Pages/Page.Master" AutoEventWireup="true" CodeBehind="CobroRecibido.aspx.cs" Inherits="Pages.CobroRecibido" %>

<%-- Head --%>
<asp:Content ID="cHead" ContentPlaceHolderID="cphHead" runat="server">

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
        body {
            padding: 0px !important;
            overflow-x: hidden;
            overflow-y: auto;
        }

        .panel-heading {
            font-size: 13px;
            font-weight: bold;
        }

        .panel-body {
            padding-top: 3px !important;
            padding-left: 3px !important;
            padding-right: 3px !important;
            padding-bottom: 0px !important;
        }

        .row {
            margin-right: 0px;
            margin-left: 0px;
        }

        #cphBody_btnAddGridRow {
            margin-top: -6px;
            margin-right: -11px;
            padding-left: 10px;
            padding-right: 10px;
        }

        #cphBody_gvDetalleCobrosRecibidos {
            margin-bottom: 0px;
        }

        .form-group .pull-right {
            padding-top: 3px;
            padding-right: 0px;
            padding-left: 3px;
            margin-bottom: 3px;
        }

        #cphBody_ctl02 > table > tbody > tr > td:nth-child(1) > label {
            margin-bottom: 0px;
            font-size: 14px;
        }

        #cphBody_txtImporte {
            border-top-left-radius: 0px;
            border-top-right-radius: 0px;
            border-bottom-left-radius: 0px;
            border-bottom-right-radius: 3px;
            width: 142px;
            font-size: 14px;
            font-weight: bold;
        }

        #cphBody_txtObservacion {
            margin-bottom: 3px;
            border-top-left-radius: 0px;
            border-top-right-radius: 0px;
            border-bottom-left-radius: 3px;
            border-bottom-right-radius: 3px;
        }

        #divImputaciones {
            margin-top: 0px;
            margin-right: -15px;
            margin-bottom: -4px;
            margin-left: -15px;
        }
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>

    <%-- Script - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {
        });

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

        // Solicita confirmación antes de guardar
        function ConfirmSave() {
            swal({
                title: "¿Confirma la generación del pago?",
                text: "Se registraá un pago por " + $("#cphBody_txtImporte").val(),
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Si",
                cancelButtonText: "No",
                closeOnConfirm: false
            },
            function () {
                __doPostBack('<%= btnSave.UniqueID %>', "");
            });
        }

    </script>
</asp:Content>

<%-- Body --%>
<asp:Content ID="cBody" ContentPlaceHolderID="cphBody" runat="server">

    <%-- Hash del CobroRecibidoo --%>
    <asp:HiddenField ID="hdnHash" runat="server"></asp:HiddenField>

    <%-- Encabezado --%>
    <asp:Panel ID="pnlEncabezado" runat="server">

        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header">

                    <%-- Atras --%>
                    <a class="btn btn-default pull-left" onclick="history.back();">
                        <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    </a>

                    <%-- Separador horizontal --%>
                    <span class="pull-left">&nbsp;</span>

                    <%-- Actualizar --%>
                    <a class="btn btn-default pull-left" onclick="window.location.reload()">
                        <i class="fa fa-refresh" aria-hidden="true"></i>
                    </a>

                    <%-- Titulo --%>
                    <asp:Label ID="lblTitulo" Text="Detalle del pago" runat="server" />

                </h3>

            </div>
        </div>

    </asp:Panel>

    <%-- Contenido --%>
    <div class="row">

        <div class="panel-body">

            <asp:Panel ID="pnlReseller" runat="server">
                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <label>
                        <asp:Label ID="lblResellerPago" Text="Reseller" runat="server" />

                    </label>
                    <asp:TextBox ID="txtResellerPago" CssClass="form-control date" Text="" runat="server" Enabled="false" />
                </div>
            </asp:Panel>

            <%-- Formas de Pago--%>
            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <div class="panel panel-default">

                    <div class="panel-heading">
                        <asp:Label Text="Formas de Pago" runat="server" />
                        <asp:Button ID="btnAddGridRow" CssClass="btn btn-info pull-right" Text=" Agregar Forma de Pago " OnClick="btnAddGridRow_Click" runat="server" />
                    </div>

                    <div class="panel-body">

                        <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12" style="display: none;">
                            <label>
                                <asp:Label ID="lblFechaComprobante" Text="Fecha Comprobante" runat="server" />
                            </label>
                            <asp:TextBox ID="txtFechaComprobante" CssClass="form-control date" Text="" runat="server" Enabled="false" />
                        </div>

                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>

                                <asp:GridView ID="gvDetalleCobrosRecibidos" CssClass="table table-striped table-bordered table-hover inline-edit"
                                    Width="100%" AutoGenerateColumns="False" runat="server" OnRowDeleting="gvDetalleCobrosRecibidos_RowDeleting"
                                    EmptyDataText="No se agregaron Formas de Pago.">
                                    <Columns>

                                        <asp:TemplateField HeaderText="IdDetalleCobroRecibido" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblIdDetalleCobroRecibido" CssClass="form-control-static" Width="100%" runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Tipo de Cobro" ItemStyle-CssClass="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="ddlTipoCobro" CssClass="form-control select-single" Width="100%" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlTipoCobro_OnSelectedIndexChanged">
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Importe">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtImporte" CssClass="form-control numeric-decimal" MaxLength="8" Width="100%" runat="server" AutoPostBack="true" OnTextChanged="txtImporte_TextChanged"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtImporte" ErrorMessage=""
                                                    CssClass="required-field-validator"
                                                    SetFocusOnError="True" runat="server">
                                                </asp:RequiredFieldValidator>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Banco Cheque">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="ddlBancoCheque" CssClass="form-control select-single" Width="100%" runat="server">
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Numero Cheque">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtNumeroCheque" CssClass="form-control" MaxLength="500" Width="100%" runat="server"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Titularidad Cheque">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtTitularidadCheque" CssClass="form-control text-capital" MaxLength="50" Width="100%" runat="server"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Fecha Cobro Cheque">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtFechaCobroCheque" CssClass="form-control date-picker" MaxLength="500" Width="100%" runat="server"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Cta. Transf.">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="ddlCuentaBancaria" CssClass="form-control select-single" Width="100%" runat="server">
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Ref. Transferencia">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtReferenciaTransferencia" CssClass="form-control" MaxLength="500" Width="100%" runat="server"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Fecha Transf.">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtFechaImputacionTransferencia" CssClass="form-control date-picker" MaxLength="500" Width="100%" runat="server"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <%-- Eliminar --%>
                                        <asp:CommandField ShowDeleteButton="True" ButtonType="Image" DeleteImageUrl="http://www.haotu.net/up/4234/24/212-bin.png" />

                                    </Columns>
                                </asp:GridView>

                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnAddGridRow" />
                            </Triggers>
                        </asp:UpdatePanel>

                        <div class="form-group pull-right">

                            <asp:UpdatePanel runat="server">
                                <ContentTemplate>

                                    <table>
                                        <tr>
                                            <td>
                                                <label>
                                                    <asp:Label ID="lblTotal" Text="Total" runat="server" />
                                                </label>
                                            </td>
                                            <td>&nbsp;
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtImporte" CssClass="form-control numeric-money-positive" Text="" runat="server" Enabled="false" />
                                            </td>
                                        </tr>
                                    </table>

                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="gvDetalleCobrosRecibidos" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>

                    </div>

                </div>

            </div>

            <%--Imputaciones--%>
            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <div class="panel panel-default">

                    <div class="panel-heading">
                        <asp:Label Text="Imputaciones" runat="server" />
                    </div>

                    <div id="divImputaciones" class="panel-body">

                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>

                                <asp:GridView ID="gvComprobantes" CssClass="table table-striped table-bordered table-hover grid-view"
                                    Width="100%" AutoGenerateColumns="False" runat="server" OnRowDataBound="gvComprobantes_RowDataBound" EmptyDataText="Sin datos para mostrar.">
                                    <Columns>

                                        <asp:BoundField HeaderText="#" DataField="IdComprobante" HeaderStyle-CssClass="crypto-id-column" ItemStyle-CssClass="crypto-id-column" />
                                        <asp:BoundField HeaderText="Fecha Emision" DataField="FHAlta" ItemStyle-CssClass="date-time" />
                                        <asp:BoundField HeaderText="Descripción" DataField="Descripcion" />
                                        <asp:BoundField HeaderText="Importe Original" DataField="ImporteTotal" ItemStyle-CssClass="numeric-money-positive" />
                                        <asp:BoundField HeaderText="Importe Saldado" DataField="ImporteSaldado" ItemStyle-CssClass="numeric-money-positive" />
                                        <asp:TemplateField HeaderText="Importe s/ Imputar" ItemStyle-CssClass="numeric-money-positive">
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<%# (decimal)Eval("ImporteTotal") - (decimal)Eval("ImporteSaldado") %>' CssClass="numeric-money-positive" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Imputación">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtImporteImputar" CssClass="form-control numeric-decimal" MaxLength="8" Width="100%" runat="server"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>

                                <asp:GridView ID="gvImputaciones" CssClass="table table-striped table-bordered table-hover grid-view"
                                    Width="100%" AutoGenerateColumns="False" runat="server" OnRowDataBound="gvImputaciones_RowDataBound" EmptyDataText="Sin datos para mostrar.">
                                    <Columns>

                                        <asp:BoundField HeaderText="#" DataField="IdComprobante" HeaderStyle-CssClass="crypto-id-column" ItemStyle-CssClass="crypto-id-column" />
                                        <asp:BoundField HeaderText="Fecha Imputación" DataField="FHAlta" ItemStyle-CssClass="date-time" />
                                        <asp:BoundField HeaderText="Nro. Comprobante" DataField="NroComprobanteInterno" />
                                        <asp:BoundField HeaderText="Descripcion" DataField="Descripcion" />
                                        <asp:BoundField HeaderText="Comprobante Original" DataField="ImporteTotal" ItemStyle-CssClass="numeric-money-positive" />
                                        <asp:BoundField HeaderText="Importe Imputacion" DataField="Importe" ItemStyle-CssClass="numeric-money-positive" />
                                        <asp:BoundField HeaderText="Faltante al Momento" DataField="Saldo" ItemStyle-CssClass="numeric-money-positive" />
                                        <asp:BoundField HeaderText="Observación" DataField="Observacion" />

                                    </Columns>
                                </asp:GridView>

                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </div>

                </div>

            </div>

            <%-- Observaciones --%>
            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <asp:Label Text="Observaciones" runat="server" />
                    </div>
                    <div class="panel-body">
                        <textarea class="form-control" rows="3" id="txtObservacion" runat="server"></textarea>
                    </div>
                </div>
            </div>

            <asp:Panel ID="pnlSave" runat="server">

                <%-- Separador vertical --%>
                <hr />

                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="btns-reset-save">
                        <asp:Button ID="btnSave" Text="Guardar" CssClass="btn btn-success" Enabled="False" runat="server" OnClientClick="ConfirmSave(); return false;" OnClick="btnSave_Click" />
                    </div>
                </div>

            </asp:Panel>

        </div>
    </div>

</asp:Content>