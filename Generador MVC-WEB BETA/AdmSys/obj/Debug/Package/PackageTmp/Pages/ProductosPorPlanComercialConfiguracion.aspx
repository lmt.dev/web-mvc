﻿<%@ Page Title="Configuración de productos asignados por plan comercial" Language="C#" MasterPageFile="~/Pages/Page.Master" AutoEventWireup="true" CodeBehind="ProductosPorPlanComercialConfiguracion.aspx.cs" Inherits="Pages.ProductosPorPlanComercialConfiguracion" %>
<%@ Register Src="~/Controls/ucProductoServicioCarga.ascx" TagPrefix="uc" TagName="ucProductoServicioCarga" %>
<%@ Register Src="~/Controls/ucProductoServicioEscala.ascx" TagPrefix="uc" TagName="ucProductoServicioEscala" %>

<%-- Head --%>
<asp:Content ID="cHead" ContentPlaceHolderID="cphHead" runat="server">

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>

    <%-- Script - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {

            // Evento click sobre una pestaña
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // Pestaña seleccionada
                var clickedTab = $(e.target).attr("href")
                // Guarda la pestaña seleccionada
                document.getElementById('<%= hdnTab.ClientID %>').value = clickedTab;
            });

            // Enfoca la pestaña seleccionada
            var storedTab = document.getElementById('<%= hdnTab.ClientID %>').value;
            $('#tabs a[href="' + storedTab + '"]').tab('show');

        });

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

    </script>
</asp:Content>

<%-- Body --%>
<asp:Content ID="cBody" ContentPlaceHolderID="cphBody" runat="server">

    <%-- Recuerda la pestaña seleccionada --%>
    <asp:HiddenField ID="hdnTab" runat="server" Value="" />

    <%-- Encabezado --%>
    <div class="row">
        <div class="col-lg-12">

            <%-- Título del formulario --%>
            <h3 class="page-header">

                <%-- Botón Actualizar --%>
                <a href="" class="btn btn-default pull-left">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                </a>

                <%-- Titulo --%>
                <asp:Label ID="lblTitulo" Text="Panel de configuración de listas de precios" runat="server" />


                <%-- Separador horizontal --%>
                <span class="pull-right">&nbsp;</span>

            </h3>

        </div>
    </div>

    <%-- Contenido --%>
    <div class="row">
        <div class="col-lg-12">

            <%-- Panel de controles del formulario --%>
            <div class="panel-body">
                <div class="row">
	                <asp:UpdatePanel ID="upProductosPorPlanComercial" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
		                <ContentTemplate>
			                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 5px;">
                                <div class="form-group col-lg-5 col-md-5 col-sm-5 col-xs-5 pull-left">
                                    <center>
                                        <h4>
                                            <label class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label Text="" ID="lblPlanComercialNombre" runat="server" />
                                            </label>
                                        </h4>
                                    </center>
                                    <p></p>
                                    <asp:UpdatePanel ID="upGridViewProductoServicio" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false" style="margin-left: 10px; margin-right: 10px;">
		                                <ContentTemplate>
                                        <asp:GridView ID="gvProductoServicio" 
                                            CssClass="table table-striped table-bordered table-hover grid-view crypto-id" 
                                            data-model="ProductoServicioCarga.aspx"
                                            Width="100%" 
                                            AutoGenerateColumns="False" 
                                            runat="server" 
                                            OnRowDataBound="gvProductoServicio_RowDataBound" 
                                            OnSelectedIndexChanging="gvProductoServicio_SelectedIndexChanging"
                                            EmptyDataText="Sin datos para mostrar.">
                                            <Columns>
                                                <asp:BoundField HeaderText="" />
                                                <%-- cryptoID --%>
                                                <asp:BoundField HeaderText="#" DataField="IdProductoServicio" HeaderStyle-CssClass="crypto-id-column" ItemStyle-CssClass="crypto-id-column" />
                                                <%-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                                                <asp:BoundField HeaderText="Código" DataField="CodigoProducto" Visible="false" ItemStyle-Width="50"/>
                                                <asp:BoundField HeaderText="Descripción" DataField="Descripcion" ItemStyle-Width="100"/>
                                                <asp:TemplateField HeaderText="Gestionar" ItemStyle-Width="25">
                                                    <ItemTemplate>
                                                        <center>
                                                        <asp:LinkButton ID="btnAsignarPoductoPlanComercial"
                                                            runat="server"
                                                            CssClass="btn btn-info"
                                                            ItemStyle-Width="25"
                                                            CommandName="Select"
                                                            title="Asignar el producto a la lista de precio">
                                                                <span aria-hidden="true" class="glyphicon glyphicon-arrow-right"></span>
                                                        </asp:LinkButton>
                                                    </center>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="25px" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
		                                </ContentTemplate>
	                                </asp:UpdatePanel>
                                </div>

                                <div class="form-group col-lg-7 col-md-7 col-sm-7 col-xs-7 pull-right">

                                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <uc:ucProductoServicioCarga runat="server" ID="ucProductoServicioCarga1" />
                                    </div>

                                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                        
                                        <asp:UpdatePanel ID="upConfiguracionPrecioEscala" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false" Visible="false">
		                                    <ContentTemplate>
			                                    <div class="row">

                                                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            
                                                        <center>
                                                            <label class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label Text="CONFIGURACION DE PRECIOS, ESCALAS Y LIQUIDACION" runat="server" />
                                                            </label>
                                                        </center>

                                                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                            <label>
                                                                <asp:Label Text="Tipo de liquidación" runat="server" />
                                                            </label>

                                                            <asp:DropDownList 
                                                                ID="ddlLiquidacionTipo" 
                                                                CssClass="form-control select-single" 
                                                                runat="server"
                                                                AutoPostBack="true" 
                                                                OnSelectedIndexChanged="ddlLiquidacionTipo_SelectedIndexChanged"
                                                                >
                                                            </asp:DropDownList>
                                                        </div>

                                                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                            <label>
                                                                <asp:Label Text="Modulo Automático" runat="server" />
                                                            </label>

	                                                        <asp:UpdatePanel ID="upModulosAutomaticos" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
		                                                        <ContentTemplate>
                                                                    <asp:DropDownList 
                                                                        ID="ddlModuloAutomatico" 
                                                                        CssClass="form-control select-single" 
                                                                        runat="server"
                                                                        >
                                                                    </asp:DropDownList>
		                                                        </ContentTemplate>
	                                                        </asp:UpdatePanel>
                                                        </div>

                                                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                            <label>
                                                                <asp:Label Text="Tipo de precio" runat="server" />
                                                            </label>

                                                            <asp:DropDownList 
                                                                ID="ddlPrecioTipo" 
                                                                CssClass="form-control select-single" 
                                                                runat="server"
                                                                AutoPostBack="true"
                                                                OnSelectedIndexChanged="ddlPrecioTipo_OnSelectedIndexChanged" 
                                                                >
                                                            </asp:DropDownList>
                                                        </div>

                                                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                            <label>
                                                                <asp:Label Text="Maneja escalas" runat="server" />
                                                            </label>

                                                            <asp:DropDownList 
                                                                ID="ddlManejaEscalas" 
                                                                AutoPostBack="true"
                                                                OnSelectedIndexChanged="ddlManejaEscalas_OnSelectedIndexChanged"
                                                                CssClass="form-control select-single" 
                                                                runat="server">
                                                            </asp:DropDownList>
                                                        </div>

                                                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                            <label>
                                                                <asp:Label Text="Moneda" runat="server" />
                                                            </label>

                                                            <asp:DropDownList 
                                                                ID="ddlMoneda"
                                                                CssClass="form-control select-single" 
                                                                runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                            
                                                        <div class="row">
                                                            <asp:UpdatePanel ID="upSinEscalaPrecioFijo" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                                                <ContentTemplate>
                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 5px;">
                                                            
                                                                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                                            <label>
                                                                                <asp:Label Text="Precio de lista $" runat="server" />
                                                                            </label>
                                                                            <asp:TextBox ID="txtPrecioLista"
                                                                            CssClass="form-control numeric-decimal-positive"
                                                                            PlaceHolder="$"
                                                                            runat="server"
                                                                            TextMode="SingleLine" />
                                                                        </div>

                                                                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                                            <label>
                                                                                <asp:Label Text="Precio de contado $" runat="server" />
                                                                            </label>
                                                                            <asp:TextBox ID="txtPrecioContado"
                                                                            CssClass="form-control numeric-decimal-positive"
                                                                            PlaceHolder="$"
                                                                            runat="server"
                                                                            TextMode="SingleLine" />
                                                                        </div>

                                                                    </div>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </div>

                                                        <div class="row">
                                                            <asp:UpdatePanel ID="upSinEscalaPrecioPorcentual" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false" Visible="false">
                                                                <ContentTemplate>
                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 5px;">
                                                            
                                                                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                                            <label>
                                                                                <asp:Label Text="Porcentaje %" runat="server" />
                                                                            </label>
                                                                            <asp:TextBox ID="txtPrecioPorcentualPorcentaje"
                                                                            CssClass="form-control numeric-integer-positive"
                                                                            PlaceHolder="%"
                                                                            runat="server"
                                                                            TextMode="SingleLine" />
                                                                        </div>

                                                                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                                            <label>
                                                                                <asp:Label Text="Cobro mínimo $" runat="server" />
                                                                            </label>
                                                                            <asp:TextBox ID="txtPrecioPorcentualCobroMinimo"
                                                                            CssClass="form-control numeric-decimal-positive"
                                                                            PlaceHolder="$"
                                                                            runat="server"
                                                                            TextMode="SingleLine" />
                                                                        </div>

                                                                    </div>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </div>

                                                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <uc:ucProductoServicioEscala runat="server" ID="ucProductoServicioEscala1" Visible="false"/>
                                                        </div>

                                                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 5px;">
                
                                                            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                <asp:Button ID="btnEliminarPrecio"
                                                                    Text="Eliminar configuración"
                                                                    CssClass="btn btn-danger col-lg-12 col-md-12 col-sm-12 col-xs-12"
                                                                    runat="server"
                                                                    OnClick="btnEliminarPrecio_Click"
                                                                    Style="margin-bottom: 5px;" />
                                                            </div>

                                                            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                <asp:Button ID="btnCancelarPrecio"
                                                                    Text="Cancelar"
                                                                    CssClass="btn btn-warning col-lg-12 col-md-12 col-sm-12 col-xs-12"
                                                                    runat="server"
                                                                    Enabled="false"
                                                                    Onclick="btnCancelarPrecio_Click"
                                                                    Style="margin-bottom: 5px;" />
                                                            </div>

                                                            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                <asp:Button ID="btnPreciosGuardar"
                                                                    Text="Guardar configuración"
                                                                    CssClass="btn btn-success col-lg-12 col-md-12 col-sm-12 col-xs-12"
                                                                    runat="server"
                                                                    OnClick="btnPreciosGuardar_Click"
                                                                    Style="margin-bottom: 5px;" />
                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>

		                                    </ContentTemplate>
	                                    </asp:UpdatePanel>
                                    </div>

                                </div>
			                </div>
		                </ContentTemplate>
	                </asp:UpdatePanel>

                </div>
            </div>

            <div class="bottom-buttons ">

                <%-- Botón _Models_ --%>
                <div class="btn-models">
                    <asp:Button ID="btnVolverAlListado"
                        Text="Volver al listado"
                        CssClass="btn btn-models btn-info"
                        runat="server"
                        OnClick="btnVolverAlListado_Click"
                        Style="margin-bottom: 5px;" />
                </div>

            </div>

        </div>
    </div>

</asp:Content>