﻿<%@ Page Title="Usuario" Language="C#" MasterPageFile="~/Pages/Page.Master" AutoEventWireup="true" CodeBehind="UsuarioSU.aspx.cs" Inherits="Pages.UsuarioSU" %>

<%-- Head --%>
<asp:Content ID="cHead" ContentPlaceHolderID="cphHead" runat="server">

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
        .tab-pane {
            padding: 20px;
        }
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>

    <%-- Script - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {

            // Evento click sobre una pestaña
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // Pestaña seleccionada
                var clickedTab = $(e.target).attr("href")
                // Guarda la pestaña seleccionada
                document.getElementById('<%= hdnTab.ClientID %>').value = clickedTab;
            });

            // Enfoca la pestaña seleccionada
            var storedTab = document.getElementById('<%= hdnTab.ClientID %>').value;
            $('#tabs a[href="' + storedTab + '"]').tab('show');

        });

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        // Solicita confirmación antes de eliminar
        function ConfirmDelete() {
            swal({
                title: "¿Eliminar el Usuario?",
                text: "Confirma la eliminación",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ac2925",
                confirmButtonText: "Si",
                closeOnConfirm: false
            },
            function () {
                __doPostBack('<%= btnDelete.UniqueID %>', "");
            });
        }
    </script>
</asp:Content>

<%-- Body --%>
<asp:Content ID="cBody" ContentPlaceHolderID="cphBody" runat="server">

    <%-- Hash del UsuarioSU --%>
    <asp:HiddenField ID="hdnHash" runat="server"></asp:HiddenField>

    <%-- Recuerda la pestaña seleccionada --%>
    <asp:HiddenField ID="hdnTab" runat="server" Value="" />

    <%-- Encabezado --%>
    <div class="row">
        <div class="col-lg-12">

            <%-- Título del formulario --%>
            <h3 class="page-header">

                <%-- Atras --%>
                <a class="btn btn-default pull-left" onclick="history.back();">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                </a>

                <%-- Separador horizontal --%>
                <span class="pull-left">&nbsp;</span>

                <%-- Actualizar --%>
                <a class="btn btn-default pull-left" onclick="window.location.reload()">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                </a>

                <%-- Titulo --%>
                <asp:Label ID="lblTitulo" Text="" runat="server" />

                <%-- Botón Eliminar --%>
                <asp:Button ID="btnDelete" Text="Dar de baja" CssClass="btn btn-danger pull-right" runat="server"
                    OnClientClick="ConfirmDelete(); return false;" OnClick="btnDelete_Click" />

                <%-- Separador horizontal --%>
                <span class="pull-right">&nbsp;</span>

                <%-- Botón Editar --%>
                <asp:Button ID="btnEdit" Text="Editar" CssClass="btn btn-warning pull-right" runat="server"
                    OnClick="btnEdit_Click" />

            </h3>

        </div>
    </div>

    <%-- Contenido --%>
    <div class="row">
        <div class="col-lg-12">

            <%-- Panel de controles del formulario --%>
            <div class="panel-body">
                <!-- Nav tabs -->
                <ul id="tabsgeneral" class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#datosUsuario" aria-controls="misc" role="tab" data-toggle="tab">Datos del Usuario</a></li>
                    <li role="presentation"><a href="#cuentasUsuario" aria-controls="misc" role="tab" data-toggle="tab">Cuentas</a></li>
                    <li role="presentation"><a href="#ipsUsuario" aria-controls="misc" role="tab" data-toggle="tab">IPs Habilitadas</a></li>
                    <li role="presentation"><a href="#registroActividades" aria-controls="misc" role="tab" data-toggle="tab">Registro de Actividades</a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="datosUsuario">
                        <br />

                        <div class="row">
                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Nombre" runat="server" />
                                </label>
                                <asp:TextBox ID="txtNombre" CssClass="form-control text-capital" Text="" runat="server" />
                                <asp:RequiredFieldValidator CssClass="required-field-validator" ControlToValidate="txtNombre" runat="server" />
                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Apellido" runat="server" />
                                </label>
                                <asp:TextBox ID="txtApellido" CssClass="form-control text-capital" Text="" runat="server" />
                                <asp:RequiredFieldValidator CssClass="required-field-validator" ControlToValidate="txtApellido" runat="server" />
                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Email" runat="server" />
                                </label>
                                <asp:TextBox ID="txtEmail" CssClass="form-control  text-lower email" Text="" runat="server" OnTextChanged="txtEmail_TextChanged" AutoPostBack="true" />
                                <asp:RequiredFieldValidator CssClass="required-field-validator" ControlToValidate="txtEmail" runat="server" />
                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Telefono" runat="server" />
                                </label>
                                <asp:TextBox ID="txtTelefono" CssClass="form-control phone" Text="" runat="server" />
                                <asp:RequiredFieldValidator CssClass="required-field-validator" ControlToValidate="txtTelefono" runat="server" />
                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Login Web" runat="server" />
                                </label>
                                <asp:TextBox ID="txtNombreUsuario" CssClass="form-control text-lower" Text="" runat="server" OnTextChanged="txtNombreUsuario_TextChanged" AutoPostBack="true" />
                                <asp:RequiredFieldValidator CssClass="required-field-validator" ControlToValidate="txtNombreUsuario" runat="server" />
                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Auditar Actividad en el Sistema" runat="server" />
                                </label>
                                <div class="checkbox">
                                    <label>
                                        <asp:CheckBox ID="chkAuditarActividadSistema" Text="" runat="server" />
                                    </label>
                                </div>
                            </div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="form-group col-xs-12">
                                <label>
                                    <asp:Label Text="Notas" runat="server" />
                                </label>
                                <textarea class="form-control" rows="3" id="txtNotas" runat="server"></textarea>
                            </div>
                        </div>

                        <!-- Nav tabs -->
                        <ul id="tabs" class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#seguridad" aria-controls="misc" role="tab" data-toggle="tab">Seguridad</a></li>
                            <li role="presentation"><a href="#idioma" aria-controls="home" role="tab" data-toggle="tab">Idioma y Sistema Métrico</a></li>
                            <li role="presentation"><a href="#numeros" aria-controls="numeric" role="tab" data-toggle="tab">Números</a></li>
                            <li role="presentation"><a href="#monedas" aria-controls="select" role="tab" data-toggle="tab">Monedas</a></li>
                            <li role="presentation"><a href="#horafecha" aria-controls="check" role="tab" data-toggle="tab">Hora y Fecha</a></li>
                            <li role="presentation"><a href="#baja" aria-controls="radio" role="tab" data-toggle="tab">Estado de Baja</a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="seguridad">
                                <br />
                                <div class="row">
                                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <label>
                                            <asp:Label Text="Pregunta Secreta 1" runat="server" />
                                        </label>
                                        <asp:TextBox ID="txtPreguntaSecreta1" CssClass="form-control" Text="" runat="server" />
                                        <asp:RequiredFieldValidator CssClass="required-field-validator" ControlToValidate="txtPreguntaSecreta1" runat="server" />
                                    </div>

                                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <label>
                                            <asp:Label Text="Respuesta 1" runat="server" />
                                        </label>
                                        <asp:TextBox ID="txtRespuesta1" CssClass="form-control" Text="" runat="server" />
                                        <asp:RequiredFieldValidator CssClass="required-field-validator" ControlToValidate="txtRespuesta1" runat="server" />
                                    </div>

                                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <label>
                                            <asp:Label Text="Pregunta Secreta 2" runat="server" />
                                        </label>
                                        <asp:TextBox ID="txtPreguntaSecreta2" CssClass="form-control" Text="" runat="server" />
                                    </div>

                                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <label>
                                            <asp:Label Text="Respuesta 2" runat="server" />
                                        </label>
                                        <asp:TextBox ID="txtRespuesta2" CssClass="form-control" Text="" runat="server" />
                                    </div>

                                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <label>
                                            <asp:Label Text="Pregunta Secreta 3" runat="server" />
                                        </label>
                                        <asp:TextBox ID="txtPreguntaSecreta3" CssClass="form-control" Text="" runat="server" />
                                    </div>

                                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <label>
                                            <asp:Label Text="Respuesta 3" runat="server" />
                                        </label>
                                        <asp:TextBox ID="txtRespuesta3" CssClass="form-control" Text="" runat="server" />
                                    </div>

                                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <label>
                                            <asp:Label Text="Pregunta Secreta 4" runat="server" />
                                        </label>
                                        <asp:TextBox ID="txtPreguntaSecreta4" CssClass="form-control" Text="" runat="server" />
                                    </div>

                                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <label>
                                            <asp:Label Text="Respuesta 4" runat="server" />
                                        </label>
                                        <asp:TextBox ID="txtRespuesta4" CssClass="form-control" Text="" runat="server" />
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="idioma">
                                <br />
                                <div class="row">
                                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <label>
                                            <asp:Label Text="Idioma" runat="server" />
                                        </label>
                                        <asp:DropDownList ID="ddlIdioma" CssClass="form-control select-single" runat="server">
                                        </asp:DropDownList>
                                    </div>

                                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <label>
                                            <asp:Label Text="Sistema de Medida" runat="server" />
                                        </label>
                                        <asp:DropDownList ID="ddlSistemaMedida" CssClass="form-control select-single" runat="server">
                                            <asp:ListItem Selected="True" Value="1">Métrico</asp:ListItem>
                                            <asp:ListItem Value="2">U. S.</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="numeros">
                                <br />
                                <div class="row">
                                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <label>
                                            <asp:Label Text="Símbolo Decimal en Números" runat="server" />
                                        </label>
                                        <asp:DropDownList ID="ddlNumeroSimboloDecimal" CssClass="form-control select-single" runat="server">
                                            <asp:ListItem Selected="True" Value=",">Coma</asp:ListItem>
                                            <asp:ListItem Value=".">Punto</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <label>
                                            <asp:Label Text="Dígitos Decimales en Números" runat="server" />
                                        </label>
                                        <asp:TextBox ID="txtNumeroDigitosDecimales" CssClass="form-control numeric-integer" Text="" runat="server" />
                                        <asp:RequiredFieldValidator CssClass="required-field-validator" ControlToValidate="txtNumeroDigitosDecimales" runat="server" />
                                    </div>

                                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <label>
                                            <asp:Label Text="Separador de Miles en Números" runat="server" />
                                        </label>
                                        <asp:DropDownList ID="ddlNumeroSeparadorMiles" CssClass="form-control select-single" runat="server">
                                            <asp:ListItem Selected="True" Value=".">Punto</asp:ListItem>
                                            <asp:ListItem Value=",">Coma</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <label>
                                            <asp:Label Text="Ceros a la Izquierda en Números" runat="server" />
                                        </label>
                                        <asp:DropDownList ID="ddlNumeroCerosIzquierda" class="form-control select-single" runat="server">
                                            <asp:ListItem Selected="True" Value="false">No</asp:ListItem>
                                            <asp:ListItem Value="true">Si</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <label>
                                            <asp:Label Text="Completar Decimales con Ceros en Números" runat="server" />
                                        </label>
                                        <asp:DropDownList ID="ddlNumeroCompletarDecimalesCeros" CssClass="form-control select-single" runat="server">
                                            <asp:ListItem Selected="True" Value="false">No</asp:ListItem>
                                            <asp:ListItem Value="true">Si</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="monedas">
                                <br />
                                <div class="row">
                                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <label>
                                            <asp:Label Text="Símbolo de Moneda" runat="server" />
                                        </label>
                                        <asp:TextBox ID="txtMonedaSimbolo" CssClass="form-control" Text="" runat="server" />
                                        <asp:RequiredFieldValidator CssClass="required-field-validator" ControlToValidate="txtMonedaSimbolo" runat="server" />
                                    </div>

                                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <label>
                                            <asp:Label Text="Formato de Moneda" runat="server" />
                                        </label>
                                        <asp:DropDownList ID="ddlMonedaFormato" CssClass="form-control select-single" runat="server">
                                            <asp:ListItem Selected="True" Value="1">$1,1</asp:ListItem>
                                            <asp:ListItem Value="2">1,1$</asp:ListItem>
                                            <asp:ListItem Value="3">$ 1,1</asp:ListItem>
                                            <asp:ListItem Value="4">1,1 $</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <label>
                                            <asp:Label Text="Símbolo Decimal en Monedas" runat="server" />
                                        </label>
                                        <asp:DropDownList ID="ddlMonedaSimboloDecimal" CssClass="form-control select-single" runat="server">
                                            <asp:ListItem Selected="True" Value=",">Coma</asp:ListItem>
                                            <asp:ListItem Value=".">Punto</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <label>
                                            <asp:Label Text="Dígitos Decimales en Monedas" runat="server" />
                                        </label>
                                        <asp:TextBox ID="txtMonedaDigitosDecimales" CssClass="form-control numeric-integer" Text="" runat="server" />
                                        <asp:RequiredFieldValidator CssClass="required-field-validator" ControlToValidate="txtMonedaDigitosDecimales" runat="server" />
                                    </div>

                                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <label>
                                            <asp:Label Text="Separador de Miles en Monedas" runat="server" />
                                        </label>
                                        <asp:DropDownList ID="ddlMonedaSeparadorMiles" CssClass="form-control select-single" runat="server">
                                            <asp:ListItem Selected="True" Value=".">Punto</asp:ListItem>
                                            <asp:ListItem Value=",">Coma</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <label>
                                            <asp:Label Text="Ceros a la Izquierda en Monedas" runat="server" />
                                        </label>
                                        <asp:DropDownList ID="ddlMonedaCerosIzquierda" CssClass="form-control select-single" runat="server">
                                            <asp:ListItem Selected="True" Value="false">No</asp:ListItem>
                                            <asp:ListItem Value="true">Si</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <label>
                                            <asp:Label Text="Completar Decimales con Ceros en Monedas" runat="server" />
                                        </label>
                                        <asp:DropDownList ID="ddlMonedaCompletarDecimalesCeros" CssClass="form-control select-single" runat="server">
                                            <asp:ListItem Selected="True" Value="false">No</asp:ListItem>
                                            <asp:ListItem Value="true">Si</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="horafecha">
                                <br />
                                <div class="row">
                                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <label>
                                            <asp:Label Text="Zona Horaria" runat="server" />
                                        </label>
                                        <asp:DropDownList ID="ddlZonaHoraria" CssClass="form-control select-single" runat="server">
                                        </asp:DropDownList>
                                    </div>

                                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <label>
                                            <asp:Label Text="Formato de Hora" runat="server" />
                                        </label>
                                        <asp:DropDownList ID="ddlFormatoHora" CssClass="form-control select-single" runat="server">
                                            <asp:ListItem Selected="True" Value="1">hh:mm tt</asp:ListItem>
                                            <asp:ListItem Value="2">h:mm tt</asp:ListItem>
                                            <asp:ListItem Value="3">HH:mm</asp:ListItem>
                                            <asp:ListItem Value="4">H:mm</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <label>
                                            <asp:Label Text="Formato de Fecha" runat="server" />
                                        </label>
                                        <asp:TextBox ID="txtFormatoFecha" CssClass="form-control" Text="" runat="server" />
                                        <asp:RequiredFieldValidator CssClass="required-field-validator" ControlToValidate="txtFormatoFecha" runat="server" />
                                    </div>

                                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <label>
                                            <asp:Label Text="Primer Día de la Semana" runat="server" />
                                        </label>
                                        <asp:DropDownList ID="ddlPrimerDiaSemana" CssClass="form-control select-single" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="baja">
                                <br />
                                <div class="row">
                                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <label>
                                            <asp:Label Text="Fecha de Baja" runat="server" />
                                        </label>
                                        <asp:TextBox ID="txtFechaBaja" CssClass="form-control" Text="" runat="server" Enabled="false" />
                                    </div>

                                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <label>
                                            <asp:Label Text="Fecha y Hora de Bloqueo" runat="server" />
                                        </label>
                                        <asp:TextBox ID="txtFechaHoraBloqueo" CssClass="form-control" Text="" runat="server" Enabled="false" />
                                    </div>

                                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <label>
                                            <asp:Label Text="Intentos de Ingreso Fallidos" runat="server" />
                                        </label>
                                        <asp:TextBox ID="txtIntentosIngresoFallidos" CssClass="form-control" Text="" runat="server" Enabled="false" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="cuentasUsuario">
                        <br />
                        <asp:GridView ID="gvCuentas" CssClass="table table-striped table-bordered table-hover grid-view"
                            Width="100%" AutoGenerateColumns="False" runat="server">
                            <Columns>
                                <%-- Requeridas para las GridViews clickeables - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                                <%-- Responsive + - --%>
                                <asp:BoundField HeaderText="" />
                                <%-- cryptoID --%>
                                <asp:BoundField HeaderText="#" DataField="IdCuenta" HeaderStyle-CssClass="crypto-id-column" ItemStyle-CssClass="crypto-id-column" />
                                <%-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                                <asp:BoundField HeaderText="Fecha de Alta" DataField="FechaHoraAlta" ItemStyle-CssClass="date-time" />
                                <asp:BoundField HeaderText="Sistema" DataField="SistemaNombre" ItemStyle-CssClass="text-capital" />
                                <asp:BoundField HeaderText="Aplicación" DataField="AplicacionNombre" ItemStyle-CssClass="text-capital" />
                                <asp:BoundField HeaderText="Perfil" DataField="PerfilNombre" ItemStyle-CssClass="text-capital" />
                                <asp:BoundField HeaderText="Tipo" DataField="PropietarioTipo" ItemStyle-CssClass="text-capital" />
                                <asp:BoundField HeaderText="Propietario" DataField="PropietarioRazonSocial" ItemStyle-CssClass="text-capital" />
                                <asp:BoundField HeaderText="Último Acceso" DataField="UltimoAcceso" ItemStyle-CssClass="date-time" />
                                <asp:BoundField HeaderText="Fecha de Baja" DataField="FechaHoraBaja" ItemStyle-CssClass="date-time" />

                            </Columns>
                        </asp:GridView>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="ipsUsuario">
                        <br />
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12" runat="server">
                                    <label>
                                        <asp:Label Text="Sistema" runat="server" />
                                    </label>
                                    <asp:DropDownList ID="ddlSistemaIps" CssClass="form-control select-single" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSistemaIps_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                                <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12" id="Div2" runat="server">
                                    <label>
                                        <asp:Label Text="Aplicación" runat="server" />
                                    </label>
                                    <asp:DropDownList ID="ddlAplicacionIps" CssClass="form-control select-single" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAplicacionIps_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                                <div class="top-buttons">
                                    <asp:Button ID="btnAddIpRow" CssClass="btn btn-info pull-right" Text="Agregar IP" OnClick="btnAddIpRow_Click" runat="server" />
                                </div>
                                <asp:GridView ID="gvIps" CssClass="table table-striped table-bordered table-hover inline-edit"
                                    Width="100%" AutoGenerateColumns="False" runat="server" OnRowDeleting="gvIps_RowDeleting"
                                    EmptyDataText="No se definieron IPs autorizadas" OnRowDataBound="gvIps_RowDataBound">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Dirección IP">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtIp" CssClass="form-control" Width="100%" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtIp" ErrorMessage=""
                                                    CssClass="required-field-validator"
                                                    SetFocusOnError="True" runat="server">
                                                </asp:RequiredFieldValidator>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <%-- Eliminar --%>
                                        <asp:CommandField ShowDeleteButton="True" ButtonType="Image" DeleteImageUrl="http://www.haotu.net/up/4234/24/212-bin.png" />
                                    </Columns>
                                </asp:GridView>

                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlSistemaIps" />
                                <asp:AsyncPostBackTrigger ControlID="ddlAplicacionIps" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="registroActividades">
                        <br />
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12" id="fgrSistema" runat="server">
                                    <label>
                                        <asp:Label Text="Sistema" runat="server" />
                                    </label>
                                    <asp:DropDownList ID="ddlSistema" CssClass="form-control select-single" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSistema_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                                <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12" id="fgrAplicacion" runat="server">
                                    <label>
                                        <asp:Label Text="Aplicación" runat="server" />
                                    </label>
                                    <asp:DropDownList ID="ddlAplicacion" CssClass="form-control select-single" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAplicacion_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>

                                <asp:GridView ID="gvActividad" CssClass="table table-striped table-bordered table-hover grid-view"
                                    Width="100%" AutoGenerateColumns="False" runat="server"
                                    EmptyDataText="Sin actividad registrada">
                                    <Columns>
                                        <asp:BoundField HeaderText="Fecha y Hora" DataField="FechaHora" ItemStyle-CssClass="date-time" />
                                        <asp:BoundField HeaderText="IP Origen" DataField="Ip" />
                                        <asp:BoundField HeaderText="Descripción" DataField="Descripcion" />
                                        <asp:BoundField HeaderText="Sesión de Trabajo" DataField="IdSesionTrabajo" />
                                    </Columns>
                                </asp:GridView>

                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlSistema" />
                                <asp:AsyncPostBackTrigger ControlID="ddlAplicacion" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>

            <%-- Separador vertical --%>
            <hr />

            <div class="bottom-buttons">

                <%-- Botón _Models_ --%>
                <div class="btn-models">
                    <a href="UsuariosSU.aspx" class="btn btn-info">
                        <asp:Label Text="Volver al listado" runat="server" />
                    </a>
                </div>

                <%-- Botones Guardar y Restablecer --%>
                <div class="btns-reset-save">
                    <button type="reset" class="btn btn-default">
                        <asp:Label Text="Restablecer" runat="server" />
                    </button>
                    <asp:Button ID="btnSave" Text="Guardar" CssClass="btn btn-success" Enabled="False" runat="server" OnClick="btnSave_Click" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>