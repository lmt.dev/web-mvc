﻿<%@ Page Title="Facturas Electrónicas con Detalle" Language="C#" MasterPageFile="~/Pages/Page.Master" AutoEventWireup="true" CodeBehind="MTXCAs.aspx.cs" Inherits="Pages.MTXCAs" %>

<%-- Head --%>
<asp:Content ID="cHead" ContentPlaceHolderID="cphHead" runat="server">

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>
    <script src="../Scripts/vendor/inputmask.min.js"></script>

    <%-- Script - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {
        });

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    </script>
</asp:Content>

<%-- Body --%>
<asp:Content ID="cBody" ContentPlaceHolderID="cphBody" runat="server">

    <%-- Encabezado --%>
    <div class="row">
        <div class="col-lg-12">

            <h3 class="page-header">

                <%-- Atras --%>
                <a class="btn btn-default pull-left" onclick="history.back();">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                </a>

                <%-- Separador horizontal --%>
                <span class="pull-left">&nbsp;</span>

                <%-- Actualizar --%>
                <a class="btn btn-default pull-left" onclick="window.location.reload()">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                </a>

                <%-- Titulo --%>
                <asp:Label ID="lblTitulo" Text="" runat="server" />

                <%-- Botón Nuevo --%>
                <asp:Button ID="btnNuevo" Text="Nuevo" CssClass="btn btn-primary pull-right" runat="server" OnClick="btnNuevo_Click" />

            </h3>

        </div>
    </div>

    <%-- Contenido --%>
    <div class="row">
        <div class="col-lg-12">

            <%-- Panel de filtros --%>
            <div class="panel-body">
                <div class="row">

                    <%-- Filtros --%>
                    <div class="panel panel-default">
                        <div class="panel-body">

                            <%-- Desde --%>
                            <div class="form-group col-lg-1 col-md-2 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Desde" runat="server" />
                                </label>
                                <asp:TextBox ID="txtDesde" CssClass="form-control date-picker" autocomplete="off" runat="server" />
                            </div>

                            <%-- Hasta --%>
                            <div class="form-group col-lg-1 col-md-2 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Hasta" runat="server" />
                                </label>
                                <asp:TextBox ID="txtHasta" CssClass="form-control date-picker" autocomplete="off" runat="server" />
                            </div>

                        </div>
                    </div>

                    <%-- Botones Filtrar y Restablecer --%>
                    <div class="col-lg-12 text-center">
                        <asp:Button ID="btnFiltrar" Text="Filtrar" CssClass="btn btn-info" runat="server" OnClick="btnFiltrar_Click" />
                        <button type="reset" class="btn btn-default">
                            <asp:Label Text="Restablecer" runat="server" />
                        </button>
                    </div>
                </div>
            </div>

            <%-- Tabla de resultados de búsqueda de MTXCAs --%>
            <asp:UpdatePanel runat="server">
                <ContentTemplate>

                    <asp:GridView ID="gvMTXCAs" CssClass="table table-striped table-bordered table-hover grid-view show-buttons crypto-id clickable" data-model="../Printables/MTXCAPrint.aspx"
                        Width="100%" AutoGenerateColumns="False" runat="server" OnRowDataBound="gvMTXCAs_RowDataBound" EmptyDataText="Sin datos para mostrar.">
                        <Columns>
                            <%-- Requeridas para las GridViews clickeables - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                            <%-- Responsive + - --%>
                            <asp:BoundField HeaderText="" />
                            <%-- cryptoID --%>
                            <asp:BoundField HeaderText="#" DataField="IdMTXCA" HeaderStyle-CssClass="crypto-id-column" ItemStyle-CssClass="crypto-id-column" />
                            <%-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                            <asp:BoundField HeaderText="Tipo Comprobante" DataField="TipoComprobante" />
                            <asp:BoundField HeaderText="Comprobante" DataField="Comprobante" />
                            <asp:BoundField HeaderText="FechaEmision" DataField="FechaEmision" ItemStyle-CssClass="date-picker" />
                            <asp:BoundField HeaderText="CUIT" DataField="CUIT" ItemStyle-CssClass="cuit" />
                            <asp:BoundField HeaderText="CAE" DataField="CAE" />
                            <asp:BoundField HeaderText="Fecha Vto." DataField="FechaVencimiento" ItemStyle-CssClass="date-picker" />
                            <asp:BoundField HeaderText="IVA" DataField="TotalIVA" ItemStyle-CssClass="numeric-money show-total" />
                            <asp:BoundField HeaderText="Total" DataField="ImporteTotal" ItemStyle-CssClass="numeric-money" />

                        </Columns>
                    </asp:GridView>

                </ContentTemplate>
                <Triggers>
                    <%-- Triggers que actualizan el UpdatePanel que contiene la GridView con los resultados --%>
                    <asp:AsyncPostBackTrigger ControlID="btnFiltrar" />
                </Triggers>
            </asp:UpdatePanel>

            <%-- Información adicional o ayuda --%>
            <div class="well">
                <h4>
                    <asp:Label Text="Información adicional o ayuda" runat="server" />
                </h4>
                <p>
                    <asp:Label Text="Tincidunt integer eu augue augue nunc elit dolor, luctus placerat scelerisque euismod, iaculis eu lacus nunc mi elit, vehicula ut laoreet ac, aliquam sit amet justo nunc tempor, metus vel." runat="server" />
                </p>
            </div>
        </div>
    </div>
</asp:Content>
