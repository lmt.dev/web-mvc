﻿<%@ Page Title="Remitos" Language="C#" MasterPageFile="~/Pages/Page.Master" AutoEventWireup="true" CodeBehind="Remitar.aspx.cs" Inherits="Pages.Remitar" %>
<%@ Register Src="~/Controls/ucFiltro_DistribuidorUnidadNegocioReseller.ascx" TagPrefix="uc" TagName="ucFiltro_DistribuidorUnidadNegocioReseller" %>
<asp:Content ID="cHead" ContentPlaceHolderID="cphHead" runat="server">

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>

    <%-- Script - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {

            // Evento click sobre una pestaña
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // Pestaña seleccionada
                var clickedTab = $(e.target).attr("href")
                // Guarda la pestaña seleccionada
                document.getElementById('<%= hdnTab.ClientID %>').value = clickedTab;
            });

            // Enfoca la pestaña seleccionada
            var storedTab = document.getElementById('<%= hdnTab.ClientID %>').value;
            $('#tabs a[href="' + storedTab + '"]').tab('show');

        });

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

    </script>
</asp:Content>

<%-- Body --%>
<asp:Content ID="cBody" ContentPlaceHolderID="cphBody" runat="server">

    <%-- Recuerda la pestaña seleccionada --%>
    <asp:HiddenField ID="hdnTab" runat="server" Value="" />

    <%-- Encabezado --%>
    <div class="row">
        <div class="col-lg-12">

            <%-- Título del formulario --%>
            <h3 class="page-header">

                <%-- Botón Actualizar --%>
                <a href="" class="btn btn-default pull-left">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                </a>

                <%-- Titulo --%>
                <asp:Label ID="lblTitulo" Text="" runat="server" />

            </h3>

        </div>
    </div>

    <%-- Contenido --%>
    <div class="row">
        <div class="col-lg-12">

            <%-- Panel de controles del formulario --%>
            <div class="panel-body">
                <div class="row">
                    <uc:ucFiltro_DistribuidorUnidadNegocioReseller runat="server" ID="ucFiltro_DistribuidorUnidadNegocioReseller1" />
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a data-toggle="tab" runat="server" id="mnuTab" href="#menuRemitos">Remitos</a></li>
                        <li><a data-toggle="tab" runat="server" id="mnuTabSeleccionUM" href="#menuRemitar">A remitar</a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="menuRemitos" class="tab-pane fade in active">
                            <p></p>
                            <asp:UpdatePanel ID="upGridViewRemitos" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false" style="margin-left: 10px; margin-right: 10px;">
		                        <ContentTemplate>
                                <asp:GridView ID="gvRemitos" CssClass="table table-striped table-bordered table-hover grid-view crypto-id clickable" data-model="RemitoCarga.aspx"
                                    Width="100%" AutoGenerateColumns="False" runat="server" OnRowDataBound="gvRemitos_RowDataBound" EmptyDataText="Sin datos para mostrar.">
                                    <Columns>
                                        <%-- Requeridas para las GridViews clickeables - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                                        <%-- Responsive + - --%>
                                        <asp:BoundField HeaderText="" />
                                        <%-- cryptoID --%>
                                        <asp:BoundField HeaderText="#" DataField="IdRemito" HeaderStyle-CssClass="crypto-id-column" ItemStyle-CssClass="crypto-id-column" />
                                        <%-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                                        <asp:BoundField HeaderText="Id Distribuidor Unidad Negocio" DataField="IdDistribuidorUnidadNegocio" />
                                        <asp:BoundField HeaderText="Fecha de Alta" DataField="FHAlta" />
                                        <asp:BoundField HeaderText="Remito Nro" DataField="RemitoImpresion" />
                                        <asp:BoundField HeaderText="Email" DataField="MailInforme" />
                                        <asp:BoundField HeaderText="Observaciones" DataField="Observacion" />
                                    </Columns>
                                </asp:GridView>
		                        </ContentTemplate>
	                        </asp:UpdatePanel>

	                        <asp:Button ID="btnNuevoRemito" 
                                    Text="Nuevo remito" 
                                    Enabled="true" 
                                    CssClass="btn btn-success col-lg-6 col-md-6 col-sm-6 col-xs-6 fa-pull-right" 
                                    runat="server"
                                    OnClick="btnNuevoRemito_Click"
                                    style="margin-bottom: 10px; margin-left: 10px; margin-right: 10px;" />
                        </div>

                        <div id="menuRemitar" class="tab-pane fade">
                            <p></p>

	                        <asp:UpdatePanel ID="upGridViewMetricasRemito" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false" style="margin-left: 10px; margin-right: 10px;">
		                        <ContentTemplate>

                                    <asp:GridView ID="gvMetricasRemitos" CssClass="table table-striped table-bordered table-hover grid-view" data-model="RemitoCarga.aspx"
                                        Width="100%" AutoGenerateColumns="False" runat="server" 
                                        OnRowDataBound="gvMetricasRemitos_RowDataBound"
                                        OnSelectedIndexChanging="gvMetricasRemitos_SelectedIndexChanging"
                                        OnRowDeleting="gvMetricasRemitos_RowDeleting"
                                        EmptyDataText="Sin datos para mostrar.">
                                        <Columns>
                                            <asp:BoundField HeaderText="IdSolicitante" DataField="IDSOLICITANTE" ItemStyle-Width="100"/>
                                            <asp:BoundField HeaderText="Razón Social" DataField="RZ" ItemStyle-Width="100"/>
                                
                                            <asp:TemplateField HeaderText="Listos para remitar" ItemStyle-Width="50">
                                                <ItemTemplate>
                                                    <center>
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                             <asp:LinkButton ID="btnIrARemitar"
                                                                    runat="server"
                                                                    CssClass="btn btn-success"
                                                                    CommandName="Select"
                                                                    Style="margin-left: 5px;"
                                                                    title="Ir a la sección para generar el remito">
                                                                </asp:LinkButton>
                                                        </div>
                                                    </center>
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>

                                            <asp:BoundField HeaderText="Ultima gestión" DataField="FechaUltimaGestion" ItemStyle-Width="100"/>

                                            <asp:TemplateField HeaderText="Pendientes" ItemStyle-Width="50">
                                                <ItemTemplate>

                                                    <center>
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                             <asp:LinkButton ID="btnIrAManufactura"
                                                                    runat="server"
                                                                    CssClass="btn btn-warning"
                                                                    CommandName="Delete"
                                                                    Style="margin-left: 5px;"
                                                                    title="Ir a la sección para gestionar la manufactura">
                                                                </asp:LinkButton>
                                                        </div>
                                                    </center>

                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>

                                            <asp:BoundField HeaderText="Tipo de solicitante" DataField="TipoSolicitante" ItemStyle-Width="100"/>
                                        </Columns>
                                    </asp:GridView>

		                        </ContentTemplate>
	                        </asp:UpdatePanel>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

</asp:Content>