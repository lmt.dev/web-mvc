﻿<%@ Page Title="Conexiones de UMs por Servidor" Language="C#" MasterPageFile="~/Pages/Page.Master" AutoEventWireup="true" CodeBehind="ConexionPorServidor.aspx.cs" Inherits="Pages.ConexionPorServidor" %>

<%-- Head --%>
<asp:Content ID="cHead" ContentPlaceHolderID="cphHead" runat="server">

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>
    <%-- Chart --%>
    <script src="../Scripts/vendor/chart.min.js"></script>
    <%-- Palette --%>
    <script src="../Scripts/vendor/palette.js"></script>

    <%-- Script - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {
        });

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        function DrawPie(myData, myLabels) {

            var ctx = document.getElementById("myChart");

            // Grafico de torta
            var myChart = new Chart(ctx, {
                type: 'pie',
                data: {
                    labels: myLabels,
                    datasets: [{
                        data: myData,
                        backgroundColor: palette('tol', myData.length).map(function (hex) {
                            return '#' + hex;
                        })
                    }]
                }
            });

            // Evento click sobre los sectores del grafico
            document.getElementById("myChart").onclick = function (evt) {
                var activePoints = myChart.getElementsAtEvent(evt);
                var firstPoint = activePoints[0];
                var label = myChart.data.labels[firstPoint._index];
                var value = myChart.data.datasets[firstPoint._datasetIndex].data[firstPoint._index];

                $("#cphBody_lblServer").text(label);
                $("#cphBody_hdnServer").val(label);
                $("#myModal").modal('show');

                __doPostBack('<%= btnListarUMs.UniqueID %>', "");
            };

        }

    </script>
</asp:Content>

<%-- Body --%>
<asp:Content ID="cBody" ContentPlaceHolderID="cphBody" runat="server">

    <asp:HiddenField ID="hdnServer" runat="server" />

    <%-- Encabezado --%>
    <div class="row">
        <div class="col-lg-12">

            <%-- Título del formulario --%>
            <h3 class="page-header">

                <%-- Botón Actualizar --%>
                <a href="" class="btn btn-default pull-left">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                </a>

                <%-- Titulo --%>
                <asp:Label ID="lblTitulo" Text="" runat="server" />

            </h3>

        </div>
    </div>

    <%-- Contenido --%>
    <div class="row">
        <div class="col-lg-12">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                <h5>
                    <asp:Label Text="Conexiones: " runat="server" />
                    <strong>
                        <asp:Label ID="lblTotal" Text="..." runat="server" />
                    </strong>
                </h5>
            </div>

            <%-- Panel de controles del formulario --%>
            <div class="panel-body">
                <div class="row">

                    <canvas id="myChart" height="100"></canvas>

                </div>
            </div>

            <%-- Modal con el detalle de las UMs conectadas al server seleccionado en el gráfico de torta --%>
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">
                                <asp:Label Text="UMs conectadas a " runat="server" />
                                <asp:Label ID="lblServer" Text="..." runat="server" />
                            </h4>
                        </div>
                        <div class="modal-body">
                            <asp:UpdatePanel ID="upUMs" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                <ContentTemplate>

                                    <asp:Button ID="btnListarUMs" Text="Actualizar" runat="server" OnClick="btnListarUMs_Click" Style="display: none;" />

                                    <asp:GridView ID="gvUMs" CssClass="table table-striped table-bordered grid-view"
                                        Width="100%" AutoGenerateColumns="false" runat="server" EmptyDataText="Sin datos para mostrar.">
                                        <Columns>
                                            <%-- Responsive + - --%>
                                            <asp:BoundField HeaderText="" />

                                            <asp:BoundField HeaderText="Serial de la UM" DataField="UM" />
                                            <asp:BoundField HeaderText="Ultima conexión" DataField="FHInicioConexion" ItemStyle-CssClass="date-time" />
                                            <asp:BoundField HeaderText="¿Cerró OK?" DataField="CerroOK" />
                                        </Columns>

                                    </asp:GridView>

                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                <asp:Label Text="Cerrar" runat="server" />
                            </button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</asp:Content>