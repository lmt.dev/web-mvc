﻿<%@ Page Title="Resellers Desasignados" Language="C#" MasterPageFile="~/Pages/Page.Master" AutoEventWireup="true" CodeBehind="ResellersNoAsignados.aspx.cs" Inherits="Pages.ResellersNoAsignados" %>

<%-- Head --%>
<asp:Content ID="cHead" ContentPlaceHolderID="cphHead" runat="server">

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>
    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {
        });

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    </script>
</asp:Content>

<%-- Body --%>
<asp:Content ID="cBody" ContentPlaceHolderID="cphBody" runat="server">

    <%-- Encabezado --%>
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">

                <%-- Atras --%>
                <a class="btn btn-default pull-left" onclick="history.back();">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                </a>

                <%-- Separador horizontal --%>
                <span class="pull-left">&nbsp;</span>

                <%-- Actualizar --%>
                <a class="btn btn-default pull-left" onclick="window.location.reload()">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                </a>

                <%-- Titulo --%>
                <asp:Label ID="lblTitulo" Text="" runat="server" />

            </h3>

        </div>
    </div>

    <%-- Contenido --%>
    <div class="row">
        <div class="col-lg-12">

            <%-- Tabla de resultados de búsqueda de Resellers --%>
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <asp:GridView ID="gvResellersNoAsignados" CssClass="table table-striped table-bordered table-hover grid-view crypto-id clickable" data-model="ResellerNoAsignado.aspx" data-Order="2-asc"
                        Width="100%" AutoGenerateColumns="False" runat="server" OnRowDataBound="gvResellersNoAsignados_RowDataBound" EmptyDataText="Sin datos para mostrar.">
                        <Columns>
                            <%-- Requeridas para las GridViews clickeables - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                            <%-- Responsive + - --%>
                            <asp:BoundField HeaderText="" />
                            <%-- cryptoID --%>
                            <asp:TemplateField HeaderText="#" HeaderStyle-CssClass="crypto-id-column" ItemStyle-CssClass="crypto-id-column">
                                <ItemTemplate>
                                    <asp:Label ID="lblComplexId" runat="server" Text='<%# Eval("IdVendingReseller") + "*" + Eval("UnidadNegocio") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                            <asp:BoundField HeaderText="Razon Social" DataField="RazonSocial" />
                            <asp:BoundField HeaderText="Cuit" DataField="Cuit" />
                            <asp:BoundField HeaderText="Fecha Hora" DataField="FechaAlta" ItemStyle-CssClass="col-date-time" />
                            <asp:BoundField HeaderText="Unidad Negocio" DataField="UnidadNegocio" />
                            <asp:BoundField HeaderText="Pais" DataField="Pais" />
                            <asp:BoundField HeaderText="Observaciones" DataField="Observaciones" />
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                </Triggers>
            </asp:UpdatePanel>

            <%-- Información adicional o ayuda --%>
            <div class="well">
                <h4>
                    <asp:Label Text="Información adicional o ayuda" runat="server" />
                </h4>
                <p>
                    <asp:Label Text="Tincidunt integer eu augue augue nunc elit dolor, luctus placerat scelerisque euismod, iaculis eu lacus nunc mi elit, vehicula ut laoreet ac, aliquam sit amet justo nunc tempor, metus vel." runat="server" />
                </p>
            </div>
        </div>
    </div>
</asp:Content>