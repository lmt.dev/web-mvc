﻿<%@ Page Title="Pre-Contacto" Language="C#" MasterPageFile="~/Pages/Page.Master" AutoEventWireup="true" CodeBehind="PreContacto.aspx.cs" Inherits="Pages.PreContacto" %>

<%-- Head --%>
<asp:Content ID="cHead" ContentPlaceHolderID="cphHead" runat="server">

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>

    <%-- Script - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {
        });

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

        // Solicita confirmación antes de eliminar
        function ConfirmDelete() {
            swal({
                title: "¿Eliminar el PreContacto?",
                text: "Confirma la eliminación",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonColor: "#ac2925",
                confirmButtonText: "Si",
                closeOnConfirm: false
            },
            function () {
                __doPostBack('<%= btnDelete.UniqueID %>', "");
            });
        }

    </script>
</asp:Content>

<%-- Body --%>
<asp:Content ID="cBody" ContentPlaceHolderID="cphBody" runat="server">

    <%-- Encabezado --%>
    <div class="row">
        <div class="col-lg-12">

            <%-- Título del formulario --%>
            <h3 class="page-header">

                <%-- Botón Actualizar --%>
                <a href="" class="btn btn-default pull-left">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                </a>

                <%-- Titulo --%>
                <asp:Label ID="lblTitulo" Text="" runat="server" />

                <%-- Botón Eliminar --%>
                <asp:Button ID="btnDelete" Text="Dar de baja" CssClass="btn btn-danger pull-right" runat="server"
                    OnClientClick="ConfirmDelete(); return false;" OnClick="btnDelete_Click" />

                <%-- Separador horizontal --%>
                <span class="pull-right">&nbsp;</span>
            </h3>

        </div>
    </div>

    <%-- Contenido --%>
    <div class="row">
        <div class="col-lg-12">

            <!-- Modal -->
            <div id="modalDistribuidor" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">
                                <asp:Label Text="Seleccione el Distribuidor" runat="server" />
                            </h4>
                            <small>
                                <asp:Label Text="El Pre-Contacto creará una Oportunidad para el Distribuidor seleccionado." runat="server" />
                            </small>
                        </div>
                        <div class="modal-body">

                            <%-- Distribuidor --%>
                            <div class="form-group">
                                <label>
                                    <asp:Label Text="Distribuidor" runat="server" />
                                </label>
                                <asp:DropDownList ID="ddlDistribuidor" CssClass="form-control select-single" runat="server" />
                            </div>

                            <%-- Descripcion --%>
                            <div class="form-group">
                                <label>
                                    <asp:Label Text="Descripción" runat="server" />
                                </label>
                                <asp:TextBox ID="txtDescripcion" CssClass="form-control" runat="server" />
                            </div>

                            <%-- Detalle --%>
                            <div class="form-group">
                                <label>
                                    <asp:Label Text="Detalle" runat="server" />
                                </label>
                                <asp:TextBox ID="txtDetalle" CssClass="form-control" runat="server" TextMode="MultiLine" />
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btnAsignar" Text="Asignar" runat="server" CssClass="btn btn-success" OnClick="btnAsignar_Click" />
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                <asp:Label Text="Cerrar" runat="server" />
                            </button>
                        </div>
                    </div>

                </div>
            </div>

            <%-- Panel de controles del formulario --%>
            <div class="panel-body">
                <div class="row">

                    <%-- Fecha Hora Alta --%>
                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Fecha Hora Alta" runat="server" />
                        </label>
                        <div class="control-div-static">
                            <asp:Label ID="lblFHAlta" CssClass="form-control-static" runat="server" />
                        </div>
                    </div>

                    <%-- Origen --%>
                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Origen" runat="server" />
                        </label>
                        <div class="control-div-static">
                            <asp:Label ID="lblOrigen" CssClass="form-control-static" runat="server" />
                        </div>
                    </div>
                </div>
                <br />
                <div class="row">

                    <%-- Nombre --%>
                    <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <label>
                            <asp:Label Text="Nombre" runat="server" />
                        </label>
                        <div class="control-div-static">
                            <asp:Label ID="lblNombre" CssClass="form-control-static" runat="server" />
                        </div>
                    </div>

                    <%-- Apellido --%>
                    <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <label>
                            <asp:Label Text="Apellido" runat="server" />
                        </label>
                        <div class="control-div-static">
                            <asp:Label ID="lblApellido" CssClass="form-control-static" runat="server" />
                        </div>
                    </div>

                    <%-- Email --%>
                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Email" runat="server" />
                        </label>
                        <div class="control-div-static">
                            <asp:Label ID="lblEmail" CssClass="form-control-static" runat="server" />
                        </div>
                    </div>
                </div>
                <div class="row">

                    <%-- Tel. Fijo --%>
                    <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <label>
                            <asp:Label Text="Teléfono Fijo" runat="server" />
                        </label>
                        <div class="control-div-static">
                            <asp:Label ID="lblFijo" CssClass="form-control-static" runat="server" />
                        </div>
                    </div>

                    <%-- Tel. Movil --%>
                    <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <label>
                            <asp:Label Text="Teléfono Móvil" runat="server" />
                        </label>
                        <div class="control-div-static">
                            <asp:Label ID="lblMovil" CssClass="form-control-static" runat="server" />
                        </div>
                    </div>

                    <%-- Empresa --%>
                    <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <label>
                            <asp:Label Text="Empresa" runat="server" />
                        </label>
                        <div class="control-div-static">
                            <asp:Label ID="lblEmpresa" CssClass="form-control-static" runat="server" />
                        </div>
                    </div>
                </div>
                <br />
                <div class="row">

                    <%-- Mensaje --%>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label>
                            <asp:Label Text="Mensaje" runat="server" />
                        </label>
                        <div class="control-div-static">
                            <asp:Label ID="lblMensaje" CssClass="form-control-static" runat="server" />
                        </div>
                    </div>
                </div>
                <br />
                <div class="row">

                    <%-- IP --%>
                    <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="IP" runat="server" />
                        </label>
                        <div class="control-div-static">
                            <asp:Label ID="lblIP" CssClass="form-control-static" runat="server" />
                        </div>
                    </div>

                    <%-- Pais --%>
                    <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Pais" runat="server" />
                        </label>
                        <div class="control-div-static">
                            <asp:Label ID="lblPais" CssClass="form-control-static" runat="server" />
                        </div>
                    </div>

                    <%-- Region --%>
                    <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Region" runat="server" />
                        </label>
                        <div class="control-div-static">
                            <asp:Label ID="lblRegion" CssClass="form-control-static" runat="server" />
                        </div>
                    </div>

                    <%-- Ciudad --%>
                    <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Ciudad" runat="server" />
                        </label>
                        <div class="control-div-static">
                            <asp:Label ID="lblCiudad" CssClass="form-control-static" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom-buttons ">

                <%-- Botones Guardar y Restablecer --%>
                <div class="btns-reset-save">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalDistribuidor">
                        <asp:Label Text="Asignar a un Distribuidor" runat="server" />
                    </button>
                </div>

                <%-- Botón PreContactos --%>
                <div class="btn-models">
                    <a href="PreContactos.aspx" class="btn btn-info">
                        <asp:Label Text="Volver al listado" runat="server" />
                    </a>
                </div>
            </div>
        </div>
    </div>

</asp:Content>