﻿<%@ Page Title="Perfiles en el Sistema de Usuarios" Language="C#" MasterPageFile="~/Pages/Page.Master" AutoEventWireup="true" CodeBehind="PerfilesSU.aspx.cs" Inherits="Pages.PerfilesSU" %>

<%-- Head --%>
<asp:Content ID="cHead" ContentPlaceHolderID="cphHead" runat="server">

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>

    <%-- Script - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {
        });

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

    </script>
</asp:Content>

<%-- Body --%>
<asp:Content ID="cBody" ContentPlaceHolderID="cphBody" runat="server">

    <%-- Encabezado --%>
    <div class="row">
        <div class="col-lg-12">

            <%-- Título del formulario --%>
            <h3 class="page-header">

                <%-- Atras --%>
                <a class="btn btn-default pull-left" onclick="history.back();">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                </a>

                <%-- Separador horizontal --%>
                <span class="pull-left">&nbsp;</span>

                <%-- Actualizar --%>
                <a class="btn btn-default pull-left" onclick="window.location.reload()">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                </a>

                <%-- Titulo --%>
                <asp:Label ID="lblTitulo" Text="" runat="server" />

                <%-- Botón Nuevo --%>
                <asp:Button ID="btnNuevo" Text="Nuevo" CssClass="btn btn-primary pull-right" runat="server" OnClick="btnNuevo_Click" Visible="false" />

            </h3>

        </div>
    </div>

    <%-- Contenido --%>
    <div class="row">
        <div class="col-lg-12">

            <%-- Panel de filtros --%>
            <div class="panel-body">
                <div class="row">

                    <%-- Filtros --%>
                    <div class="panel panel-default">

                        <div class="panel-body">

                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12" id="fgrSistema" runat="server">
                                <label>
                                    <asp:Label Text="Sistema" runat="server" />
                                </label>
                                <asp:DropDownList ID="ddlSistema" CssClass="form-control select-single" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSistema_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>

                            <asp:UpdatePanel runat="server">
                                <ContentTemplate>

                                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12" id="fgrAplicacion" runat="server">
                                        <label>
                                            <asp:Label Text="Aplicación" runat="server" />
                                        </label>
                                        <asp:DropDownList ID="ddlAplicacion" CssClass="form-control select-single" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAplicacion_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>

                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ddlSistema" />
                                </Triggers>
                            </asp:UpdatePanel>

                        </div>
                    </div>

                    <%-- Botones Filtrar y Restablecer --%>
                    <div class="col-lg-12 text-center">
                        <asp:Button ID="btnFiltrar" Text="Filtrar" CssClass="btn btn-info" runat="server" OnClick="btnFiltrar_Click" />
                        <button type="reset" class="btn btn-default">
                            <asp:Label Text="Restablecer" runat="server" />
                        </button>
                    </div>

                </div>
            </div>

            <%-- Tabla de resultados de búsqueda de PerfilesSU --%>
            <asp:UpdatePanel runat="server">
                <ContentTemplate>

                    <asp:GridView ID="gvSistemas" CssClass="table table-striped table-bordered table-hover grid-view crypto-id clickable" data-model="PerfilSU.aspx"
                        Width="100%" AutoGenerateColumns="False" runat="server" OnRowDataBound="gvSistemas_RowDataBound" EmptyDataText="Sin datos para mostrar.">
                        <Columns>
                            <%-- Requeridas para las GridViews clickeables - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                            <%-- Responsive + - --%>
                            <asp:BoundField HeaderText="" />
                            <%-- cryptoID --%>
                            <asp:BoundField HeaderText="#" DataField="IdPerfil" HeaderStyle-CssClass="crypto-id-column" ItemStyle-CssClass="crypto-id-column" />
                            <%-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                            <asp:BoundField HeaderText="Nombre" DataField="Nombre" ItemStyle-CssClass="text-capital" />
                            <asp:BoundField HeaderText="Tipo" DataField="PropietarioTipo" ItemStyle-CssClass="text-capital" />
                            <asp:BoundField HeaderText="Propietario" DataField="PropietarioRazonSocial" ItemStyle-CssClass="text-capital" />
                            <asp:BoundField HeaderText="Fecha de Baja" DataField="FechaHoraBaja" ItemStyle-CssClass="date-time" />

                        </Columns>
                    </asp:GridView>

                </ContentTemplate>
                <Triggers>
                    <%-- Triggers que actualizan el UpdatePanel que contiene la GridView con los resultados --%>
                    <asp:AsyncPostBackTrigger ControlID="btnFiltrar" />
                </Triggers>
            </asp:UpdatePanel>

            <%-- Información adicional o ayuda --%>
            <div class="well">
                <h4>
                    <asp:Label Text="Información adicional o ayuda" runat="server" />
                </h4>
                <p>
                    <asp:Label Text="Tincidunt integer eu augue augue nunc elit dolor, luctus placerat scelerisque euismod, iaculis eu lacus nunc mi elit, vehicula ut laoreet ac, aliquam sit amet justo nunc tempor, metus vel." runat="server" />
                </p>
            </div>

        </div>
    </div>

</asp:Content>