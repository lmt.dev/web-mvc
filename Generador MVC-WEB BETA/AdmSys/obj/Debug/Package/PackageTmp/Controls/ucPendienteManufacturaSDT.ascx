﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucPendienteManufacturaSDT.ascx.cs" Inherits="AdmSys.Controls.ucPendienteManufacturaSDT" %>
<%@ Register Src="~/Controls/ucSDTDetalleFajaIngreso.ascx" TagPrefix="uc" TagName="ucSDTDetalleFajaIngreso" %>

<asp:UpdatePanel ID="upErrorHandler" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
    <ContentTemplate>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 5px;">
            <label>
                <asp:Label ID="lblErrorHandler" Text="" runat="server" />
            </label>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>

<asp:UpdatePanel ID="upFiltros" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
    <ContentTemplate>
        <asp:Label ID="lblIdDistribuidorFiltro" Text="" runat="server" Visible="false" />
        <asp:Label ID="lblIdUnidadNegocioFiltro" Text="" runat="server" Visible="false" />
        <asp:Label ID="lblIdResellerFiltro" Text="" runat="server" Visible="false" />
    </ContentTemplate>
</asp:UpdatePanel>

<div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">

    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#menuPresupuestar">

            <asp:UpdatePanel ID="upCantidadPresupuestar" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                <ContentTemplate>
                    <h5>PRESUPUESTAR <span class="label label-info">
                        <asp:Label ID="lblCantidadPresupuestar" Text="" runat="server" Visible="true" /></span>
                    </h5>
                </ContentTemplate>
            </asp:UpdatePanel>

        </a></li>

        <li><a data-toggle="tab" href="#menuEnEspera">

            <asp:UpdatePanel ID="upCantidadEnEspera" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                <ContentTemplate>
                    <h5>EN ESPERA <span class="label label-warning">
                        <asp:Label ID="lblCantidadEnEspera" Text="" runat="server" Visible="true" /></span>
                    </h5>
                </ContentTemplate>
            </asp:UpdatePanel>

        </a></li>

        <li><a data-toggle="tab" href="#menuAReparar">

            <asp:UpdatePanel ID="upCantidadAReparar" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                <ContentTemplate>
                    <h5>A REPARAR <span class="label label-danger">
                        <asp:Label ID="lblCantidadAReparar" Text="" runat="server" Visible="true" /></span>
                    </h5>
                </ContentTemplate>
            </asp:UpdatePanel>

        </a></li>

        <li><a data-toggle="tab" href="#menuARemitar">
            <asp:UpdatePanel ID="upCantidadAremitar" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                <ContentTemplate>
                    <h5>A REMITAR <span class="label label-success">
                        <asp:Label ID="lblCantidadARemitar" Text="" runat="server" Visible="true" /></span>
                    </h5>
                </ContentTemplate>
            </asp:UpdatePanel>

        </a></li>
    </ul>

    <div class="tab-content">
        <div id="menuPresupuestar" class="tab-pane fade-in active">
            <br />
            <asp:UpdatePanel ID="upGestionRecibidos" Visible="true" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                <ContentTemplate>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 5px;">
                        <label>
                            <asp:Label Text="Seleccione los artefactos que desee diagnosticar. Recuerde ingresar precio y descripción de cada diagnóstico" runat="server" />
                        </label>

                        <asp:GridView ID="gvGestionRecibidos"
                            CssClass="table table-bordered table-hover col-lg-8 col-md-8 col-sm-8 col-xs-8 grid-view"
                            Width="100%"
                            AutoGenerateColumns="False"
                            runat="server"
                            EmptyDataText="No hay artefactos disponibles para diagnosticar."
                            OnSelectedIndexChanging="gvGestionRecibidos_SelectedIndexChanging"
                            OnRowDeleting="gvGestionRecibidos_RowDeleting"
                            OnRowCommand="gvGestionRecibidos_RowCommand"
                            AutoPostBack="true">
                            <Columns>
                                <asp:BoundField HeaderText="#" DataField="IdSDTDetalle" ItemStyle-Width="75" />

                                <asp:TemplateField HeaderText="Descripción" ItemStyle-Width="250">
                                    <ItemTemplate>
                                        <div class="input-group">
                                            <h5>
                                                <span><span class="glyphicon glyphicon-pushpin"></span></span>
                                                <asp:Label ID="lblArtefacto" Text="" runat="server" />
                                            </h5>
                                        </div>

                                        <div class="input-group">
                                            <h5>
                                                <span><span class="glyphicon glyphicon-tasks"></span></span>
                                                <asp:Label ID="lblIncidencia" Text="" runat="server" />
                                            </h5>
                                        </div>
                                        <div class="input-group">
                                            <uc:ucSDTDetalleFajaIngreso runat="server" ID="ucSDTDetalleFajaIngresoRow"  />
                                        </div>

                                        <div class="input-group">
                                            <h5>
                                                <span><span class="glyphicon glyphicon-user"></span></span>
                                                <asp:Label ID="lblReseller" Text="" runat="server" />
                                            </h5>
                                        </div>

                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Diagnóstico" ItemStyle-Width="450">
                                    <ItemTemplate>
                                        <div class="input-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 5px;">

                                            <span class="input-group-addon transparent">
                                                <span class="glyphicon glyphicon-wrench"></span></span>
                                            <asp:TextBox ID="txtDiagnostico"
                                                runat="server"
                                                CssClass="form-control"
                                                PlaceHolder="Ej: Se cambiará el backlight; Se instalará nuevo firmware; etc" />
                                        </div>

                                        <div class="input-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 5px;">
                                            <span class="input-group-addon transparent col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                                <span class="glyphicon glyphicon-usd"></span></span>
                                            <asp:TextBox
                                                ID="txtPresupuesto"
                                                runat="server"
                                                CssClass="form-control numeric-integer-positive col-lg-8 col-md-8 col-sm-8 col-xs-8"
                                                ItemStyle-CssClass="numeric-money"
                                                PlaceHolder="Ej: $520"
                                                Width="75"
                                                Style="margin-right: 5px;" />

                                            <asp:LinkButton ID="btnAgregarDiagnostico"
                                                runat="server"
                                                CssClass="btn btn-primary col-lg-2 col-md-2 col-sm-2 col-xs-2"
                                                CommandName="Select"
                                                Style="margin-left: 5px;">
                                                    <span aria-hidden="true" class="glyphicon glyphicon-plus"></span>
                                            </asp:LinkButton>
                                        </div>

                                        <div class="input-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 5px;">
                                            <asp:GridView ID="gvDiagnosticos"
                                                CssClass="table table-bordered"
                                                Width="100%"
                                                AutoGenerateColumns="False"
                                                runat="server"
                                                EmptyDataText="Sin diagnóstico"
                                                Style="margin-top: 10px;"
                                                OnRowDeleting="gvDiagnosticos1_RowDeleting">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Presupuesto" DataField="Presupuesto" ItemStyle-Width="50" />
                                                    <asp:BoundField HeaderText="Descripción" DataField="Diagnostico" HtmlEncode="false" />
                                                    <asp:TemplateField HeaderText="Eliminar" ItemStyle-Width="50">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnEliminarDiagnostico"
                                                                runat="server"
                                                                CssClass="btn btn-danger"
                                                                CommandName="Delete">
                                                            <span aria-hidden="true" class="glyphicon glyphicon-remove"></span>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                        <ItemStyle />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                        <div class="input-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 5px;">
                                            <asp:LinkButton ID="btnGestionDiagnostico"
                                                runat="server"
                                                CssClass="btn btn-success pull-right"
                                                CommandName="Delete"
                                                Style="margin-bottom: 5px;"
                                                ItemStyle-Width="200"
                                                title="Enviar artefacto a estado 'Diagnosticado'">
                                            <span aria-hidden="true" class="glyphicon glyphicon-forward"></span>
                                            </asp:LinkButton>
                                        </div>
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>

                                <asp:BoundField HeaderText="SDT Nro" DataField="SolNro" ItemStyle-Width="75" />

                                <asp:TemplateField HeaderText="Id de Serial" ItemStyle-Width="125">
                                    <ItemTemplate>
                                        
                                        <asp:TextBox ID="txtIdSerial" runat="server" CssClass="form-control" Width="150" />

                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:BoundField HeaderText="Tipo de Hardware" DataField="ArtefactoTipo" ItemStyle-Width="100" />
                            </Columns>
                        </asp:GridView>

                    </div>


                </ContentTemplate>
            </asp:UpdatePanel>
        </div>

        <div id="menuEnEspera" class="tab-pane fade-in">
            <br />
            <asp:UpdatePanel ID="upGestionDiagnosticados" Visible="true" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                <ContentTemplate>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 5px;">
                        <label>
                            <asp:Label Text="Menú de gestion de artefactos diagnosticados" runat="server" />
                        </label>

                        <asp:GridView ID="gvGestionDiagnosticados"
                            CssClass="table table-bordered table-hover col-lg-8 col-md-8 col-sm-8 col-xs-8 grid-view"
                            Width="100%"
                            AutoGenerateColumns="False"
                            runat="server"
                            EmptyDataText="No hay artefactos diagnosticados disponibles para gestionar"
                            AutoPostBack="true"
                            OnSelectedIndexChanging="gvGestionDiagnosticados_SelectedIndexChanging"
                            OnRowDeleting="gvGestionDiagnosticados_RowDeleting">
                            <Columns>
                                <asp:BoundField HeaderText="#" DataField="IdSDTDetalle" ItemStyle-Width="75" />
                                <asp:TemplateField HeaderText="Descripción" ItemStyle-Width="250">
                                    <ItemTemplate>
                                        <div class="input-group">
                                            <h5>
                                                <span><span class="glyphicon glyphicon-pushpin"></span></span>
                                                <asp:Label ID="lblArtefacto" Text="" runat="server" />
                                            </h5>
                                        </div>

                                        <div class="input-group">
                                            <h5>
                                                <span><span class="glyphicon glyphicon-tasks"></span></span>
                                                <asp:Label ID="lblIncidencia" Text="" runat="server" />
                                            </h5>
                                        </div>

                                        <div class="input-group">
                                            <uc:ucSDTDetalleFajaIngreso runat="server" ID="ucSDTDetalleFajaIngresoRow"  />
                                        </div>

                                        <div class="input-group">
                                            <h5>
                                                <span><span class="glyphicon glyphicon-user"></span></span>
                                                <asp:Label ID="lblReseller" Text="" runat="server" />
                                            </h5>
                                        </div>

                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="SDT Nro" DataField="SolNro" ItemStyle-Width="100" />
                                <asp:BoundField HeaderText="Serial" DataField="Serial" ItemStyle-Width="100" />
                                <asp:BoundField HeaderText="Tipo de Hardware" DataField="ArtefactoTipo" ItemStyle-Width="100" />
                                <asp:TemplateField HeaderText="Diagnóstico" ItemStyle-Width="450">
                                    <ItemTemplate>

                                        <asp:GridView ID="gvDiagnosticos"
                                            CssClass="table table-bordered"
                                            Width="100%"
                                            AutoGenerateColumns="False"
                                            runat="server"
                                            EmptyDataText="Sin diagnóstico"
                                            Style="margin-top: 5px;">
                                            <Columns>
                                                <asp:BoundField HeaderText="Id" DataField="IdSDTDetalleDiagnostico" ItemStyle-Width="50" />
                                                <asp:BoundField HeaderText="Presupuesto" DataField="Presupuesto" ItemStyle-Width="50" />
                                                <asp:BoundField HeaderText="Descripción" DataField="Diagnostico" HtmlEncode="false" />
                                                <asp:TemplateField HeaderText="Aprobar" ItemStyle-Width="50">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="cbxAgregar" Checked="false" runat="server" Width="50" />
                                                    </ItemTemplate>
                                                    <ItemStyle Width="50px" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Gestionar" ItemStyle-Width="50">
                                    <ItemTemplate>
                                        <center>
                                        <asp:LinkButton ID="btnAprobarDiagnostico"
                                            runat="server"
                                            CssClass="btn btn-success"
                                            CommandName="Select"
                                            Style="margin-bottom: 5px;"
                                            ItemStyle-Width="50"
                                            title="Aprobar diagnóstico">
                                                <span aria-hidden="true" class="glyphicon glyphicon-saved"></span>
                                        </asp:LinkButton>

                                        <asp:LinkButton ID="LinkButton1"
                                            runat="server"
                                            CssClass="btn btn-danger"
                                            CommandName="Delete"
                                            Style="margin-bottom: 5px;"
                                            ItemStyle-Width="50"
                                            title="Cancelar diagnóstico">
                                                <span aria-hidden="true" class="glyphicon glyphicon-remove"></span>
                                        </asp:LinkButton>

                                        <asp:LinkButton ID="LinkButton2"
                                            runat="server"
                                            CssClass="btn btn-info"
                                            Style="margin-bottom: 5px;"
                                            ItemStyle-Width="50"
                                            title="Ver detalles del artefacto">
                                                <span aria-hidden="true" class="glyphicon glyphicon-search"></span>
                                        </asp:LinkButton>
                                    </center>
                                    </ItemTemplate>
                                    <ItemStyle Width="50px" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>

                </ContentTemplate>
            </asp:UpdatePanel>
        </div>

        <div id="menuAReparar" class="tab-pane fade-in">
            <br />
            <asp:UpdatePanel ID="upGestionAprobados" Visible="true" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                <ContentTemplate>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 5px;">
                        <label>
                            <asp:Label Text="Menú de gestion de artefactos con presupuesto aprobado" runat="server" />
                        </label>

                        <asp:GridView ID="gvGestionAprobados"
                            CssClass="table table-bordered table-hover col-lg-8 col-md-8 col-sm-8 col-xs-8 grid-view"
                            Width="100%"
                            AutoGenerateColumns="False"
                            runat="server"
                            EmptyDataText="No hay artefactos con diagnosticos aprobados disponibles para gestionar"
                            AutoPostBack="true"
                            OnSelectedIndexChanging="gvGestionAprobados_SelectedIndexChanging"
                            OnRowDeleting="gvGestionAprobados_RowDeleting">
                            <Columns>
                                <asp:BoundField HeaderText="#" DataField="IdSDTDetalle" ItemStyle-Width="75" />
                                <asp:TemplateField HeaderText="Descripción" ItemStyle-Width="250">
                                    <ItemTemplate>
                                        <div class="input-group">
                                            <h5>
                                                <span><span class="glyphicon glyphicon-pushpin"></span></span>
                                                <asp:Label ID="lblArtefacto"
                                                    Text=""
                                                    runat="server" />
                                            </h5>
                                        </div>

                                        <div class="input-group">
                                            <h5>
                                                <span><span class="glyphicon glyphicon-tasks"></span></span>
                                                <asp:Label
                                                    ID="lblIncidencia"
                                                    Text=""
                                                    runat="server" />
                                            </h5>
                                        </div>

                                        <div class="input-group">
                                            <uc:ucSDTDetalleFajaIngreso runat="server" ID="ucSDTDetalleFajaIngresoRow"  />
                                        </div>

                                        <div class="input-group">
                                            <h5>
                                                <span><span class="glyphicon glyphicon-user"></span></span>
                                                <asp:Label ID="lblReseller" Text="" runat="server" />
                                            </h5>
                                        </div>

                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="SDT Nro" DataField="SolNro" ItemStyle-Width="100" />
                                <asp:BoundField HeaderText="Serial" DataField="Serial" ItemStyle-Width="100" />
                                <asp:BoundField HeaderText="Tipo de Hardware" DataField="ArtefactoTipo" ItemStyle-Width="100" />
                                <asp:TemplateField HeaderText="Diagnósticos aprobados" ItemStyle-Width="450">
                                    <ItemTemplate>

                                        <asp:GridView ID="gvDiagnosticos"
                                            CssClass="table table-bordered"
                                            Width="100%"
                                            AutoGenerateColumns="False"
                                            runat="server"
                                            EmptyDataText="Sin diagnóstico"
                                            Style="margin-top: 10px;">
                                            <Columns>
                                                <asp:BoundField HeaderText="Id" DataField="IdSDTDetalleDiagnostico" ItemStyle-Width="50" />
                                                <asp:BoundField HeaderText="Presupuesto" DataField="Presupuesto" ItemStyle-Width="50" />
                                                <asp:BoundField HeaderText="Descripción" DataField="Diagnostico" HtmlEncode="false" />
                                            </Columns>
                                        </asp:GridView>
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Gestionar" ItemStyle-Width="50">
                                    <ItemTemplate>
                                        <center>

                                        <asp:LinkButton ID="btnEnviarAEnReparacion"
                                            runat="server"
                                            CssClass="btn btn-success"
                                            CommandName="Select"
                                            Style="margin-bottom: 5px;"
                                            ItemStyle-Width="50"
                                            title="Ingresar a estado 'En reparación'">
                                                <span class="glyphicon glyphicon-play-circle"></span>
                                        </asp:LinkButton>

                                        <asp:LinkButton ID="btnEnviarADiagnostico"
                                            runat="server"
                                            CssClass="btn btn-info"
                                            CommandName="Delete"
                                            Style="margin-bottom: 5px;"
                                            ItemStyle-Width="50"
                                            title="Volver a etapa de diagnóstico">
                                                <span aria-hidden="true" class="glyphicon glyphicon-repeat"></span>
                                        </asp:LinkButton>

                                    </center>

                                    </ItemTemplate>
                                    <ItemStyle Width="50px" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>

                    </div>


                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel ID="upGestionEnReparacion" Visible="true" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                <ContentTemplate>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 5px;">
                        <label>
                            <asp:Label Text="Menú para la gestión de artefactos en estado 'En reparación'" runat="server" />
                        </label>

                        <asp:GridView ID="gvGestionEnReparacion"
                            CssClass="table table-bordered table-hover col-lg-8 col-md-8 col-sm-8 col-xs-8 grid-view"
                            Width="100%"
                            AutoGenerateColumns="False"
                            runat="server"
                            EmptyDataText="No hay artefactos en estado 'En reparación' disponibles para gestionar"
                            AutoPostBack="true"
                            OnSelectedIndexChanging="gvGestionEnReparacion_SelectedIndexChanging"
                            OnRowDeleting="gvGestionEnReparacion_RowDeleting">
                            <Columns>
                                <asp:BoundField HeaderText="#" DataField="IdSDTDetalle" ItemStyle-Width="75" />
                                <asp:TemplateField HeaderText="Descripción" ItemStyle-Width="250">
                                    <ItemTemplate>
                                        <div class="input-group">
                                            <h5>
                                                <span><span class="glyphicon glyphicon-pushpin"></span></span>
                                                <asp:Label ID="lblArtefacto" Text="" runat="server" />
                                            </h5>
                                        </div>

                                        <div class="input-group">
                                            <h5>
                                                <span><span class="glyphicon glyphicon-tasks"></span></span>
                                                <asp:Label ID="lblIncidencia" Text="" runat="server" />
                                            </h5>
                                        </div>

                                        <div class="input-group">
                                            <uc:ucSDTDetalleFajaIngreso runat="server" ID="ucSDTDetalleFajaIngresoRow"  />
                                        </div>

                                        <div class="input-group">
                                            <h5>
                                                <span><span class="glyphicon glyphicon-user"></span></span>
                                                <asp:Label ID="lblReseller" Text="" runat="server" />
                                            </h5>
                                        </div>

                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="SDT Nro" DataField="SolNro" ItemStyle-Width="100" />
                                <asp:BoundField HeaderText="Serial" DataField="Serial" ItemStyle-Width="100" />
                                <asp:BoundField HeaderText="Tipo de Hardware" DataField="ArtefactoTipo" ItemStyle-Width="100" />
                                <asp:TemplateField HeaderText="Reparaciones a imputar" ItemStyle-Width="450">
                                    <ItemTemplate>

                                        <asp:GridView ID="gvDiagnosticos"
                                            CssClass="table table-bordered"
                                            Width="100%"
                                            AutoGenerateColumns="False"
                                            runat="server"
                                            EmptyDataText="Sin diagnóstico"
                                            Style="margin-top: 10px;">
                                            <Columns>
                                                <asp:BoundField HeaderText="Id" DataField="IdSDTDetalleDiagnostico" ItemStyle-Width="50" />
                                                <asp:BoundField HeaderText="Presupuesto" DataField="Presupuesto" ItemStyle-Width="50" />
                                                <asp:BoundField HeaderText="Descripción" DataField="Diagnostico" HtmlEncode="false" ItemStyle-Width="450" />
                                            </Columns>
                                        </asp:GridView>
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Gestionar" ItemStyle-Width="50">
                                    <ItemTemplate>
                                        <center>
                                        <asp:LinkButton ID="btnEquipoReparado"
                                            runat="server"
                                            CssClass="btn btn-success"
                                            CommandName="Select"
                                            Style="margin-bottom: 5px;"
                                            ItemStyle-Width="50"
                                            title="Marcar equipo como 'Reparado'">
                                                <span aria-hidden="true" class="glyphicon glyphicon-flag"></span>
                                        </asp:LinkButton>

                                        <asp:LinkButton ID="btnEnviarADiagnostico"
                                            runat="server"
                                            CssClass="btn btn-info"
                                            CommandName="Delete"
                                            Style="margin-bottom: 5px;"
                                            ItemStyle-Width="50"
                                            title="Volver a etapa de diagnóstico">
                                                <span aria-hidden="true" class="glyphicon glyphicon-repeat"></span>
                                        </asp:LinkButton>
                                    </center>
                                    </ItemTemplate>
                                    <ItemStyle Width="50px" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>

                    </div>


                </ContentTemplate>
            </asp:UpdatePanel>
        </div>

        <div id="menuARemitar" class="tab-pane fade-in">
            <br />
            <asp:UpdatePanel ID="upGestionReparados" Visible="true" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                <ContentTemplate>
                    <center>
                        <asp:Button ID="btnIrARemitar"
                        Text="Ir a remitar"
                        CssClass="btn btn-success col-lg-6 col-md-6 col-sm-6 col-xs-6"
                        runat="server"
                        OnClick="btnIrARemitar_Click"
                        Style="margin-bottom: 10px;margin-left: 10px;" />
                    </center>
                    <div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <asp:GridView ID="gvGestionReparados"
                            CssClass="table table-bordered table-hover col-lg-12 col-md-12 col-sm-12 col-xs-12 grid-view"
                            Width="100%"
                            AutoGenerateColumns="False"
                            runat="server"
                            EmptyDataText="No hay artefactos en estado 'Reparado' disponibles para gestionar"
                            AutoPostBack="true"
                            Visible="False"
                            >
                            <Columns>
                                <asp:BoundField HeaderText="#" DataField="IdSDTDetalle" ItemStyle-Width="75" />
                                <asp:TemplateField HeaderText="Descripción" ItemStyle-Width="250">
                                    <ItemTemplate>
                                        <div class="input-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <h5>
                                                <span><span class="glyphicon glyphicon-pushpin"></span></span>
                                                <asp:Label ID="lblArtefacto" Text="" runat="server" />
                                            </h5>
                                        </div>

                                        <div class="input-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <h5>
                                                <span><span class="glyphicon glyphicon-tasks"></span></span>
                                                <asp:Label ID="lblIncidencia" Text="" runat="server" />
                                            </h5>
                                        </div>

                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="SDT Nro" DataField="SolNro" ItemStyle-Width="100" />
                                <asp:BoundField HeaderText="Serial" DataField="Serial" ItemStyle-Width="100" />
                                <asp:BoundField HeaderText="Tipo de Hardware" DataField="ArtefactoTipo" ItemStyle-Width="100" />
                                <asp:BoundField HeaderText="Subtotal sin IVA" DataField="Precio" ItemStyle-Width="100" />
                                <asp:TemplateField HeaderText="Remitar" ItemStyle-Width="50">
                                    <ItemTemplate>
                                        <center>
                                            <asp:CheckBox
                                            ID="cbxEnviarADevolucion"
                                            Enabled="true"
                                            runat="server" Width="50"
                                            DataField="EnDevolucion" 
                                            />
                                        </center>
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>

                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>
