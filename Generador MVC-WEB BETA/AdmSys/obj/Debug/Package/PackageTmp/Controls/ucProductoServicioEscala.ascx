﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucProductoServicioEscala.ascx.cs" Inherits="AdmSys.Controls.ucProductoServicioEscala" %>

<asp:UpdatePanel ID="upProductoServicioEscala" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
	<ContentTemplate>
        <asp:LinkButton ID="btnAgregarEscala"
            runat="server"
            CssClass="btn btn-success pull-left"
            Style="margin-bottom: 5px;"
            title="Agregar escala"
            OnClick="btnAgregarEscala_Click"
            >
            <span aria-hidden="true" class="glyphicon glyphicon-plus"></span>
        </asp:LinkButton> <asp:Label id="lblIdListaPrecio" Visible="false" Text="" runat="server" />
        
        <asp:GridView ID="gvProductoServicioEscala" 
            CssClass="table table-striped table-bordered table-hover" 
            data-model="ProductoServicioCarga.aspx"
            Width="100%" 
            AutoGenerateColumns="False" 
            runat="server" 
            EmptyDataText="Sin escalas."
            OnRowDeleting="gvProductoServicioEscala_RowDeleting"
            >
            <Columns>
                <asp:BoundField HeaderText="Id" DataField="IdEscala" ItemStyle-Width="1" />
                    
                <asp:TemplateField HeaderText="Cantidad mínima" ItemStyle-Width="75">
                    <ItemTemplate>
                        <asp:TextBox 
                            ID="txtCantidadMinima" 
                            runat="server" 
                            CssClass="form-control numeric-integer-positive" 
                            Width="75" 
                            />
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Cantidad máxima" ItemStyle-Width="75">
                    <ItemTemplate>
                        <asp:TextBox 
                            ID="txtCantidadMaxima" 
                            runat="server" 
                            CssClass="form-control numeric-integer-positive" 
                            Width="75" 
                            />
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Precio de lista" ItemStyle-Width="75">
                    <ItemTemplate>
                        <asp:TextBox 
                            ID="txtPrecioFijoLista" 
                            runat="server" 
                            CssClass="form-control numeric-decimal-positive" 
                            Width="75" 
                            PlaceHolder="$"
                            />
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Precio de contado" ItemStyle-Width="75">
                    <ItemTemplate>
                        <asp:TextBox 
                            ID="txtPrecioFijoContado" 
                            runat="server" 
                            CssClass="form-control numeric-decimal-positive" 
                            Width="75" 
                            PlaceHolder="$"
                            />
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Porcentaje" ItemStyle-Width="60">
                    <ItemTemplate>
                        <asp:TextBox 
                            ID="txtPrecioPorcentualPorcentaje" 
                            runat="server" 
                            CssClass="form-control numeric-integer-positive" 
                            Width="60" 
                            PlaceHolder="%"
                            />
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Cobro mínimo" ItemStyle-Width="75">
                    <ItemTemplate>
                        <asp:TextBox 
                            ID="txtPrecioPorcentualCobroMinimo" 
                            runat="server" 
                            CssClass="form-control numeric-decimal-positive" 
                            Width="75" 
                            PlaceHolder="$"
                            />
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Quitar" ItemStyle-Width="50">
                    <ItemTemplate>
                        <asp:LinkButton ID="btnQuitarEscala"
                            runat="server"
                            CssClass="btn btn-danger"
                            CommandName="Delete"
                            >
                        <span aria-hidden="true" class="glyphicon glyphicon-trash"></span>
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>

            </Columns>
        </asp:GridView>

        <asp:GridView ID="gvProductoServicioEscalasFijasMuestra" 
            CssClass="table table-striped table-bordered table-hover" 
            data-model="ProductoServicioCarga.aspx"
            Width="100%" 
            AutoGenerateColumns="False" 
            runat="server" 
            EmptyDataText="Sin escalas."
            Visible="false"
            Enabled="false"
            >
            <Columns>
                    
                <asp:TemplateField HeaderText="Cantidad mínima" ItemStyle-Width="75">
                    <ItemTemplate>
                        <center>
                            <asp:Label
                            ID="lblCantidadMinima" 
                            runat="server"
                            Width="75" 
                            />
                        </center>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Cantidad máxima" ItemStyle-Width="75">
                    <ItemTemplate>
                        <center>
                            <asp:Label
                            ID="lblCantidadMaxima" 
                            runat="server"
                            Width="75" 
                            /> 
                        </center>                      
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Precio de lista" ItemStyle-Width="75">
                    <ItemTemplate>
                        <center>
                                <asp:Label
                                ID="lblPrecioFijoLista" 
                                runat="server"
                                Width="75" 
                                />
                        </center>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Precio de contado" ItemStyle-Width="75">
                    <ItemTemplate>
                        <center>
                            <asp:Label
                            ID="lblPrecioFijoContado" 
                            runat="server"
                            Width="75" 
                            />
                        </center>
                    </ItemTemplate>
                </asp:TemplateField>

            </Columns>
        </asp:GridView>

        <asp:GridView ID="gvProductoServicioEscalasPorcentualMuestra" 
            CssClass="table table-striped table-bordered table-hover" 
            data-model="ProductoServicioCarga.aspx"
            Width="100%" 
            AutoGenerateColumns="False" 
            runat="server" 
            EmptyDataText="Sin escalas."
            Visible="false"
            Enabled="false"
            >
            <Columns>
                    
                <asp:TemplateField HeaderText="Cantidad mínima" ItemStyle-Width="75">
                    <ItemTemplate>
                        <asp:TextBox 
                            ID="txtCantidadMinima" 
                            runat="server" 
                            CssClass="form-control numeric-integer-positive" 
                            Width="75" 
                            />
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Cantidad máxima" ItemStyle-Width="75">
                    <ItemTemplate>
                        <asp:TextBox 
                            ID="txtCantidadMaxima" 
                            runat="server" 
                            CssClass="form-control numeric-integer-positive" 
                            Width="75" 
                            />
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Porcentaje" ItemStyle-Width="60">
                    <ItemTemplate>
                        <asp:TextBox 
                            ID="txtPrecioPorcentualPorcentaje" 
                            runat="server" 
                            CssClass="form-control numeric-integer-positive" 
                            Width="60" 
                            PlaceHolder="%"
                            />
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Cobro mínimo" ItemStyle-Width="75">
                    <ItemTemplate>
                        <asp:TextBox 
                            ID="txtPrecioPorcentualCobroMinimo" 
                            runat="server" 
                            CssClass="form-control numeric-integer-positive" 
                            Width="75" 
                            PlaceHolder="$"
                            />
                    </ItemTemplate>
                </asp:TemplateField>

            </Columns>
        </asp:GridView>
	</ContentTemplate>
</asp:UpdatePanel>
