﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucFiltro_DistribuidorUnidadNegocioReseller.ascx.cs" Inherits="AdmSys.Controls.ucFiltro_DistribuidorUnidadNegocioReseller" %>

<asp:UpdatePanel ID="upCombosDistribuidorUnidadNegocioReseller" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
	<ContentTemplate>
        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
            <label>
                <asp:Label Text="Distribuidor" runat="server" />
            </label>
            <asp:DropDownList ID="ddlDistribuidor" CssClass="form-control select-single" AutoPostBack="true" OnSelectedIndexChanged="ddlDistribuidor_OnSelectedIndexChanged" runat="server" />
        </div>

        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
            <label>
                <asp:Label Text="Unidad Negocio" runat="server" />
            </label>

            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <asp:DropDownList ID="ddlUnidadNegocio" CssClass="form-control select-single" runat="server" 
                        AutoPostBack="true" 
                        OnSelectedIndexChanged="ddlUnidadNegocio_OnSelectedIndexChanged" />
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlDistribuidor" />
                </Triggers>
            </asp:UpdatePanel>
        </div>

        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
            <label>
                <asp:Label Text="Reseller" runat="server" />
            </label>
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <asp:DropDownList ID="ddlReseller" CssClass="form-control select-single" runat="server" OnSelectedIndexChanged="ddlReseller_SelectedIndexChanged" AutoPostBack="True"/>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlUnidadNegocio" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
	</ContentTemplate>
</asp:UpdatePanel>