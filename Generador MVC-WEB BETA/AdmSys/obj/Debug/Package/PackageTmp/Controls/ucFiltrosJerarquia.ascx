﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucFiltrosJerarquia.ascx.cs" Inherits="AdmSys.Controls.ucFiltrosJerarquia" %>

<%-- Unidad de Negocio --%>
<div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
    <label>
        <asp:Label Text="Unidad de Negocio" runat="server" />
    </label>
    <asp:DropDownList ID="ddlUnidadNegocio" CssClass="form-control select-single" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlUnidadNegocio_OnSelectedIndexChanged" />
</div>

<%-- Distribuidor (Access) --%>
<asp:UpdatePanel runat="server">
    <ContentTemplate>

        <asp:Panel ID="pnlDistribuidor" CssClass="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12" runat="server">
            <label>
                <asp:Label Text="Distribuidor" runat="server" />
            </label>
            <asp:DropDownList ID="ddlDistribuidor" CssClass="form-control select-single" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlDistribuidor_OnSelectedIndexChanged" />
        </asp:Panel>

    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="ddlUnidadNegocio" />
    </Triggers>
</asp:UpdatePanel>

<%-- Reseller (Vending & Access) --%>
<asp:UpdatePanel runat="server">
    <ContentTemplate>

        <asp:Panel ID="pnlReseller" CssClass="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12" runat="server">
            <label>
                <asp:Label Text="Reseller" runat="server" />
            </label>
            <asp:DropDownList ID="ddlReseller" CssClass="form-control select-single" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlReseller_OnSelectedIndexChanged" />
        </asp:Panel>

    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="ddlUnidadNegocio" />
    </Triggers>
</asp:UpdatePanel>

<%-- Contratista (Vending & Access) --%>
<asp:UpdatePanel runat="server">
    <ContentTemplate>

        <asp:Panel ID="pnlContratista" CssClass="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12" runat="server">
            <label>
                <asp:Label Text="Contratista" runat="server" />
            </label>
            <asp:DropDownList ID="ddlContratista" CssClass="form-control select-single" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlContratista_OnSelectedIndexChanged" />
        </asp:Panel>

    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="ddlUnidadNegocio" />
    </Triggers>
</asp:UpdatePanel>

<%-- Consumidor (Vending) --%>
<asp:UpdatePanel runat="server">
    <ContentTemplate>

        <asp:Panel ID="pnlConsumidor" CssClass="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12" runat="server">
            <label>
                <asp:Label Text="Consumidor" runat="server" />
            </label>
            <select id="selConsumidor" class="form-control"></select>
            <asp:HiddenField ID="hdnIdConsumidor" runat="server" />
        </asp:Panel>

    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="ddlUnidadNegocio" />
    </Triggers>
</asp:UpdatePanel>

<%-- Agente (Access) --%>
<asp:UpdatePanel runat="server">
    <ContentTemplate>

        <asp:Panel ID="pnlAgente" CssClass="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12" runat="server">
            <label>
                <asp:Label Text="Agente" runat="server" />
            </label>
            <asp:DropDownList ID="ddlAgente" CssClass="form-control select-single" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlAgente_OnSelectedIndexChanged" />
        </asp:Panel>

    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="ddlUnidadNegocio" />
    </Triggers>
</asp:UpdatePanel>

<%-- Llave (Access) --%>
<asp:UpdatePanel runat="server">
    <ContentTemplate>

        <asp:Panel ID="pnlLlave" CssClass="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12" runat="server">
            <label>
                <asp:Label Text="Llave" runat="server" />
            </label>
            <select id="selLlave" class="form-control"></select>
            <asp:HiddenField ID="hdnIdLlave" runat="server" />
        </asp:Panel>

    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="ddlUnidadNegocio" />
    </Triggers>
</asp:UpdatePanel>

<script>

    function SelectAjaxConsumidor() {

        $('#selConsumidor').select2({
            theme: "bootstrap",
            minimumInputLength: 3,
            ajax: {
                url: '../Handlers/FiltrosJerarquiaConsumidor.ashx?idContratista=' + $('#cphBody_ucFiltrosJerarquia_ddlContratista').val(),
                dataType: 'json',
                delay: 250,
                placeholder: 'Buscando...',
                processResults: function (data) {
                    return {
                        results: data
                    };
                }
            }
        });
        $('#selConsumidor').on('change', function () {
            $("#cphBody_ucFiltrosJerarquia_hdnIdConsumidor").val(this.value);
        });
    }

    function SelectAjaxLlave() {

        $('#selLlave').select2({
            theme: "bootstrap",
            minimumInputLength: 3,
            ajax: {
                url: '../Handlers/FiltrosJerarquiaLlave.ashx?idAgente=' + $('#cphBody_ucFiltrosJerarquia_ddlAgente').val(),
                dataType: 'json',
                delay: 250,
                placeholder: 'Buscando...',
                processResults: function (data) {
                    return {
                        results: data
                    };
                }
            }
        });
        $('#selLlave').on('change', function () {
            $("#cphBody_ucFiltrosJerarquia_hdnIdLlave").val(this.value);
        });
    }

</script>