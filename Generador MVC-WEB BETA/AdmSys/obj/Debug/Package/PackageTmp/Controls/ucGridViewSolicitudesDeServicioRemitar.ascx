﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucGridViewSolicitudesDeServicioRemitar.ascx.cs" Inherits="AdmSys.Controls.ucGridViewSolicitudesDeServicioRemitar" %>
<asp:UpdatePanel ID="upSolicitudesServicio" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
    <ContentTemplate>
        <label>
            <asp:Label Text="Solicitudes de servicios" runat="server" />
        </label>
        <asp:GridView ID="gvSolicitudesDeServicios"
            CssClass="table table-striped table-bordered table-hover"
            data-model="SolicitudDeServicio.aspx"
            Width="100%" AutoGenerateColumns="False" runat="server"
            EmptyDataText="Sin solicitudes de servicio compatibles con los filtros ingresados"
            OnSelectedIndexChanging="gvSolicitudesDeServicios_SelectedIndexChanging"
            >
            <Columns>
                <%-- Requeridas para las GridViews clickeables - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                <%-- Responsive + - --%>
                <%-- cryptoID --%>
                <asp:BoundField HeaderText="#" DataField="IdSDS" HeaderStyle-CssClass="crypto-id-column" ItemStyle-CssClass="crypto-id-column" />
                <%-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                <asp:BoundField HeaderText="Id Distribuidor Unidad Negocio" DataField="IdDistribuidorUnidadNegocio" />
                <asp:BoundField HeaderText="SDS Nro" DataField="SDSNro" />
                <asp:BoundField HeaderText="Mail informe" DataField="MailInforme" />

                <%--Ocultar identificadores por el momento--%>
                <asp:BoundField HeaderText="Id Plan Comercial" Visible="false" DataField="IdPlanComercial" />
                <asp:BoundField HeaderText="Id Reseller" Visible="false" DataField="IdReseller" />
                <asp:BoundField HeaderText="Id Imagen" Visible="false" DataField="IdImagenSDS" />

                <asp:BoundField HeaderText="Fecha de Alta" DataField="FHAlta" ItemStyle-CssClass="date-time" />
                <asp:BoundField HeaderText="Obs Alta" DataField="ObservacionAlta" />

                <asp:BoundField HeaderText="Fecha de Aceptación" DataField="FHAceptacion" ItemStyle-CssClass="date-time" />
                <asp:BoundField HeaderText="Obs Aceptación" DataField="ObservacionAceptacion" />

                <asp:TemplateField HeaderText="Remitar" ItemStyle-Width="50">
                    <ItemTemplate>
                        <asp:CheckBox ID="cbxRemitar" runat="server" Width="50" />
                    </ItemTemplate>
                    <ItemStyle Width="50px" />
                </asp:TemplateField>


                <asp:TemplateField HeaderText="Ver solicitud" ItemStyle-Width="50">
                    <ItemTemplate>
                        <asp:LinkButton ID="btnVerSolicitud"
                            runat="server"
                            CssClass="btn btn-info"
                            CommandName="Select"
                            title="Ver detalle de la solicitud"
                            Style="margin-left: 5px;">
                                <span aria-hidden="true" class="glyphicon glyphicon-search"></span>
                        </asp:LinkButton>
                    </ItemTemplate>
                    <ItemStyle Width="50px" />
                </asp:TemplateField>

            </Columns>
        </asp:GridView>
    </ContentTemplate>
</asp:UpdatePanel>