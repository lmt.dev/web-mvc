﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucProductoServicioCarga.ascx.cs" Inherits="AdmSys.Controls.ucProductoServicioCarga" %>

<asp:UpdatePanel ID="upProductoServicioCarga" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
    <ContentTemplate>

        <div class="row">
            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <label>
                    <asp:Label Text="Unidad Negocio" runat="server" />
                </label>

                <asp:DropDownList 
                    ID="ddlUnidadNegocio" 
                    CssClass="form-control select-single" 
                    runat="server">
                </asp:DropDownList>
            </div>
            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <label>
                    <asp:Label Text="Tipo" runat="server" CssClass="tituloChk" />
                </label>
                <br />
                <label>
                    <asp:Label Text="Servicio" runat="server" 
                        CssClass="opcionChk" />
                    <asp:CheckBox ID="chkProducto" 
                        runat="server" 
                        AutoPostBack="true" 
                        OnCheckedChanged="chkProducto_CheckedChanged" />
                    <asp:Label Text="Producto" runat="server" CssClass="opcionChk" />
                </label>
            </div>
            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <label>
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:Label ID="lblServicioTipo" Text="Tipo de Servicio" runat="server" />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="chkProducto" />
                        </Triggers>
                    </asp:UpdatePanel>
                </label>
                            
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <asp:DropDownList 
                            ID="ddlServicioTipo" 
                            CssClass="form-control select-single" 
                            ToolTip="Sirve para agrupar servicios y/o productos para leerlos mejor en un comprobante, por ejemplo." 
                            runat="server"
                            AutoPostBack="true"
                            OnSelectedIndexChanged="ddlServicioTipo_OnSelectedIndexChanged"
                            >
                        </asp:DropDownList>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="chkProducto" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>

        <div class="row">

            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
	            <asp:UpdatePanel ID="upModeloProducto" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
		            <ContentTemplate>
                        <label>
                            <asp:Label Text="Modelo" runat="server" Enabled="false" />
                        </label>

                        <asp:DropDownList 
                            ID="ddlModeloProducto" 
                            CssClass="form-control select-single"
                            Enabled="false"
                            runat="server">
                        </asp:DropDownList>
		            </ContentTemplate>
	            </asp:UpdatePanel>
            </div>

            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <label>
                    <asp:Label Text="Código de producto" runat="server" />
                </label>
                <asp:TextBox ID="txtProductoCodigo"
                CssClass="form-control"
                runat="server"
                TextMode="SingleLine" />
            </div>

            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <label>
                    <asp:Label Text="Descripción" runat="server" />
                </label>
                <asp:TextBox ID="txtProductoDescripcion"
                CssClass="form-control"
                runat="server"
                TextMode="SingleLine" />
            </div>

        </div>

        <div class="row">

            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <label>
                    <asp:Label Text="Observaciones" runat="server" />
                </label>
                <asp:TextBox ID="txtProductoObservaciones"
                CssClass="form-control"
                runat="server"
                TextMode="SingleLine" />
            </div>

            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <label>
                    <asp:Label Text="Imagen" runat="server" />
                </label>
                <asp:FileUpload ID="fileImagen" runat="server" CssClass="form-control file" data-show-preview="true" />
            </div>

        </div>

    </ContentTemplate>
</asp:UpdatePanel>