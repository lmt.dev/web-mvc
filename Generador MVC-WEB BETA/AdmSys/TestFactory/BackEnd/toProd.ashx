﻿<%@ WebHandler Language="C#" Class="toProd" %>

using System;
using System.Collections.Generic;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

public class toProd : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        string UMID = context.Request["UMID"];

        string connectionString = ConfigurationManager.ConnectionStrings["TestFactory"].ConnectionString;
        using (SqlConnection connection = new SqlConnection(connectionString))
        {
            string tsql = @"UPDATE UMs 
                            SET FechaHoraProduccion = GETDATE() 
                            WHERE IdDispositivo = '" + UMID + "'";
            //using (SqlCommand command = new SqlCommand(tsql, connection))
            //{
            //    connection.Open();
            //    command.ExecuteNonQuery();
            //}
        }
        context.Response.ContentType = "application/json";
        context.Response.Write("{'Result':'OK'}".Replace("'", "\""));
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}