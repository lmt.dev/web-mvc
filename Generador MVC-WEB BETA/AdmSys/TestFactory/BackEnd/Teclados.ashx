﻿<%@ WebHandler Language="C#" Class="Teclados" %>

using System;
using System.Collections.Generic;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

public class Teclados : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        string j = "";
        string connectionString = ConfigurationManager.ConnectionStrings["TestFactory"].ConnectionString;
        using (SqlConnection connection = new SqlConnection(connectionString))
        {
            string tsql = @"SELECT IdDispositivo ID
                                  ,VersionHardware Hard
                                  ,IdFirmware Soft
                                  ,CONVERT(VARCHAR, FechaHoraAlta,120) Reportado
                            FROM [TestFactory].[dbo].[Teclados]";
            using (SqlCommand command = new SqlCommand(tsql, connection))
            {
                command.CommandType = CommandType.Text;
                connection.Open();
                SqlDataReader dataReader = command.ExecuteReader();
                DataTable dataTable = new DataTable();
                dataTable.Load(dataReader);
                j = AdmSys.TestFactory.BackEnd.json.DataTable2JSON(dataTable);
            }
        }
        context.Response.ContentType = "application/json";
        context.Response.Write(j);
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
    
}