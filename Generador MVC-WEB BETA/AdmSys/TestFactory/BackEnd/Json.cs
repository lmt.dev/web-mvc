﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Script.Serialization;

namespace AdmSys.TestFactory.BackEnd
{
    public class json
    {
        public static string DataTable2JSON(DataTable dt)
        {
            List<Object> RowList = new List<Object>();
            foreach (DataRow dr in dt.Rows)
            {
                Dictionary<Object, Object> ColList = new Dictionary<Object, Object>();
                foreach (DataColumn dc in dt.Columns)
                {
                    string t = (string)((string.Empty == dr[dc].ToString()) ? null : dr[dc].ToString());
                    ColList.Add(dc.ColumnName, t);
                }
                RowList.Add(ColList);
            }
            JavaScriptSerializer js = new JavaScriptSerializer();
            string JSON = js.Serialize(RowList);
            return JSON;
        }
    }
}