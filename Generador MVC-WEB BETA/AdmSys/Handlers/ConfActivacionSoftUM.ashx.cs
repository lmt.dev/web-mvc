﻿using MagicSQL;
using System.Data;
using System.Web;

namespace AdmSys.Handlers
{
    /// <summary>
    /// Descripción breve de ConfActivacionSoftUM
    /// </summary>
    public class ConfActivacionSoftUM : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            try
            {
                // Serial de la UM
                string serialUM = context.Request.QueryString["serialUM"];
                string confirmActivacionUM = "";

                DataTable dt;
                using (SP sp = new SP("CGADM"))
                {
                    // ¿La UM tiene un soft pendiente de envio o activacion?
                    dt = sp.Execute("usp_GetSoftUmPendiente", P.Add("SerialUM", serialUM));

                    // Se encontro algun registro?
                    if (dt.Rows.Count == 0)
                    {
                        confirmActivacionUM = "Ok";
                    }
                }

                // Respuesta en formato json
                context.Response.ContentType = "application/json";
                string response = new
                {
                    Result = confirmActivacionUM
                }.ToJson();
                context.Response.Write(response);
            }
            catch { }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}