﻿using MagicSQL;
using System.Web;

namespace AdmSys.Handlers
{
    public class PercentEnvioSoftDisp : IHttpHandler
    {
        // *** Dispositivo ***

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                // Serial de la UM
                string serialUM = context.Request.QueryString["serialUM"];

                // Ejecutal el SP que devuelve el porcentaje de envio del soft al/los Dispositivos
                int percentage = 0;
                using (SP sp = new SP("CGADM"))
                {
                    percentage = sp.Execute("usp_GetPercentEnvioSoftDisp", P.Add("serialUM", serialUM)).Rows[0][0].ToString().ToInt();
                }
                if (percentage > 100) percentage = 100;

                // Respuesta en formato json
                context.Response.ContentType = "application/json";
                string response = new
                {
                    Percentage = percentage
                }.ToJson();
                context.Response.Write(response);
            }
            catch { }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}