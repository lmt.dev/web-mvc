﻿using MagicSQL;
using System.Data;
using System.Web;

namespace AdmSys.Handlers
{
    /// <summary>
    /// Descripción breve de ServicesStatus
    /// </summary>
    public class ServicesStatus : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            DataTable dt;
            using (SP sp = new SP("CGADM"))
            {
                dt = sp.Execute("usp_GetServicesStatus");
            }

            // Estado de todos los servicio:
            // 0: Todos Down
            // 1: Alguno/s Down otro/s Up
            // 2: Todos Up
            int status = -1;
            for (int i = 1; i < dt.Columns.Count; i++)
            {
                if ((bool)dt.Rows[0][i - 1] != (bool)dt.Rows[0][i])
                {
                    status = 1;
                    break;
                }
            }
            if (status == -1)
            {
                if ((bool)dt.Rows[0][0])
                {
                    status = 2;
                }
                else
                {
                    status = 0;
                }
            }

            context.Response.ContentType = "application/json";
            string response = new
            {
                Status = status,
                Liquidaciones = -1,
                PrincipalCevallos = (bool)dt.Rows[0]["PrincipalCevallos"] ? 1 : 0,
                PrincipalRingo = (bool)dt.Rows[0]["PrincipalRingo"] ? 1 : 0,
                ConfiguracionUM = (bool)dt.Rows[0]["ConfiguracionUM"] ? 1 : 0,
                VendingControl = (bool)dt.Rows[0]["VendingControl"] ? 1 : 0,
                RenovacionesVending = -1,
                CashVend = (bool)dt.Rows[0]["CashVend"] ? 1 : 0,
                AccessAsigns = (bool)dt.Rows[0]["AccessAsigns"] ? 1 : 0,
                AccessBusiness = (bool)dt.Rows[0]["AccessBusiness"] ? 1 : 0
            }.ToJson();
            context.Response.Write(response);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}