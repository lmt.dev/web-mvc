﻿//------------------------------------------------------------------------------
// <generado automáticamente>
//     Este código fue generado por una herramienta.
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código. 
// </generado automáticamente>
//------------------------------------------------------------------------------



public partial class Login {
    
    /// <summary>
    /// Control body.
    /// </summary>
    /// <remarks>
    /// Campo generado automáticamente.
    /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    /// </remarks>
    protected global::System.Web.UI.HtmlControls.HtmlGenericControl body;
    
    /// <summary>
    /// Control lblApp.
    /// </summary>
    /// <remarks>
    /// Campo generado automáticamente.
    /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Label lblApp;
    
    /// <summary>
    /// Control pnlLoginError.
    /// </summary>
    /// <remarks>
    /// Campo generado automáticamente.
    /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Panel pnlLoginError;
    
    /// <summary>
    /// Control lblError.
    /// </summary>
    /// <remarks>
    /// Campo generado automáticamente.
    /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Label lblError;
    
    /// <summary>
    /// Control txtUsername.
    /// </summary>
    /// <remarks>
    /// Campo generado automáticamente.
    /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    /// </remarks>
    protected global::System.Web.UI.WebControls.TextBox txtUsername;
    
    /// <summary>
    /// Control txtPassword.
    /// </summary>
    /// <remarks>
    /// Campo generado automáticamente.
    /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    /// </remarks>
    protected global::System.Web.UI.WebControls.TextBox txtPassword;
    
    /// <summary>
    /// Control btnLogin.
    /// </summary>
    /// <remarks>
    /// Campo generado automáticamente.
    /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Button btnLogin;
    
    /// <summary>
    /// Control app.
    /// </summary>
    /// <remarks>
    /// Campo generado automáticamente.
    /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    /// </remarks>
    protected global::System.Web.UI.WebControls.HiddenField app;
    
    /// <summary>
    /// Control appId.
    /// </summary>
    /// <remarks>
    /// Campo generado automáticamente.
    /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    /// </remarks>
    protected global::System.Web.UI.WebControls.HiddenField appId;
    
    /// <summary>
    /// Control lblVersion.
    /// </summary>
    /// <remarks>
    /// Campo generado automáticamente.
    /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Label lblVersion;
}
