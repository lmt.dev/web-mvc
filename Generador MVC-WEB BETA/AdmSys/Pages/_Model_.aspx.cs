﻿using AdmSys.Controls;
using Code;

using System;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;
using static Code.Utils;

namespace Pages
{
    public partial class _Model_ : BasePage
    {
        // Modelo que se utilizará en la página
        private CGADM.Model _model_ = new CGADM.Model();

        // Id del Modelo
        private int id_Model_ = 0;

        protected void UserControl_btnSaveClick(object sender, EventArgs e)
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // Id del Modelo recuperado del Request
            if (Request["cryptoID"] != null)
            {
                try
                {
                    id_Model_ = Request["cryptoID"].ToIntID();
                }
                catch (Exception)
                {
                    NavigateTo("~/Content/html/Alert.html");
                    return;
                }
            }

            if (!IsPostBack)
            {
                // Establece Título del Formulario = Título de la Página
                lblTitulo.Text = Title;

                if (id_Model_ == 0)
                { // Nuevo Modelo
                    // Deshabilita los botones Editar y Eliminar
                    btnEdit.Enabled = false;
                    btnDelete.Enabled = false;

                    // Habilita el boton Guardar
                    btnSave.Enabled = true;
                }
                else
                { // Ver Modelo
                    // Recupera el Modelo a partir del Id encriptado del Modelo
                    _model_.Select(id_Model_);

                    // Genera el Hash del Modelo y lo guarda en el control hdnHash
                    string jsonModel = _model_.ToJson();
                    hdnHash.Value = ModelHash(jsonModel);

                    // Establece los valores de los controles desde las propiedades del Modelo
                    SetControls();
                }

                // Habilita o Deshabilita los controles según el estado del btnSave
                EnableControls();

                // A modo de ejemplo se cargan los selects single y multiple
                ddlSelect.Items.Clear();
                ddlSelect.Items.Add(new ListItem("Seleccione 1", "-1"));
                foreach (CGADM.Model model in new CGADM.Model().Select().Where(m => m.FHBaja == null))
                {
                    ddlSelect.Items.Add(new ListItem(model.Nombre, model.IdModel.ToCryptoID()));
                }

                lstMultiple.Items.Clear();
                foreach (CGADM.Model model in new CGADM.Model().Select().Where(m => m.FHBaja == null))
                {
                    lstMultiple.Items.Add(new ListItem(model.Nombre, model.IdModel.ToCryptoID()));
                }

                // A modo de ejemplo se carga la GridView checkeable con la lista de Modelos
                gv_Models_.DataSource = _model_.Select();
                gv_Models_.DataBind();
            }
            else
            {
                // A modo de ejemplo se recuperan los items seleccionado en el select-multiple
                string x = "";
                foreach (ListItem li in lstMultiple.Items)
                {
                    if (li.Selected == true)
                    {
                        x = x + li.Value + ", ";
                    }
                }
            }
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            // Deshabilita el botón Editar
            btnEdit.Enabled = false;

            // Deshabilita el botón Eliminar
            btnDelete.Enabled = false;

            // Habilita el botón Guardar
            btnSave.Enabled = true;

            // Habilita o Deshabilita los controles según el estado del btnSave
            EnableControls();
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            // Recupera el Modelo a partir del Id encriptado
            _model_.Select(id_Model_);

            // Genera el Hash del Modelo de la DB y lo compara con el guardado en el control hdnHash
            string jsonModel = _model_.ToJson();
            if (hdnHash.Value.Equals(ModelHash(jsonModel)))
            {// El Modelo está actualizado
                // Actualiza la Fecha Hora Baja
                _model_.FHBaja = DateTime.Now;

                // Actualiza el Modelo
                _model_.Update();

                // Confirma al usuario
                Message("_Model_ dado de baja", "El _Model_ ha sido dado de baja exitosamente.", MessagesTypes.success, "_Models_.aspx");
            }
            else
            {// El Modelo está desactualizado
             // Notifica que el Modelo está desactualizado en la DB
                Message("_Model_ Desactualizado", "El _Model_ cambió en la DB desde la última vez que se lo recuperó.", MessagesTypes.warning, Request.Url.ToString());
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            // A modo de ejemplo se recuperan los Ids de las filas seleccionadas
            foreach (GridViewRow row in gv_Models_.Rows)
            {
                CheckBox selected = (CheckBox)row.FindControl("chkSelected");
                if (selected.Checked)
                {
                    string cryptoId = row.Cells[0].Text;
                    string plainID = cryptoId.ToIntID().ToString();

                    string nombre = row.Cells[2].Text;
                }
            }

            if (id_Model_ == 0)
            {
                // Establece los valores de las propiedades del Modelo desde los controles
                SetProperties();

                // Inserta el Modelo
                _model_.Insert();

                // Confirma al usuario
                Message("_Model_ creado", "El _Model_ ha sido creado exitosamente.", MessagesTypes.success, "_Models_.aspx");
            }
            else
            {
                // Recupera el Modelo a partir del Id encriptado
                _model_.Select(id_Model_);

                // Genera el Hash del Modelo de la DB y lo compara con el guardado en el control hdnHash
                string jsonModel = _model_.ToJson();
                if (hdnHash.Value.Equals(ModelHash(jsonModel)))
                {// El Modelo está actualizado
                    // Establece los valores de las propiedades desde los controles
                    SetProperties();

                    // Actualiza el Modelo
                    _model_.Update();

                    // Confirma al usuario
                    Message("_Model_ Editado", "El _Model_ ha sido modificado exitosamente.", MessagesTypes.success, "_Models_.aspx");
                }
                else
                {// El Modelo está desactualizado
                    // Notifica que el Modelo está desactualizado en la DB
                    Message("_Model_ Desactualizado", "El _Model_ cambió en la DB desde la última vez que se lo recuperó.", MessagesTypes.warning, Request.Url.ToString());
                }
            }
        }

        protected void EnableControls()
        {
            /* TO-DO */
            txtNombre.Enabled = btnSave.Enabled;
        }

        protected void SetProperties()
        {
            /* TO-DO */
            _model_.Nombre = txtNombre.Text;
            _model_.Descripcion = txtDescripcion.Text;
            _model_.FHAlta = DateTime.Now;
            _model_.Seleccionado = true;

            _model_.Entero = txtEntero.Text.ToInt();
            _model_.Decimal = txtDecimal.Text.ToDecimal();
            _model_.Moneda = txtMoneda.Text.ToDecimal();
        }

        protected void SetControls()
        {
            /* TO-DO */
            txtNombre.Text = _model_.Nombre;
            txtDescripcion.Text = _model_.Descripcion;

            txtEntero.Text = _model_.Entero.ToString();
            txtDecimal.Text = _model_.Decimal.ToString();
            txtMoneda.Text = _model_.Moneda.ToString();

            gv_Models_.DataSource = _model_.Select();
            gv_Models_.DataBind();
        }

        // A medida que se va agregando una fila producto del DataBind, se dispara este evento que encripta el Id.
        protected void gv_Models__RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string plainId = e.Row.Cells[0].Text;
                e.Row.Cells[0].Text = plainId.ToInt().ToCryptoID();
            }
        }

        #region EjemplosMensajesAlUsuario

        // Mensajes en UserInterface desde CodeBehind
        protected void btnInfo_Click(object sender, EventArgs e)
        {
            Message("Info", "Texto del mensaje", MessagesTypes.info, "");
        }

        protected void btnSuccess_Click(object sender, EventArgs e)
        {
            Message("Success", "Texto del mensaje", MessagesTypes.success, "");
        }

        protected void btnWarning_Click(object sender, EventArgs e)
        {
            Message("Warning", "Texto del mensaje", MessagesTypes.warning, "");
        }

        protected void btnError_Click(object sender, EventArgs e)
        {
            Message("Error", "Texto del mensaje", MessagesTypes.error, "");
        }

        #endregion EjemplosMensajesAlUsuario

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            Print("Printables/Factura.aspx?IdFactura=100001");
        }
    }
}