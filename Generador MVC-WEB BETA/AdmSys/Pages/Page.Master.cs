﻿using System;

namespace Pages
{
    public partial class Page : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Registra la ejecucion de la función que resalta los campos requeridos no ingresados
            Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "JS", "OnUpdateValidators();");
        }
    }
}