﻿using Code;

using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Pages
{
    public partial class _Model_Empty_ : BasePage
    {
        private int i = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Establece Título del Formulario = Título de la Página
                lblTitulo.Text = Title;

                // Pestañas
                rptTabs.DataSource = new CGADM.Model().Select();
                rptTabs.DataBind();

                // Contenido de las pestañas
                rptContent.DataSource = new CGADM.Model().Select();
                rptContent.DataBind();
            }
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        { }

        protected void rptTabs_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            dynamic dataItem = (dynamic)e.Item.DataItem;

            Label lblTabCaption = e.Item.FindControl("lblTabCaption") as Label;
            lblTabCaption.Text = dataItem.Nombre;

            HtmlAnchor aTab = (HtmlAnchor)e.Item.FindControl("aTab");
            aTab.HRef = "#cphBody_rptContent_divContent_" + i;
            i++;
        }

        protected void rptContent_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            dynamic dataItem = (dynamic)e.Item.DataItem;

            Label lblContentCaption = e.Item.FindControl("lblContentCaption") as Label;
            lblContentCaption.Text = dataItem.Nombre;

            Control divContent = FindControl("divContent") as Control;
        }
    }
}