﻿// Created for MagicSQL

using MagicSQL;

namespace CGUsuarios
{
    public partial class AplicacionComponente : ISUD<AplicacionComponente>
    {
        public AplicacionComponente() : base(2)
        {
        } // base(SPs_Version)

        // Properties

        public long IdAplicacionComponente { get; set; }

        public short IdAplicacion { get; set; }

        public long IdComponente { get; set; }

        public bool Excluido { get; set; }
    }
}