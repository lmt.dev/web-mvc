﻿// Created for MagicSQL

using MagicSQL;
using System;

namespace CGUsuarios
{
    public partial class Usuario : ISUD<Usuario>
    {
        public Usuario() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public long IdUsuario { get; set; }

        public DateTime FechaHoraAlta { get; set; }

        public DateTime? FechaHoraBaja { get; set; }

        public string Nombre { get; set; }

        public string Apellido { get; set; }

        public string Email { get; set; }

        public string NombreUsuario { get; set; }

        public string Password { get; set; }

        public string Telefono { get; set; }

        public long? IdImagen { get; set; }

        public long? IdDireccion { get; set; }

        public string Notas { get; set; }

        public short? IdIdioma { get; set; }

        public byte? SistemaMedida { get; set; }

        public string NumeroSimboloDecimal { get; set; }

        public byte? NumeroDigitosDecimales { get; set; }

        public string NumeroSeparacionMiles { get; set; }

        public bool? NumeroCerosIzquierda { get; set; }

        public bool? NumeroCompletarDecimalesCeros { get; set; }

        public string MonedaSimbolo { get; set; }

        public byte? MonedaFormato { get; set; }

        public string MonedaSimboloDecimal { get; set; }

        public byte? MonedaDigitosDecimales { get; set; }

        public string MonedaSeparacionMiles { get; set; }

        public bool? MonedaCerosIzquierda { get; set; }

        public bool? MonedaCompletarDecimalesCeros { get; set; }

        public long? IdZonaHoraria { get; set; }

        public byte? FormatoHora { get; set; }

        public string FormatoFecha { get; set; }

        public byte? PrimerDiaSemana { get; set; }

        public DateTime? FechaBaja { get; set; }

        public DateTime? FechaHoraBloqueo { get; set; }

        public string IntentosFallidos { get; set; }

        public string Pregunta1 { get; set; }

        public string Respuesta1 { get; set; }

        public string Pregunta2 { get; set; }

        public string Respuesta2 { get; set; }

        public string Pregunta3 { get; set; }

        public string Respuesta3 { get; set; }

        public string Pregunta4 { get; set; }

        public string Respuesta4 { get; set; }

        public string IpRecuperacion { get; set; }

        public DateTime? FechaHoraActivacion { get; set; }

        public bool AuditarActividadSistema { get; set; }
    }
}