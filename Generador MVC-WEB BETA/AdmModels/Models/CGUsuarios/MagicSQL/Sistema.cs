﻿// Created for MagicSQL

using MagicSQL;
using System;

namespace CGUsuarios
{
    public partial class Sistema : ISUD<Sistema>
    {
        public Sistema() : base(2)
        {
        } // base(SPs_Version)

        // Properties

        public short IdSistema { get; set; }

        public DateTime FechaHoraAlta { get; set; }

        public DateTime? FechaHoraBaja { get; set; }

        public string Nombre { get; set; }

        public string Descripcion { get; set; }

        public string Url { get; set; }
    }
}