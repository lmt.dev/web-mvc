﻿// Created for MagicSQL

using MagicSQL;
using System;

namespace BulkMail
{
    public partial class Mail : ISUD<Mail>
    {
        public Mail() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public long IdMail { get; set; }

        public DateTime AddDateTime { get; set; }

        public DateTime? SentDateTime { get; set; }

        public string DisplayName { get; set; }

        public string EmailFrom { get; set; }

        public string Email { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }
    }
}