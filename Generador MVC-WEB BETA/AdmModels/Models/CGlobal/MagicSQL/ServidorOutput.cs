﻿// Created for Magic 3
using MagicSQL;
using System;

namespace CGlobal
{
    public partial class ServidorOutput : ISUD<ServidorOutput>
    {
        public ServidorOutput() : base(1)
        {
        }

        public long IdServidorOutput { get; set; }

        public int UMId { get; set; }

        public int ProtocoloId { get; set; }

        public byte[] Paquete { get; set; }

        public int? LongitudPaquete { get; set; }

        public DateTime? FechaHoraAlta { get; set; }

        public bool Anulado { get; set; }

        public DateTime? FechaHoraProcesadoEmitido { get; set; }
    }
}