﻿// Created for MagicSQL
using MagicSQL;
using System;

namespace CGVending
{
    public partial class VendingUmxReseller : ISUD<VendingUmxReseller>
    {
        public VendingUmxReseller() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdVendingUmxReseller { get; set; }

        public DateTime FechaAltaAsignacion { get; set; }

        public int IdVendingReseller { get; set; }

        public int UMId { get; set; }

        public DateTime? FechaBajaAsignacion { get; set; }

        public string Observaciones { get; set; }

        public int? IdVendingUmxResellerEstadoHistorialActual { get; set; }
    }
}