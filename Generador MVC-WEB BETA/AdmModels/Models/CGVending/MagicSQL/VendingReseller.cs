﻿// Created for MagicSQL
using MagicSQL;
using System;

namespace CGVending
{
    public partial class VendingReseller : ISUD<VendingReseller>
    {
        public VendingReseller() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdVendingReseller { get; set; }

        public string Calle { get; set; }

        public string Altura { get; set; }

        public string Dto { get; set; }

        public string CPA { get; set; }

        public string Manzana { get; set; }

        public string Casa { get; set; }

        public string KM { get; set; }

        public int PaisId { get; set; }

        public int ProvinciaId { get; set; }

        public int LocalidadId { get; set; }

        public string RazonSocial { get; set; }

        public string CUIT { get; set; }

        public string CatIVA { get; set; }

        public string IngBrutos { get; set; }

        public DateTime? FechaAlta { get; set; }

        public DateTime? FechaBaja { get; set; }

        public string Telefono { get; set; }

        public string Email { get; set; }

        public string Observaciones { get; set; }

        public bool DeudaVigente { get; set; }

        public string UbicacionMaps { get; set; }

        public string Color { get; set; }

        public int? SecuenciaOrdenDeAbastecimiento { get; set; }

        public long? SecuenciaRecorrido { get; set; }

        public int? IdVendingImagen { get; set; }

        public bool? CreditoExterno { get; set; }

        public long? IdImagen { get; set; }

        public long? IdDireccion { get; set; }

        public string Direccion { get; set; }

        public int? IdDistribuidor { get; set; }
    }
}