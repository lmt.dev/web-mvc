﻿// Created for MagicSQL
using MagicSQL;

namespace CGADM
{
    public partial class PendienteEstado : ISUD<PendienteEstado>
    {
        public PendienteEstado() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdPendienteEstado { get; set; }

        public string Descripcion { get; set; }
    }
}