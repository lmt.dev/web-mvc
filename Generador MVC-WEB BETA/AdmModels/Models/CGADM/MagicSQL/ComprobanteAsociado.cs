﻿// Created for MagicSQL

using System;
using MagicSQL;

namespace CGADM
{
    public partial class ComprobanteAsociado : ISUD<ComprobanteAsociado>
    {
        public ComprobanteAsociado() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdComprobanteAsociado { get; private set; }

        public DateTime FHAlta { get; set; }

        public int IdMTXCA { get; set; }

        public byte CodigoTipoComprobante { get; set; }

        public short PuntoVenta { get; set; }

        public long Comprobante { get; set; }
    }
}