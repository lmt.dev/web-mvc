﻿// Created for MagicSQL
using MagicSQL;
using System;

namespace CGADM
{
    public partial class PresupuestoDetalle : ISUD<PresupuestoDetalle>
    {
        public PresupuestoDetalle() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdPresupuestoDetalle { get; set; }

        public int IdPresupuesto { get; set; }

        public DateTime FHAlta { get; set; }
    }
}