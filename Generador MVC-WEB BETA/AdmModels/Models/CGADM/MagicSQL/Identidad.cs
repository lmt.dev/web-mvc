﻿// Created for MagicSQL

using System;
using MagicSQL;

namespace CGADM
{
  public partial class Identidad : ISUD<Identidad>
  {
    public Identidad() : base(1) { } // base(SPs_Version)

    // Properties
   
    public int IdIdentidad { get; set; }

    public DateTime FHAlta { get; set; }

    public int IdUnidadNegocio { get; set; }

    public string Descripcion { get; set; }

    public int Orden { get; set; }

    public DateTime? FHBaja { get; set; }
  }
}