﻿// Created for MagicSQL

using System;
using MagicSQL;

namespace CGADM
{
  public partial class SoporteHardwareSeguimiento : ISUD<SoporteHardwareSeguimiento>
  {
    public SoporteHardwareSeguimiento() : base(1) { } // base(SPs_Version)

    // Properties
   
    public int IdSoporteHardwareSeguimiento { get; set; }

    public DateTime FHAlta { get; set; }

    public int IdSoporteHardware { get; set; }

    public string VersionFirmware { get; set; }

    public int? IdChip { get; set; }

    public string Telco { get; set; }

    public DateTime? FHUltConexion { get; set; }

    public int? Online { get; set; }

    public string Observaciones { get; set; }

    public DateTime? FHBaja { get; set; }
  }
}