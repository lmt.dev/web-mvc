﻿// Created for MagicSQL

using System;
using MagicSQL;

namespace CGADM
{
    public partial class ResellerLiquidacion : ISUD<ResellerLiquidacion>
    {
        public ResellerLiquidacion() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdResellerLiquidacion { get; set; }

        public int IdUnidadNegocio { get; set; }

        public int IdReseller { get; set; }

        public DateTime FHAlta { get; set; }

        public long IdComprobante { get; set; }

        public DateTime? FHFin { get; set; }

        public string Observaciones { get; set; }

        public int? ForzarUMCantidad { get; set; }

        public int? LiquidaSoloOperativas { get; set; }

        public int? IdListaPrecio { get; set; }

        public int? IdEscala { get; set; }

        public int? IdEscalaForzada { get; set; }

        public int? CantidadUMActivas { get; set; }

        public int? CantidadUMDisponibles { get; set; }

        public int? CantidadUMSuspendidas { get; set; }

        public int? CantidadUMBajas { get; set; }

        public int? IdModuloAutomatico { get; set; }
    }
}