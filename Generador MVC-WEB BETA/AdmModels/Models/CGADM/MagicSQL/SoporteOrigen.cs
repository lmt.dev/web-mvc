﻿// Created for MagicSQL

using System;
using MagicSQL;

namespace CGADM
{
  public partial class SoporteOrigen : ISUD<SoporteOrigen>
  {
    public SoporteOrigen() : base(1) { } // base(SPs_Version)

    // Properties
   
    public int IdSoporteOrigen { get; set; }

    public DateTime FHAlta { get; set; }

    public string Descripcion { get; set; }

    public DateTime? FHBaja { get; set; }
  }
}