﻿// Created for MagicSQL

using System;
using MagicSQL;

namespace CGADM
{
    public partial class SDSDetalle : ISUD<SDSDetalle>
    {
        public SDSDetalle() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdSDSDetalle { get; set; }

        public int IdSDS { get; set; }

        public int IdListaPrecio { get; set; }

        public int Cantidad { get; set; }

        public DateTime? FHAlta { get; set; }

        public string CodigoProducto { get; set; }

        public string Descripcion { get; set; }

        public DateTime? FHModificacion { get; set; }

        public int? CantidadRemitados { get; set; }

        public decimal? PrecioLista { get; set; }

        public decimal? PrecioContado { get; set; }

        public DateTime? FHBaja { get; set; }
    }
}