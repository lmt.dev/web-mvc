﻿// Created for MagicSQL
using MagicSQL;
using System;

namespace CGADM
{
    public partial class Componente : ISUD<Componente>
    {
        public Componente() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdComponente { get; set; }

        public int? IdProveedorComponente { get; set; }

        public string Descripcion { get; set; }

        public string Observacion { get; set; }

        public decimal Precio { get; set; }

        public DateTime FHAlta { get; set; }

        public DateTime? FHBaja { get; set; }
    }
}