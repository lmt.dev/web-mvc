﻿// Created for MagicSQL
using MagicSQL;
using System;

namespace CGADM
{
    public partial class Archivo : ISUD<Archivo>
    {
        public Archivo() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public long IdArchivo { get; set; }

        public DateTime FHAlta { get; set; }

        public DateTime? FHBaja { get; set; }

        public byte[] ArchivoContenido { get; set; }

        public long? IdSesionTrabajoDetalle { get; set; }

        public string TipoArchivo { get; set; }
    }
}