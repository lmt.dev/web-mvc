﻿// Created for MagicSQL

using System;
using MagicSQL;

namespace CGADM
{
    public partial class Item : ISUD<Item>
    {
        public Item() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdItem { get; set; }

        public DateTime FHAlta { get; set; }

        public int IdMTXCA { get; set; }

        public int? UnidadesMtx { get; set; }

        public string CodigoMtx { get; set; }

        public string Codigo { get; set; }

        public string Descripcion { get; set; }

        public decimal? Cantidad { get; set; }

        public byte? CodigoUnidadMedida { get; set; }

        public decimal? PrecioUnitario { get; set; }

        public decimal? ImporteBonificacion { get; set; }

        public byte? CodigoTipoAlicuotaIVA { get; set; }

        public decimal? ImporteIVA { get; set; }

        public decimal? ImporteItem { get; set; }
    }
}