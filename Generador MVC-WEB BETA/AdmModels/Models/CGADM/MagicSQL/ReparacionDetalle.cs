﻿// Created for MagicSQL
using MagicSQL;

namespace CGADM
{
    public partial class ReparacionDetalle : ISUD<ReparacionDetalle>
    {
        public ReparacionDetalle() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdReparacionDetalle { get; set; }

        public int IdReparacion { get; set; }

        public int IdComponente { get; set; }
    }
}