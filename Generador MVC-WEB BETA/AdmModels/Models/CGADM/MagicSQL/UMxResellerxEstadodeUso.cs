﻿// Created for MagicSQL
using MagicSQL;
using System;

namespace CGADM
{
    public partial class UMxResellerxEstadodeUso : ISUD<UMxResellerxEstadodeUso>
    {
        public UMxResellerxEstadodeUso() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdUMxResellerxEstadodeUso { get; set; }

        public DateTime FHAlta { get; set; }

        public DateTime FHInicioEstado { get; set; }

        public string ObservacionInicio { get; set; }

        public int IdTipoEstadoUsoUM { get; set; }

        public DateTime? FHFinEstado { get; set; }

        public string ObservacionFin { get; set; }

        public int IdSDS { get; set; }
    }
}