﻿// Created for MagicSQL
using MagicSQL;
using System;

namespace CGADM
{
    public partial class UMsTestFactory_temp : ISUD<UMsTestFactory_temp>
    {
        public UMsTestFactory_temp() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int UMId { get; set; }

        public string Id_UM { get; set; }

        public string Hard { get; set; }

        public int? IdFirmware { get; set; }

        public long? IMEI { get; set; }

        public string MAC { get; set; }

        public DateTime FHAlta { get; set; }

        public DateTime? FHProduccion { get; set; }
    }
}