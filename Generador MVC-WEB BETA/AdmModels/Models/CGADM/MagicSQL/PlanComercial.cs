﻿// Created for MagicSQL
using MagicSQL;
using System;

namespace CGADM
{
    public partial class PlanComercial : ISUD<PlanComercial>
    {
        public PlanComercial() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdPlanComercial { get; set; }

        public int IdDistribuidorUnidadNegocio { get; set; }

        public DateTime FHAlta { get; set; }

        public string Descripcion { get; set; }

        public string Observacion { get; set; }

        public DateTime? FHBaja { get; set; }

        public int DiasVencimientoFactura { get; set; }

        public decimal PorcentajeInteresMensual { get; set; }
    }
}