﻿// Created for MagicSQL

using System;
using MagicSQL;

namespace CGADM
{
  public partial class SoporteHardware : ISUD<SoporteHardware>
  {
    public SoporteHardware() : base(1) { } // base(SPs_Version)

    // Properties
   
    public int IdSoporteHardware { get; set; }

    public DateTime FHAlta { get; set; }

    public int IdSoporte { get; set; }

    public int TipoHardware { get; set; }

    public int? UMId { get; set; }

    public string IdDispositivos { get; set; }

    public string Observaciones { get; set; }

    public DateTime? FHBaja { get; set; }
  }
}