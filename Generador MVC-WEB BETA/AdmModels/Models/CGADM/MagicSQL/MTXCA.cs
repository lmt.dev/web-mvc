﻿// Created for MagicSQL

using System;
using MagicSQL;

namespace CGADM
{
    public partial class MTXCA : ISUD<MTXCA>
    {
        public MTXCA() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdMTXCA { get; private set; }

        public DateTime FHAlta { get; set; }

        public int IdCuenta { get; set; }

        public string CUIT { get; set; }

        public byte CodigoTipoComprobante { get; set; }

        public short NumeroPuntoVenta { get; set; }

        public int NumeroComprobante { get; set; }

        public DateTime? FechaEmision { get; set; }

        public string CodigoTipoAutorizacion { get; set; }

        public long? CodigoAutorizacion { get; set; }

        public DateTime? FechaVencimiento { get; set; }

        public byte? CodigoTipoDocumento { get; set; }

        public long? NumeroDocumento { get; set; }

        public decimal? ImporteGravado { get; set; }

        public decimal? ImporteNoGravado { get; set; }

        public decimal? ImporteExento { get; set; }

        public decimal ImporteSubtotal { get; set; }

        public decimal? ImporteOtrosTributos { get; set; }

        public decimal ImporteTotal { get; set; }

        public string CodigoMoneda { get; set; }

        public decimal CotizacionMoneda { get; set; }

        public string Observaciones { get; set; }

        public byte? CodigoConcepto { get; set; }

        public DateTime? FechaServicioDesde { get; set; }

        public DateTime? FechaServicioHasta { get; set; }

        public DateTime? FechaVencimientoPago { get; set; }

        public DateTime? FechaHoraGen { get; set; }

        public string CodigoBarra { get; set; }
    }
}