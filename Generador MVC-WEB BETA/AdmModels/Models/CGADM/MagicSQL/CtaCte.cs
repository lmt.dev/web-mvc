﻿// Created for MagicSQL
using MagicSQL;
using System;

namespace CGADM
{
    public partial class CtaCte : ISUD<CtaCte>
    {
        public CtaCte() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdCtaCte { get; set; }

        public int IdUnidadNegocio { get; set; }

        public int IdReseller { get; set; }

        public DateTime FHAlta { get; set; }

        public string TipoMov { get; set; }

        public int? IdRecibo { get; set; }

        public long? IdComprobante { get; set; }

        public decimal Saldo { get; set; }

        public int? IdDistribuidor { get; set; }
    }
}