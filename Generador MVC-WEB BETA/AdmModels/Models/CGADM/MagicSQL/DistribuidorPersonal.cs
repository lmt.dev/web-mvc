﻿// Created for MagicSQL
using MagicSQL;
using System;

namespace CGADM
{
    public partial class DistribuidorPersonal : ISUD<DistribuidorPersonal>
    {
        public DistribuidorPersonal() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdDistribuidorPersonal { get; set; }

        public int IdDistribuidor { get; set; }

        public DateTime FHAlta { get; set; }

        public int? IdCuenta { get; set; }

        public string Observaciones { get; set; }

        public DateTime? FHBaja { get; set; }

        public long? IdSesionTrabajoDetalle { get; set; }
    }
}