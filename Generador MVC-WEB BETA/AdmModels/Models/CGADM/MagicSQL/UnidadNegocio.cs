﻿// Created for MagicSQL
using MagicSQL;
using System;

namespace CGADM
{
    public partial class UnidadNegocio : ISUD<UnidadNegocio>
    {
        public UnidadNegocio() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdUnidadNegocio { get; set; }

        public DateTime FHAlta { get; set; }

        public string Descripcion { get; set; }

        public string Observacion { get; set; }

        public DateTime? FHBaja { get; set; }
    }
}