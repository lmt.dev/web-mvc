﻿// Created for MagicSQL

using System;
using MagicSQL;

namespace CGADM
{
    public partial class RemitoExterno : ISUD<RemitoExterno>
    {
        public RemitoExterno() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdRemitoExterno { get; set; }

        public string Numero { get; set; }

        public string Fecha { get; set; }

        public string Responsable { get; set; }

        public string Observaciones { get; set; }

        public long? IdArchivo { get; set; }

        public DateTime FHAlta { get; set; }

        public DateTime? FHBaja { get; set; }
    }
}