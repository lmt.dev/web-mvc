﻿// Created for MagicSQL

using System;
using MagicSQL;

namespace CGADM
{
    public partial class Distribuidor : ISUD<Distribuidor>
    {
        public Distribuidor() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdDistribuidor { get; set; }

        public DateTime FHAlta { get; set; }

        public string Observacion { get; set; }

        public DateTime? FHBaja { get; set; }

        public string Email { get; set; }

        public string Telefono { get; set; }

        public string RazonSocial { get; set; }

        public long? IdDireccion { get; set; }

        public long? IdImagen { get; set; }

        public string Cuit { get; set; }

        public string Dirección { get; set; }

        public int? UltimaFactura { get; set; }

        public bool PermitePagoSobrante { get; set; }

        public bool PermiteImputacionParcial { get; set; }

        public int? UltimoRecibo { get; set; }

        public int? UltimaNotaDebito { get; set; }

        public int? UltimaNotaCredito { get; set; }

        public bool? InvertirNegatividad { get; set; }

        public int? IdRemitero { get; set; }

        public int? UltimoRemito { get; set; }
    }
}