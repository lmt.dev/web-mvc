﻿// Created for MagicSQL

using MagicSQL;
using System;

namespace CGADM
{
    public partial class Oportunidad : ISUD<Oportunidad>
    {
        public Oportunidad() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdOportunidad { get; set; }

        public int IdDistribuidor { get; set; }

        public DateTime FHAlta { get; set; }

        public string Descripcion { get; set; }

        public string Detalle { get; set; }

        public int? IdOrigen { get; set; }

        public int? IdPreContacto { get; set; }

        public DateTime? FHBaja { get; set; }

        public string DetalleBaja { get; set; }
    }
}