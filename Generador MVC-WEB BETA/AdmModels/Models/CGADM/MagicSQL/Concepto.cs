﻿// Created for MagicSQL
using MagicSQL;

namespace CGADM
{
    public partial class Concepto : ISUD<Concepto>
    {
        public Concepto() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdConcepto { get; set; }

        public string Descripcion { get; set; }
    }
}