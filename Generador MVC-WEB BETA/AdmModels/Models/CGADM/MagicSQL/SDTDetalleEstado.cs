﻿// Created for MagicSQL

using MagicSQL;
using System;

namespace CGADM
{
    public partial class SDTDetalleEstado : ISUD<SDTDetalleEstado>
    {
        public SDTDetalleEstado() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdSDTDetalleEstado { get; set; }

        public string Descripcion { get; set; }

        public DateTime? FHAlta { get; set; }

        public DateTime? FHBaja { get; set; }
    }
}