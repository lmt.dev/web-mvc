﻿// Created for MagicSQL

using System;
using MagicSQL;

namespace CGADM
{
    public partial class SubtotalIVA : ISUD<SubtotalIVA>
    {
        public SubtotalIVA() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdSubtotalIVA { get; set; }

        public DateTime FHAlta { get; set; }

        public int IdMTXCA { get; set; }

        public byte Codigo { get; set; }

        public decimal Importe { get; set; }
    }
}