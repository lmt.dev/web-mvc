﻿// Created for MagicSQL

using System;
using MagicSQL;

namespace CGADM
{
  public partial class Soporte : ISUD<Soporte>
  {
    public Soporte() : base(1) { } // base(SPs_Version)

    // Properties
   
    public int IdSoporte { get; set; }

    public DateTime FHAlta { get; set; }

    public int? IdCuenta { get; set; }

    public int IdUnidadNegocio { get; set; }

    public int IdSoporteTipo { get; set; }

    public int ID { get; set; }

    public string AsuntoSoporte { get; set; }

    public string Detalle { get; set; }

    public string MailNotificacion { get; set; }

    public int? IdSoportePrioridad { get; set; }

    public int? IdSoporteOrigen { get; set; }

    public int? IdIdentidad { get; set; }

    public int? IdUsuario { get; set; }
  }
}