﻿// Created for MagicSQL

using System;
using MagicSQL;

namespace CGADM
{
  public partial class SoporteTipoMovimiento : ISUD<SoporteTipoMovimiento>
  {
    public SoporteTipoMovimiento() : base(1) { } // base(SPs_Version)

    // Properties
   
    public int IdSoporteTipoMovimiento { get; set; }

    public DateTime FHAlta { get; set; }

    public int? IdTipoSoporte { get; set; }

    public string Descripcion { get; set; }

    public string Color { get; set; }

    public DateTime? FHBaja { get; set; }
  }
}