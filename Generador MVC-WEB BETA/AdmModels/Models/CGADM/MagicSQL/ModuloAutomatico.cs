﻿// Created for MagicSQL
using MagicSQL;
using System;

namespace CGADM
{
    public partial class ModuloAutomatico : ISUD<ModuloAutomatico>
    {
        public ModuloAutomatico() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdModuloAutomatico { get; set; }

        public DateTime FHAlta { get; set; }

        public int IdUnidadNegocio { get; set; }

        public string Descripcion { get; set; }
    }
}