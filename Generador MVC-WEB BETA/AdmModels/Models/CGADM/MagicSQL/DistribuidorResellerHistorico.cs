﻿// Created for MagicSQL
using MagicSQL;
using System;

namespace CGADM
{
    public partial class DistribuidorResellerHistorico : ISUD<DistribuidorResellerHistorico>
    {
        public DistribuidorResellerHistorico() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdDistribuidorResellerHistorico { get; set; }

        public DateTime FHAltaDistribuidorResellerHistorico { get; set; }

        public int IdDistribuidorReseller { get; set; }

        public int? IdDistribuidorUnidadNegocio { get; set; }

        public DateTime? FHAlta { get; set; }

        public int? FHAltaPlanComercial { get; set; }

        public bool? ForzarCantidadUM { get; set; }

        public int? CantidadUM { get; set; }

        public DateTime? FHHastaForzar { get; set; }

        public string Observaciones { get; set; }
    }
}