﻿// Created for MagicSQL
using MagicSQL;
using System;

namespace CGADM
{
    public partial class ImputacionCobro : ISUD<ImputacionCobro>
    {
        public ImputacionCobro() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public long IdImputacionCobro { get; set; }

        public DateTime FHAlta { get; set; }

        public int? IdCobroRecibido { get; set; }

        public long IdComprobante { get; set; }

        public decimal Importe { get; set; }

        public decimal Saldo { get; set; }

        public long? IdNotaCredito { get; set; }
    }
}