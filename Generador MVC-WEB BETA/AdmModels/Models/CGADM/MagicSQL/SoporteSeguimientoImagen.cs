﻿// Created for MagicSQL

using System;
using MagicSQL;

namespace CGADM
{
  public partial class SoporteSeguimientoImagen : ISUD<SoporteSeguimientoImagen>
  {
    public SoporteSeguimientoImagen() : base(1) { } // base(SPs_Version)

    // Properties
   
    public int IdSoporteSeguimientoImagen { get; set; }

    public DateTime FHAlta { get; set; }

    public int IdSoporteSeguimiento { get; set; }

    public byte[] Imagen { get; set; }

    public DateTime? FHBaja { get; set; }
  }
}