﻿// Created for MagicSQL

using System;
using MagicSQL;

namespace CGADM
{
    public partial class Remito : ISUD<Remito>
    {
        public Remito() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdRemito { get; set; }

        public DateTime FHAlta { get; set; }

        public int IdDistribuidorUnidadNegocio { get; set; }

        public int? IdResellerDestino { get; set; }

        public int? IdCuentaAlta { get; set; }

        public string MailInforme { get; set; }

        public string Observacion { get; set; }

        public int? RemitoNro { get; set; }

        public int? RemiteroId { get; set; }

        public DateTime? FHBaja { get; set; }

        public string RemitoImpresion { get; set; }
    }
}