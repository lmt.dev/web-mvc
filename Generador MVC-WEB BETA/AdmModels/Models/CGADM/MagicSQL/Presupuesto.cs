﻿// Created for MagicSQL
using MagicSQL;
using System;

namespace CGADM
{
    public partial class Presupuesto : ISUD<Presupuesto>
    {
        public Presupuesto() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdPresupuesto { get; set; }

        public DateTime FHAlta { get; set; }
    }
}