﻿// Created for MagicSQL
using MagicSQL;
using System;

namespace CGADM
{
    public partial class Model : ISUD<Model>
    {
        public Model() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdModel { get; set; }

        public DateTime FHAlta { get; set; }

        public string Nombre { get; set; }

        public string Descripcion { get; set; }

        public int? Entero { get; set; }

        public decimal? Decimal { get; set; }

        public decimal? Moneda { get; set; }

        public DateTime? FHBaja { get; set; }

        public bool Seleccionado { get; set; }
    }
}