﻿// Created for MagicSQL
using MagicSQL;
using System;

namespace CGADM
{
    public partial class Moneda : ISUD<Moneda>
    {
        public Moneda() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdMoneda { get; set; }

        public DateTime FHAlta { get; set; }

        public string Descripcion { get; set; }

        public DateTime? FHBaja { get; set; }
    }
}