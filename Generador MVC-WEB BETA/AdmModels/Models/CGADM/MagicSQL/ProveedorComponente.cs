﻿// Created for MagicSQL
using MagicSQL;
using System;

namespace CGADM
{
    public partial class ProveedorComponente : ISUD<ProveedorComponente>
    {
        public ProveedorComponente() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdProveedorComponente { get; set; }

        public string Descripcion { get; set; }

        public DateTime FHAlta { get; set; }

        public DateTime? FHBaja { get; set; }
    }
}