﻿// Created for MagicSQL

using System;
using MagicSQL;

namespace CGADM
{
    public partial class ProductoServicio : ISUD<ProductoServicio>
    {
        public ProductoServicio() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdProductoServicio { get; set; }

        public DateTime FHAlta { get; set; }

        public int? IdUnidadNegocio { get; set; }

        public bool Servicio { get; set; }

        public int? IdServicioTipo { get; set; }

        public string CodigoProducto { get; set; }

        public string Descripcion { get; set; }

        public string Observacion { get; set; }

        public long? IdImagen { get; set; }

        public DateTime? FHDiscontinuado { get; set; }

        public DateTime? FHBaja { get; set; }

        public string ObservacionBaja { get; set; }

        public int? UMModeloId { get; set; }

        public int? IdDispositivosModelos { get; set; }
    }
}