﻿// Created for MagicSQL

using MagicSQL;
using System;

namespace CGADM
{
    public partial class OportunidadTipoMovimiento : ISUD<OportunidadTipoMovimiento>
    {
        public OportunidadTipoMovimiento() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdOportunidadTipoMovimiento { get; set; }

        public DateTime FHAlta { get; set; }

        public string Descripcion { get; set; }

        public DateTime? FHBaja { get; set; }
    }
}