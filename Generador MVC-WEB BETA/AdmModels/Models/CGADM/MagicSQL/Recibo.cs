﻿// Created for MagicSQL
using MagicSQL;
using System;

namespace CGADM
{
    public partial class Recibo : ISUD<Recibo>
    {
        public Recibo() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdRecibo { get; set; }

        public DateTime FHAlta { get; set; }

        public string NumeroReferencia { get; set; }

        public string Estado { get; set; }

        public int? IdMotivoRechazo { get; set; }

        public decimal Importe { get; set; }

        public decimal Saldo { get; set; }

        public string Observacion { get; set; }

        public int? NumeroRecibo { get; set; }
    }
}