﻿// Created for MagicSQL
using MagicSQL;
using System;

namespace CGADM
{
    public partial class CGPersonal : ISUD<CGPersonal>
    {
        public CGPersonal() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdCGPersonal { get; set; }

        public DateTime FHAlta { get; set; }

        public int IdCuenta { get; set; }

        public DateTime? FHBaja { get; set; }

        public string IdSesionTrabajoDetalle { get; set; }
    }
}