﻿// Created for MagicSQL
using MagicSQL;

namespace CGADM
{
    public partial class Region : ISUD<Region>
    {
        public Region() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdRegion { get; set; }

        public string Descripcion { get; set; }

        public long IdImagen { get; set; }
    }
}