﻿// Created for MagicSQL

using System;
using MagicSQL;

namespace CGADM
{
  public partial class SoporteIdentidad : ISUD<SoporteIdentidad>
  {
    public SoporteIdentidad() : base(1) { } // base(SPs_Version)

    // Properties
   
    public int IdSoporteIdentidad { get; set; }

    public DateTime FHAlta { get; set; }

    public int IdSoporte { get; set; }

    public int IdentidadOrigen { get; set; }

    public int IdentidadDestino { get; set; }

    public int? IdOrigenDistribuidor { get; set; }

    public int IdDestino { get; set; }

    public int? IdOrigenReseller { get; set; }

    public int? IdOrigenContratista { get; set; }

    public int? IdOrigenConsumidor { get; set; }

    public int? IdOrigenAgente { get; set; }

    public int? IdOrigenUsuario { get; set; }

    public DateTime? FHBaja { get; set; }
  }
}