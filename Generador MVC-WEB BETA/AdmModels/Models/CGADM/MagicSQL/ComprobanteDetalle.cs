﻿// Created for MagicSQL
using MagicSQL;
using System;

namespace CGADM
{
    public partial class ComprobanteDetalle : ISUD<ComprobanteDetalle>
    {
        public ComprobanteDetalle() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdComprobanteDetalle { get; set; }

        public long IdComprobante { get; set; }

        public DateTime FHAlta { get; set; }

        public int? IdListaPrecio { get; set; }

        public int? IdEscala { get; set; }

        public string Descripcion { get; set; }

        public int Cantidad { get; set; }

        public int IdMoneda { get; set; }

        public decimal PrecioUnitario { get; set; }

        public decimal? PrecioTotal { get; set; }

        public string Observacion { get; set; }
    }
}