﻿// Created for MagicSQL

using System;
using MagicSQL;

namespace CGADM
{
    public partial class SDS : ISUD<SDS>
    {
        public SDS() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdSDS { get; set; }

        public int IdDistribuidorUnidadNegocio { get; set; }

        public int SDSNro { get; set; }

        public string MailInforme { get; set; }

        public int IdPlanComercial { get; set; }

        public int IdReseller { get; set; }

        public long? IdImagenSDS { get; set; }

        public DateTime FHAlta { get; set; }

        public string ObservacionAlta { get; set; }

        public long? IdCuentaAlta { get; set; }

        public DateTime? FHAceptacion { get; set; }

        public string ObservacionAceptacion { get; set; }

        public long? IdCuentaAceptacion { get; set; }

        public DateTime? FHBaja { get; set; }

        public long? IdPDFGenerado { get; set; }

        public int? IdFormaPago { get; set; }
    }
}