﻿// Created for MagicSQL

using System;
using MagicSQL;

namespace CGADM
{
    public partial class PreContacto : ISUD<PreContacto>
    {
        public PreContacto() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdPreContacto { get; set; }

        public DateTime FHAlta { get; set; }

        public int IdOrigen { get; set; }

        public string Nombre { get; set; }

        public string Apellido { get; set; }

        public string Email { get; set; }

        public string Fijo { get; set; }

        public string Movil { get; set; }

        public string Empresa { get; set; }

        public string Mensaje { get; set; }

        public string IP { get; set; }

        public string CodigoContinente { get; set; }

        public string NombreContinente { get; set; }

        public string CodigoPais { get; set; }

        public string NombrePais { get; set; }

        public string CodigoRegion { get; set; }

        public string NombreRegion { get; set; }

        public string Ciudad { get; set; }

        public string CP { get; set; }

        public double? Latitud { get; set; }

        public double? Longitud { get; set; }

        public DateTime? FHProcesado { get; set; }

        public int? IdCGPersonalProcesado { get; set; }

        public bool? Descartado { get; set; }

        public string Observacion { get; set; }

        public DateTime? FHBaja { get; set; }
    }
}