﻿// Created for MagicSQL

using System;
using MagicSQL;

namespace CGADM
{
  public partial class SoporteContacto : ISUD<SoporteContacto>
  {
    public SoporteContacto() : base(1) { } // base(SPs_Version)

    // Properties
   
    public int IdSoporteContacto { get; set; }

    public DateTime FHAlta { get; set; }

    public int? IdContacto { get; set; }

    public int IdSoporte { get; set; }

    public DateTime? FHBaja { get; set; }
  }
}