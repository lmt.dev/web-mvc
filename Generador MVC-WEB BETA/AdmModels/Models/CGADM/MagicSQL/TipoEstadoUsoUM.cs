﻿// Created for MagicSQL
using MagicSQL;
using System;

namespace CGADM
{
    public partial class TipoEstadoUsoUM : ISUD<TipoEstadoUsoUM>
    {
        public TipoEstadoUsoUM() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdTipoEstadoUsoUM { get; set; }

        public DateTime FHAlta { get; set; }

        public string Descripcion { get; set; }

        public DateTime? FHBaja { get; set; }
    }
}