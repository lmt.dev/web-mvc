﻿// Created for MagicSQL
using MagicSQL;
using System;

namespace CGADM
{
    public partial class Comprobante : ISUD<Comprobante>
    {
        public Comprobante() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public long IdComprobante { get; set; }

        public DateTime FHAlta { get; set; }

        public string Descripcion { get; set; }

        public short TipoComprobanteInterno { get; set; }

        public int NroComprobanteInterno { get; set; }

        public short? TipoComprobante { get; set; }

        public int? NroComprobante { get; set; }

        public DateTime? FHEmisionComprobante { get; set; }

        public int IdMoneda { get; set; }

        public decimal ImporteTotal { get; set; }

        public int? IdSDS { get; set; }

        public int? IdSDT { get; set; }

        public long? IdImagen { get; set; }

        public string Observacion { get; set; }

        public decimal? ImporteSaldado { get; set; }

        public string Generacion { get; set; }

        public decimal ImporteNetoGravado { get; set; }

        public decimal ImporteIva { get; set; }

        public DateTime? FHVencimiento { get; set; }

        public decimal SaldoFavor { get; set; }
    }
}