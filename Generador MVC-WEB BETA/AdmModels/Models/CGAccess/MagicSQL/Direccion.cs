﻿// Created for MagicSQL

using MagicSQL;

namespace CGAccess
{
    public partial class Direccion : ISUD<Direccion>
    {
        public Direccion() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public long IdDireccion { get; set; }

        public string Ingresada { get; set; }

        public string Notas { get; set; }

        public double? Latitud { get; set; }

        public double? Longitud { get; set; }

        public string DireccionFormateada { get; set; }

        public string Calle { get; set; }

        public string Numero { get; set; }

        public string CodigoPostal { get; set; }

        public string Localidad { get; set; }

        public string Provincia { get; set; }

        public string Pais { get; set; }

        public string CodigoPais { get; set; }

        public string IdLugar { get; set; }

        public string Id { get; set; }

        public string Referencia { get; set; }

        public string URL { get; set; }

        public byte[] Mapa { get; set; }
    }
}