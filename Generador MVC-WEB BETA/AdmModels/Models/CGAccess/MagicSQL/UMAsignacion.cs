﻿// Created for MagicSQL

using MagicSQL;
using System;

namespace CGAccess
{
    public partial class UMAsignacion : ISUD<UMAsignacion>
    {
        public UMAsignacion() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdUMAsignacion { get; set; }

        public DateTime FHAlta { get; set; }

        public int UMid { get; set; }

        public int? IdReseller { get; set; }

        public DateTime? FHAltaReseller { get; set; }

        public string ObservacionesAltaReseller { get; set; }

        public DateTime? FHAceptacionReseller { get; set; }

        public int? IdTecnicoAceptacionReseller { get; set; }

        public long? IdServidorDialogoAceptacionReseller { get; set; }

        public DateTime? FHBajaReseller { get; set; }

        public string ObservacionesBajaReseller { get; set; }

        public int? IdContratista { get; set; }

        public DateTime? FHAltaContratista { get; set; }

        public string ObservacionesAltaContratista { get; set; }

        public DateTime? FHAceptacionUM { get; set; }

        public int? IdTecnicoAceptacionUM { get; set; }

        public long? IdServidorDialogoAceptacionUM { get; set; }

        public DateTime? FHBajaContratista { get; set; }

        public string ObservacionesBajaContratista { get; set; }

        public int? IdAcceso { get; set; }

        public long? IdServidorOutput { get; set; }

        public DateTime? FHServidorOutputProcesadoEmitido { get; set; }

        public string AccionABMServidorOutput { get; set; }

        public bool? CambioEnPasos { get; set; }

        public DateTime? FHEmitido { get; set; }

        public DateTime? FHRechazado { get; set; }

        public DateTime? FHBaja { get; set; }

        public long? IdSesionTrabajoDetalle { get; set; }
    }
}